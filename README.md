# vv221ʼs games collection

The codebase is maintained at [https://git.vv221.fr/play.it-vv221/]

Bug reports and feature requests are tracked at [https://forge.dotslashplay.it/vv221/games/-/issues]

[https://git.vv221.fr/play.it-vv221/]: https://git.vv221.fr/play.it-vv221/
[https://forge.dotslashplay.it/vv221/games/-/issues]: https://forge.dotslashplay.it/vv221/games/-/issues

## Description

./play.it is a free software building native packages from installers for
Windows or Linux, mainly those sold by stores focusing on DRM-free games
distribution. The goal is that a game installed via ./play.it is
indistinguishable from a game installed via the official repositories of your
favourite distribution.

This collection adds support for a collection of games curated by vv221,
./play.it original author. It can be seen as a very subjective "best of".

## Installation

### From Debian/Ubuntu repositories

This package is only provided from Debian 13 "Trixie" or newer, and Ubuntu 24.04 "Noble Numbat" or newer.
Users of older versions of these distributions should follow the next instructions about installing this collection from the git repository.

```
apt install play.it-vv221
```

### From the git repository

```
git clone --branch main --depth 1 http://git.vv221.fr/play.it-vv221 play.it-vv221.git
cd play.it-vv221.git
make install
```

## Usage

Usage instructions are provided on the main ./play.it repository: [./play.it: Installer for DRM-free commercial games — Usage]

[./play.it: Installer for DRM-free commercial games — Usage]: https://git.dotslashplay.it/scripts/about/#usage

## Supported games

### Action

- Bastion
- Beyond Good & Evil
- Children of Morta
- Deus Ex
- Diablo
- Grim Dawn
- Jade Empire
- Jazz Jackrabbit 2
- Painkiller
- Prince of Persia: The Sands of Time
- Pyre
- Raji: An Ancient Epic (full game and free demo)
- Rayman series:
    - Rayman
    - Rayman Origins
- Sales Gosses !
- Scrapland
- Shogo: Mobile Armor Division
- The Elder Scrolls III: Morrowind
- The Witcher
- Titan Quest
- Tomb Raider series:
    - Tomb Raider (remastered)
    - Tomb Raider 2 (classic and remastered)
    - Tomb Raider 3 (remastered)
- Tonight We Riot
- Torchlight series:
    - Torchlight
    - Torchlight II
- Total Overdose
- Transistor
- Unreal Tournament series:
    - Unreal Tournament
    - Unreal Tournament 2004
- Vampire: The Masquerade - Bloodlines
- Victor Vran

### Adventure

- Blacksad: Under the Skin
- Blackwell series:
    - The Blackwell Legacy
    - Blackwell Unbound
    - The Blackwell Convergence
    - The Blackwell Deception
    - The Blackwell Epiphany
- Day of the Tentacle
- Gamedec (full game and free demo)
- Gibbous - A Cthulhu Adventure
- Gobliiins series:
    - Gobliiins
    - Gobliins 2: The Prince Buffoon
    - Goblins Quest 3
    - Gobliiins 5
- Eliza
- Kathy Rain
- Mask of the Rose (full game and free demo)
- Monkey Island series:
    - The Secret of Monkey Island: Special Edition
    - Monkey Island 2: LeChuck's Revenge: Special Edition
    - The Curse of Monkey Island
    - Escape from Monkey Island
    - Return to Monkey Island
- Myst series:
    - Myst
    - realMyst: Masterpiece Edition
    - Riven: The Sequel to Myst
    - Myst III: Exile
- Orwell series:
    - Orwell
    - Orwell: Ignorance is Strength
- Primordia
- Roadwarden
- Sam and Max Hit the Road
- Shardlight
- Slay the Princess
- Song of Farca
- Superfluous Riteurnz
- Unavowed
- Whispers of a Machine
- World of Darkness setting:
    - Vampire: The Masquerade - Coteries of New York
    - Vampire: The Masquerade - Shadows of New York
    - Werewolf: The Apocalypse - Heart of the Forest
    - Werewolf: The Apocalypse - Purgatory

### Arcade

- A Dance of Fire and Ice
- BallisticNG
- Distance
- Pyre
- Trailblazers

### Cyberpunk

- Deus Ex
- Exapunks
- Gamedec (full game and free demo)
- Shadowrun series (by Harebrained Schemes):
    - Shadowrun Returns
    - Shadowrun: Dragonfall
    - Shadowrun: Hong Kong
- Song of Farca
- Whispers of a Machine

### Horror

- Cultist Simulator
- Darkest Dungeon
- Diablo
- Fallen London setting:
    - Sunless Sea
    - Sunless Skies
- Ghost Master
- Gibbous - A Cthulhu Adventure
- Grim Dawn
- Painkiller
- Slay the Princess
- World of Darkness setting:
    - Vampire: The Masquerade - Bloodlines
    - Vampire: The Masquerade - Coteries of New York
    - Vampire: The Masquerade - Shadows of New York
    - Werewolf: The Apocalypse - Heart of the Forest
    - Werewolf: The Apocalypse - Purgatory
- Victor Vran

### Puzzle

- 7 Billion Humans
- Ghost Master
- Human Resource Machine
- Little Inferno
- Myst series:
    - Myst
    - realMyst: Masterpiece Edition
    - Riven: The Sequel to Myst
    - Myst III: Exile
- Orwell series:
    - Orwell
    - Orwell: Ignorance is Strength
- World of Goo series:
    - World of Goo
    - World of Goo 2
- Zachtronics games:
    - SpaceChem
    - Opus Magnum
    - Exapunks
    - Molek-Syntez
    - Last Call BBS
    - Zachtronics Solitaire Collection

### Role-playing

- Anachronox
- Baldur's Gate series:
    - Baldur's Gate (classic version and Enhanced Edition)
    - Baldur's Gate II (classic version and Enhanced Edition)
    - Baldur's Gate 3
- Bastion
- Children of Morta
- Darkest Dungeon
- Deus Ex
- Diablo
- Disco Elysium
- Fallen London setting:
    - Sunless Sea
    - Sunless Skies
- Freelancer (free demo only)
- Gamedec (full game and free demo)
- Grim Dawn
- Icewind Dale series:
    - Icewind Dale (classic version and Enhanced Edition)
    - Icewind Dale II
- Jade Empire
- Loop Hero (full game and free demo)
- Pillars of Eternity
- Planescape: Torment (classic version and Enhanced Edition)
- Pyre
- Shadowrun series (by Harebrained Schemes):
    - Shadowrun Returns
    - Shadowrun: Dragonfall
    - Shadowrun: Hong Kong
- Star Wars: Knights of the Old Republic series:
    - Star Wars: Knights of the Old Republic
    - Star Wars: Knights of the Old Republic II: The Sith Lords
- The Elder Scrolls III: Morrowind
- The Lamplighters League
- The Witcher
- Titan Quest
- Torchlight series:
    - Torchlight
    - Torchlight II
- Torment: Tides of Numenera
- Transistor
- Tyranny
- Vampire: The Masquerade - Bloodlines
- Victor Vran
- Warhammer 40,000: Rogue Trader
- Wasteland 2

### Strategy

- Age of Mythology (free demo only)
- Alpha Centauri
- City Building series (by Impression Games and BreakAway Games):
    - Caesar III
    - Pharaoh
    - Zeus: Master of Olympus
    - Emperor: Rise of the Middle Kingdom
- Cultist Simulator
- Democratic Socialism Simulator
- Desperados series:
    - Desperados: Wanted Dead or Alive
    - Desperados III (full game and free demo)
- Dark Reign 2
- Dungeon Keeper series:
    - Dungeon Keeper
    - Dungeon Keeper 2
- Factorio
- Frostpunk
- FTL: Faster Than Light
- Ghost Master
- Heroes of Might and Magic series:
    - Heroes of Might and Magic IV
    - Heroes of Might and Magic V
- Into the Breach
- Iron Harvest
- Kingdom Rush series:
    - Kingdom Rush
    - Kingdom Rush: Frontiers
    - Kingdom Rush: Origins
- Möbius Front '83
- Off-World Resource Base
- Oil Rush
- Pandora: First Contact
- Renowned Explorers
- Reus
- Shadow Gambit: The Cursed Crew (full game and free demo)
- Shadow Tactics: Blades of the Shogun
- Songs of Conquest
- Songs of Silence
- Space Run: Fast and safe delivery
- Star Wars: Galactic Battlegrounds
- StarCraft
- Startopia
- Stellaris
- The Fertile Crescent
- The Settlers II
- Transistor
- Tropico series:
    - Tropico
    - Tropico 2: Pirate Cove
- War for the Overworld
- Warcraft series:
    - Warcraft: Orcs & Humans
    - Warcraft III: Reign of Chaos + The Frozen Throne
- Warhammer 40,000 setting:
    - Warhammer 40,000: Dawn of War
    - Warhammer 40,000: Gladius - Relics of War
- Warlords Battlecry 2
- Worms series:
    - Worms 2
    - Worms Armageddon
