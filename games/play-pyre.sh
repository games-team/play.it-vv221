#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pyre
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='pyre'
GAME_NAME='Pyre'

ARCHIVE_BASE_ITCH_1_NAME='pyre-linux.zip'
ARCHIVE_BASE_ITCH_1_MD5='44e0fe30a9d8bcf73c4fa288dd4324ab'
ARCHIVE_BASE_ITCH_1_SIZE='8051755'
ARCHIVE_BASE_ITCH_1_VERSION='1.50476-itch.2019.03.19'
ARCHIVE_BASE_ITCH_1_URL='https://supergiant-games.itch.io/pyre'

ARCHIVE_BASE_GOG_1_NAME='pyre_1_50427_11957_23366.sh'
ARCHIVE_BASE_GOG_1_MD5='ae34d8b4c069ffd7a98f295af4596e1f'
ARCHIVE_BASE_GOG_1_SIZE='8104850'
ARCHIVE_BASE_GOG_1_VERSION='1.50427-gog23366'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/pyre'

ARCHIVE_BASE_GOG_0_NAME='pyre_en_1_0_18732.sh'
ARCHIVE_BASE_GOG_0_MD5='83ea264e95e2519aba72078d35290d49'
ARCHIVE_BASE_GOG_0_SIZE='8100000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog18732'

CONTENT_PATH_DEFAULT_ITCH='.'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_LIBS_FILES='
libBink2.so
libFModPlugins.so
libfmod.so.8
libfmodstudio.so.8
liblua52.so
libMonoPosixHelper.so'
CONTENT_LIBS_BIN64_RELATIVE_PATH='lib64'
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_BIN32_RELATIVE_PATH='lib'
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_BIN64_FILES='
Pyre.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
Pyre.bin.x86'
CONTENT_GAME_DATA_FILES='
gamecontrollerdb.txt
monoconfig
monomachineconfig
Content
*.bmp
*.config
*.cur
*.dll
*.exe
*.pdb
*.xml'
CONTENT_DOC_DATA_FILES='
Linux.README
ReadMe.txt'

## The shipped binaries are used instead of system-provided Mono to avoid a crash on initial loading.
## See notes/pyre for more details.
APP_MAIN_EXE_BIN64='Pyre.bin.x86_64'
APP_MAIN_EXE_BIN32='Pyre.bin.x86'
APP_MAIN_ICON='PyreIcon.bmp'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libstdc++.so.6
libudev.so.1
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Apply common Mono tweaks.
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(mono_launcher_tweaks)"

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
