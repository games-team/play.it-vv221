#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# Pillars of Eternity
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='pillars-of-eternity-1'
GAME_NAME='Pillars of Eternity'

ARCHIVE_BASE_3_NAME='pillars_of_eternity_en_3_07_0_1318_17461.sh'
ARCHIVE_BASE_3_MD5='57164ad0cbc53d188dde0b38e7491916'
ARCHIVE_BASE_3_SIZE='14801525'
ARCHIVE_BASE_3_VERSION='3.7.0.1318-gog17461'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/pillars_of_eternity_hero_edition'

ARCHIVE_BASE_2_NAME='pillars_of_eternity_en_3_07_16405.sh'
ARCHIVE_BASE_2_MD5='e4271b5e72f1ecc9fbbc4d90937ede05'
ARCHIVE_BASE_2_SIZE='15000000'
ARCHIVE_BASE_2_VERSION='3.7.0.1284-gog16405'

ARCHIVE_BASE_1_NAME='gog_pillars_of_eternity_2.16.0.20.sh'
ARCHIVE_BASE_1_MD5='0d21cf95bda070bdbfbe3e79f8fc32d6'
ARCHIVE_BASE_1_SIZE='15000000'
ARCHIVE_BASE_1_VERSION='3.06.1254-gog2.16.0.20'

ARCHIVE_BASE_0_NAME='gog_pillars_of_eternity_2.15.0.19.sh'
ARCHIVE_BASE_0_MD5='2000052541abb1ef8a644049734e8526'
ARCHIVE_BASE_0_SIZE='15000000'
ARCHIVE_BASE_0_VERSION='3.05.1186-gog2.15.0.19'

UNITY3D_NAME='PillarsOfEternity'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_DATA_AREAS_FILES="
${UNITY3D_NAME}_Data/assetbundles/st_ar_*"

PACKAGES_LIST='
PKG_BIN
PKG_DATA_AREAS
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_AREAS'

PKG_DATA_AREAS_ID="${PKG_DATA_ID}-areas"
PKG_DATA_AREAS_DESCRIPTION="$PKG_DATA_DESCRIPTION - areas"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
