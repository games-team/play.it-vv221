#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2018 Phil Morrell
set -o errexit

###
# Unreal Tournament
###

script_version=20250104.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='unreal-tournament'
GAME_NAME='Unreal Tournament'

# Archives

## Unreal Tournament (game installer)
## This game is no longer available for sale from gog.com since 2022-12-23.

ARCHIVE_BASE_0_NAME='setup_ut_goty_2.0.0.5.exe'
ARCHIVE_BASE_0_MD5='0d25ec835648710a098aff7106187f38'
## Do not convert file paths to lowercase
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_SIZE='640000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.5'

## Native Linux engine

ARCHIVE_REQUIRED_ENGINE_1_NAME='OldUnreal-UTPatch469d-Linux-amd64.tar.bz2'
ARCHIVE_REQUIRED_ENGINE_1_MD5='d0e133165bf1630288583e52a40b90db'
ARCHIVE_REQUIRED_ENGINE_1_URL='https://github.com/OldUnreal/UnrealTournamentPatches/releases/tag/v469d'

ARCHIVE_REQUIRED_ENGINE_0_NAME='OldUnreal-UTPatch469c-Linux-amd64.tar.bz2'
ARCHIVE_REQUIRED_ENGINE_0_MD5='6cd032e70460b1393d9514ffe81dcb1a'
ARCHIVE_REQUIRED_ENGINE_0_URL='https://github.com/OldUnreal/UnrealTournamentPatches/releases/tag/v469c'

# Archives content

## Unreal Tournament (game installer)

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_DATA_FILES='
Maps
Music
Sounds
Textures'
CONTENT_DOC_DATA_FILES='
Help
Manual'

## Native Linux engine

CONTENT_PATH_DEFAULT_ENGINE='.'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT_ENGINE}/System64"
CONTENT_LIBS_BIN_FILES='
ALAudio.so
Cluster.so
Core.so
Editor.so
Engine.so
Fire.so
IpDrv.so
libfmod.so.13
OpenGLDrv.so
Render.so
SDLDrv.so
udemo.so
UWeb.so
XOpenGLDrv.so'
CONTENT_GAME_BIN_PATH="$CONTENT_PATH_DEFAULT_ENGINE"
CONTENT_GAME_BIN_FILES='
System
System64'
CONTENT_GAME0_DATA_PATH="$CONTENT_PATH_DEFAULT_ENGINE"
CONTENT_GAME0_DATA_FILES='
SystemLocalized
Textures
Web'
CONTENT_DOC0_DATA_PATH="$CONTENT_PATH_DEFAULT_ENGINE"
CONTENT_DOC0_DATA_FILES='
Help
LICENSE.md'


APP_MAIN_EXE='System64/ut-bin-amd64'
APP_MAIN_ICON='System/Unreal.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libmpg123.so.0
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libxmp.so.4'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Chek for the presence of the required game engine

archive_initialize_required \
	'ARCHIVE_ENGINE' \
	'ARCHIVE_REQUIRED_ENGINE_1' \
	'ARCHIVE_REQUIRED_ENGINE_0'

## Update the version string based on the engine build
current_archive=$(current_archive)
engine_version=$(archive_name 'ARCHIVE_ENGINE' | sed 's/OldUnreal-UTPatch\([a-z0-9]\+\)-Linux-amd64.tar.bz2/\1/')
archive_version_updated=$(get_value "${current_archive}_VERSION" | sed "s/^1.0-/${engine_version}-/")
export "${current_archive}_VERSION=${archive_version_updated}"

# Extract game data

archive_extraction_default
archive_extraction 'ARCHIVE_ENGINE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_BIN')"

	# Drop some shipped libraries
	rm \
		'System64/libmpg123.so' \
		'System64/libopenal.so.1' \
		'System64/libSDL2-2.0.so.0' \
		'System64/libxmp.so.4'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## The game engine looks for libmpg123.so instead of libmpg123.so.0
case "$(option_value 'package')" in
	('arch')
		# Arch Linux already provides the unversioned .so
	;;
	('gentoo'|'egentoo')
		# Gentoo already provides the unversioned .so
	;;
	('deb')
		library_destination="$(package_path 'PKG_BIN')$(path_libraries)/libmpg123.so"
		mkdir --parents "$(dirname "$library_destination")"
		ln --symbolic "$(path_libraries_system)/libmpg123.so.0" "$library_destination"
	;;
esac

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd System64
	./ut-bin-amd64 "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
