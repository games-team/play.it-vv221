#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Total Overdose
###

script_version=20241125.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='total-overdose'
GAME_NAME='Total Overdose'

ARCHIVE_BASE_0_NAME='setup_total_overdose_1.00_(18925).exe'
ARCHIVE_BASE_0_MD5='df9f6750e5f12fd83ee775365dfd9637'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1700000'
ARCHIVE_BASE_0_VERSION='1.00-gog18925'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/total_overdose_a_gunslingers_tale_in_mexico'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
tod.exe
*.dll'
CONTENT_GAME_DATA_FILES='
*.dpc
*.naz'
CONTENT_DOC_DATA_FILES='
*.doc
*.docx
*.pdf'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Total Overdose'

APP_MAIN_EXE='tod.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=101'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
