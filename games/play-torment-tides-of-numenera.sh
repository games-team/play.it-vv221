#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Torment: Tides of Numenera
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='torment-tides-of-numenera'
GAME_NAME='Torment: Tides of Numenera'

ARCHIVE_BASE_0_NAME='gog_torment_tides_of_numenera_2.3.0.4.sh'
ARCHIVE_BASE_0_MD5='839337b42a1618f3b445f363eca210d3'
ARCHIVE_BASE_0_SIZE='9300000'
ARCHIVE_BASE_0_VERSION='1.1.0-gog2.3.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/torment_tides_of_numenera'

UNITY3D_NAME='TidesOfNumenera'
UNITY3D_PLUGINS='
libAkFlanger.so
libAkGuitarDistortion.so
libAkHarmonizer.so
libAkPitchShifter.so
libAkSoundEngine.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_DATA_AUDIO_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/Audio"
CONTENT_GAME_DATA_RESOURCES_FILES="
${UNITY3D_NAME}_Data/resources.assets
${UNITY3D_NAME}_Data/resources.assets.resS"

PACKAGES_LIST='
PKG_BIN
PKG_DATA_AUDIO
PKG_DATA_RESOURCES
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_AUDIO
PKG_DATA_RESOURCES'

PKG_DATA_AUDIO_ID="${PKG_DATA_ID}-audio"
PKG_DATA_AUDIO_DESCRIPTION="$PKG_DATA_DESCRIPTION - audio"
## Ensure smooth upgrades from packages generated with pre-20231020.1 game scripts.
PKG_DATA_AUDIO_PROVIDES="${PKG_DATA_AUDIO_PROVIDES:-}
torment-tides-of-numenera-audio"

PKG_DATA_RESOURCES_ID="${PKG_DATA_ID}-resources"
PKG_DATA_RESOURCES_DESCRIPTION="$PKG_DATA_DESCRIPTION - resources"
## Ensure smooth upgrades from packages generated with pre-20231020.1 game scripts.
PKG_DATA_RESOURCES_PROVIDES="${PKG_DATA_RESOURCES_PROVIDES:-}
torment-tides-of-numenera-resources"

# Include a workaround for the quest-breaking Anechoic Lazaret bugs
# cf. https://steamcommunity.com/app/272270/discussions/1/1473096694453357831/?ctp=15#c1708438376918556245

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
sed
unix2dos"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Include a workaround for the quest-breaking Anechoic Lazaret bugs
	## cf. https://steamcommunity.com/app/272270/discussions/1/1473096694453357831/?ctp=15#c1708438376918556245
	conversation_file="$(unity3d_name)_Data/StreamingAssets/data/conversations/a_sagus/a2623_damaged_peerless_drone.conversation"
	sed_pattern='      <OnEnterScripts />'
	sed_replacement='      <OnEnterScripts>\n'
	sed_replacement="$sed_replacement"'        <ScriptCall>\n'
	sed_replacement="$sed_replacement"'          <Data>\n'
	sed_replacement="$sed_replacement"'            <HasDifficultTaskAttribute>false</HasDifficultTaskAttribute>\n'
	sed_replacement="$sed_replacement"'            <FullName>Void SetGlobalValue(String, Int32)</FullName>\n'
	sed_replacement="$sed_replacement"'            <Parameters>\n'
	sed_replacement="$sed_replacement"'              <string>Quest_AnechoicLazaret_DefeatedDrones</string>\n'
	sed_replacement="$sed_replacement"'              <string>1</string>\n'
	sed_replacement="$sed_replacement"'            </Parameters>\n'
	sed_replacement="$sed_replacement"'          </Data>\n'
	sed_replacement="$sed_replacement"'        </ScriptCall>\n'
	sed_replacement="$sed_replacement"'      </OnEnterScripts>'
	sed_expression="1010s#${sed_pattern}#${sed_replacement}#"
	sed --in-place --expression="$sed_expression" "$conversation_file"
	unix2dos --quiet "$conversation_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
