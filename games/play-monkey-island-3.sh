#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Monkey Island 3
###

script_version=20241121.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='monkey-island-3'
GAME_NAME='Monkey Island 3: The Curse of Monkey Island'

ARCHIVE_BASE_EN_2_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(20627).exe'
ARCHIVE_BASE_EN_2_MD5='42de5e8ca7f26eb8350db318430d24a1'
ARCHIVE_BASE_EN_2_TYPE='innosetup'
ARCHIVE_BASE_EN_2_PART1_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(20627)-1.bin'
ARCHIVE_BASE_EN_2_PART1_MD5='1f89d0a37bd2e82361b11cff176daf90'
ARCHIVE_BASE_EN_2_SIZE='1224692'
ARCHIVE_BASE_EN_2_VERSION='1.0l-gog20627'
ARCHIVE_BASE_EN_2_URL='https://www.gog.com/game/the_curse_of_monkey_island'

ARCHIVE_BASE_FR_2_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(french)_(20627).exe'
ARCHIVE_BASE_FR_2_MD5='2999c1f94902ce2093347f4f53adb14f'
ARCHIVE_BASE_FR_2_TYPE='innosetup'
ARCHIVE_BASE_FR_2_PART1_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(french)_(20627)-1.bin'
ARCHIVE_BASE_FR_2_PART1_MD5='9431d663d5264b41e03118cdad24f217'
ARCHIVE_BASE_FR_2_SIZE='1238398'
ARCHIVE_BASE_FR_2_VERSION='1.0l-gog20627'
ARCHIVE_BASE_FR_2_URL='https://www.gog.com/game/the_curse_of_monkey_island'

ARCHIVE_BASE_EN_1_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(20628).exe'
ARCHIVE_BASE_EN_1_MD5='fcd4a7cd9c0304c15a0a059f6eb299e8'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_PART1_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(20628)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='931e6e35fdc7e0a14f2559984620f8f3'
ARCHIVE_BASE_EN_1_SIZE='1200000'
ARCHIVE_BASE_EN_1_VERSION='1.0l-gog20628'

ARCHIVE_BASE_FR_1_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(french)_(20628).exe'
ARCHIVE_BASE_FR_1_MD5='a0ebaa26154de4a76d20b0ef882445cf'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_PART1_NAME='setup_the_curse_of_monkey_islandtm_1.0l_(french)_(20628)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='10356610b119b917dbba13df9ed207a5'
ARCHIVE_BASE_FR_1_SIZE='1200000'
ARCHIVE_BASE_FR_1_VERSION='1.0l-gog20628'

ARCHIVE_BASE_EN_0_NAME='setup_the_curse_of_monkey_island_1.0_(18253).exe'
ARCHIVE_BASE_EN_0_MD5='20c74e5f60bd724182ec2bdbae6d9a49'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1200000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog18253'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_GAME_L10N_FILES='
resource/bbsan.san
resource/curserng.san
resource/finale.san
resource/language.tab
resource/lavaride.san
resource/liftcrse.san
resource/moreslaw.san
resource/newboots.san
resource/opening.san
resource/sinkshp.san
resource/voxdisk1.bun
resource/voxdisk2.bun
resource/wrecksan.san'
CONTENT_GAME_MAIN_FILES='
comi.la?
resource'
CONTENT_DOC_L10N_FILES='
*.pdf'

APP_MAIN_SCUMMID='scumm:comi'
APP_MAIN_ICON='app/goggame-1528148981.ico'
APP_MAIN_ICON_EN_0='goggame-1528148981.ico'

PACKAGES_LIST='
PKG_L10N
PKG_MAIN'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

# Work around "insufficient image data" error with convert from imagemagick

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
icotool"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

## Work around "insufficient image data" error with convert from imagemagick
## TODO: Check if it is still required with current imagemagick
icon_extract_png_from_ico() {
	local icon destination
	icon="$1"
	destination="$2"

	local icon_file
	icon_file=$(icon_full_path "$icon")
	icotool --extract --output="$destination" "$icon_file" 2>/dev/null
}
content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
