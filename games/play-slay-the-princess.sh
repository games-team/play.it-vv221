#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2025 BetaRays
set -o errexit

###
# Slay the Princess
###

script_version=20250211.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='slay-the-princess'
GAME_NAME='Slay the Princess'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

## The Pristine Cut

ARCHIVE_BASE_16_NAME='slay_the_princess_the_pristine_cut_the_pristine_cut_linux_1_1a_79534.sh'
ARCHIVE_BASE_16_MD5='4b7dfeb531a7431bbd2fe84e0fc2cfde'
ARCHIVE_BASE_16_SIZE='12700851'
ARCHIVE_BASE_16_VERSION='1.1a-gog79534'
ARCHIVE_BASE_16_URL='https://www.gog.com/game/slay_the_princess'

ARCHIVE_BASE_15_NAME='slay_the_princess_the_pristine_cut_the_pristine_cut_linux_1_0z_77466.sh'
ARCHIVE_BASE_15_MD5='9492a8cffbf5ab5ac79593ceb763d072'
ARCHIVE_BASE_15_SIZE='12706022'
ARCHIVE_BASE_15_VERSION='1.0z-gog77466'
ARCHIVE_BASE_15_URL='https://www.gog.com/game/slay_the_princess'

ARCHIVE_BASE_14_NAME='slay_the_princess_the_pristine_cut_the_pristine_cut_linux_1_0p_77354.sh'
ARCHIVE_BASE_14_MD5='9cde15f2957c37f7679d73f3fa8eed8c'
ARCHIVE_BASE_14_SIZE='12689218'
ARCHIVE_BASE_14_VERSION='1.0p-gog77354'

ARCHIVE_BASE_13_NAME='slay_the_princess_the_pristine_cut_the_pristine_cut_linux_1_0l_77303.sh'
ARCHIVE_BASE_13_MD5='23fb0cd438f5936479d56846c65be179'
ARCHIVE_BASE_13_SIZE='12684306'
ARCHIVE_BASE_13_VERSION='1.0l-gog77303'

ARCHIVE_BASE_12_NAME='slay_the_princess_the_pristine_cut_linux_1_0_77121.sh'
ARCHIVE_BASE_12_MD5='df352f8474c406976f9cd3d87d0b9e79'
ARCHIVE_BASE_12_SIZE='12679961'
ARCHIVE_BASE_12_VERSION='1.0-gog77121'
ARCHIVE_BASE_12_URL='https://www.gog.com/game/slay_the_princess'

## End of Everything update

ARCHIVE_BASE_11_NAME='slay_the_princess_end_of_everything_update_1_2e_scrollbar_73222.sh'
ARCHIVE_BASE_11_MD5='f1e7f54e99ceae8aa4c1314274708b1d'
ARCHIVE_BASE_11_SIZE='8799577'
ARCHIVE_BASE_11_VERSION='1.2e-gog73222'
ARCHIVE_BASE_11_URL='https://www.gog.com/game/slay_the_princess'

ARCHIVE_BASE_10_NAME='slay_the_princess_end_of_everything_update_1_2d_linux_better_controller_73049.sh'
ARCHIVE_BASE_10_MD5='bf46a8f8820d6821bd9313dd49c35771'
ARCHIVE_BASE_10_SIZE='8799553'
ARCHIVE_BASE_10_VERSION='1.2d-gog73049'

ARCHIVE_BASE_9_NAME='slay_the_princess_end_of_everything_update_1_2c_linux_better_controller_72957.sh'
ARCHIVE_BASE_9_MD5='c93f7c398fc157d14679c93fcea26299'
ARCHIVE_BASE_9_SIZE='8799548'
ARCHIVE_BASE_9_VERSION='1.2c-gog72957'

ARCHIVE_BASE_8_NAME='slay_the_princess_end_of_everything_update_1_2b_linux_72856.sh'
ARCHIVE_BASE_8_MD5='6b9588bc80145f05bfc0105d4f97fda7'
ARCHIVE_BASE_8_SIZE='8799541'
ARCHIVE_BASE_8_VERSION='1.2b-gog72856'

ARCHIVE_BASE_7_NAME='slay_the_princess_end_of_everything_update_1_2a_linux_72334.sh'
ARCHIVE_BASE_7_MD5='caa8432a8b7e8c6d75191bf143714957'
ARCHIVE_BASE_7_SIZE='8799532'
ARCHIVE_BASE_7_VERSION='1.2a-gog72334'

ARCHIVE_BASE_6_NAME='slay_the_princess_end_of_everything_update_1_2_linux_72260.sh'
ARCHIVE_BASE_6_MD5='950c30b1f3496691ce2560cf3d55ed07'
ARCHIVE_BASE_6_SIZE='8799446'
ARCHIVE_BASE_6_VERSION='1.2-gog72260'

## Original builds

ARCHIVE_BASE_5_NAME='slay_the_princess_linux_1_1c_release_70031.sh'
ARCHIVE_BASE_5_MD5='d2ab1e74725c1e28f2f3a8e74347b4ee'
ARCHIVE_BASE_5_SIZE='8362317'
ARCHIVE_BASE_5_VERSION='1.1c-gog70031'

ARCHIVE_BASE_4_NAME='slay_the_princess_linux_1_1af_release_69960.sh'
ARCHIVE_BASE_4_MD5='f179e501f2edd3357c73b81eca40b34d'
ARCHIVE_BASE_4_SIZE='8378490'
ARCHIVE_BASE_4_VERSION='1.1af-gog69960'

ARCHIVE_BASE_3_NAME='slay_the_princess_linux_1_0h_release_69787.sh'
ARCHIVE_BASE_3_MD5='851f53732e9dc6314677271f67852f35'
ARCHIVE_BASE_3_SIZE='11000045'
ARCHIVE_BASE_3_VERSION='1.0h-gog69787'

ARCHIVE_BASE_2_NAME='slay_the_princess_linux_1_0g_release_68787.sh'
ARCHIVE_BASE_2_MD5='3b8417d581d1d721ab4016720a1ea881'
ARCHIVE_BASE_2_SIZE='10989272'
ARCHIVE_BASE_2_VERSION='1.0g-gog68787'

ARCHIVE_BASE_1_NAME='slay_the_princess_linux_1_0f_release_68611.sh'
ARCHIVE_BASE_1_MD5='a4fd4334624bbc469a481fc874df2750'
ARCHIVE_BASE_1_SIZE='11003752'
ARCHIVE_BASE_1_VERSION='1.0f-gog68611'

ARCHIVE_BASE_0_NAME='slay_the_princess_linux_1_0d_release_68521.sh'
ARCHIVE_BASE_0_MD5='7acf7f8093cdddff8d484aa4043e5c51'
ARCHIVE_BASE_0_SIZE='11014500'
ARCHIVE_BASE_0_VERSION='1.0d-gog68521'

## Free demo

ARCHIVE_BASE_DEMO_0_NAME='SlaythePrincessDemo-1.0-market.zip'
ARCHIVE_BASE_DEMO_0_MD5='135628b57b5e4debbaf21680d493cd74'
ARCHIVE_BASE_DEMO_0_SIZE='2839604'
ARCHIVE_BASE_DEMO_0_VERSION='1.0-itch'
ARCHIVE_BASE_DEMO_0_URL='https://blacktabbygames.itch.io/slay-the-princess'

CONTENT_PATH_DEFAULT='data/noarch/game/game'
CONTENT_PATH_DEFAULT_DEMO='game'
CONTENT_GAME_MAIN_FILES='
audio
cache
gui
images
staging
script_version.txt'
CONTENT_GAME0_MAIN_FILES_DEMO='
tl
archive.rpa'
CONTENT_GAME_ARCHIVE_1_FILES='
archive.rpa.split00'
CONTENT_GAME_ARCHIVE_2_FILES='
archive.rpa.split01'
## archive.rpa.split02 is only provided with some old builds.
CONTENT_GAME0_ARCHIVE_2_FILES='
archive.rpa.split02'

APP_MAIN_TYPE='renpy'
APP_MAIN_ICON='../SlaythePrincess.exe'
APP_MAIN_ICON_DEMO='../SlaythePrincessDemo.exe'

PACKAGES_LIST='
PKG_MAIN
PKG_ARCHIVE_1
PKG_ARCHIVE_2'
PACKAGES_LIST_DEMO='
PKG_MAIN'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_ARCHIVE_1
PKG_ARCHIVE_2'

PKG_ARCHIVE_ID="${GAME_ID}-archive"
PKG_ARCHIVE_1_ID="${PKG_ARCHIVE_ID}-1"
PKG_ARCHIVE_2_ID="${PKG_ARCHIVE_ID}-2"
PKG_ARCHIVE_DESCRIPTION='archive chunks'
PKG_ARCHIVE_1_DESCRIPTION="$PKG_ARCHIVE_DESCRIPTION - 1"
PKG_ARCHIVE_2_DESCRIPTION="$PKG_ARCHIVE_DESCRIPTION - 2"

## Ensure easy upgrades from packages generated with pre-20240427.1 game scripts
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
slay-the-princess-data"
PKG_ARCHIVE_1_PROVIDES="${PKG_ARCHIVE_1_PROVIDES:-}
slay-the-princess-data-archive-1"
PKG_ARCHIVE_2_PROVIDES="${PKG_ARCHIVE_2_PROVIDES:-}
slay-the-princess-data-archive-2"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

## Check for the presence of the original game icon, fall back on the GOG-specific one if required
case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		# The game demo is not provided by GOG.
	;;
	(*)
		if [ ! -e "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/${APP_MAIN_ICON}" ]; then
			APP_MAIN_ICON='../../support/icon.png'
		fi
	;;
esac

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		# Prevent some properties of the full game from applying to the demo
		unset PKG_MAIN_DEPENDENCIES_SIBLINGS
		unset PKG_MAIN_PROVIDES
	;;
	(*)
		# Rebuild the huge file from its chunks
		huge_file='archive.rpa'
		PKG_MAIN_POSTINST_RUN="$(package_postinst_actions 'PKG_MAIN')
		$(huge_file_concatenate "$huge_file")"
		PKG_MAIN_PRERM_RUN="$(package_prerm_actions 'PKG_MAIN')
		$(huge_file_delete "$huge_file")"
	;;
esac

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
