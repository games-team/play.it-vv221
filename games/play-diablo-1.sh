#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Diablo 1
###

script_version=20250211.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='diablo-1'
GAME_NAME='Diablo'

# Archives

## Diablo 1 (game data)

ARCHIVE_BASE_8_NAME='setup_diablo_1.09_hellfire_v4_(78466).exe'
ARCHIVE_BASE_8_MD5='c4d36192c35eb5f34faa1d97aae34455'
ARCHIVE_BASE_8_TYPE='innosetup'
ARCHIVE_BASE_8_SIZE='845401'
ARCHIVE_BASE_8_VERSION='1.09-gog78466'
ARCHIVE_BASE_8_URL='https://www.gog.com/game/diablo'

ARCHIVE_BASE_7_NAME='setup_diablo_1.09_hellfire_v3_(78106).exe'
ARCHIVE_BASE_7_MD5='b8d5a38fd4b21311853f19559c2ebfee'
ARCHIVE_BASE_7_TYPE='innosetup'
ARCHIVE_BASE_7_SIZE='845020'
ARCHIVE_BASE_7_VERSION='1.09-gog78106'

ARCHIVE_BASE_6_NAME='setup_diablo_1.09_hellfire_v2_(30037).exe'
ARCHIVE_BASE_6_MD5='2b8f0eafc528a56452b0008dde4151a8'
ARCHIVE_BASE_6_TYPE='innosetup'
ARCHIVE_BASE_6_SIZE='843414'
ARCHIVE_BASE_6_VERSION='1.09-gog30037'

ARCHIVE_BASE_5_NAME='setup_diablo_1.09_hellfire_v2_(30038).exe'
ARCHIVE_BASE_5_MD5='e70187d92fa120771db99dfa81679cfc'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_SIZE='850000'
ARCHIVE_BASE_5_VERSION='1.09-gog30038'

ARCHIVE_BASE_4_NAME='setup_diablo_1.09_v6_(28378).exe'
ARCHIVE_BASE_4_MD5='588ab50c1ef25abb682b86ea4306ea50'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_SIZE='670000'
ARCHIVE_BASE_4_VERSION='1.09-gog28378'

ARCHIVE_BASE_3_NAME='setup_diablo_1.09_v4_(27989).exe'
ARCHIVE_BASE_3_MD5='8dac74a616646fa41d5d73f4765cef40'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_SIZE='670000'
ARCHIVE_BASE_3_VERSION='1.09-gog27989'

ARCHIVE_BASE_2_NAME='setup_diablo_1.09_v3_(27965).exe'
ARCHIVE_BASE_2_MD5='38d654af858d7a2591711f0e6324fcd0'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='670000'
ARCHIVE_BASE_2_VERSION='1.09-gog27695'

ARCHIVE_BASE_1_NAME='setup_diablo_1.09_v2_(27882).exe'
ARCHIVE_BASE_1_MD5='83b2d6b8551a9825a426dac7b9302654'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='670000'
ARCHIVE_BASE_1_VERSION='1.09-gog27882'

ARCHIVE_BASE_0_NAME='setup_diablo_1.09_(27873).exe'
ARCHIVE_BASE_0_MD5='bf57594f5218a794a284b5e2a0f5ba14'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='680000'
ARCHIVE_BASE_0_VERSION='1.09-gog27873'

## DevilutionX

ARCHIVE_REQUIRED_DEVILUTIONX_1_NAME='devilutionx-x86_64-linux-gnu.tar.xz'
ARCHIVE_REQUIRED_DEVILUTIONX_1_MD5='f5b953adfbc9ea206a7fb393d6a45299'
ARCHIVE_REQUIRED_DEVILUTIONX_1_SIZE='39601'
ARCHIVE_REQUIRED_DEVILUTIONX_1_VERSION='1.5.4'
ARCHIVE_REQUIRED_DEVILUTIONX_1_URL='https://github.com/diasurgical/devilutionX/releases/tag/1.5.4'

ARCHIVE_REQUIRED_DEVILUTIONX_0_NAME='devilutionx-linux-x86_64.tar.xz'
ARCHIVE_REQUIRED_DEVILUTIONX_0_MD5='36dce8b296aabcefac2dcd3e7bd2a5c7'
ARCHIVE_REQUIRED_DEVILUTIONX_0_SIZE='39208'
ARCHIVE_REQUIRED_DEVILUTIONX_0_VERSION='1.5.3'
ARCHIVE_REQUIRED_DEVILUTIONX_0_URL='https://github.com/diasurgical/devilutionX/releases/tag/1.5.3'


CONTENT_PATH_DEFAULT='.'
## The devilutionx binary is linked against discord_game_sdk.so, so we can not drop it.
CONTENT_LIBS_BIN_FILES='
discord_game_sdk.so'
CONTENT_GAME_BIN_FILES='
devilutionx'
CONTENT_GAME_DATA_FILES='
*.mpq'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

APP_MAIN_EXE='devilutionx'
APP_MAIN_ICON='diablo.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of the archive providing the game engine

archive_initialize_required \
	'ARCHIVE_DEVILUTIONX' \
	'ARCHIVE_REQUIRED_DEVILUTIONX_1' \
	'ARCHIVE_REQUIRED_DEVILUTIONX_0'
## TODO: Update the version string based on the engine build
##       cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/550

# Extract game data

archive_extraction_default
archive_extraction 'ARCHIVE_DEVILUTIONX'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --force --recursive \
		'dx' \
		'hellfire' \
		'tmp'

	# Rename documentation files
	mv 'license.txt' 'license.diablo-1.txt'
	mv 'readme.txt' 'readme.diablo-1.txt'
	mv 'README.txt' 'readme.devilutionx.txt'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
