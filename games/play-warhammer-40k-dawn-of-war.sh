#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Hoël Bézier
set -o errexit

###
# Warhammer 40,000: Dawn of War:
# - Dawn of War (original game)
# - Winter Assault (full game and free demo)
# - Dark Crusade
# - Soulstorm
###

script_version=20241021.5

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warhammer-40k-dawn-of-war'
GAME_NAME='Warhammer 40,000: Dawn of War'

GAME_ID_WA="${GAME_ID}-winter-assault"
GAME_NAME_WA="$GAME_NAME - Winter Assault"

GAME_ID_WA_DEMO="${GAME_ID_WA}-demo"
GAME_NAME_WA_DEMO="$GAME_NAME_WA (demo)"

GAME_ID_DC="${GAME_ID}-dark-crusade"
GAME_NAME_DC="$GAME_NAME - Dark Crusade"

GAME_ID_SS="${GAME_ID}-soulstorm"
GAME_NAME_SS="$GAME_NAME - Soulstorm"

# Archives

## Dawn of War (base game)

ARCHIVE_BASE_DOW_EN_0_NAME='setup_warhammer_40000_dawn_of_war_0.19_(64626).exe'
ARCHIVE_BASE_DOW_EN_0_MD5='a45fc06675d678b282e7f72f39c2591c'
ARCHIVE_BASE_DOW_EN_0_TYPE='innosetup'
ARCHIVE_BASE_DOW_EN_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_0.19_(64626)-1.bin'
ARCHIVE_BASE_DOW_EN_0_PART1_MD5='e89415bc33d078598a19650ddd80d2be'
ARCHIVE_BASE_DOW_EN_0_SIZE='2800000'
ARCHIVE_BASE_DOW_EN_0_VERSION='1.51-gog64626'
ARCHIVE_BASE_DOW_EN_0_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

ARCHIVE_BASE_DOW_FR_0_NAME='setup_warhammer_40000_dawn_of_war_0.19_(french)_(64626).exe'
ARCHIVE_BASE_DOW_FR_0_MD5='e1cdc8cc2b9a81b390cd1996053d5c04'
ARCHIVE_BASE_DOW_FR_0_TYPE='innosetup'
ARCHIVE_BASE_DOW_FR_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_0.19_(french)_(64626)-1.bin'
ARCHIVE_BASE_DOW_FR_0_PART1_MD5='32eecc81507d84f6d39f9c74aeb5e1ef'
ARCHIVE_BASE_DOW_FR_0_SIZE='2800000'
ARCHIVE_BASE_DOW_FR_0_VERSION='1.51-gog64626'
ARCHIVE_BASE_DOW_FR_0_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

## Winter Assault (full game)

ARCHIVE_BASE_WA_EN_1_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(64626).exe'
ARCHIVE_BASE_WA_EN_1_MD5='223385b79c6ee7eb691248a943eb53fa'
ARCHIVE_BASE_WA_EN_1_TYPE='innosetup'
ARCHIVE_BASE_WA_EN_1_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(64626)-1.bin'
ARCHIVE_BASE_WA_EN_1_PART1_MD5='61e354453cb8638b995f5ed17a4bdbbd'
ARCHIVE_BASE_WA_EN_1_SIZE='2763147'
ARCHIVE_BASE_WA_EN_1_VERSION='1.51-gog64626'
ARCHIVE_BASE_WA_EN_1_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

ARCHIVE_BASE_WA_FR_1_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(french)_(64626).exe'
ARCHIVE_BASE_WA_FR_1_MD5='99640045bdd625ff55aed497aef96e78'
ARCHIVE_BASE_WA_FR_1_TYPE='innosetup'
ARCHIVE_BASE_WA_FR_1_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(french)_(64626)-1.bin'
ARCHIVE_BASE_WA_FR_1_PART1_MD5='e6263848739e9c9975632c0766fae2e1'
ARCHIVE_BASE_WA_FR_1_SIZE='2769494'
ARCHIVE_BASE_WA_FR_1_VERSION='1.51-gog64626'
ARCHIVE_BASE_WA_FR_1_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

ARCHIVE_BASE_WA_EN_0_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(64626).exe'
ARCHIVE_BASE_WA_EN_0_MD5='a610066c44aee835f0ac608864c31ef9'
ARCHIVE_BASE_WA_EN_0_TYPE='innosetup'
ARCHIVE_BASE_WA_EN_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(64626)-1.bin'
ARCHIVE_BASE_WA_EN_0_PART1_MD5='fd954304b3f38cccf859edf7ca4e1dd5'
ARCHIVE_BASE_WA_EN_0_SIZE='2800000'
ARCHIVE_BASE_WA_EN_0_VERSION='1.51-gog64626'

ARCHIVE_BASE_WA_FR_0_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(french)_(64626).exe'
ARCHIVE_BASE_WA_FR_0_MD5='9423b22d50f707d794e961aef63a9a44'
ARCHIVE_BASE_WA_FR_0_TYPE='innosetup'
ARCHIVE_BASE_WA_FR_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_winter_assault_0.19_(french)_(64626)-1.bin'
ARCHIVE_BASE_WA_FR_0_PART1_MD5='b4a188c7ed2012a9c4fd300af07c831d'
ARCHIVE_BASE_WA_FR_0_SIZE='2800000'
ARCHIVE_BASE_WA_FR_0_VERSION='1.51-gog64626'

## Winter Assault (free demo)

ARCHIVE_BASE_WA_DEMO_EN_0_NAME='Dawn of War - Winter Assault.rar'
ARCHIVE_BASE_WA_DEMO_EN_0_MD5='555f5b3844c80866b0cb9fa536692380'
ARCHIVE_BASE_WA_DEMO_EN_0_SIZE='480000'
ARCHIVE_BASE_WA_DEMO_EN_0_VERSION='1.0-archiveorg1'
ARCHIVE_BASE_WA_DEMO_EN_0_URL='https://archive.org/details/DawnOfWarWinterAssault_201404'

## Dark Crusade

ARCHIVE_BASE_DC_EN_0_NAME='setup_warhammer_40000_dawn_of_war_-_dark_crusade_0.19_(64626).exe'
ARCHIVE_BASE_DC_EN_0_MD5='e54248e3e006fa378e374a9a60f33418'
ARCHIVE_BASE_DC_EN_0_TYPE='innosetup'
ARCHIVE_BASE_DC_EN_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_dark_crusade_0.19_(64626)-1.bin'
ARCHIVE_BASE_DC_EN_0_PART1_MD5='8830adc7aa004a800411b0f95919af5f'
ARCHIVE_BASE_DC_EN_0_SIZE='4500000'
ARCHIVE_BASE_DC_EN_0_VERSION='1.20-gog64626'
ARCHIVE_BASE_DC_EN_0_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

ARCHIVE_BASE_DC_FR_0_NAME='setup_warhammer_40000_dawn_of_war_-_dark_crusade_0.19_(french)_(64626).exe'
ARCHIVE_BASE_DC_FR_0_MD5='5a254443909788b595baf16610809056'
ARCHIVE_BASE_DC_FR_0_TYPE='innosetup'
ARCHIVE_BASE_DC_FR_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_dark_crusade_0.19_(french)_(64626)-1.bin'
ARCHIVE_BASE_DC_FR_0_PART1_MD5='0a64baaffaa392b9afd570a0a811cb2f'
ARCHIVE_BASE_DC_FR_0_SIZE='4600000'
ARCHIVE_BASE_DC_FR_0_VERSION='1.20-gog64626'
ARCHIVE_BASE_DC_FR_0_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

## Soulstorm

ARCHIVE_BASE_SS_EN_1_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.21_(64955).exe'
ARCHIVE_BASE_SS_EN_1_MD5='8da443afebb589ca1f1b0100579325d2'
ARCHIVE_BASE_SS_EN_1_TYPE='innosetup'
ARCHIVE_BASE_SS_EN_1_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.21_(64955)-1.bin'
ARCHIVE_BASE_SS_EN_1_PART1_MD5='ac634ca5d861d3fc502325646cffbd83'
ARCHIVE_BASE_SS_EN_1_PART2='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.21_(64955)-2.bin'
ARCHIVE_BASE_SS_EN_1_PART2_MD5='5c4cde5ecf4dae72bbdbc97a7659dbe5'
ARCHIVE_BASE_SS_EN_1_SIZE='5458668'
ARCHIVE_BASE_SS_EN_1_VERSION='1.2.0-gog64955'
ARCHIVE_BASE_SS_EN_1_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

ARCHIVE_BASE_SS_FR_1_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.21_(french)_(64955).exe'
ARCHIVE_BASE_SS_FR_1_MD5='e2fd0c36f0b36a9ad06c10ff4d044478'
ARCHIVE_BASE_SS_FR_1_TYPE='innosetup'
ARCHIVE_BASE_SS_FR_1_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.21_(french)_(64955)-1.bin'
ARCHIVE_BASE_SS_FR_1_PART1_MD5='29203f3fc2424b33f3251e44fbe65d13'
ARCHIVE_BASE_SS_FR_1_PART2='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.21_(french)_(64955)-2.bin'
ARCHIVE_BASE_SS_FR_1_PART2_MD5='6ad688745bba4fe353b1da9b330e956a'
ARCHIVE_BASE_SS_FR_1_SIZE='5459544'
ARCHIVE_BASE_SS_FR_1_VERSION='1.2.0-gog64955'
ARCHIVE_BASE_SS_FR_1_URL='https://www.gog.com/game/warhammer_40000_dawn_of_war'

ARCHIVE_BASE_SS_EN_0_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.19_(64626).exe'
ARCHIVE_BASE_SS_EN_0_MD5='ccf68576db62f61c3d4967a1e66d3863'
ARCHIVE_BASE_SS_EN_0_TYPE='innosetup'
ARCHIVE_BASE_SS_EN_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.19_(64626)-1.bin'
ARCHIVE_BASE_SS_EN_0_PART1_MD5='944e2a30b2036dbfb37b1b649c370541'
ARCHIVE_BASE_SS_EN_0_PART2_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.19_(64626)-2.bin'
ARCHIVE_BASE_SS_EN_0_PART2_MD5='949587a4ce461c4900636913ab53e5a1'
ARCHIVE_BASE_SS_EN_0_SIZE='5500000'
ARCHIVE_BASE_SS_EN_0_VERSION='1.2.0-gog64626'

ARCHIVE_BASE_SS_FR_0_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.19_(french)_(64626).exe'
ARCHIVE_BASE_SS_FR_0_MD5='a652d618bdc2071ad476842939b63373'
ARCHIVE_BASE_SS_FR_0_TYPE='innosetup'
ARCHIVE_BASE_SS_FR_0_PART1_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.19_(french)_(64626)-1.bin'
ARCHIVE_BASE_SS_FR_0_PART1_MD5='3ffd1c39b68c984acb23263c9ffec133'
ARCHIVE_BASE_SS_FR_0_PART2_NAME='setup_warhammer_40000_dawn_of_war_-_soulstorm_0.19_(french)_(64626)-2.bin'
ARCHIVE_BASE_SS_FR_0_PART2_MD5='6ba9d30068b91823d838812c8930c8b3'
ARCHIVE_BASE_SS_FR_0_SIZE='5500000'
ARCHIVE_BASE_SS_FR_0_VERSION='1.2.0-gog64626'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bugreport
*.dll
*.exe
*.ini'
CONTENT_GAME_L10N_FILES='
bugreport/english
bugreport/french
dxp2/locale
engine/locale
graphicsoptions/locale
w40k/locale
wxp/locale
regions.ini'
CONTENT_GAME_DATA_FILES='
drivers
dxp2
engine
graphicsoptions
patch
w40k
wxp
*.dat
*.module'
CONTENT_DOC_L10N_FILES='
*.htm'

USER_PERSISTENT_DIRECTORIES='
badges
banners
playback
profiles
screenshots
stats'
USER_PERSISTENT_FILES='
drivers/spdx9_config.txt
local.ini'

WINE_WINEPREFIX_TWEAKS='mono'
## Mono is not required by the Winter Assault demo.
WINE_WINEPREFIX_TWEAKS_WA_DEMO=' '

APP_MAIN_EXE_DOW='w40k.exe'
APP_MAIN_EXE_WA='w40kwa.exe'
APP_MAIN_EXE_WA_DEMO='winterassault.exe'
APP_MAIN_EXE_DC='darkcrusade.exe'
APP_MAIN_EXE_SS='soulstorm.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=101'
APP_MAIN_ICON_WRESTOOL_OPTIONS_WA_DEMO='--type=14'
## Type must be set explicitly,
## or it will be wrongly identified as a Mono application.
APP_MAIN_TYPE='wine'

# Packages

PACKAGES_LIST='
PKG_L10N
PKG_BIN
PKG_DATA'

PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_DATA_DESCRIPTION='data'

## Dawn of War (base game)

PKG_L10N_BASE_ID_DOW="${GAME_ID}-l10n"
PKG_L10N_ID_DOW_EN="${PKG_L10N_BASE_ID_DOW}-en"
PKG_L10N_ID_DOW_FR="${PKG_L10N_BASE_ID_DOW}-fr"
PKG_L10N_PROVIDES_DOW="
$PKG_L10N_BASE_ID_DOW"
PKG_L10N_DESCRIPTION_DOW_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_DOW_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_DOW="${GAME_ID}-data"

## Winter Assault (full game)

PKG_L10N_BASE_ID_WA="${GAME_ID_WA}-l10n"
PKG_L10N_ID_WA_EN="${PKG_L10N_BASE_ID_WA}-en"
PKG_L10N_ID_WA_FR="${PKG_L10N_BASE_ID_WA}-fr"
PKG_L10N_PROVIDES_WA="
$PKG_L10N_BASE_ID_WA"
PKG_L10N_DESCRIPTION_WA_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_WA_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_WA="${GAME_ID_WA}-data"

## Winter Assault (free demo)

PKG_L10N_BASE_ID_WA_DEMO="${GAME_ID_WA_DEMO}-l10n"
PKG_L10N_ID_WA_DEMO_EN="${PKG_L10N_BASE_ID_WA_DEMO}-en"
PKG_L10N_PROVIDES_WA_DEMO="
$PKG_L10N_BASE_ID_WA_DEMO"
PKG_L10N_DESCRIPTION_WA_DEMO_EN="$PKG_L10N_DESCRIPTION_EN"

PKG_DATA_ID_WA_DEMO="${GAME_ID_WA_DEMO}-data"

## Dark Crusade

PKG_L10N_BASE_ID_DC="${GAME_ID_DC}-l10n"
PKG_L10N_ID_DC_EN="${PKG_L10N_BASE_ID_DC}-en"
PKG_L10N_ID_DC_FR="${PKG_L10N_BASE_ID_DC}-fr"
PKG_L10N_PROVIDES_DC="
$PKG_L10N_BASE_ID_DC"
PKG_L10N_DESCRIPTION_DC_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_DC_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_DC="${GAME_ID_DC}-data"

## Soulstorm

PKG_L10N_BASE_ID_SS="${GAME_ID_SS}-l10n"
PKG_L10N_ID_SS_EN="${PKG_L10N_BASE_ID_SS}-en"
PKG_L10N_ID_SS_FR="${PKG_L10N_BASE_ID_SS}-fr"
PKG_L10N_PROVIDES_SS="
$PKG_L10N_BASE_ID_SS"
PKG_L10N_DESCRIPTION_SS_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_SS_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_SS="${GAME_ID_SS}-data"

# Winter Assault (free demo)
# - cabextract is required to extract the content of an inner archive
# - unix2dos is required to generate a .ini file

## Warning: REQUIREMENTS_LIST has no support for contextual values as of ./play.it 3.31
REQUIREMENTS_LIST_WA_DEMO="${REQUIREMENTS_LIST:-}
cabextract
unix2dos"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Prevent a presence check for the Mono archive if the current game build does not require it

## FIXME: This is required because ./play.it 2.31 has no context support for the variable WINE_WINEPREFIX_TWEAKS
init_extra_archives_required() {
	WINE_WINEPREFIX_TWEAKS=$(context_value 'WINE_WINEPREFIX_TWEAKS')
	if ! archives_required_extra_presence_check; then
		# Delete temporary files
		working_directory_cleanup

		exit 1
	fi
}

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of archive-specific requirements

REQUIREMENTS_LIST=$(context_value 'REQUIREMENTS_LIST')
requirements_check

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete unwanted files.
	## TODO: We should set a more explicit list of files to include to avoid that step.
	case "$(current_archive)" in
		('ARCHIVE_BASE_WA_DEMO_'*)
			rm -- *.exe *.ini *.msi
		;;
		(*)
			rm --force --recursive \
				'__redist' \
				'app' \
				'commonappdata' \
				'tmp'
		;;
	esac
)
## Winter Assault (free demo) - Extract game data from the cabinet installer.
case "$(current_archive)" in
	('ARCHIVE_BASE_WA_DEMO_'*)
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/WinterAssaultDemo1.cab"
		archive_extraction 'ARCHIVE_INNER'
		rm "$ARCHIVE_INNER_PATH"
	;;
esac
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Winter Assault (free demo) - Create the expected game arborescence.
	case "$(current_archive)" in
		('ARCHIVE_BASE_WA_DEMO_'*)
			mkdir --parents \
				'badges' \
				'banners' \
				'bugreport/english' \
				'drivers' \
				'engine' \
				'engine/data' \
				'engine/locale/english' \
				'engine/movies' \
				'graphicsoptions/data' \
				'graphicsoptions/locale/english' \
				'logfiles' \
				'patch' \
				'playback' \
				'profiles' \
				'screenshots' \
				'stats' \
				'w40k' \
				'w40k/data' \
				'w40k/locale/english' \
				'wxp' \
				'wxp/data' \
				'wxp/locale/english' \
				'wxp/movies'
			mv 'bugreport.exe'                  'bugreport/bugreport.exe'
			mv 'bugreport.ini'                  'bugreport/bugreport.ini'
			mv 'bugreport.ucs'                  'bugreport/english/bugreport.ucs'
			mv 'ati.txt'                        'drivers/ati.txt'
			mv 'nvidia.txt'                     'drivers/nvidia.txt'
			mv 'shader.txt'                     'drivers/shader.txt'
			mv 'spdx9_config.txt'               'drivers/spdx9_config.txt'
			mv 'engine.sga'                     'engine/engine.sga'
			mv 'engine.ucs'                     'engine/locale/english/engine.ucs'
			mv 'enginloc.sga'                   'engine/locale/english/enginloc.sga'
			mv 'dow_intro.avi'                  'engine/movies/dow_intro.avi'
			mv 'dow_intro.lua'                  'engine/movies/dow_intro.lua'
			mv 'dxp_relic_intro.avi'            'engine/movies/dxp_relic_intro.avi'
			mv 'dxp_relic_intro.lua'            'engine/movies/dxp_relic_intro.lua'
			mv 'gotdata.sga'                    'graphicsoptions/gotdata.sga'
			mv 'graphicsoptionsutility.ucs'     'graphicsoptions/locale/english/graphicsoptionsutility.ucs'
			mv 'w40k.ucs'                       'w40k/locale/english/w40k.ucs'
			mv 'w40kdatasoundspeech.sga'        'w40k/locale/english/w40kdata-sound-speech.sga'
			mv 'w40kdatakeys.sga'               'w40k/locale/english/w40kdatakeys.sga'
			mv 'w40kdataloc.sga'                'w40k/locale/english/w40kdataloc.sga'
			mv 'w40kdata.sga'                   'w40k/w40kdata.sga'
			mv 'w40kdatasharedtexturesfull.sga' 'w40k/w40kdata-sharedtextures-full.sga'
			mv 'w40kdatasoundmed.sga'           'w40k/w40kdata-sound-med.sga'
			mv 'w40kdatawhmmedium.sga'          'w40k/w40kdata-whm-medium.sga'
			mv 'wxp.ucs'                        'wxp/locale/english/wxp.ucs'
			mv 'wxpdatasoundspeech.sga'         'wxp/locale/english/wxpdata-sound-speech.sga'
			mv 'wxpdatakeys.sga'                'wxp/locale/english/wxpdatakeys.sga'
			mv 'wxpdataloc.sga'                 'wxp/locale/english/wxpdataloc.sga'
			mv 'wxp_order.avi'                  'wxp/movies/wxp_order.avi'
			mv 'wxp_order.lua'                  'wxp/movies/wxp_order.lua'
			mv 'wxpdata.sga'                    'wxp/wxpdata.sga'
			mv 'wxpdatamusic.sga'               'wxp/wxpdata-music.sga'
			mv 'wxpdatasharedtexturesfull.sga'  'wxp/wxpdata-sharedtextures-full.sga'
			mv 'wxpdatasoundmed.sga'            'wxp/wxpdata-sound-med.sga'
			mv 'wxpdatawhmmedium.sga'           'wxp/wxpdata-whm-medium.sga'
		;;
	esac

	## Prevent mouse cursor flickering.
	sed_pattern='allowhwcursor 1'
	sed_replacement='allowhwcursor 0'
	sed_expression="s/${sed_pattern}/${sed_replacement}/"
	sed --in-place --expression="$sed_expression" 'drivers/spdx9_config.txt'

	## Winter Assault (free demo) - Generate a required configuration file.
	case "$(current_archive)" in
		('ARCHIVE_BASE_WA_DEMO_'*)
			config_file="${PLAYIT_WORKDIR}/gamedata/regions.ini"
			cat > "$config_file" <<- 'EOF'
			[mods]
			wxp=english
			[global]
			lang=english
			EOF
			unix2dos --quiet "$config_file"
		;;
	esac
)


# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
