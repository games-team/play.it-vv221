#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Impression Games city builders:
# - Zeus: Master of Olympus
# - Emperor: Rise of the Middle Kingdom
###

script_version=20241226.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_ZEUS='zeus-master-of-olympus'
GAME_NAME_ZEUS='Zeus: Master of Olympus'

GAME_ID_EMPEROR='emperor-rise-of-the-middle-kingdom'
GAME_NAME_EMPEROR='Emperor: Rise of the Middle Kingdom'

# Game archives

## Zeus

ARCHIVE_BASE_ZEUS_0_NAME='setup_zeus_and_poseidon_2.1.0.10.exe'
ARCHIVE_BASE_ZEUS_0_MD5='f26f9ed5ecaa4e58fca64acb88255107'
ARCHIVE_BASE_ZEUS_0_TYPE='innosetup'
ARCHIVE_BASE_ZEUS_0_SIZE='800000'
ARCHIVE_BASE_ZEUS_0_VERSION='2.1-gog2.1.0.10'
ARCHIVE_BASE_ZEUS_0_URL='https://www.gog.com/game/zeus_poseidon'

## Emperor

ARCHIVE_BASE_EMPEROR_0_NAME='setup_emperor_rise_of_the_middle_kingdom_2.0.0.2.exe'
ARCHIVE_BASE_EMPEROR_0_MD5='5e50e84c028a85eafe5dd5f2aa277fea'
ARCHIVE_BASE_EMPEROR_0_TYPE='innosetup'
ARCHIVE_BASE_EMPEROR_0_SIZE='820000'
ARCHIVE_BASE_EMPEROR_0_VERSION='1.0.1.0-gog2.0.0.2'
ARCHIVE_BASE_EMPEROR_0_URL='https://www.gog.com/game/emperor_rise_of_the_middle_kingdom'


CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.asi
*.dll
*.exe
*.ini
*.m3d'
CONTENT_GAME_DATA_FILES='
adventures
audio
binks
campaigns
cities
data
model
res
dragon.ico
poseidon.ico
zeus.ico
*.eng
*.inf'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'
CONTENT_DOC_DATA_FILES='
*readme.txt
*editor*.txt
*.pdf'

# Applications

USER_PERSISTENT_DIRECTORIES='
save'
USER_PERSISTENT_FILES='
*.ini'

## With the default OpenGL renderer, text boxes are not displayed (WINE 9.0).
WINE_DIRECT3D_RENDERER='wined3d/gdi'
## The game window fails to render anything unless the game runs in a WINE virtual desktop (WINE 9.0).
WINE_VIRTUAL_DESKTOP='auto'
## Disable CSMT to avoid degraded performances
## TODO: Check if it still required with current WINE builds
WINE_WINETRICKS_VERBS='csmt=off'

APPLICATIONS_LIST='APP_MAIN'

## Zeus

APP_MAIN_EXE_ZEUS='zeus.exe'
APP_MAIN_ICON_ZEUS='poseidon.ico'

## Emperor

APPLICATIONS_LIST_EMPEROR="$APPLICATIONS_LIST APP_EDIT"

APP_MAIN_EXE_EMPEROR='emperor.exe'
APP_MAIN_ICON_EMPEROR='dragon.ico'

APP_EDIT_ID="${GAME_ID_EMPEROR}-editor"
APP_EDIT_NAME="$GAME_NAME_EMPEROR - Editor"
APP_EDIT_EXE='emperoredit.exe'
APP_EDIT_ICON="$APP_MAIN_ICON_EMPEROR"


PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID_ZEUS="${GAME_ID_ZEUS}-data"
PKG_DATA_ID_EMPEROR="${GAME_ID_EMPEROR}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
