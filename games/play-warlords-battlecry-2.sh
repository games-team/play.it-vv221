#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warlords Battlecry 2
###

script_version=20241125.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warlords-battlecry-2'
GAME_NAME='Warlords Battlecry Ⅱ'

ARCHIVE_BASE_1_NAME='setup_warlords_battlecry_2_1.04_(30487).exe'
ARCHIVE_BASE_1_MD5='25cf7418f5b954b97fc0e8b6d32039f2'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='930000'
ARCHIVE_BASE_1_VERSION='1.04-gog30487'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/warlords_battlecry_2'

ARCHIVE_BASE_0_NAME='setup_warlords_battlecry2_2.0.0.4.exe'
ARCHIVE_BASE_0_MD5='baa54ca0285182d18d532abfcbb8769f'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='940000'
ARCHIVE_BASE_0_VERSION='1.04-gog2.0.0.4'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
terrain.cfg
binkw32.dll
cpuinf32.dll
mss32.dll
wetstd32.dll
battlecry ii.exe
*.asi
*.cfg
*.m3d
*.ini'
CONTENT_GAME_DATA_FILES='
campaignscenario
customai
customunitai
data
documentation
english
events
fonts
herodata
music
namingsets
scenario
soundfx
tutorial
video
war4gfx.xcg
war4int.xci
wbc.dat
*.xcr'
CONTENT_DOC_DATA_FILES='
*.pdf'

USER_PERSISTENT_FILES='
*.cfg
*.ini
*.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Warlords Battlecry II'
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='battlecry ii.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
