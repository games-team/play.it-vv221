#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Victor Vran
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='victor-vran'
GAME_NAME='Victor Vran'

ARCHIVE_BASE_0_NAME='victor_vran_2_07_20181005_24296.sh'
ARCHIVE_BASE_0_MD5='506f55f5521131e7ab69b656a3e55582'
ARCHIVE_BASE_0_SIZE='4721209'
ARCHIVE_BASE_0_VERSION='2.07.20181005-gog24296'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/victor_vran'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
VictorVranGOG'
CONTENT_GAME_DATA_FILES='
DLC
Local
Movies
Packs'

APP_MAIN_EXE='VictorVranGOG'
APP_MAIN_ICON='../support/icon.png'
## Work around a failure to launch due to not parsing the correct SSL configuration
APP_MAIN_PRERUN='
# Work around a failure to launch due to not parsing the correct SSL configuration
export OPENSSL_CONF=/etc/ssl
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl-gnutls.so.4
libdl.so.2
libgcc_s.so.1
libgpg-error.so.0
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXrender.so.1
libXt.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PRELOAD_HACKS_LIST='
HACK_NOATIME'

## LD_PRELOAD shim working around the engine expectation that files are owned by the current user
HACK_NOATIME_NAME='vv_noatime'
HACK_NOATIME_DESCRIPTION='LD_PRELOAD shim working around the engine expectation that files are owned by the current user'
HACK_NOATIME_PACKAGE='PKG_BIN'
HACK_NOATIME_SOURCE='
#define _GNU_SOURCE
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdarg.h>

typedef int (*orig_open_f_type)(const char *pathname, int flags, mode_t mode);

int open(const char *pathname, int flags, ...)
{
	va_list valist;
	mode_t mode;
	static orig_open_f_type orig_open = NULL;
	if (orig_open == NULL)
		orig_open = (orig_open_f_type)dlsym(RTLD_NEXT, "open");

	flags &= ~O_NOATIME;

	if (flags & (O_CREAT | O_TMPFILE)) {
		va_start(valist, flags);
		mode = va_arg(valist, mode_t);
		va_end(valist);
	}

	return orig_open(pathname, flags, mode);
}
'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Build and include the LD_PRELOAD shims

hacks_inclusion_default

# Include shipped libgcrypt.so.11 on Debian
# Use the system-provided libraries on Arch Linux and Gentoo

case "$(option_value 'package')" in
	('deb')
		CONTENT_LIBS_BIN_PATH="$(content_path_default)/i386/lib/i386-linux-gnu"
		CONTENT_LIBS_BIN_FILES='
		libgcrypt.so.11
		libgcrypt.so.11.7.0'
	;;
	(*)
		PKG_BIN_DEPENDENCIES_LIBRARIES="$(dependencies_list_native_libraries 'PKG_BIN')
		libgcrypt.so.11"
	;;
esac

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
