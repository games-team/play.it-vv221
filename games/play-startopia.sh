#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Startopia
###

script_version=20250123.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='startopia'
GAME_NAME='Startopia'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

## Archives

### Full game

ARCHIVE_BASE_0_NAME='setup_startopia_2.0.0.17.exe'
ARCHIVE_BASE_0_MD5='4fe8d194afc1012e136ed3e82f1de171'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='600000'
ARCHIVE_BASE_0_VERSION='1.01b-gog2.0.0.17'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/startopia'

### Free demo

ARCHIVE_BASE_DEMO_0_NAME='startopia_demo.exe'
ARCHIVE_BASE_DEMO_0_MD5='2e170f8c4700b2d271ac2a6fac23920a'
ARCHIVE_BASE_DEMO_0_EXTRACTOR='unzip'
ARCHIVE_BASE_DEMO_0_SIZE='149364'
ARCHIVE_BASE_DEMO_0_VERSION='2001.5.15.1-archive.org'
ARCHIVE_BASE_DEMO_0_URL='https://archive.org/details/startopia_demo'


CONTENT_PATH_DEFAULT='app'
CONTENT_PATH_DEFAULT_DEMO='.'
CONTENT_GAME_BIN_FILES='
binkw32.dll
startopia.exe'
CONTENT_GAME_L10N_FILES='
data/speech/english
text/english
startopia.ini'
CONTENT_GAME_DATA_FILES='
startopia.jpg
cardid.tom
data
intro
languageinis
missions'
CONTENT_DOC_DATA_FILES='
eula
weblinks
*.doc
*.html
*.pdf
*.rtf
*.txt'

USER_PERSISTENT_DIRECTORIES='
profiles'
USER_PERSISTENT_FILES='
startopia.ini'

WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='startopia.exe'
APP_MAIN_EXE_DEMO='StarTopia.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_DEMO="${GAME_ID_DEMO}-l10n"
PKG_L10N_DESCRIPTION='English localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Generate a minimal configuration file
game_path="C:\\$(game_id)"
intro_path="${game_path}\\intro\\"
drive_path_music="${game_path}\\data\\"
drive_path_sound="${game_path}\\data\\"
sound_path_voice="${game_path}\\data\\"
config_file="$(package_path 'PKG_L10N')$(path_game_data)/startopia.ini"
cat > "$config_file" <<- EOF
[Intro]
IntroPath=$intro_path
[Sound]
DrivePathMusic=$drive_path_music
DrivePathSound=$drive_path_sound
SoundPathVoice=$sound_path_voice
[Language]
TextLanguage=English
SpeechLanguage=English
EOF

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
