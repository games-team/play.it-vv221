#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 Phil Morrell
set -o errexit

###
# Deus Ex
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='deus-ex'
GAME_NAME='Deus Ex'

ARCHIVE_BASE_7_NAME='setup_deus_ex_goty_1.112fm(revision_1.6.3.0)_(51757).exe'
ARCHIVE_BASE_7_MD5='9dd2b837300bfa19c6b5b8fde5d38df6'
ARCHIVE_BASE_7_TYPE='innosetup'
ARCHIVE_BASE_7_SIZE='870816'
ARCHIVE_BASE_7_VERSION='1.112fm-gog51757'
ARCHIVE_BASE_7_URL='https://www.gog.com/game/deus_ex'

ARCHIVE_BASE_6_NAME='setup_deus_ex_goty_1.112fm(revision_1.6.1.0)_(45326).exe'
ARCHIVE_BASE_6_MD5='688495ac0f2e6f05f1b47bdc40cee198'
ARCHIVE_BASE_6_TYPE='innosetup'
ARCHIVE_BASE_6_SIZE='880000'
ARCHIVE_BASE_6_VERSION='1.112fm-gog45326'

ARCHIVE_BASE_5_NAME='setup_deus_ex_goty_1.112fm(revision_1.6.0.0)_(42784).exe'
ARCHIVE_BASE_5_MD5='0ff01014f9364c3487a5193f9ac30dc1'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_SIZE='880000'
ARCHIVE_BASE_5_VERSION='1.112fm-gog42784'

ARCHIVE_BASE_4_NAME='setup_deus_ex_goty_1.112fm(revision_1.5.0.0)_(35268).exe'
ARCHIVE_BASE_4_MD5='3c5693ff82d754d4fe0d6be14e5337dd'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_SIZE='880000'
ARCHIVE_BASE_4_VERSION='1.112fm-gog35268'

ARCHIVE_BASE_3_NAME='setup_deus_ex_goty_1.112fm_(revision_1.4.0.2)_nglide_fix_(34088).exe'
ARCHIVE_BASE_3_MD5='085d7ea792d002236999dfd3697b85de'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_SIZE='760000'
ARCHIVE_BASE_3_VERSION='1.112fm-gog34088'

ARCHIVE_BASE_2_NAME='setup_deus_ex_goty_1.112fm(revision_1.4.0.2)_(26650).exe'
ARCHIVE_BASE_2_MD5='ab165b74b26623ccee5bfd7b6f65f734'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='760000'
ARCHIVE_BASE_2_VERSION='1.112fm-gog26650'

ARCHIVE_BASE_1_NAME='setup_deus_ex_goty_1.112fm(revision_1.4.0.1.5)_(24946).exe'
ARCHIVE_BASE_1_MD5='daa330f1e7a427af64b952cd138cfc59'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='760000'
ARCHIVE_BASE_1_VERSION='1.112fm-gog24946'

ARCHIVE_BASE_0_NAME='setup_deus_ex_goty_1.112fm(revision_1.4)_(21273).exe'
ARCHIVE_BASE_0_MD5='9ec295ecad72e96fb7b9f0109dd90324'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='750000'
ARCHIVE_BASE_0_VERSION='1.112fm-gog21273'

ARCHIVE_BASE_OLDTEMPLATE_3_NAME='setup_deus_ex_goty_1.112fm(revision_1.3.1)_(17719).exe'
ARCHIVE_BASE_OLDTEMPLATE_3_MD5='92e9e6a33642f9e6c41cb24055df9b3c'
ARCHIVE_BASE_OLDTEMPLATE_3_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_3_SIZE='750000'
ARCHIVE_BASE_OLDTEMPLATE_3_VERSION='1.112fm-gog17719'

ARCHIVE_BASE_OLDTEMPLATE_2_NAME='setup_deus_ex_goty_1.112fm(revision_1.3.0.1)_(16231).exe'
ARCHIVE_BASE_OLDTEMPLATE_2_MD5='eaaf7c7c3052fbf71f5226e2d4495268'
ARCHIVE_BASE_OLDTEMPLATE_2_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_2_SIZE='750000'
ARCHIVE_BASE_OLDTEMPLATE_2_VERSION='1.112fm-gog16231'

ARCHIVE_BASE_OLDTEMPLATE_1_NAME='setup_deus_ex_goty_1.112fm(revision_1.2.2)_(15442).exe'
ARCHIVE_BASE_OLDTEMPLATE_1_MD5='573582142424ba1b5aba1f6727276450'
ARCHIVE_BASE_OLDTEMPLATE_1_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_1_SIZE='750000'
ARCHIVE_BASE_OLDTEMPLATE_1_VERSION='1.112fm-gog15442'

ARCHIVE_BASE_OLDTEMPLATE_0_NAME='setup_deus_ex_2.1.0.12.exe'
ARCHIVE_BASE_OLDTEMPLATE_0_MD5='cc2c6e43b2e8e67c7586bbab5ef492ee'
ARCHIVE_BASE_OLDTEMPLATE_0_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_0_SIZE='750000'
ARCHIVE_BASE_OLDTEMPLATE_0_VERSION='1.112fm-gog2.1.0.12'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_OLDTEMPLATE='app'
CONTENT_GAME_BIN_FILES='
system/*.dll
system/*.exe
system/*.ini
system/*.int'
CONTENT_GAME_DATA_FILES='
help
maps
music
sounds
textures
system/*.u'
CONTENT_DOC_DATA_FILES='
manual.pdf
system/*.txt'

USER_PERSISTENT_FILES='
system/*.ini
system/*.log'
USER_PERSISTENT_DIRECTORIES='
save'

## TODO: Check if the virtual desktop prevents the game from messing with the desktop gamma value on X.org.
WINE_VIRTUAL_DESKTOP='auto'
## Disable csmt, as it would cause performance issues with single CPU affinity.
WINE_WINETRICKS_VERBS='csmt=off'

APP_MAIN_EXE='system/deusex.exe'
## Run the game binary from its parent directory.
APP_MAIN_PRERUN='
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Set OpenGL as the default rendering engine

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
sed
unix2dos"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set OpenGL as the default rendering engine.
	ini_file='system/deusex.ini'
	ini_field='GameRenderDevice'
	ini_value='OpenGLDrv.OpenGLRenderDevice'
	sed_expression="s/^${ini_field}=.*$/${ini_field}=${ini_value}/"
	ini_field='FirstRun'
	ini_value='1100'
	sed_expression="${sed_expression};s/^${ini_field}=.*$/${ini_field}=${ini_value}/"
	dos2unix --quiet "$ini_file"
	sed --in-place --expression="$sed_expression" "$ini_file"
	unix2dos --quiet "$ini_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Set the CPU affinity to a single core, to work around a random crash on launch.
## cf. https://www.gamingonlinux.com/2020/02/the-sad-case-of-unreal-engine-1-on-mesa-and-linux-in-2020/page=2#r174041
game_exec_line() {
	cat <<- 'EOF'
	# Set the CPU affinity to a single core, to work around a random crash on launch
	# cf. https://www.gamingonlinux.com/2020/02/the-sad-case-of-unreal-engine-1-on-mesa-and-linux-in-2020/page=2#r174041
	taskset --cpu-list 0 $(wine_command) "$APP_EXE" "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
