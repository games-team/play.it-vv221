#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 macaron
set -o errexit

###
# Monkey Island 4
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='monkey-island-4'
GAME_NAME='Monkey Island 4: Escape from Monkey Island'

# Archives

## Monkey Island 4 (game installer)

ARCHIVE_BASE_EN_0_NAME='setup_escape_from_monkey_islandtm_1.1_(20987).exe'
ARCHIVE_BASE_EN_0_MD5='54978965b60294d5c1639b71c0a8159a'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_escape_from_monkey_islandtm_1.1_(20987)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='21bc4e362f73b76e6808649167ee9d20'
ARCHIVE_BASE_EN_0_SIZE='1300000'
ARCHIVE_BASE_EN_0_VERSION='1.1-gog20987'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/escape_from_monkey_island'

ARCHIVE_BASE_FR_0_NAME='setup_escape_from_monkey_islandtm_1.1_(french)_(20987).exe'
ARCHIVE_BASE_FR_0_MD5='5ca039d42d53ad7fe206b289abe15deb'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_escape_from_monkey_islandtm_1.1_(french)_(20987)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='c5bf233f09cca2a8e33d78d25cf58329'
ARCHIVE_BASE_FR_0_SIZE='1300000'
ARCHIVE_BASE_FR_0_VERSION='1.1-gog20987'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/escape_from_monkey_island'

## ScummVM patches

ARCHIVE_REQUIRED_PATCH_EN_NAME='MonkeyUpdate.exe'
ARCHIVE_REQUIRED_PATCH_EN_MD5='7c7dbd2349d49e382a2dea40bed448e0'
ARCHIVE_REQUIRED_PATCH_EN_URL='https://downloads.scummvm.org/frs/extras/patches/'

ARCHIVE_REQUIRED_PATCH_FR_NAME='MonkeyUpdate_FRA.exe'
ARCHIVE_REQUIRED_PATCH_FR_MD5='cc5ff3bb8f78a0eb4b8e0feb9cdd2e87'
ARCHIVE_REQUIRED_PATCH_FR_URL='https://downloads.scummvm.org/frs/extras/patches/'

# Archives content

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_L10N_FILES='
movies
art???.m4b
i9n.m4b
lip.m4b
voice???.m4b'
CONTENT_GAME_DATA_FILES='
textures
local.m4b
patch.m4b
sfx.m4b'
CONTENT_DOC_L10N_FILES='
*.pdf
*.txt'

## WINE

CONTENT_GAME_BIN_WINE_FILES='
*.asi
*.dll
*.exe
*.flt'
CONTENT_GAME0_BIN_WINE_PATH='__support/save'
CONTENT_GAME0_BIN_WINE_FILES='
saves'

# Launchers

## ScummVM

APP_SCUMMVM_SCUMMID='grim:monkey4'
APP_SCUMMVM_ICON='monkey4.exe'

## WINE

USER_PERSISTENT_DIRECTORIES='
saves'

WINE_VIRTUAL_DESKTOP='auto'

APP_WINE_EXE='monkey4.exe'

# Packages

PACKAGES_LIST='
PKG_BIN_SCUMMVM
PKG_BIN_WINE
PKG_L10N
PKG_DATA'

PKG_BIN_BASE_ID="$GAME_ID"
PKG_BIN_PROVIDES="
$PKG_BIN_BASE_ID"
## A dependency on the language-specific variant of the localization package is set,
## not on the common name provided by both variants of this package.
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

## ScummVM

PKG_BIN_SCUMMVM_BASE_ID="${PKG_BIN_BASE_ID}-scummvm"
PKG_BIN_SCUMMVM_ID_EN="${PKG_BIN_SCUMMVM_BASE_ID}-en"
PKG_BIN_SCUMMVM_ID_FR="${PKG_BIN_SCUMMVM_BASE_ID}-fr"
PKG_BIN_SCUMMVM_PROVIDES="$PKG_BIN_BASE_PROVIDES"
PKG_BIN_SCUMMVM_DESCRIPTION_EN='English version'
PKG_BIN_SCUMMVM_DESCRIPTION_FR='French version'
PKG_BIN_SCUMMVM_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"

## WINE

PKG_BIN_WINE_ARCH='32'
PKG_BIN_WINE_BASE_ID="${PKG_BIN_BASE_ID}-wine"
PKG_BIN_WINE_ID_EN="${PKG_BIN_WINE_BASE_ID}-en"
PKG_BIN_WINE_ID_FR="${PKG_BIN_WINE_BASE_ID}-fr"
PKG_BIN_WINE_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_WINE_DESCRIPTION_EN='English version'
PKG_BIN_WINE_DESCRIPTION_FR='French version'
PKG_BIN_WINE_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"

# WINE - Register install path

install_path="C:\\\\${GAME_ID}"
registry_dump_installpath_file='registry-dumps/install-path.reg'
registry_dump_installpath_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\LucasArts Entertainment Company LLC\Monkey4\Retail]
"Install path"="'"${install_path}"'"'
CONTENT_GAME_BIN_WINE_FILES="${CONTENT_GAME_BIN_WINE_FILES:-}
$registry_dump_installpath_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_installpath_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of required extra archives

case "$(current_archive)" in
	('ARCHIVE_BASE_EN_'*)
		archive_initialize_required \
			'ARCHIVE_PATCH' \
			'ARCHIVE_REQUIRED_PATCH_EN'
	;;
	('ARCHIVE_BASE_FR_'*)
		archive_initialize_required \
			'ARCHIVE_PATCH' \
			'ARCHIVE_REQUIRED_PATCH_FR'
	;;
esac

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete unwanted files.
	## TODO: Check if this can be dropped in favour of a more explicit list of files to include.
	rm --force --recursive \
		'__redist' \
		'commonappdata' \
		'install' \
		'tmp'

	## WINE - Register install path.
	mkdir --parents "$(dirname "$registry_dump_installpath_file")"
	printf '%s' "$registry_dump_installpath_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_installpath_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default
## ScummVM - Include required patch.
install -D --mode=644 \
	"$(archive_path 'ARCHIVE_PATCH')" \
	"$(package_path 'PKG_BIN_SCUMMVM')$(path_game_data)/$(archive_name 'ARCHIVE_PATCH')"

# Write launchers

launchers_generation 'PKG_BIN_SCUMMVM' 'APP_SCUMMVM'
launchers_generation 'PKG_BIN_WINE' 'APP_WINE'

# Build packages

packages_generation
printf '\n'
printf 'ScummVM:'
print_instructions 'PKG_DATA' 'PKG_L10N' 'PKG_BIN_SCUMMVM'
printf 'WINE:'
print_instructions 'PKG_DATA' 'PKG_L10N' 'PKG_BIN_WINE'

# Clean up

working_directory_cleanup

exit 0
