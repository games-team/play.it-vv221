#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Settlers 2
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-settlers-2'
GAME_NAME='The Settlers Ⅱ'

# Archives

## German version

ARCHIVE_BASE_DE_1_NAME='setup_the_settlers_2_gold_1.5.1_(german)_(30319).exe'
ARCHIVE_BASE_DE_1_MD5='c360aaabd05e99b0f0752e52dd105107'
ARCHIVE_BASE_DE_1_TYPE='innosetup'
ARCHIVE_BASE_DE_1_SIZE='358538'
ARCHIVE_BASE_DE_1_VERSION='1.5.1-gog30319'
ARCHIVE_BASE_DE_1_URL='https://www.gog.com/game/the_settlers_2_gold_edition'

ARCHIVE_BASE_DE_0_NAME='setup_settlers2_gold_german_2.1.0.17.exe'
ARCHIVE_BASE_DE_0_MD5='f87a8fded6de455af4e6a284b3c4ed5e'
ARCHIVE_BASE_DE_0_TYPE='innosetup'
ARCHIVE_BASE_DE_0_SIZE='370000'
ARCHIVE_BASE_DE_0_VERSION='1.5.1-gog2.1.0.17'

## English version

ARCHIVE_BASE_EN_1_NAME='setup_the_settlers_2_gold_1.5.1_(30319).exe'
ARCHIVE_BASE_EN_1_MD5='8381240ee580a298798b6afe863bac52'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_SIZE='359765'
ARCHIVE_BASE_EN_1_VERSION='1.5.1-gog30319'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/the_settlers_2_gold_edition'

ARCHIVE_BASE_EN_0_NAME='setup_settlers2_gold_2.0.0.14.exe'
ARCHIVE_BASE_EN_0_MD5='6f64b47b15f6ba5d43670504dd0bb229'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='370000'
ARCHIVE_BASE_EN_0_VERSION='1.5.1-gog2.0.0.14'

## French version

ARCHIVE_BASE_FR_1_NAME='setup_the_settlers_2_gold_1.5.1_(french)_(30319).exe'
ARCHIVE_BASE_FR_1_MD5='55a9d15f1260de5e711ea649120ece50'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_SIZE='395295'
ARCHIVE_BASE_FR_1_VERSION='1.5.1-gog30319'
ARCHIVE_BASE_FR_1_URL='https://www.gog.com/game/the_settlers_2_gold_edition'

ARCHIVE_BASE_FR_0_NAME='setup_settlers2_gold_french_2.1.0.16.exe'
ARCHIVE_BASE_FR_0_MD5='1eca72ca45d63e4390590d495657d213'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='410000'
ARCHIVE_BASE_FR_0_VERSION='1.5.1-gog2.1.0.16'


CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_DE_0='app'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_PATH_DEFAULT_FR_0='app'
CONTENT_GAME_COMMON_FILES='
DOS4GW.EXE
SETTLER2.VMC
SETTLERS2.GOG
SETTLERS2.INS
DATA/*.DAT
DATA/EDITRES.IDX
DATA/ANIMDAT
DATA/BOBS
DATA/CBOB
DATA/IO/*.DAT
DATA/IO/*.FNT
DATA/*.LST
DATA/MASKS
DATA/MBOB
DATA/MISSIONS/MIS_00*.RTX
DATA/MISSIONS/MIS_10*.RTX
DATA/SOUNDDAT/SNG
DATA/SOUNDDAT/SOUND.LST
DATA/TEXTURES
DRIVERS/*.AD
DRIVERS/*.DIG
DRIVERS/DIG.INI
DRIVERS/*.EXE
DRIVERS/*.LST
DRIVERS/*.MDI
DRIVERS/*.OPL
GFX/PALETTE
GFX/PICS2
GFX/PICS/INSTALL.LBM
GFX/PICS/MISSION
GFX/PICS/SETUP013.LBM
GFX/PICS/SETUP015.LBM
GFX/PICS/SETUP666.LBM
GFX/PICS/SETUP667.LBM
GFX/PICS/SETUP801.LBM
GFX/PICS/SETUP802.LBM
GFX/PICS/SETUP803.LBM
GFX/PICS/SETUP804.LBM
GFX/PICS/SETUP805.LBM
GFX/PICS/SETUP806.LBM
GFX/PICS/SETUP810.LBM
GFX/PICS/SETUP811.LBM
GFX/PICS/SETUP895.LBM
GFX/PICS/SETUP896.LBM
GFX/PICS/SETUP899.LBM
GFX/PICS/SETUP990.LBM
GFX/PICS/WORLD.LBM
GFX/PICS/WORLDMSK.LBM
GFX/TEXTURES
VIDEO/SMACKPLY.EXE'
CONTENT_GAME_MAIN_FILES='
DATA/RESOURCE.IDX
DATA/IO/*.IDX
DATA/MAPS*
DATA/MISSIONS/MIS_0100.RTX
DATA/ONLINE
DATA/TXT*
DRIVERS/MDI.INI
GFX/PICS/SETUP000.LBM
GFX/PICS/SETUP010.LBM
GFX/PICS/SETUP011.LBM
GFX/PICS/SETUP012.LBM
GFX/PICS/SETUP014.LBM
GFX/PICS/SETUP897.LBM
GFX/PICS/SETUP898.LBM
GFX/PICS/SETUP900.LBM
GFX/PICS/SETUP901.LBM
GFX/PICS/SETUP996.LBM
GFX/PICS/SETUP997.LBM
GFX/PICS/SETUP998.LBM
SAVE/MISSION.DAT
VIDEO/*.SMK
*.EXE
*.INI
*.SCR'
CONTENT_GAME0_MAIN_RELATIVE_PATH='__SUPPORT/SAVE'
CONTENT_GAME0_MAIN_FILES='
SAVE/MISSION.DAT'
CONTENT_DOC_MAIN_FILES='
EULA
*.TXT'

USER_PERSISTENT_FILES='
SETUP.INI'
USER_PERSISTENT_DIRECTORIES='
DATA
GFX
SAVE
WORLDS'

GAME_IMAGE='SETTLERS2.INS'
GAME_IMAGE_TYPE='iso'

APP_MAIN_EXE='S2.EXE'
APP_MAIN_ICON='APP/GOGGAME-1207658786.ICO'
## Play the intro movie before starting the game.
APP_MAIN_DOSBOX_PRERUN='
@VIDEO\SMACKPLY VIDEO\INTRO.SMK'

APP_EDITOR_ID="${GAME_ID}-editor"
APP_EDITOR_EXE='S2EDIT.EXE'
APP_EDITOR_NAME="$GAME_NAME - Editor"
APP_EDITOR_ICON="$APP_MAIN_ICON"

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_EXE='SETUP.EXE'
APP_SETUP_NAME="$GAME_NAME - Setup"
APP_SETUP_CAT='Settings'
APP_SETUP_ICON="$APP_MAIN_ICON"

PACKAGES_LIST='
PKG_COMMON
PKG_MAIN'

PKG_COMMON_ID="${GAME_ID}-common"
PKG_COMMON_DESCRIPTION='common data'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_DE="${GAME_ID}-de"
PKG_MAIN_ID_EN="${GAME_ID}-en"
PKG_MAIN_ID_FR="${GAME_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"
PKG_MAIN_DESCRIPTION_DE='German version'
PKG_MAIN_DESCRIPTION_EN='English version'
PKG_MAIN_DESCRIPTION_FR='French version'
PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_COMMON'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to uppercase.
	toupper .

	## Enforce disk image name.
	if [ -e 'SETTLERS2.INST' ]; then
		mv 'SETTLERS2.INST' "$GAME_IMAGE"
	fi

	## Ensure case consistency in disk image table of contents.
	sed_pattern='settlers2.gog'
	sed_replacement='SETTLERS2.GOG'
	sed_expression="s/${sed_pattern}/${sed_replacement}/i"
	sed --in-place --expression="$sed_expression" "$GAME_IMAGE"

	## Enforce icon name and path.
	for icon_path in \
		'GOGGAME-1207658786.ICO' \
		'GFW_HIGH.ICO'
	do
		if [ -e "$icon_path" ]; then
			mkdir --parents 'APP'
			mv "$icon_path" "$(icon_path 'APP_MAIN_ICON')"
		fi
	done
)

# Include game data

content_inclusion_icons 'PKG_COMMON'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
