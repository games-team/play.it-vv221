#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tyranny expansions:
# - Coat of Arms
# - Portrait Pack
# - Tales from the Tiers
# - Bastard's Wound
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='tyranny'
GAME_NAME='Tyranny'

EXPANSION_ID_COATOFARMS='coat-of-arms'
EXPANSION_NAME_COATOFARMS='Coat of Arms'

EXPANSION_ID_PORTRAIT='portrait-pack'
EXPANSION_NAME_PORTRAIT='Portrait Pack'

EXPANSION_ID_TALES='tales-from-the-tiers'
EXPANSION_NAME_TALES='Tales from the Tiers'

EXPANSION_ID_WOUND='bastards-wound'
EXPANSION_NAME_WOUND='Bastardʼs Wound'

# Archives

## Coat of Arms

ARCHIVE_BASE_COATOFARMS_0_NAME='tyranny_coat_of_arms_dlc_en_1_0_14773.sh'
ARCHIVE_BASE_COATOFARMS_0_MD5='ba1403ce1f2535a00ae137c19204459f'
ARCHIVE_BASE_COATOFARMS_0_SIZE='1342'
ARCHIVE_BASE_COATOFARMS_0_VERSION='1.0-gog14773'
ARCHIVE_BASE_COATOFARMS_0_URL='https://www.gog.com/game/tyranny_overlord_edition_upgrade'

## Portrait Pack

ARCHIVE_BASE_PORTRAIT_0_NAME='tyranny_portrait_pack_dlc_en_1_0_14773.sh'
ARCHIVE_BASE_PORTRAIT_0_MD5='165e1cf02119a0b2ddb4d8bfa8b6819d'
ARCHIVE_BASE_PORTRAIT_0_SIZE='4150'
ARCHIVE_BASE_PORTRAIT_0_VERSION='1.0-gog14773'
ARCHIVE_BASE_PORTRAIT_0_URL='https://www.gog.com/game/tyranny_portrait_pack'

## Tales from the Tiers

ARCHIVE_BASE_TALES_0_NAME='tyranny_tales_from_the_tiers_dlc_en_1_2_1_0158_15398.sh'
ARCHIVE_BASE_TALES_0_MD5='08676e535375b162e2fa32b1bc4488a3'
ARCHIVE_BASE_TALES_0_SIZE='4386'
ARCHIVE_BASE_TALES_0_VERSION='1.2.1.0158-gog15398'
ARCHIVE_BASE_TALES_0_URL='https://www.gog.com/game/tyranny_tales_from_the_tiers'

## Bastard's Wound

ARCHIVE_BASE_WOUND_0_NAME='tyranny_bastard_s_wound_dlc_en_1_2_1_0158_15398.sh'
ARCHIVE_BASE_WOUND_0_MD5='dbd2b51a410db2357b2e144f44139c19'
ARCHIVE_BASE_WOUND_0_SIZE='1159755'
ARCHIVE_BASE_WOUND_0_VERSION='1.2.1.0158-gog15398'
ARCHIVE_BASE_WOUND_0_URL='https://www.gog.com/game/tyranny_bastards_wound'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
Tyranny_Data'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Bastard's Wound - Delete duplicated files already provided by the base game
	case "$(current_archive)" in
		('ARCHIVE_BASE_WOUND_'*)
			rm \
				'Tyranny_Data/bundles/st_vx1_ar_0901_bastardswound_ent_01.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0902_bastardswound_ent_02.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0903_bastardswound_central_01.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0904_bastardswound_farm_01.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0905_bastardswound_water_01.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0906_bastardswound_interior_01.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0907_bastardswound_interior_02.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0908_bastardswound_oldwall_int_01.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0909_bastardswound_oldwall_int_02.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0910_bastardswound_oldwall_int_03.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0911_southhaven.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_0912_burnedvillage.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1001_merchant_cart.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1002_wilderness_camp.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1003_torn_strand.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1004_wilderness_home.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1005_burned_home.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1006_military_fort.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1007_merchant_nocart.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1101_sentinel_stand_outskirts.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1102_edgering_ruins_revisit.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1103_workshop.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1201_berry_field.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1202_lantry_house.unity3d' \
				'Tyranny_Data/bundles/st_vx1_ar_1203_lantry_house_int.unity3d' \
				'Tyranny_Data/bundles/vx1_characters.unity3d' \
				'Tyranny_Data/bundles/vx1_items.unity3d' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_bs_unbroken_soldier.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_catorius.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_essa.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_krokus.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_krokus_patrol.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_merchant_contact.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_unbroken_captain.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_cv_verse.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_10_verse/vx1_10_msv_neratintro.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_11_barik/vx1_11_cv_barik.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_11_barik/vx1_11_cv_carelessspark.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_11_barik/vx1_11_cv_lohara.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_11_barik/vx1_11_cv_lycentia.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_11_barik/vx1_11_cv_zdenya.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_ambush.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_berriesfound.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_berrytrip_chiasmus.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_berrytrip_lexeme.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_berrytrip_nerat.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_berrytrip_roundtwo.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_chronicle_farm.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_chronicle_oldwalls_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_chronicle_oldwalls_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_chronicle_oldwalls_03.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_lantryact2complete.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_lantryact3start.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_lantrylexeme.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_lantryshatteredbastionchat.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_questsend.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_cv_stashedchronicle.conversation' \
				'Tyranny_Data/data/design/conversations/vx1_12_lantry/vx1_12_poi_lantryshouse.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_00_cv_act3_companion_interjection.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_burnedvillage_hordeargue.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_companion_interjections.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_denizens_jaspos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_denizens_wagstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_farmhands.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_fatebinder_leaving.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_gatekeeper_beast01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_gatekeeper_beast02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_hyrax.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_intro_runningaway.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_lower_horde_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_lower_horde_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_lullaby_adds_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_lullaby_adds_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_lullaby_adds_03.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_mercenaries_jaspos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_mercenaries_wagstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_partii_sleepless.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_refugee_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_refugee_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_sleepless.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_southhaven_playercaught.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_southhaven_villagers.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_upper_chanter.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_upper_fury.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_woundkin.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_bs_woundkin_cubs.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_argaen.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_basila.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_beast_gatekeeper.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_callia_stabbed.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_capteron.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_cassandra.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_contusion.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_crasscallia.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_cyril.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_ebstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_eisly.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_elyane.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_erasmus.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_execution_start.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_final_confrontation.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_final_decision.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_final_leader_jaspos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_final_leader_player.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_final_leader_reeftalon.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_final_leader_wagstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_gloomofdawn.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_gwyneth.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_hyacine.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_hyrax.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_insipidmoniker.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_jaspos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_killsinshadow_mural_epiphany.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_kleitos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_lexeme.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_lohara.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_lullaby_fight_intro.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_lunet.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_melitta.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_mell.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_merchant_anaxios.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_merchant_eirena.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_merchant_elegy.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_merchant_neread.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_merchant_pelagia.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_needletoe.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_needletoe_fledreaction.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_nikodemus.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oceansfury.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_barrier.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_dead_woundkin.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_huntingparty_jaspos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_huntingparty_wagstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mercenaries_jaspos.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mercenaries_wagstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mural_fractured_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mural_fractured_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mural_fractured_03.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mural_hollow_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mural_hollow_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_mural_hollow_03.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_01.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_02.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_03.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_04.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_05.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_06.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_07.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_oldwalls_sleepless_08.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_phoibe.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_ragwort.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_ratvek_the_butcher.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_reeftalon.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_reeftalon_firstmeeting.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_rostomlenk.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_sirin_cairnaltar.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_sirin_foundparchment.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_sirin_haseverything.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_sirin_librarian.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_skinner.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_telesophia.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_tonves.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_tycho.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_ulantis.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_wagstaff.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_cv_warbler.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_msv_lexemeintromissive.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_beaconpuzzlecomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_bloodfarmcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_farmcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_friendlylullabycomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_huntingpartycomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_lullabybattlecomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_muralcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_oldwallsfloodingcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_sirin_entrance.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_sirin_shatteredbastion.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_sleeplesscomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_teleporterintocomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_wardens_key.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_wardens_key_used.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundbeastdencomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundcentralcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundfirstarrival.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundjasposcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundslaughtercomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundslaughterkeystone.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_poi_woundwagstaffcomments.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_tree_contusion.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_tree_needletoe.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_09_tree_ragwort.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_bs_burnedvillage_hordeargue.conversation' \
				'Tyranny_Data/data/design/conversations/vx1/vx1_debug_cv_testiclees.conversation' \
				'Tyranny_Data/data/design/quests/vx1_10_verse/vx1_10_qst_verse_act2.quest' \
				'Tyranny_Data/data/design/quests/vx1_10_verse/vx1_10_qst_verse_act3.quest' \
				'Tyranny_Data/data/design/quests/vx1_11_barik/vx1_11_qst_barik_act2.quest' \
				'Tyranny_Data/data/design/quests/vx1_11_barik/vx1_11_qst_barik_act3.quest' \
				'Tyranny_Data/data/design/quests/vx1_12_lantry/vx1_12_qst_lantry_act2.quest' \
				'Tyranny_Data/data/design/quests/vx1_12_lantry/vx1_12_qst_lantry_act3.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_bloodofthebeast.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_bonesofthebeast.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_cleansingthewound.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_fatebindersjudgement.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_gamblersruin.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_hiddentruths.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_historyoftheoldwallspartii.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_historyoftheoldwallsparti.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_knappingonthejob.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_midnightwanderer.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_theoneswhogotaway.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_tidetogether.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_waterintoxication.quest' \
				'Tyranny_Data/data/design/quests/vx1/vx1_qst_whatliesbeyond.quest'
		;;
	esac
)

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
