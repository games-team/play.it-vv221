#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Caesar 3
###

script_version=20250112.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='caesar-3'
GAME_NAME='Cæsar Ⅲ'

# Archives

## Base game installer

ARCHIVE_BASE_1_NAME='setup_caesartm_3_1.0.1.0_(76354).exe'
ARCHIVE_BASE_1_MD5='6a5ee051d469f1b3eea42d84e483b066'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='650439'
ARCHIVE_BASE_1_VERSION='1.0.1.0-gog76354'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/caesar_3'

ARCHIVE_BASE_0_NAME='setup_caesar3_2.0.0.9.exe'
ARCHIVE_BASE_0_MD5='2ee16fab54493e1c2a69122fd2e56635'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='550000'
ARCHIVE_BASE_0_VERSION='1.1-gog2.0.0.9'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
555
smk
wavs
mission1.pak
c3_model.txt
*.555
*.emp
*.eng
*.map
*.sg2'
CONTENT_DOC_MAIN_FILES='
readme.txt
*.pdf'

USER_PERSISTENT_FILES='
c3_model.txt
status.txt
*.ini
*.sav'

APP_MAIN_TYPE='custom'
APP_MAIN_ICON='c3.exe'

PKG_MAIN_DEPENDENCIES_COMMANDS='
julius'
## Ensure easy upgrades from packages generated with pre-20250112.1 game scripts
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
caesar-3-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

custom_launcher() {
	local application
	application="$1"

	launcher_headers

	# Set the paths that should be available to the generated launcher
	launcher_init_paths "$application"

	# Generate the game prefix
	prefix_generate_links_farm
	launcher_prefix_symlinks_build

	# Set up the paths diversion to persistent storage
	persistent_storage_initialization
	persistent_storage_common
	persistent_path_diversion
	persistent_storage_update_directories
	persistent_storage_update_files

	native_launcher_run "$application"

	# Update persistent storage with files from the current prefix
	persistent_storage_update_files_from_prefix

	launcher_exit
}
native_launcher_binary_copy() { return 0; }
case "$(option_value 'package')" in
	('gentoo'|'egentoo')
		game_exec_line() {
			cat <<- 'EOF'
			julius-game "$@"
			EOF
		}
	;;
	(*)
		game_exec_line() {
			cat <<- 'EOF'
			julius "$@"
			EOF
		}
	;;
esac
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
