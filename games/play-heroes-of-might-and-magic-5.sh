#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroes of Might and Magic 5:
# - Heroes of Might and Magic 5 (base game) + Hammer of Fate
# - Tribes of the East stand-alone expansion
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='heroes-of-might-and-magic-5'
GAME_NAME='Heroes of Might and Magic Ⅴ'

GAME_ID_TOTE="${GAME_ID}-tribes-of-the-east"
GAME_NAME_TOTE="${GAME_NAME} - Tribes of the East"

# Archives

## Heroes of Might and Magic 5 (base game) + Hammer of Fate

ARCHIVE_BASE_EN_1_NAME='setup_heroes_of_might_and_magic_v_2.1_v2_(28567).exe'
ARCHIVE_BASE_EN_1_MD5='657775b4eb545150f5895e61e67eda73'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_PART1_NAME='setup_heroes_of_might_and_magic_v_2.1_v2_(28567)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='bb4dd38f472fd94f82aa22cb256f4b9c'
ARCHIVE_BASE_EN_1_SIZE='2600000'
ARCHIVE_BASE_EN_1_VERSION='2.1-gog28567'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_5_bundle'

ARCHIVE_BASE_FR_1_NAME='setup_heroes_of_might_and_magic_v_2.1_v2_(french)_(28567).exe'
ARCHIVE_BASE_FR_1_MD5='78e860af17d9ce220d8c60c1e594cf40'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_PART1_NAME='setup_heroes_of_might_and_magic_v_2.1_v2_(french)_(28567)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='9e8017cc5d84231bf8eb9c8c757631f8'
ARCHIVE_BASE_FR_1_SIZE='2600000'
ARCHIVE_BASE_FR_1_VERSION='2.1-gog28567'
ARCHIVE_BASE_FR_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_5_bundle'

ARCHIVE_BASE_EN_0_NAME='setup_heroes_of_might_and_magic_v_2.1_(25025).exe'
ARCHIVE_BASE_EN_0_MD5='6e36b7fb9f1e8362326688d383e4bdb9'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_heroes_of_might_and_magic_v_2.1_(25025)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='3e38f48f450f58833728cd73e9266d2d'
ARCHIVE_BASE_EN_0_SIZE='2600000'
ARCHIVE_BASE_EN_0_VERSION='2.1-gog25025'

ARCHIVE_BASE_FR_0_NAME='setup_heroes_of_might_and_magic_v_2.1_(french)_(25025).exe'
ARCHIVE_BASE_FR_0_MD5='b9e278ee60d574b89068479a4e6c84c1'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_heroes_of_might_and_magic_v_2.1_(french)_(25025)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='57ca61178fca9ed2e50a5dc667f6d565'
ARCHIVE_BASE_FR_0_SIZE='2600000'
ARCHIVE_BASE_FR_0_VERSION='2.1-gog25025'

## Tribes of the East stand-alone expansion

ARCHIVE_BASE_TOTE_EN_1_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_v2_(28569).exe'
ARCHIVE_BASE_TOTE_EN_1_MD5='9593ad538a39638bacb4d7ef45368ce2'
ARCHIVE_BASE_TOTE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_TOTE_EN_1_PART1_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_v2_(28569)-1.bin'
ARCHIVE_BASE_TOTE_EN_1_PART1_MD5='8e03271dc4aff5834110664b5d6eefde'
ARCHIVE_BASE_TOTE_EN_1_SIZE='2300000'
ARCHIVE_BASE_TOTE_EN_1_VERSION='3.1-gog28569'
ARCHIVE_BASE_TOTE_EN_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_5_bundle'

ARCHIVE_BASE_TOTE_FR_1_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_v2_(french)_(28569).exe'
ARCHIVE_BASE_TOTE_FR_1_MD5='6a1a915180d1cee32e78419f6917be87'
ARCHIVE_BASE_TOTE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_TOTE_FR_1_PART1_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_v2_(french)_(28569)-1.bin'
ARCHIVE_BASE_TOTE_FR_1_PART1_MD5='f48ed6725126696bf3e67ce327db6263'
ARCHIVE_BASE_TOTE_FR_1_SIZE='2300000'
ARCHIVE_BASE_TOTE_FR_1_VERSION='3.1-gog28569'
ARCHIVE_BASE_TOTE_FR_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_5_bundle'

ARCHIVE_BASE_TOTE_EN_0_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_(25025).exe'
ARCHIVE_BASE_TOTE_EN_0_MD5='3096f296d5d8b6cb0b4ab479fc06474b'
ARCHIVE_BASE_TOTE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_TOTE_EN_0_PART1_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_(25025)-1.bin'
ARCHIVE_BASE_TOTE_EN_0_PART1_MD5='5f4840b0105bd6b4228ff9b707bc0434'
ARCHIVE_BASE_TOTE_EN_0_SIZE='2300000'
ARCHIVE_BASE_TOTE_EN_0_VERSION='3.1-gog25025'

ARCHIVE_BASE_TOTE_FR_0_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_(french)_(25025).exe'
ARCHIVE_BASE_TOTE_FR_0_MD5='a2b5d18f34d3fa1a760de4fa63aa3819'
ARCHIVE_BASE_TOTE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_TOTE_FR_0_PART1_NAME='setup_heroes_of_might_and_magic_v_-_tribes_of_the_east_3.1_(french)_(25025)-1.bin'
ARCHIVE_BASE_TOTE_FR_0_PART1_MD5='08a5ec9aaf674235db4d96072bf373fc'
ARCHIVE_BASE_TOTE_FR_0_SIZE='2300000'
ARCHIVE_BASE_TOTE_FR_0_VERSION='3.1-gog25025'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bin
bina1
bindm
fandocuments/*.exe'
CONTENT_GAME_L10N_FILES='
music/cs/death-berein.ogg
music/cs/death-nico.ogg
music/cs/heart-griffin.ogg
music/cs/isabel-trap.ogg
music/cs/nico-vampire.ogg
music/cs/ritual-isabel.ogg
video/intro.ogg
video/outro.ogg
data*/*sound.pak
data*/*texts.pak'
CONTENT_GAME_DATA_FILES='
customcontentdm
duelpresets
editor
hwcursors
music
profiles
video
data*
*.bmp'
CONTENT_DOC_L10N_FILES='
editor documentation
*.pdf
*.txt'

# Applications

APPLICATIONS_LIST='
APP_MAIN
APP_HOF'
APPLICATIONS_LIST_TOTE='
APP_MAIN
APP_DM
APP_SKILLS'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Heroes of Might and Magic V'
WINE_PERSISTENT_DIRECTORIES_TOTE='
users/${USER}/Documents/My Games/Heroes of Might and Magic V - Tribes of the East'

APP_MAIN_EXE='bin/h5_game.exe'

## Hammer of Fate

APP_HOF_ID="${GAME_ID}-hammers-of-fate"
APP_HOF_NAME="$GAME_NAME - Hammers of Fate"
APP_HOF_EXE='bina1/h5_game.exe'

## Tribes of the East

APP_DM_ID="${GAME_ID_TOTE}-dark-messiah"
APP_DM_NAME="$GAME_NAME_TOTE - Dark Messiah"
APP_DM_EXE='bindm/h5_game.exe'
APP_DM_ICON_WRESTOOL_OPTIONS='--type=14 --name=101'

APP_SKILLS_ID="${GAME_ID_TOTE}-skill-wheel"
APP_SKILLS_NAME="$GAME_NAME_TOTE - SkillWheel"
APP_SKILLS_EXE='fandocuments/skillwheel.exe'
APP_SKILLS_ICON_WRESTOOL_OPTIONS='--type=14 --name=200'


PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_BASE_ID_TOTE="${GAME_ID_TOTE}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_ID_TOTE_EN="${PKG_L10N_BASE_ID_TOTE}-en"
PKG_L10N_ID_TOTE_FR="${PKG_L10N_BASE_ID_TOTE}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_PROVIDES_TOTE="
$PKG_L10N_BASE_ID_TOTE"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'
PKG_L10N_DESCRIPTION_TOTE_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_TOTE_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_TOTE="${GAME_ID_TOTE}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	local application
	application="$1"

	local application_exe
	application_exe=$(application_exe "$application")
	cat <<- EOF
	cd "$(dirname "$application_exe")"
	\$(wine_command) "$(basename "$application_exe")" "\$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
