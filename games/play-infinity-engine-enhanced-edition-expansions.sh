#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Dawnmist
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Infinity Engine Enhanced Edition expansions:
# - Baldur's Gate 1 Enhanced Edition - Siege of Dragonspear
# - Baldur's Gate 1 Enhanced Edition - Faces of Good and Evil
###

script_version=20241226.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_DRAGONSPEAR='baldurs-gate-1-enhanced-edition'
GAME_NAME_DRAGONSPEAR='Baldurʼs Gate Enhanced Edition'

GAME_ID_FACES='baldurs-gate-1-enhanced-edition'
GAME_NAME_FACES='Baldurʼs Gate Enhanced Edition'

EXPANSION_ID_DRAGONSPEAR='siege-of-dragonspear'
EXPANSION_NAME_DRAGONSPEAR='Siege of Dragonspear'

EXPANSION_ID_FACES='faces-of-good-and-evil'
EXPANSION_NAME_FACES='Faces of Good and Evil'

# Archives

## Baldur's Gate 1 Enhanced Edition - Siege of Dragonspear

ARCHIVE_BASE_DRAGONSPEAR_1_NAME='baldur_s_gate_siege_of_dragonspear_2_6_6_0_47291.sh'
ARCHIVE_BASE_DRAGONSPEAR_1_MD5='36d275f6822b3cd2946ca606c0ebdb67'
ARCHIVE_BASE_DRAGONSPEAR_1_SIZE='1900000'
ARCHIVE_BASE_DRAGONSPEAR_1_VERSION='2.6.6.0-gog47291'
ARCHIVE_BASE_DRAGONSPEAR_1_URL='https://www.gog.com/game/baldurs_gate_siege_of_dragonspear'

ARCHIVE_BASE_DRAGONSPEAR_0_NAME='baldur_s_gate_siege_of_dragonspear_2_6_5_0_46477.sh'
ARCHIVE_BASE_DRAGONSPEAR_0_MD5='27970876d9252fcb3174df8201db3ca3'
ARCHIVE_BASE_DRAGONSPEAR_0_SIZE='1900000'
ARCHIVE_BASE_DRAGONSPEAR_0_VERSION='2.6.5.0-gog46477'

## Baldur's Gate 1 Enhanced Edition - Faces of Good and Evil

ARCHIVE_BASE_FACES_1_NAME='baldur_s_gate_faces_of_good_and_evil_2_6_6_0_47291.sh'
ARCHIVE_BASE_FACES_1_MD5='5bcc622e44bdf9b03af64f5fe0a83d38'
ARCHIVE_BASE_FACES_1_SIZE='2500'
ARCHIVE_BASE_FACES_1_VERSION='2.6.6.0-gog47291'
ARCHIVE_BASE_FACES_1_URL='https://www.gog.com/game/baldurs_gate_faces_of_good_and_evil'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES_DRAGONSPEAR='
sod-dlc.zip'
CONTENT_GAME_MAIN_FILES_FACES='
override/yanner2?.bmp
override/M_YANN2.lua'

PKG_PARENT_ID_DRAGONSPEAR="$GAME_ID_DRAGONSPEAR"
PKG_PARENT_ID_FACES="$GAME_ID_FACES"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
