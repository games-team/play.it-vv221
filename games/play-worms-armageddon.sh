#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2020 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Hubert Ray
set -o errexit

###
# Worms: Armageddon
###

script_version=20241230.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='worms-armageddon'
GAME_NAME='Worms: Armageddon'

ARCHIVE_BASE_3_NAME='setup_worms_armageddon_gog-3.8.1_(43454).exe'
ARCHIVE_BASE_3_MD5='f84e60ba11363219c582a4ff65301692'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_SIZE='648070'
ARCHIVE_BASE_3_VERSION='3.8.1-gog43454'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/worms_armageddon'

ARCHIVE_BASE_2_NAME='setup_worms_armageddon_gog-2_(40354).exe'
ARCHIVE_BASE_2_MD5='db2087029ee8c069c9006ebeedc76bbf'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='650000'
ARCHIVE_BASE_2_VERSION='3.8-gog40354'

ARCHIVE_BASE_1_NAME='setup_worms_armageddon_gog-7_(40119).exe'
ARCHIVE_BASE_1_MD5='8e904d462327917452a47572a38b772a'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='660000'
ARCHIVE_BASE_1_VERSION='3.8-gog40119'

ARCHIVE_BASE_0_NAME='setup_worms_armageddon_2.0.0.2.exe'
ARCHIVE_BASE_0_MD5='7f0bb89729662ebe74b7c9c2cd97d1c8'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='570000'
ARCHIVE_BASE_0_VERSION='3.7.2.1-gog2.0.0.2'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
lfcmp10n.dll
ltfil10n.dll
lfbmp10n.dll
ltkrn10n.dll
lflmb10n.dll
lftga10n.dll
wa.exe
user/bankeditor.exe'
## TODO: Check if the Steam library is required
CONTENT_GAME0_BIN_FILES='
steam_api.dll'
CONTENT_GAME_DATA_FILES='
data
fesfx
graphics
tweaks
user'
CONTENT_GAME0_DATA_RELATIVE_PATH='__support'
CONTENT_GAME0_DATA_FILES='
save'
CONTENT_DOC_DATA_FILES='
wa_manual.pdf
worms armageddon update documentation.rtf'

USER_PERSISTENT_DIRECTORIES='
save
user'
USER_PERSISTENT_FILES='
graphics/font.bmp'

APP_MAIN_EXE='wa.exe'
## Create a required empty file prior to game run
APP_MAIN_PRERUN='
# Create a required empty file prior to game run
touch steam.dat
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
