#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Infinity Engine games:
# - Baldur's Gate 1
# - Baldur's Gate 2
# - Icewind Dale 1
# - Icewind Dale 2
# - Planescape: Torment
###

script_version=20241121.6

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_BG1='baldurs-gate-1'
GAME_NAME_BG1='Baldurʼs Gate'

GAME_ID_BG2='baldurs-gate-2'
GAME_NAME_BG2='Baldurʼs Gate Ⅱ'

GAME_ID_IWD1='icewind-dale-1'
GAME_NAME_IWD1='Icewind Dale'

GAME_ID_IWD2='icewind-dale-2'
GAME_NAME_IWD2='Icewind Dale Ⅱ'

GAME_ID_PST='planescape-torment'
GAME_NAME_PST='Planescape: Torment'

# Game archives

## Baldur's Gate 1

ARCHIVE_BASE_BG1_EN_1_NAME='baldur_s_gate_the_original_saga_gog_3_23532.sh'
ARCHIVE_BASE_BG1_EN_1_MD5='f1750a05b52a5c8bb4810f0dbdb92091'
ARCHIVE_BASE_BG1_EN_1_SIZE='3400000'
ARCHIVE_BASE_BG1_EN_1_VERSION='1.3.5521-gog23532'
ARCHIVE_BASE_BG1_EN_1_URL='https://www.gog.com/game/baldurs_gate_enhanced_edition'

ARCHIVE_BASE_BG1_FR_1_NAME='baldur_s_gate_the_original_saga_french_gog_3_23532.sh'
ARCHIVE_BASE_BG1_FR_1_MD5='09073e75602383c2c90d7c82436a8d91'
ARCHIVE_BASE_BG1_FR_1_VERSION='1.3.5521-gog23532'
ARCHIVE_BASE_BG1_FR_1_SIZE='3400000'
ARCHIVE_BASE_BG1_FR_1_URL='https://www.gog.com/game/baldurs_gate_enhanced_edition'

ARCHIVE_BASE_BG1_PL_1_NAME='baldur_s_gate_the_original_saga_polish_gog_3_23532.sh'
ARCHIVE_BASE_BG1_PL_1_MD5='9db5d4dd953e4bc7b42fbb6d0680437a'
ARCHIVE_BASE_BG1_PL_1_SIZE='3400000'
ARCHIVE_BASE_BG1_PL_1_VERSION='1.3.5521-gog23532'
ARCHIVE_BASE_BG1_PL_1_URL='https://www.gog.com/game/baldurs_gate_enhanced_edition'

ARCHIVE_BASE_BG1_EN_0_NAME='gog_baldur_s_gate_the_original_saga_2.1.0.10.sh'
ARCHIVE_BASE_BG1_EN_0_MD5='6810388ef67960dded254db5750f9aa5'
ARCHIVE_BASE_BG1_EN_0_SIZE='3100000'
ARCHIVE_BASE_BG1_EN_0_VERSION='1.3.5521-gog2.1.0.10'

ARCHIVE_BASE_BG1_FR_0_NAME='gog_baldur_s_gate_the_original_saga_french_2.1.0.10.sh'
ARCHIVE_BASE_BG1_FR_0_MD5='87ed67decb79e497b8c0ce9e0b16ac4c'
ARCHIVE_BASE_BG1_FR_0_SIZE='3100000'
ARCHIVE_BASE_BG1_FR_0_VERSION='1.3.5521-gog2.1.0.10'

## Baldur's Gate 2

ARCHIVE_BASE_BG2_EN_1_NAME='baldur_s_gate_2_complete_gog_3_23651.sh'
ARCHIVE_BASE_BG2_EN_1_MD5='030a61ce961ac88cd9506f1fd42135d6'
ARCHIVE_BASE_BG2_EN_1_SIZE='3400000'
ARCHIVE_BASE_BG2_EN_1_VERSION='2.5.26498-gog23651'
ARCHIVE_BASE_BG2_EN_1_URL='https://www.gog.com/game/baldurs_gate_2_enhanced_edition'

ARCHIVE_BASE_BG2_FR_1_NAME='baldur_s_gate_2_complete_french_gog_3_23651.sh'
ARCHIVE_BASE_BG2_FR_1_MD5='c72eb1b9bae7109de6a7005b3dc44e2c'
ARCHIVE_BASE_BG2_FR_1_SIZE='3400000'
ARCHIVE_BASE_BG2_FR_1_VERSION='2.5.26498-gog23651'
ARCHIVE_BASE_BG2_FR_1_URL='https://www.gog.com/game/baldurs_gate_2_enhanced_edition'

ARCHIVE_BASE_BG2_EN_0_NAME='gog_baldur_s_gate_2_complete_2.1.0.7.sh'
ARCHIVE_BASE_BG2_EN_0_MD5='e92161d7fc0a2eea234b2c93760c9cdb'
ARCHIVE_BASE_BG2_EN_0_SIZE='3000000'
ARCHIVE_BASE_BG2_EN_0_VERSION='2.5.26498-gog2.1.0.7'

ARCHIVE_BASE_BG2_FR_0_NAME='gog_baldur_s_gate_2_complete_french_2.1.0.7.sh'
ARCHIVE_BASE_BG2_FR_0_MD5='6551bda3d8c7330b7ad66842ac1d4ed4'
ARCHIVE_BASE_BG2_FR_0_SIZE='3000000'
ARCHIVE_BASE_BG2_FR_0_VERSION='2.5.26498-gog2.1.0.7'

## Icewind Dale 1

ARCHIVE_BASE_IWD1_0_NAME='setup_icewind_dale_complete_2.0.0.11.exe'
ARCHIVE_BASE_IWD1_0_MD5='b1395109232aac8d7f8455dad418b084'
ARCHIVE_BASE_IWD1_0_TYPE='innosetup'
ARCHIVE_BASE_IWD1_0_SIZE='2100000'
ARCHIVE_BASE_IWD1_0_VERSION='1.42.062714-gog2.0.0.11'
ARCHIVE_BASE_IWD1_0_URL='https://www.gog.com/game/icewind_dale_enhanced_edition'

## Icewind Dale 2

ARCHIVE_BASE_IWD2_EN_1_NAME='setup_icewind_dale_2_2.01_fixes_(77030).exe'
ARCHIVE_BASE_IWD2_EN_1_MD5='7579f564757a6d9261a29b599a11f335'
ARCHIVE_BASE_IWD2_EN_1_TYPE='innosetup'
ARCHIVE_BASE_IWD2_EN_1_PART1_NAME='setup_icewind_dale_2_2.01_fixes_(77030)-1.bin'
ARCHIVE_BASE_IWD2_EN_1_PART1_MD5='79a7e58ea11380bbbf7329a97d522e3f'
ARCHIVE_BASE_IWD2_EN_1_SIZE='1555964'
ARCHIVE_BASE_IWD2_EN_1_VERSION='2.01.101615-gog77030'
ARCHIVE_BASE_IWD2_EN_1_URL='https://www.gog.com/game/icewind_dale_2'

ARCHIVE_BASE_IWD2_FR_1_NAME='setup_icewind_dale_2_2.01_fixes_(french)_(77030).exe'
ARCHIVE_BASE_IWD2_FR_1_MD5='8b25fcc3e12641efd79d6c2780f3cc03'
ARCHIVE_BASE_IWD2_FR_1_TYPE='innosetup'
ARCHIVE_BASE_IWD2_FR_1_PART1_NAME='setup_icewind_dale_2_2.01_fixes_(french)_(77030)-1.bin'
ARCHIVE_BASE_IWD2_FR_1_PART1_MD5='0d25ffba9a37471185327d0beedd7aaf'
ARCHIVE_BASE_IWD2_FR_1_SIZE='1552369'
ARCHIVE_BASE_IWD2_FR_1_VERSION='2.01.101615-gog77030'
ARCHIVE_BASE_IWD2_FR_1_URL='https://www.gog.com/game/icewind_dale_2'

ARCHIVE_BASE_IWD2_EN_0_NAME='setup_icewind_dale2_2.1.0.13.exe'
ARCHIVE_BASE_IWD2_EN_0_MD5='9a68fdabdaff58bebc67092d47d4174e'
ARCHIVE_BASE_IWD2_EN_0_TYPE='innosetup'
ARCHIVE_BASE_IWD2_EN_0_SIZE='1500000'
ARCHIVE_BASE_IWD2_EN_0_VERSION='2.01.101615-gog2.1.0.13'

ARCHIVE_BASE_IWD2_FR_0_NAME='setup_icewind_dale2_french_2.1.0.13.exe'
ARCHIVE_BASE_IWD2_FR_0_MD5='04f25433d405671a8975be6540dd55fa'
ARCHIVE_BASE_IWD2_FR_0_TYPE='innosetup'
ARCHIVE_BASE_IWD2_FR_0_SIZE='1500000'
ARCHIVE_BASE_IWD2_FR_0_VERSION='2.01.101615-gog2.1.0.13'

## Planescape: Torment

ARCHIVE_BASE_PST_EN_1_NAME='planescape_torment_gog_3_23483.sh'
ARCHIVE_BASE_PST_EN_1_MD5='3eb98c2c34d628b7da6e4e914ac8e622'
ARCHIVE_BASE_PST_EN_1_SIZE='2700000'
ARCHIVE_BASE_PST_EN_1_VERSION='1.1-gog23483'
ARCHIVE_BASE_PST_EN_1_URL='https://www.gog.com/game/planescape_torment_enhanced_edition'

ARCHIVE_BASE_PST_FR_1_NAME='planescape_torment_french_gog_3_23483.sh'
ARCHIVE_BASE_PST_FR_1_MD5='3374385ab6c5ca8aa489ee8de6161637'
ARCHIVE_BASE_PST_FR_1_SIZE='2700000'
ARCHIVE_BASE_PST_FR_1_VERSION='1.1-gog23483'
ARCHIVE_BASE_PST_FR_1_URL='https://www.gog.com/game/planescape_torment_enhanced_edition'

ARCHIVE_BASE_PST_RU_1_NAME='planescape_torment_russian_gog_3_23483.sh'
ARCHIVE_BASE_PST_RU_1_MD5='6f6744e90691126c884dccf925423e2d'
ARCHIVE_BASE_PST_RU_1_SIZE='2700000'
ARCHIVE_BASE_PST_RU_1_VERSION='1.1-gog23483'
ARCHIVE_BASE_PST_RU_1_URL='https://www.gog.com/game/planescape_torment_enhanced_edition'

ARCHIVE_BASE_PST_EN_0_NAME='gog_planescape_torment_2.1.0.9.sh'
ARCHIVE_BASE_PST_EN_0_MD5='a48bb772f60da3b5b2cac804b6e92670'
ARCHIVE_BASE_PST_EN_0_SIZE='2400000'
ARCHIVE_BASE_PST_EN_0_VERSION='1.1-gog2.1.0.9'

ARCHIVE_BASE_PST_FR_0_NAME='gog_planescape_torment_french_2.1.0.9.sh'
ARCHIVE_BASE_PST_FR_0_MD5='c3af554300a90297d4fca0b591d9c3fd'
ARCHIVE_BASE_PST_FR_0_SIZE='2400000'
ARCHIVE_BASE_PST_FR_0_VERSION='1.1-gog2.1.0.9'

ARCHIVE_BASE_PST_RU_0_NAME='gog_planescape_torment_russian_2.2.0.10.sh'
ARCHIVE_BASE_PST_RU_0_MD5='d6fd52fe9946bcc067eed441945127f1'
ARCHIVE_BASE_PST_RU_0_SIZE='2400000'
ARCHIVE_BASE_PST_RU_0_VERSION='1.1-gog2.2.0.10'


CONTENT_PATH_DEFAULT_BG1_EN="data/noarch/prefix/drive_c/GOG Games/Baldur's Gate"
CONTENT_PATH_DEFAULT_BG1_FR="data/noarch/prefix/drive_c/GOG Games/Baldur's Gate (French)"
CONTENT_PATH_DEFAULT_BG1_PL="data/noarch/prefix/drive_c/GOG Games/Baldur's Gate (Polish)"
CONTENT_PATH_DEFAULT_BG2_EN="data/noarch/prefix/drive_c/GOG Games/Baldur's Gate 2"
CONTENT_PATH_DEFAULT_BG2_FR="data/noarch/prefix/drive_c/GOG Games/Baldur's Gate 2 (French)"
CONTENT_PATH_DEFAULT_IWD1='app'
CONTENT_PATH_DEFAULT_IWD2='.'
CONTENT_PATH_DEFAULT_IWD2_EN_0='app'
CONTENT_PATH_DEFAULT_IWD2_FR_0='app'
CONTENT_PATH_DEFAULT_PST_EN='data/noarch/prefix/drive_c/GOG Games/Planescape Torment'
CONTENT_PATH_DEFAULT_PST_RU='data/noarch/prefix/drive_c/GOG Games/Planescape Torment (Russian)'
CONTENT_PATH_DEFAULT_PST_FR='data/noarch/prefix/drive_c/GOG Games/Planescape Torment (French)'
## TODO: Game-specific lists would be easier to review
CONTENT_GAME_BIN_FILES='
script compiler
luaauto.cfg
lasnil32.dll
3dfx.dll
binkw32.dll
baldur.exe
bgdxtest.exe
bggltest.exe
bcgonfig.exe
bgmain.exe
bgmain2.exe
charview.exe
config.exe
glsetup.exe
idmain.exe
iwd2.exe
mconvert.exe
mplaynow.exe
torment.exe
autonote.ini
beast.ini
keymap.ini
layout.ini
quests.ini'
CONTENT_GAME0_BIN_RELATIVE_PATH='__support/app'
CONTENT_GAME0_BIN_FILES='
iwd2.exe
keymap.ini'
## The following configuration files are generated by the current game script,
## the shipped versions are ignored.
CONTENT_GAME1_BIN_FILES='
baldur.ini
icewind.ini
icewind2.ini
torment.ini'
CONTENT_GAME_L10N_FILES='
cachemos.bif
crefiles.bif
cs_0404.bif
interface.bif
sound.bif
voice.bif
language.ini
party.ini
language.txt
characters
override
movies
mpsave
save
sounds
cd2/data/*.mve
cd2/data/sndvo.bif
data/*sound*.bif
data/25npcso.bif
data/areas.bif
data/genmova.bif
data/movies?.bif
data/npcsocd?.bif
data/npchd0so.bif
data/objanim.bif
data/scripts.bif
data/movies
*.key
*.tlk'
CONTENT_GAME0_L10N_RELATIVE_PATH='__support/app'
CONTENT_GAME0_L10N_FILES='
party.ini'
CONTENT_GAME_DATA_FILES='
baldur.ico
var.var
cd2
cd3
data
music
scripts
*.bif
*.mpi'
CONTENT_DOC_L10N_FILES='
*.htm
*.pdf
*.txt'
CONTENT_DOC0_L10N_PATH_BG1_EN='data/noarch/docs/english'
CONTENT_DOC0_L10N_PATH_BG1_FR='data/noarch/docs/french'
CONTENT_DOC0_L10N_PATH_BG1_PL='data/noarch/docs/polish'
CONTENT_DOC0_L10N_PATH_BG1_EN_0='data/noarch/docs'
CONTENT_DOC0_L10N_PATH_BG1_FR_0='data/noarch/docs'
CONTENT_DOC0_L10N_PATH_BG2_EN='data/noarch/docs/english'
CONTENT_DOC0_L10N_PATH_BG2_FR='data/noarch/docs/french'
CONTENT_DOC0_L10N_PATH_BG2_EN_0='data/noarch/docs'
CONTENT_DOC0_L10N_PATH_BG2_FR_0='data/noarch/docs'
CONTENT_DOC0_L10N_PATH_PST_EN='data/noarch/docs/english'
CONTENT_DOC0_L10N_PATH_PST_FR='data/noarch/docs/french'
CONTENT_DOC0_L10N_PATH_PST_RU='data/noarch/docs/russian'
CONTENT_DOC0_L10N_PATH_PST_EN_0='data/noarch/docs'
CONTENT_DOC0_L10N_PATH_PST_FR_0='data/noarch/docs'
CONTENT_DOC0_L10N_PATH_PST_RU_0='data/noarch/docs'
## FIXME: An explicit files list should be given.
CONTENT_DOC0_L10N_FILES_BG1='
*'
CONTENT_DOC0_L10N_FILES_BG2='
*'
CONTENT_DOC0_L10N_FILES_PST='
*'

# Applications

USER_PERSISTENT_FILES='
*.ini'
USER_PERSISTENT_DIRECTORIES='
characters
mpsave
save'

## Disable the multi-threaded command stream feature, as it has a very severe impact on performances.
WINE_WINETRICKS_VERBS='csmt=off'

APPLICATIONS_LIST='APP_MAIN APP_CONFIG'
APPLICATIONS_LIST_PST='APP_MAIN'

APP_CONFIG_CAT='Settings'

## Baldur's Gate 1

APP_MAIN_EXE_BG1='bgmain2.exe'
APP_MAIN_ICON_BG1='baldur.exe'

APP_CONFIG_ID_BG1="${GAME_ID_BG1}-config"
APP_CONFIG_NAME_BG1="$GAME_NAME_BG1 - configuration"
APP_CONFIG_EXE_BG1='config.exe'

## Baldur's Gate 2

APP_MAIN_EXE_BG2='bgmain.exe'
APP_MAIN_ICON_BG2='baldur.exe'
## Merge "data" and "data/data".
## TODO: The symbolic link could probably be shipped in the package instead.
APP_MAIN_PRERUN_BG2='
# Merge "data" and "data/data"
ln --symbolic --force --no-target-directory . "data/data"
'

APP_CONFIG_ID_BG2="${GAME_ID_BG2}-config"
APP_CONFIG_NAME_BG2="$GAME_NAME_BG2 - configuration"
APP_CONFIG_EXE_BG2='bgconfig.exe'

## Icewind Dale 1

APP_MAIN_EXE_IWD1='idmain.exe'

APP_CONFIG_ID_IWD1="${GAME_ID_IWD1}-config"
APP_CONFIG_NAME_IWD1="$GAME_NAME_IWD1 - configuration"
APP_CONFIG_EXE_IWD1='config.exe'

## Icewind Dale 2

APP_MAIN_EXE_IWD2='iwd2.exe'
APP_MAIN_ICON_IWD2='__support/app/iwd2.exe'
APP_MAIN_ICON_IWD2_EN_0='iwd2.exe'
APP_MAIN_ICON_IWD2_FR_0='iwd2.exe'

APP_CONFIG_ID_IWD2="${GAME_ID_IWD2}-config"
APP_CONFIG_NAME_IWD2="$GAME_NAME_IWD2 - configuration"
APP_CONFIG_EXE_IWD2='config.exe'

## Planescape: Torment

APP_MAIN_EXE_PST='torment.exe'

# Packages

## Common

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'
PKG_L10N_DESCRIPTION_PL='Polish localization'
PKG_L10N_DESCRIPTION_RU='Russian localization'

PKG_DATA_DESCRIPTION='data'

## Baldur's Gate 1

PKG_L10N_BASE_ID_BG1="${GAME_ID_BG1}-l10n"
PKG_L10N_ID_BG1_EN="${PKG_L10N_BASE_ID_BG1}-en"
PKG_L10N_ID_BG1_FR="${PKG_L10N_BASE_ID_BG1}-fr"
PKG_L10N_ID_BG1_PL="${PKG_L10N_BASE_ID_BG1}-pl"
PKG_L10N_PROVIDES_BG1="
$PKG_L10N_BASE_ID_BG1"
PKG_L10N_DESCRIPTION_BG1_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_BG1_FR="$PKG_L10N_DESCRIPTION_FR"
PKG_L10N_DESCRIPTION_BG1_PL="$PKG_L10N_DESCRIPTION_PL"

PKG_DATA_ID_BG1="${GAME_ID_BG1}-data"

## Baldur's Gate 2

PKG_L10N_BASE_ID_BG2="${GAME_ID_BG2}-l10n"
PKG_L10N_ID_BG2_EN="${PKG_L10N_BASE_ID_BG2}-en"
PKG_L10N_ID_BG2_FR="${PKG_L10N_BASE_ID_BG2}-fr"
PKG_L10N_PROVIDES_BG2="
$PKG_L10N_BASE_ID_BG2"
PKG_L10N_DESCRIPTION_BG2_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_BG2_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_BG2="${GAME_ID_BG2}-data"

## Icewind Dale 1

PKG_L10N_BASE_ID_IWD1="${GAME_ID_IWD1}-l10n"
PKG_L10N_ID_IWD1="${PKG_L10N_BASE_ID_IWD1}-en"
PKG_L10N_PROVIDES_IWD1="
$PKG_L10N_BASE_ID_IWD1"
PKG_L10N_DESCRIPTION_IWD1="$PKG_L10N_DESCRIPTION_EN"

PKG_DATA_ID_IWD1="${GAME_ID_IWD1}-data"

## Icewind Dale 2

PKG_L10N_BASE_ID_IWD2="${GAME_ID_IWD2}-l10n"
PKG_L10N_ID_IWD2_EN="${PKG_L10N_BASE_ID_IWD2}-en"
PKG_L10N_ID_IWD2_FR="${PKG_L10N_BASE_ID_IWD2}-fr"
PKG_L10N_PROVIDES_IWD2="
$PKG_L10N_BASE_ID_IWD2"
PKG_L10N_DESCRIPTION_IWD2_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_IWD2_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_IWD2="${GAME_ID_IWD2}-data"

## Planescape: Torment

PKG_L10N_BASE_ID_PST="${GAME_ID_PST}-l10n"
PKG_L10N_ID_PST_EN="${PKG_L10N_BASE_ID_PST}-en"
PKG_L10N_ID_PST_FR="${PKG_L10N_BASE_ID_PST}-fr"
PKG_L10N_ID_PST_RU="${PKG_L10N_BASE_ID_PST}-ru"
PKG_L10N_PROVIDES_PST="
$PKG_L10N_BASE_ID_PST"
PKG_L10N_DESCRIPTION_PST_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_PST_FR="$PKG_L10N_DESCRIPTION_FR"
PKG_L10N_DESCRIPTION_PST_RU="$PKG_L10N_DESCRIPTION_RU"

PKG_DATA_ID_PST="${GAME_ID_PST}-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Mojosetup installers
	## - Convert all file names to lowercase
	if [ "$(archive_type "$(current_archive)")" = 'mojosetup' ]; then
		tolower .
	fi

	## Baldur's Gate 2
	## - Delete broken symbolic links
	## - Merge "data" and "data/data"
	case "$(current_archive)" in
		('ARCHIVE_BASE_BG2_'*)
			rm --force --recursive \
				'mpsave' \
				'temp'
			if [ -e 'data/data' ]; then
				cp --link --recursive --update=none 'data/data/'* 'data/'
				rm --force --recursive 'data/data'
			fi
		;;
	esac

	## Generate a minimal configuration file
	case "$(current_archive)" in
		('ARCHIVE_BASE_BG1_'*)
			config_file='baldur.ini'
		;;
		('ARCHIVE_BASE_BG2_'*)
			config_file='baldur.ini'
		;;
		('ARCHIVE_BASE_IWD1_'*)
			config_file='icewind.ini'
		;;
		('ARCHIVE_BASE_IWD2_'*)
			config_file='icewind2.ini'
		;;
		('ARCHIVE_BASE_PST_'*)
			config_file='torment.ini'
		;;
	esac
	game_path="C:\\$(game_id)"
	case "$(current_archive)" in
		('ARCHIVE_BASE_BG1_'*)
			cat > "$config_file" <<- EOF
			[Alias]
			HD0:=${game_path}\\
			CD1:=${game_path}\\data\\
			CD2:=${game_path}\\data\\
			CD3:=${game_path}\\data\\
			CD4:=${game_path}\\data\\
			CD5:=${game_path}\\data\\
			CD6:=${game_path}\\data\\
			EOF
		;;
		('ARCHIVE_BASE_BG2_'*)
			cat > "$config_file" <<- EOF
			[Alias]
			HD0:=${game_path}\\
			CD1:=${game_path}\\data\\
			CD2:=${game_path}\\data\\
			CD3:=${game_path}\\data\\
			CD4:=${game_path}\\data\\
			CD5:=${game_path}\\data\\
			CD6:=${game_path}\\data\\
			EOF
		;;
		('ARCHIVE_BASE_IWD1_'*)
			cat > "$config_file" <<- EOF
			[Alias]
			HD0:=${game_path}\\
			CD1:=${game_path}\\
			CD2:=${game_path}\\cd2\\
			CD3:=${game_path}\\cd3\\
			EOF
		;;
		('ARCHIVE_BASE_IWD2_'*)
			cat > "$config_file" <<- EOF
			[Alias]
			HD0:=${game_path}\\
			CD1:=${game_path}\\data\\
			CD2:=${game_path}\\cd2\\
			[Movies]
			INTRO=1
			EOF
		;;
		('ARCHIVE_BASE_PST_'*)
			cat > "$config_file" <<- EOF
			[Alias]
			HD0:=${game_path}\\
			CD1:=${game_path}\\data\\
			CD2:=${game_path}\\data\\
			CD3:=${game_path}\\data\\
			CD4:=${game_path}\\data\\
			CD5:=${game_path}\\data\\
			EOF
		;;
	esac
	## The native windowed mode is used instead of a WINE virtual desktop,
	## as using a WINE virtual desktop would cause cursor flickering (WINE 9.0).
	## cf. https://bugs.winehq.org/show_bug.cgi?id=56986
	cat >> "$config_file" <<- EOF
	[Program Options]
	Full Screen=0
	EOF
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
