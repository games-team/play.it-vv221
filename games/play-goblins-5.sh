#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gobliiins 5: L'invasion des Morglotons
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='goblins-5'
GAME_ID_PART1="${GAME_ID}-part-1"
GAME_ID_PART2="${GAME_ID}-part-2"
GAME_ID_PART3="${GAME_ID}-part-3"
GAME_ID_PART4="${GAME_ID}-part-4"
GAME_NAME='Gobliiins 5: L’invasion des Morglotons'
GAME_NAME_PART1="$GAME_NAME - Part 1"
GAME_NAME_PART2="$GAME_NAME - Part 2"
GAME_NAME_PART3="$GAME_NAME - Part 3"
GAME_NAME_PART4="$GAME_NAME - Part 4"

ARCHIVE_BASE_0_NAME='Gobliiins5.zip'
ARCHIVE_BASE_0_MD5='9ec74c3b393736b3e7ea3b345355d537'
ARCHIVE_BASE_0_SIZE='939801'
ARCHIVE_BASE_0_VERSION='1.0-itch.2023.11.17'
ARCHIVE_BASE_0_URL='https://pierre-gilhodes.itch.io/gobliiins5'

ARCHIVE_BASE_DEMO_0_NAME='Gobliiins5-Part1.zip'
ARCHIVE_BASE_DEMO_0_MD5='814a2e01fbf5a253fa56dd51c2e4d206'
ARCHIVE_BASE_DEMO_0_SIZE='182592'
ARCHIVE_BASE_DEMO_0_VERSION='1.0-itch.2023.11.17'
ARCHIVE_BASE_DEMO_0_URL='https://pierre-gilhodes.itch.io/gobliiins5'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_COMMON_FILES='
acsetup.cfg
English.tra
audio.vox'
CONTENT_GAME_PART1_FILES="
$CONTENT_GAME_COMMON_FILES
Gobliiins5-Part1.ags"
CONTENT_GAME_PART2_FILES="
$CONTENT_GAME_COMMON_FILES
Gobliiins5-Part2.ags"
CONTENT_GAME_PART3_FILES="
$CONTENT_GAME_COMMON_FILES
Gobliiins5-Part3.ags"
CONTENT_GAME_PART4_FILES="
$CONTENT_GAME_COMMON_FILES
Gobliiins5-Part4.ags"

APP_MAIN_SCUMMID_PART1='ags:gobliiins5-1'
APP_MAIN_SCUMMID_PART2='ags:gobliiins5-2'
APP_MAIN_SCUMMID_PART3='ags:gobliiins5-3'
APP_MAIN_SCUMMID_PART4='ags:gobliiins5-4'
APP_MAIN_ICON_PART1='Gobliiins5-Part1.exe'
APP_MAIN_ICON_PART2='Gobliiins5-Part2.exe'
APP_MAIN_ICON_PART3='Gobliiins5-Part3.exe'
APP_MAIN_ICON_PART4='Gobliiins5-Part4.exe'

PACKAGES_LIST='
PKG_PART1
PKG_PART2
PKG_PART3
PKG_PART4'
PACKAGES_LIST_DEMO='
PKG_PART1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract and include data

archive_extraction_default

## Each game part is handled one at a time to prevent unwanted file overwrites.
case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		## The game demo only includes the first part of the game.
		content_inclusion_icons 'PKG_PART1'
		content_inclusion 'GAME_PART1' 'PKG_PART1' "$(
			set_current_package 'PKG_PART1'
			path_game_data
		)"
	;;
	(*)
		## Allow the content inclusion message to be shown multiple times
		information_content_inclusion() {
			local messages_language message
			messages_language=$(messages_language)
			case "$messages_language" in
				('fr')
					message='Inclusion des fichiers du jeu…\n'
				;;
				('en'|*)
					message='Including game files…\n'
				;;
			esac
			print_message 'info' "$message"
		}

		ARCHIVE_PART1_PATH="${PLAYIT_WORKDIR}/gamedata/Gobliiins5-Part1.zip"
		archive_extraction 'ARCHIVE_PART1'
		content_inclusion_icons 'PKG_PART1'
		content_inclusion 'GAME_PART1' 'PKG_PART1' "$(
			set_current_package 'PKG_PART1'
			path_game_data
		)"
		rm "$ARCHIVE_PART1_PATH"
		
		ARCHIVE_PART2_PATH="${PLAYIT_WORKDIR}/gamedata/Gobliiins5-Part2.zip"
		archive_extraction 'ARCHIVE_PART2'
		content_inclusion_icons 'PKG_PART2'
		content_inclusion 'GAME_PART2' 'PKG_PART2' "$(
			set_current_package 'PKG_PART2'
			path_game_data
		)"
		rm "$ARCHIVE_PART2_PATH"
		
		ARCHIVE_PART3_PATH="${PLAYIT_WORKDIR}/gamedata/Gobliiins5-Part3.zip"
		archive_extraction 'ARCHIVE_PART3'
		content_inclusion_icons 'PKG_PART3'
		content_inclusion 'GAME_PART3' 'PKG_PART3' "$(
			set_current_package 'PKG_PART3'
			path_game_data
		)"
		rm "$ARCHIVE_PART3_PATH"
		
		ARCHIVE_PART4_PATH="${PLAYIT_WORKDIR}/gamedata/Gobliiins5-Part4.zip"
		archive_extraction 'ARCHIVE_PART4'
		content_inclusion_icons 'PKG_PART4'
		content_inclusion 'GAME_PART4' 'PKG_PART4' "$(
			set_current_package 'PKG_PART4'
			path_game_data
		)"
		rm "$ARCHIVE_PART4_PATH"
	;;
esac
rm --force --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		## The game demo only includes the first part of the game.
		launchers_generation 'PKG_PART1'
	;;
	(*)
		launchers_generation 'PKG_PART1'
		launchers_generation 'PKG_PART2'
		launchers_generation 'PKG_PART3'
		launchers_generation 'PKG_PART4'
	;;
esac

# Build packages

packages_generation
## Ensure that the generic game name is shown, not a package-specific one.
unset GAME_NAME_PART1
print_instructions

# Clean up

working_directory_cleanup

exit 0
