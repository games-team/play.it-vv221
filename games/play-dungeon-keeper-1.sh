#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dungeon Keeper 1
###

script_version=20241123.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='dungeon-keeper-1'
GAME_NAME='Dungeon Keeper'

ARCHIVE_BASE_2_NAME='setup_dungeon_keeper_gold_1.01_fix_(76190).exe'
ARCHIVE_BASE_2_MD5='bb51a55e868c7b22c505cbf6bfaa3789'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='393045'
ARCHIVE_BASE_2_VERSION='1.01-gog76190'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/dungeon_keeper'

ARCHIVE_BASE_1_NAME='setup_dungeon_keeper_gold_10.1_(28184).exe'
ARCHIVE_BASE_1_MD5='5d9c6f723c0375590cd77f79bed44eff'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='370026'
ARCHIVE_BASE_1_VERSION='1.01-gog28184'

ARCHIVE_BASE_0_NAME='setup_dungeon_keeper_gold_2.1.0.7.exe'
ARCHIVE_BASE_0_MD5='8f8890d743c171fb341c9d9c87c52343'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='400000'
ARCHIVE_BASE_0_VERSION='1.01-gog2.1.0.7'

# Archives content

## Main game content

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
data
ldata
levels
sound
game.gog
game.ins
dpatch.1
wpatch.1
deeper.exe
dos4gw.exe
keeper.exe
patch1.exe
keeper.pat
keeper.pti
*.ogg'
## In the 2.1.0.7 GOG installer, the disk image table of contents is named "game.inst" instead of "game.ins"
CONTENT_GAME0_MAIN_FILES='
game.inst'
CONTENT_GAME1_MAIN_PATH='__support/save'
CONTENT_GAME1_MAIN_PATH_0='app'
CONTENT_GAME1_MAIN_FILES='
keeper.cfg
sound/dig.ini'
CONTENT_DOC_MAIN_FILES='
readme.txt
*.pdf'

## Localizations

CONTENT_L10N_TXT_PATH='keeper/data'
CONTENT_L10N_VOICES_SPEECH_PATH='keeper/sound/speech'
CONTENT_L10N_VOICES_ATLAS_PATH='keeper/sound/atlas'

### English

CONTENT_L10N_TXT_EN_PATH="${CONTENT_L10N_TXT_PATH}/english"
CONTENT_L10N_TXT_EN_FILES='
dd1text.dat
text.dat'
CONTENT_L10N_VOICES_SPEECH_EN_PATH="${CONTENT_L10N_VOICES_SPEECH_PATH}/english"
CONTENT_L10N_VOICES_SPEECH_EN_FILES='
speech.dat'
CONTENT_L10N_VOICES_ATLAS_EN_PATH="${CONTENT_L10N_VOICES_ATLAS_PATH}/english"
CONTENT_L10N_VOICES_ATLAS_EN_FILES='
bad??.wav
good??.wav'

### French

CONTENT_L10N_TXT_FR_PATH="${CONTENT_L10N_TXT_PATH}/french"
CONTENT_L10N_TXT_FR_FILES='
dd1text.dat
text.dat'
CONTENT_L10N_VOICES_SPEECH_FR_PATH="${CONTENT_L10N_VOICES_SPEECH_PATH}/french"
CONTENT_L10N_VOICES_SPEECH_FR_FILES='
speech.dat'
CONTENT_L10N_VOICES_ATLAS_FR_PATH="${CONTENT_L10N_VOICES_ATLAS_PATH}/french"
CONTENT_L10N_VOICES_ATLAS_FR_FILES='
bad??.wav
good??.wav'


GAME_IMAGE='game.ins'
GAME_IMAGE_TYPE='iso'

USER_PERSISTENT_DIRECTORIES='
save'
USER_PERSISTENT_FILES='
*.cfg
data/HISCORES.DAT'

APP_MAIN_EXE='keeper.exe'
APP_MAIN_ICON='goggame-1207658934.ico'

APP_ADDON_ID="${GAME_ID}-deeper-dungeons"
APP_ADDON_NAME="$GAME_NAME - Deeper Dungeons"
APP_ADDON_EXE='deeper.exe'
APP_ADDON_ICON='gfw_high_addon.ico'

PACKAGES_LIST='
PKG_L10N_TXT_EN
PKG_L10N_TXT_FR
PKG_L10N_VOICES_EN
PKG_L10N_VOICES_FR
PKG_MAIN'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_TXT
PKG_L10N_VOICES'

PKG_L10N_TXT_ID="${GAME_ID}-l10n-txt"
PKG_L10N_TXT_EN_ID="${PKG_L10N_TXT_ID}-en"
PKG_L10N_TXT_FR_ID="${PKG_L10N_TXT_ID}-fr"
PKG_L10N_TXT_PROVIDES="
$PKG_L10N_TXT_ID"
PKG_L10N_TXT_EN_PROVIDES="$PKG_L10N_TXT_PROVIDES"
PKG_L10N_TXT_FR_PROVIDES="$PKG_L10N_TXT_PROVIDES"
PKG_L10N_TXT_EN_DESCRIPTION='English text'
PKG_L10N_TXT_FR_DESCRIPTION='French text'

PKG_L10N_VOICES_ID="${GAME_ID}-l10n-voices"
PKG_L10N_VOICES_EN_ID="${PKG_L10N_VOICES_ID}-en"
PKG_L10N_VOICES_FR_ID="${PKG_L10N_VOICES_ID}-fr"
PKG_L10N_VOICES_PROVIDES="
$PKG_L10N_VOICES_ID"
PKG_L10N_VOICES_EN_PROVIDES="$PKG_L10N_VOICES_PROVIDES"
PKG_L10N_VOICES_FR_PROVIDES="$PKG_L10N_VOICES_PROVIDES"
PKG_L10N_VOICES_EN_DESCRIPTION='English voices'
PKG_L10N_VOICES_FR_DESCRIPTION='French voices'

# unar is used to extract the contents of the disk image

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
unar"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete files that should not be included in the base package.
	## These files are provided by the localization packages.
	rm --force --recursive \
		'data/dd1text.dat' \
		'data/text.dat' \
		'sound/atlas' \
		'sound/speech.dat'

	## Enforce a consistent name for the disk image table of content.
	## TODO: This would not be required if the DISK_IMAGE variable was context-sensitive
	##       cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/549
	if [ -e 'game.inst' ]; then
		mv 'game.inst' "$GAME_IMAGE"
	fi
)
# Extract localization content from the disk image
ARCHIVE_L10N_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/game.gog"
ARCHIVE_L10N_EXTRACTOR='unar'
archive_extraction 'ARCHIVE_L10N'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion 'L10N_TXT_EN' 'PKG_L10N_TXT_EN' "$(path_game_data)/data"
content_inclusion 'L10N_TXT_FR' 'PKG_L10N_TXT_FR' "$(path_game_data)/data"
content_inclusion 'L10N_VOICES_SPEECH_EN' 'PKG_L10N_VOICES_EN' "$(path_game_data)/sound"
content_inclusion 'L10N_VOICES_SPEECH_FR' 'PKG_L10N_VOICES_FR' "$(path_game_data)/sound"
content_inclusion 'L10N_VOICES_ATLAS_EN' 'PKG_L10N_VOICES_EN' "$(path_game_data)/sound/atlas"
content_inclusion 'L10N_VOICES_ATLAS_FR' 'PKG_L10N_VOICES_FR' "$(path_game_data)/sound/atlas"
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_L10N_TXT_EN' 'PKG_L10N_VOICES_EN' 'PKG_MAIN'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_L10N_TXT_FR' 'PKG_L10N_VOICES_FR' 'PKG_MAIN'

# Clean up

working_directory_cleanup

exit 0
