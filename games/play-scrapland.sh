#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Scrapland
###

script_version=20250104.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='scrapland'
GAME_NAME='Scrapland'

ARCHIVE_BASE_0_NAME='setup_scrapland_remastered_1.6.1_(52159).exe'
ARCHIVE_BASE_0_MD5='5f1a6494309632a2536c46e2bf3226bc'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_scrapland_remastered_1.6.1_(52159)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='93c07bedabd25a0d63c7ae36cf5abd47'
ARCHIVE_BASE_0_PART2_NAME='setup_scrapland_remastered_1.6.1_(52159)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='8543d603b9c8eb6d666aec35387c311c'
ARCHIVE_BASE_0_SIZE='8943631'
ARCHIVE_BASE_0_VERSION='1.6.1-gog52159'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/scrapland_remastered'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bin
dedicated
scrap.cfg'
CONTENT_GAME_L10N_EN_FILES='
language/english.txt
video/bishop.vds
video/intro.vds
video/mayor.vds
video/police.vds
video/viscous.vds
english.packed'
CONTENT_GAME_L10N_FR_FILES='
language/french.txt
french.packed'
CONTENT_GAME_DATA_FILES='
video/mse.vds
video/mse-short.vds
data.packed
data??.packed'
CONTENT_VIDEO_L10N_FR_PATH='video/french'
CONTENT_VIDEO_L10N_FR_FILES='
*.vds'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/MercurySteam/Scrapland'
## TODO: Check if the virtual desktop is still required with current WINE
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='bin/scrap.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/x-msvideo'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='English localization'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'

## Include language setting file
language_file_origin="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/goggame-1688988400.info"
language_file_l10n_en_destination="$(package_path 'PKG_L10N_EN')$(path_game_data)/goggame-1688988400.info"
language_file_l10n_fr_destination="$(package_path 'PKG_L10N_FR')$(path_game_data)/goggame-1688988400.info"
mkdir --parents \
	"$(dirname "$language_file_l10n_en_destination")" \
	"$(dirname "$language_file_l10n_fr_destination")"
sed --expression='s/"language": ".*",/"language": "English",/' \
	"$language_file_origin" > "$language_file_l10n_en_destination"
sed --expression='s/"language": ".*",/"language": "French",/' \
	"$language_file_origin" > "$language_file_l10n_fr_destination"

## Include videos in the French localization
content_inclusion 'VIDEO_L10N_FR' 'PKG_L10N_FR' "$(path_game_data)/video"

content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd bin
	$(wine_command) scrap.exe "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_L10N_EN' 'PKG_DATA' 'PKG_BIN'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_L10N_FR' 'PKG_DATA' 'PKG_BIN'

# Clean up

working_directory_cleanup

exit 0
