#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tomb Raider Remastered:
# - Tomb Raider
# - Tomb Raider 2
# - Tomb Raider 3
###

script_version=20241228.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='tomb-raider-remastered-1-2-3'
GAME_NAME='Tomb Raider I-III Remastered'

ARCHIVE_BASE_0_NAME='setup_tomb_raider_i-iii_remastered_1.01_patch_4_(77558).exe'
ARCHIVE_BASE_0_MD5='9172c7570c8c0dfd47c3925aaecf360d'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_tomb_raider_i-iii_remastered_1.01_patch_4_(77558)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='dbcec0aba1dd597efe96bace51e79140'
ARCHIVE_BASE_0_SIZE='6109084'
ARCHIVE_BASE_0_VERSION='1.01-gog77558'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/tomb_raider_i_to_iii_remastered'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
tomb123.exe
1/tomb1.dll
2/tomb2.dll
3/tomb3.dll'
CONTENT_GAME_DATA_1_FILES='
1'
CONTENT_GAME_DATA_2_FILES='
2'
CONTENT_GAME_DATA_3_FILES='
3'

WINE_PERSISTENT_DIRECTORIES='
users/$[USER}/AppData/Roaming/TRX'

APP_MAIN_EXE='tomb123.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_1
PKG_DATA_2
PKG_DATA_3
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_1_ID="${PKG_DATA_ID}-episode-1"
PKG_DATA_2_ID="${PKG_DATA_ID}-episode-2"
PKG_DATA_3_ID="${PKG_DATA_ID}-episode-3"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_1_DESCRIPTION="$PKG_DATA_DESCRIPTION - Episode 1"
PKG_DATA_2_DESCRIPTION="$PKG_DATA_DESCRIPTION - Episode 2"
PKG_DATA_3_DESCRIPTION="$PKG_DATA_DESCRIPTION - Episode 3"
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_1
PKG_DATA_2
PKG_DATA_3'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
