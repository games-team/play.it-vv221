#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Hoël Bézier
set -o errexit

###
# Into the Breach
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='into-the-breach'
GAME_NAME='Into the Breach'

ARCHIVE_BASE_HUMBLE_4_NAME='IntoTheBreach_1_2_76_Linux.zip'
ARCHIVE_BASE_HUMBLE_4_MD5='79f506d656c088c67dbf9286936e0e97'
ARCHIVE_BASE_HUMBLE_4_SIZE='540000'
ARCHIVE_BASE_HUMBLE_4_VERSION='1.2.76-humble.2022.08.02'
ARCHIVE_BASE_HUMBLE_4_URL='https://subsetgames.com/itb.html#HumbleWidgetBox5'

ARCHIVE_BASE_HUMBLE_3_NAME='IntoTheBreach_1_2_75_Linux.zip'
ARCHIVE_BASE_HUMBLE_3_MD5='b9912dc0b9bdd2013704b918ce438592'
ARCHIVE_BASE_HUMBLE_3_SIZE='540000'
ARCHIVE_BASE_HUMBLE_3_VERSION='1.2.75-humble.2022.07.28'

ARCHIVE_BASE_HUMBLE_2_NAME='IntoTheBreach_1_2_71_Linux.zip'
ARCHIVE_BASE_HUMBLE_2_MD5='afa9f960d757ec72c537f306a2065d4f'
ARCHIVE_BASE_HUMBLE_2_SIZE='540000'
ARCHIVE_BASE_HUMBLE_2_VERSION='1.2.71-humble.2022.07.19'

ARCHIVE_BASE_HUMBLE_1_NAME='Into_the_Breach_Linux.1.2.23.zip'
ARCHIVE_BASE_HUMBLE_1_MD5='35cbc7ad39661ac97f41760f77f71fab'
ARCHIVE_BASE_HUMBLE_1_SIZE='320000'
ARCHIVE_BASE_HUMBLE_1_VERSION='1.2.23-humble.2020.05.15'

ARCHIVE_BASE_HUMBLE_0_NAME='Into_the_Breach_Linux.1.2.20.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='5a8b33e1ccbc2953c99aacf0ad38ca37'
ARCHIVE_BASE_HUMBLE_0_SIZE='320000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.2.20-humble.2020.04.20'

ARCHIVE_BASE_GOG_1_NAME='into_the_breach_1_2_75_57446.sh'
ARCHIVE_BASE_GOG_1_MD5='4cfb207c03f337e6f04faa1fa5bd5aea'
ARCHIVE_BASE_GOG_1_SIZE='540000'
ARCHIVE_BASE_GOG_1_VERSION='1.2.75-gog57446'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/into_the_breach'

ARCHIVE_BASE_GOG_0_NAME='into_the_breach_1_2_24_38624.sh'
ARCHIVE_BASE_GOG_0_MD5='dbc8142ec1da82284b7959a48b50d512'
ARCHIVE_BASE_GOG_0_SIZE='320000'
ARCHIVE_BASE_GOG_0_VERSION='1.2.24-gog38624'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='into-the-breach_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='ce72ae946c4708feabb324493dc197b1'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/into-the-breach/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
32x32
48x48
64x64
128x128
256x256'

CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_PATH_DEFAULT_HUMBLE_1='Into the Breach'
CONTENT_PATH_DEFAULT_HUMBLE_0='Into the Breach Linux'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_LIBS_BIN_RELATIVE_PATH='linux_x64'
## Using the system-provided libSDL2-2.0.so.0 instead of the shipped one triggers a segfault on a Debian Trixie/Sid.
CONTENT_LIBS_BIN_FILES='
libfmod.so.10
libfmodstudio.so.10
libSDL2-2.0.so.0'
CONTENT_GAME_BIN_FILES='
Breach'
CONTENT_GAME_DATA_FILES='
maps
resources
scripts
shadersOGL'
CONTENT_DOC_DATA_FILES='
licenses'

APP_MAIN_EXE='Breach'
## The game crashes on launch when SDL_VIDEODRIVER is set to "wayland".
APP_MAIN_PRERUN='# The game crashes on launch when SDL_VIDEODRIVER is set to "wayland".
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
