#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Stellaris expansions (narrative expansions):
# - Astral Planes
###

script_version=20241215.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='stellaris'
GAME_NAME='Stellaris'

EXPANSION_ID_PLANES='astral-planes'
EXPANSION_NAME_PLANES='Astral Planes'

# Archives

## Astral Planes

ARCHIVE_BASE_PLANES_16_NAME='stellaris_astral_planes_3_14_15926_78464.sh'
ARCHIVE_BASE_PLANES_16_MD5='d59eceb01b9846df80331cd57e41c899'
ARCHIVE_BASE_PLANES_16_SIZE='46789'
ARCHIVE_BASE_PLANES_16_VERSION='3.14.15926-gog78464'
ARCHIVE_BASE_PLANES_16_URL='https://www.gog.com/game/stellaris_astral_planes'

ARCHIVE_BASE_PLANES_15_NAME='stellaris_astral_planes_3_13_2_0_76713.sh'
ARCHIVE_BASE_PLANES_15_MD5='8350550427b8df4980d5b10b680f8068'
ARCHIVE_BASE_PLANES_15_SIZE='46789'
ARCHIVE_BASE_PLANES_15_VERSION='3.13.2-gog76713'

ARCHIVE_BASE_PLANES_12_NAME='stellaris_astral_planes_3_12_5_74193.sh'
ARCHIVE_BASE_PLANES_12_MD5='8cbaaaa68306cc90effacd2510cbe089'
ARCHIVE_BASE_PLANES_12_SIZE='46789'
ARCHIVE_BASE_PLANES_12_VERSION='3.12.5-gog74193'

ARCHIVE_BASE_PLANES_8_NAME='stellaris_astral_planes_3_11_3_0_72561.sh'
ARCHIVE_BASE_PLANES_8_MD5='7d43cac5e6dd18238fff6f06b82be7c1'
ARCHIVE_BASE_PLANES_8_SIZE='46789'
ARCHIVE_BASE_PLANES_8_VERSION='3.11.3-gog72561'

ARCHIVE_BASE_PLANES_5_NAME='stellaris_astral_planes_3_10_4_70670.sh'
ARCHIVE_BASE_PLANES_5_MD5='fab105e52dffbf0ec425d4cedf89feae'
ARCHIVE_BASE_PLANES_5_SIZE='46789'
ARCHIVE_BASE_PLANES_5_VERSION='3.10.4-gog70670'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
dlc'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
