#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# War for the Overworld
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='war-for-the-overworld'
GAME_NAME='War for the Overworld'

ARCHIVE_BASE_8_NAME='war_for_the_overworld_v2_1_2_76431.sh'
ARCHIVE_BASE_8_MD5='e834b2a81bff0a36dc50fcf7a061abec'
ARCHIVE_BASE_8_SIZE='4814079'
ARCHIVE_BASE_8_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_8_URL='https://www.gog.com/game/war_for_the_overworld'

ARCHIVE_BASE_7_NAME='war_for_the_overworld_v2_1_1_73576.sh'
ARCHIVE_BASE_7_MD5='d3095d210fda9c39cd2cec6c2d0a882f'
ARCHIVE_BASE_7_SIZE='4792845'
ARCHIVE_BASE_7_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_6_NAME='war_for_the_overworld_v2_1_0f4_55096.sh'
ARCHIVE_BASE_6_MD5='97a9bd6ee2f88fa9c64a75cd578d3677'
ARCHIVE_BASE_6_SIZE='4800000'
ARCHIVE_BASE_6_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_5_NAME='war_for_the_overworld_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_5_MD5='e957781ef8559841ed2e32032e43d2cd'
ARCHIVE_BASE_5_SIZE='4700000'
ARCHIVE_BASE_5_VERSION='2.0.7f1-gog36563'

ARCHIVE_BASE_4_NAME='war_for_the_overworld_2_0_7f1_30014.sh'
ARCHIVE_BASE_4_MD5='a352307c8fbf70c33bdfdd97a82c6530'
ARCHIVE_BASE_4_SIZE='4700000'
ARCHIVE_BASE_4_VERSION='2.0.6f1-gog30014'

ARCHIVE_BASE_3_NAME='war_for_the_overworld_2_0_6f1_24637.sh'
ARCHIVE_BASE_3_MD5='e58f2720ed974185e9e5b29d08aa6238'
ARCHIVE_BASE_3_SIZE='4700000'
ARCHIVE_BASE_3_VERSION='2.0.6f1-gog24637'

ARCHIVE_BASE_2_NAME='war_for_the_overworld_2_0_5_24177.sh'
ARCHIVE_BASE_2_MD5='79b604f0d19caf3af5fdc4cb3903b370'
ARCHIVE_BASE_2_SIZE='4700000'
ARCHIVE_BASE_2_VERSION='2.0.5-gog24177'

ARCHIVE_BASE_1_NAME='war_for_the_overworld_en_2_0_4_23102.sh'
ARCHIVE_BASE_1_MD5='2873095f86b17c613b84af9624986f42'
ARCHIVE_BASE_1_SIZE='4700000'
ARCHIVE_BASE_1_VERSION='2.0.4-gog23102'

ARCHIVE_BASE_0_NAME='war_for_the_overworld_en_2_0_3f1_22287.sh'
ARCHIVE_BASE_0_MD5='4f1ff4e136aeaa795fce8ba26445cbe8'
ARCHIVE_BASE_0_SIZE='4700000'
ARCHIVE_BASE_0_VERSION='2.0.3f1-gog22287'

UNITY3D_NAME='WFTOGame'
UNITY3D_PLUGINS='
libCoherentGTCore.so
libCoherentGTJS.so
libcoherenticudata.so
libcoherenticui18n.so
libcoherenticuuc.so
libCoherentUIGT_Native.so
libGameLogic.so
libRenoirCore.Linux.so
libWTF.so
ScreenSelector.so'
## If libsteam_api.so is not included, ending a level will fail.
UNITY3D_PLUGINS="${UNITY3D_PLUGINS:-}
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_BIN_FILES="
${UNITY3D_NAME}_Data/CoherentUI_Host"
CONTENT_GAME0_DATA_FILES='
GameData
*.info'

USER_PERSISTENT_DIRECTORIES="
GameData
${UNITY3D_NAME}_Data/GameData
${UNITY3D_NAME}_Data/uiresources/minimapSnapshot"
## Work around the engine inability to play video files without write permissions
USER_PERSISTENT_DIRECTORIES="${USER_PERSISTENT_DIRECTORIES:-}
${UNITY3D_NAME}_Data/uiresources/wftoUI/menu/vids"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"
	
	## Add required execution permissions (this is only required by some old builds of the game)
	for file in \
		"$(unity3d_name)_Data/CoherentUI_Host/linux/CoherentUI_Host" \
		"$(unity3d_name)_Data/CoherentUI_Host/linux/CoherentUI_Host.bin"
	do
		if [ -e "$file" ]; then
			chmod 755 "$file"
		fi
	done
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Link some Unity3D plugin libraries in the game data path as the game engine fails to find them otherwise
libraries_destination="$(package_path 'PKG_BIN')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86_64"
mkdir --parents "$libraries_destination"
ln --symbolic \
	"$(path_libraries)/libCoherentUIGT_Native.so" \
	"$(path_libraries)/libGameLogic.so" \
	"$libraries_destination"

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
