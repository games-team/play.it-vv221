#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# Infinity Engine games (Enhanced Edition):
# - Baldur's Gate 1 Enhanced Edition
# - Baldur's Gate 2 Enhanced Edition
# - Icewind Dale 1 Enhanced Edition
# - Planescape: Torment Enhanced Edition
###

script_version=20250227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_BG1='baldurs-gate-1-enhanced-edition'
GAME_NAME_BG1='Baldurʼs Gate Enhanced Edition'

GAME_ID_BG2='baldurs-gate-2-enhanced-edition'
GAME_NAME_BG2='Baldurʼs Gate Ⅱ Enhanced Edition'

GAME_ID_IWD1='icewind-dale-1-enhanced-edition'
GAME_NAME_IWD1='Icewind Dale Enhanced Edition'

GAME_ID_PST='planescape-torment-enhanced-edition'
GAME_NAME_PST='Planescape: Torment Enhanced Edition'

# Game archives

## Baldur's Gate 1 Enhanced Edition

ARCHIVE_BASE_BG1_1_NAME='baldur_s_gate_enhanced_edition_2_6_6_0_47291.sh'
ARCHIVE_BASE_BG1_1_MD5='6f7be163ebb80a0fbc9d6331f9c6f09c'
ARCHIVE_BASE_BG1_1_SIZE='3300000'
ARCHIVE_BASE_BG1_1_VERSION='2.6.6.0-gog47291'
ARCHIVE_BASE_BG1_1_URL='https://www.gog.com/game/baldurs_gate_enhanced_edition'

ARCHIVE_BASE_BG1_0_NAME='baldur_s_gate_enhanced_edition_2_6_5_0_46477.sh'
ARCHIVE_BASE_BG1_0_MD5='a87444f36602b5059e3c885ec2ff50e1'
ARCHIVE_BASE_BG1_0_SIZE='3300000'
ARCHIVE_BASE_BG1_0_VERSION='2.6.5.0-gog46477'

### Option icons pack

ARCHIVE_OPTIONAL_ICONS_NAME_BG1='baldurs-gate-1-enhanced-edition_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5_BG1='58401cf80bc9f1a9e9a0896f5d74b02a'
ARCHIVE_OPTIONAL_ICONS_URL_BG1='https://downloads.dotslashplay.it/resources/baldurs-gate-1-enhanced-edition/'
CONTENT_ICONS_PATH_BG1='.'
CONTENT_ICONS_FILES_BG1='
16x16
24x42
32x32
48x48
64x64
256x256'

## Baldur's Gate 2 Enhanced Edition

ARCHIVE_BASE_BG2_1_NAME='baldur_s_gate_ii_enhanced_edition_2_6_6_0_47292.sh'
ARCHIVE_BASE_BG2_1_MD5='43b37a554ffb712176ea8709fc98ed84'
ARCHIVE_BASE_BG2_1_SIZE='380000'
ARCHIVE_BASE_BG2_1_VERSION='2.6.6.0-gog47292'
ARCHIVE_BASE_BG2_1_URL='https://www.gog.com/game/baldurs_gate_2_enhanced_edition'

ARCHIVE_BASE_BG2_0_NAME='baldur_s_gate_ii_enhanced_edition_2_6_5_0_46477.sh'
ARCHIVE_BASE_BG2_0_MD5='aa62efd4b1c69f074a784e637234e7c4'
ARCHIVE_BASE_BG2_0_SIZE='3800000'
ARCHIVE_BASE_BG2_0_VERSION='2.6.5.0-gog46477'

### Optional icons pack

ARCHIVE_OPTIONAL_ICONS_NAME_BG2='baldurs-gate-2-enhanced-edition_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5_BG2='36055600f9461793995a318e74b133e8'
ARCHIVE_OPTIONAL_ICONS_URL_BG2='https://downloads.dotslashplay.it/games/baldurs-gate-2-enhanced-edition/'
CONTENT_ICONS_PATH_BG2='.'
CONTENT_ICONS_FILES_BG2='
16x16
32x32
48x48
64x64
128x128
256x256'

## Icewind Dale 1 Enhanced Edition

ARCHIVE_BASE_IWD1_1_NAME='icewind_dale_enhanced_edition_2_6_6_0_47290.sh'
ARCHIVE_BASE_IWD1_1_MD5='fd7721f10a6d39c545ef46c58d80e2cc'
ARCHIVE_BASE_IWD1_1_SIZE='3000000'
ARCHIVE_BASE_IWD1_1_VERSION='2.6.6.0-gog47290'
ARCHIVE_BASE_IWD1_1_URL='https://www.gog.com/game/icewind_dale_enhanced_edition'

ARCHIVE_BASE_IWD1_0_NAME='icewind_dale_enhanced_edition_2_6_5_0_46474.sh'
ARCHIVE_BASE_IWD1_0_MD5='7fa481705b5d5b7f5d714a6a19c856e0'
ARCHIVE_BASE_IWD1_0_SIZE='3000000'
ARCHIVE_BASE_IWD1_0_VERSION='2.6.5.0-gog46474'

### Optional icons pack

ARCHIVE_OPTIONAL_ICONS_NAME_IWD1='icewind-dale-1-enhanced-edition_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5_IWD1='2e7db406aca79f9182c4efa93df80bf4'
ARCHIVE_OPTIONAL_ICONS_URL_IWD1='https://downloads.dotslashplay.it/resources/icewind-dale-1-enhanced-edition/'
CONTENT_ICONS_PATH_IWD1='.'
CONTENT_ICONS_FILES_IWD1='
16x16
32x32
48x48
64x64
128x128
256x256'

## Planescape: Torment

ARCHIVE_BASE_PST_1_NAME='planescape_torment_enhanced_edition_3_1_4_26532.sh'
ARCHIVE_BASE_PST_1_MD5='7d42ae99df8c0a4ff460235ae406f2d6'
ARCHIVE_BASE_PST_1_SIZE='1800000'
ARCHIVE_BASE_PST_1_VERSION='3.1.4-gog26532'
ARCHIVE_BASE_PST_1_URL='https://www.gog.com/game/planescape_torment_enhanced_edition'

ARCHIVE_BASE_PST_0_NAME='gog_planescape_torment_enhanced_edition_2.1.0.3.sh'
ARCHIVE_BASE_PST_0_MD5='649c1bf9d7ccd81553c574ff1bec2cef'
ARCHIVE_BASE_PST_0_SIZE='1800000'
ARCHIVE_BASE_PST_0_VERSION='3.1.3-gog2.1.0.3'

### Optional icons pack

ARCHIVE_OPTIONAL_ICONS_NAME_PST='planescape-torment-enhanced-edition_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5_PST='ed1c9961e2ecfa401af825d75723fe9c'
ARCHIVE_OPTIONAL_ICONS_URL_PST='https://downloads.dotslashplay.it/games/planescape-torment-enhanced-edition/'
CONTENT_ICONS_PATH_PST='.'
CONTENT_ICONS_FILES_PST='
16x16
32x32
48x48
64x64
96x96
128x128
256x256'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES_BG1='
BaldursGate'
CONTENT_GAME_BIN_FILES_BG2='
BaldursGateII'
CONTENT_GAME_BIN_FILES_IWD1='
IcewindDale'
CONTENT_GAME_BIN_FILES_PST='
Torment64'
CONTENT_GAME_L10N_FR_FILES='
lang/fr_FR'
CONTENT_GAME_DATA_FILES='
chitin.key
engine.lua
Manuals
movies
music
scripts
data'
## The English localization files are always included,
## so they can be used as a fallback for incomplete localizations.
CONTENT_GAME0_DATA_FILES='
lang/en_US'
CONTENT_DOC_DATA_RELATIVE_PATH='../docs'
## FIXME: An explicit list of files should be set.
CONTENT_DOC_DATA_FILES='
*'

APP_MAIN_EXE_BG1='BaldursGate'
APP_MAIN_EXE_BG2='BaldursGateII'
APP_MAIN_EXE_IWD1='IcewindDale'
APP_MAIN_EXE_PST='Torment64'
APP_MAIN_ICON='../support/icon.png'

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_L10N_FR
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libexpat.so.1
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libssl.so.1.0.0
libstdc++.so.6
libX11.so.6'

PKG_L10N_FR_DESCRIPTION='French localization'
PKG_L10N_DEPENDENCIES_SIBLINGS='
PKG_BIN'
PKG_L10N_FR_DEPENDENCIES_SIBLINGS="$PKG_L10N_DEPENDENCIES_SIBLINGS"

PKG_DATA_DESCRIPTION='data'

## Baldur's Gate 1 Enhanced Edition

PKG_L10N_ID_BG1="${GAME_ID_BG1}-l10n"
PKG_L10N_FR_ID_BG1="${PKG_L10N_ID_BG1}-fr"
PKG_L10N_PROVIDES_BG1="
$PKG_L10N_ID_BG1"
PKG_L10N_FR_PROVIDES_BG1="$PKG_L10N_PROVIDES_BG1"

PKG_DATA_ID_BG1="${GAME_ID_BG1}-data"
## Ensure easy upgrades from packaged generated with pre-20250103.1 game scripts
PKG_DATA_PROVIDES_BG1="${PKG_DATA_PROVIDES_BG1:-}
baldurs-gate-1-enhanced-edition-l10n-en"

## Baldur's Gate 2 Enhanced Edition

PKG_L10N_ID_BG2="${GAME_ID_BG2}-l10n"
PKG_L10N_FR_ID_BG2="${PKG_L10N_ID_BG2}-fr"
PKG_L10N_PROVIDES_BG2="
$PKG_L10N_ID_BG2"
PKG_L10N_FR_PROVIDES_BG2="$PKG_L10N_PROVIDES_BG2"

PKG_DATA_ID_BG2="${GAME_ID_BG2}-data"
## Ensure easy upgrades from packaged generated with pre-20250103.1 game scripts
PKG_DATA_PROVIDES_BG2="${PKG_DATA_PROVIDES_BG2:-}
baldurs-gate-2-enhanced-edition-l10n-en"

## Icewind Dale 1 Enhanced Edition

PKG_L10N_ID_IWD1="${GAME_ID_IWD1}-l10n"
PKG_L10N_FR_ID_IWD1="${PKG_L10N_ID_IWD1}-fr"
PKG_L10N_PROVIDES_IWD1="
$PKG_L10N_ID_IWD1"
PKG_L10N_FR_PROVIDES_IWD1="$PKG_L10N_PROVIDES_IWD1"

PKG_DATA_ID_IWD1="${GAME_ID_IWD1}-data"
## Ensure easy upgrades from packaged generated with pre-20250103.1 game scripts
PKG_DATA_PROVIDES_IWD1="${PKG_DATA_PROVIDES_IWD1:-}
icewind-dale-1-enhanced-edition-l10n-en"

## Planescape: Torment Enhanced Edition

PKG_L10N_ID_PST="${GAME_ID_PST}-l10n"
PKG_L10N_FR_ID_PST="${PKG_L10N_ID_PST}-fr"
PKG_L10N_PROVIDES_PST="
$PKG_L10N_ID_PST"
PKG_L10N_FR_PROVIDES_PST="$PKG_L10N_PROVIDES_PST"

PKG_DATA_ID_PST="${GAME_ID_PST}-data"
## Ensure easy upgrades from packaged generated with pre-20250103.1 game scripts
PKG_DATA_PROVIDES_PST="${PKG_DATA_PROVIDES_PST:-}
planescape-torment-enhanced-edition-l10n-en"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the use of system-provided SDL
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(launcher_tweak_sdl_override)"

## The game segfaults on launch when SDL_VIDEODRIVER is set to "wayland".
## Forcing the use of system-provided SDL with SDL_DYNAMIC_API does not help.
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')"'
# The game segfaults on launch when SDL_VIDEODRIVER is set to "wayland".
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_DATA'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_DATA' 'PKG_L10N_FR'

# Clean up

working_directory_cleanup

exit 0
