#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rayman Origins
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='rayman-origins'
GAME_NAME='Rayman Origins'

ARCHIVE_BASE_HUMBLE_0_NAME='RaymanOrigins_windows.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='f9e657afbfac436fe2aea720cdc72196'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0.32504-humble'
ARCHIVE_BASE_HUMBLE_0_SIZE='2400000'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/rayman-origins'

ARCHIVE_BASE_GOG_0_NAME='setup_rayman_origins_1.0.32504_(18757).exe'
ARCHIVE_BASE_GOG_0_MD5='a1021275180a433cd26ccb708c03dde4'
ARCHIVE_BASE_GOG_0_TYPE='innosetup'
ARCHIVE_BASE_GOG_0_PART1_NAME='setup_rayman_origins_1.0.32504_(18757)-1.bin'
ARCHIVE_BASE_GOG_0_PART1_MD5='813c51f290371869157b62b26abad411'
ARCHIVE_BASE_GOG_0_SIZE='2500000'
ARCHIVE_BASE_GOG_0_VERSION='1.0.32504-gog18757'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/rayman_origins'

CONTENT_PATH_DEFAULT_GOG='app'
CONTENT_PATH_DEFAULT_HUMBLE='game'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe
*.ini'
CONTENT_GAME_DATA_FILES='
gamedata'
## These documentation files are not provided by the Humble Bundle archive
CONTENT_DOC_DATA_RELATIVE_PATH_GOG='support'
CONTENT_DOC_DATA_FILES_GOG='
*.pdf
*.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Rayman Origins'
## Work around rendering issues making the game menu unusable.
WINE_WINETRICKS_VERBS='d3dcompiler_47'

APPLICATIONS_LIST='APP_MAIN'
APPLICATIONS_LIST_GOG="$APPLICATIONS_LIST APP_L10N"

APP_MAIN_EXE='rayman origins.exe'

## This application is only provided by the gog.com archive.
APP_L10N_ID="${GAME_ID}-language-setup"
APP_L10N_NAME="${GAME_NAME} - Language setup"
APP_L10N_CAT='Settings'
APP_L10N_EXE='language_setup.exe'
APP_L10N_ICON='rayman origins.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Set extra tools required to extract data from the Humble Bundle archive

## Warning: REQUIREMENTS_LIST has no support for contextual values as of ./play.it 2.31
REQUIREMENTS_LIST_HUMBLE="${REQUIREMENTS_LIST:-}
dd
truncate
unshield"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the availability of the extra tools required to extract data from the Humble Bundle archive

REQUIREMENTS_LIST=$(context_value 'REQUIREMENTS_LIST')
requirements_check

# Extract game data

archive_extraction_default
case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE'*)
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/RaymondOrigins_windows/Rayman Origins.exe"
		information_archive_data_extraction "$(archive_name 'ARCHIVE_INNER')"
		(
			cd "${PLAYIT_WORKDIR}/gamedata"

			archive_path=$(archive_path 'ARCHIVE_INNER')
			dd \
				if="$archive_path" \
				of='data1.hdr' \
				bs=3 skip=7740856 count=11107 2>/dev/null
			dd \
				if="$archive_path" \
				of='data1.cab' \
				bs=8 skip=2655105 count=247706 2>/dev/null
	
			## The extraction of data2.cab is done in two steps with big block size values.
			## This is a big file that would take a lot of time to get using a small block size.
			dd \
				if="$archive_path" \
				of='data2.cab' \
				bs=567219 skip=41 2>/dev/null
			rm "$archive_path"
			truncate --io-blocks --size=2125482963 'data2.cab'
		)
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/data1.hdr"
		ARCHIVE_INNER_TYPE='installshield'
		archive_extraction 'ARCHIVE_INNER'
		rm \
			"${PLAYIT_WORKDIR}/gamedata/data1.hdr" \
			"${PLAYIT_WORKDIR}/gamedata/data1.cab" \
			"${PLAYIT_WORKDIR}/gamedata/data2.cab"
	;;
esac

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
