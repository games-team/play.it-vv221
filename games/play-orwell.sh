#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Orwell series:
# - Orwell
# - Orwell: Ignorance is Strength
###

script_version=20241126.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_ORWELL1='orwell-1'
GAME_NAME_ORWELL1='Orwell'

GAME_ID_ORWELL2='orwell-2'
GAME_NAME_ORWELL2='Orwell: Ignorance is Strength'

# Archives

## Orwell

ARCHIVE_BASE_ORWELL1_2_NAME='orwell_1_4_7424_39231.sh'
ARCHIVE_BASE_ORWELL1_2_MD5='8ac1915d9de7532717730b7f33a544b9'
ARCHIVE_BASE_ORWELL1_2_SIZE='570000'
ARCHIVE_BASE_ORWELL1_2_VERSION='1.4.7424.gog39231'
ARCHIVE_BASE_ORWELL1_2_URL='https://www.gog.com/game/orwell'

ARCHIVE_BASE_ORWELL1_1_NAME='orwell_en_1_21_21014.sh'
ARCHIVE_BASE_ORWELL1_1_MD5='8fcd84cd3989175d1da377c78e1b4ff2'
ARCHIVE_BASE_ORWELL1_1_SIZE='360000'
ARCHIVE_BASE_ORWELL1_1_VERSION='1.21-gog21014'

ARCHIVE_BASE_ORWELL1_0_NAME='gog_orwell_2.0.0.1.sh'
ARCHIVE_BASE_ORWELL1_0_MD5='471470546952015fce024bdcb5431a07'
ARCHIVE_BASE_ORWELL1_0_SIZE='630000'
ARCHIVE_BASE_ORWELL1_0_VERSION='1.1-gog2.0.0.1'

### Optional icons pack

ARCHIVE_OPTIONAL_ICONS_NAME_ORWELL1='orwell-1_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5_ORWELL1='acb6711e5f6b26828097b2828f74e522'
ARCHIVE_OPTIONAL_ICONS_URL_ORWELL1='https://downloads.dotslashplay.it/games/orwell-1/'
CONTENT_ICONS_PATH_ORWELL1='.'
CONTENT_ICONS_FILES_ORWELL1='
128x128'

## Orwell: Ignorance is Strength

ARCHIVE_BASE_ORWELL2_1_NAME='orwell_ignorance_is_strength_en_1_1_6771_23686_22333.sh'
ARCHIVE_BASE_ORWELL2_1_MD5='a69fb6e02fdce982cb3a56b2b32a70e6'
ARCHIVE_BASE_ORWELL2_1_SIZE='730000'
ARCHIVE_BASE_ORWELL2_1_VERSION='1.1.6771-gog22333'
ARCHIVE_BASE_ORWELL2_1_URL='https://www.gog.com/game/orwell_ignorance_is_strength'

ARCHIVE_BASE_ORWELL2_0_NAME='orwell_ignorance_is_strength_en_1_11_21014.sh'
ARCHIVE_BASE_ORWELL2_0_MD5='3d50e953ad7029ac9a292bcc9b137426'
ARCHIVE_BASE_ORWELL2_0_SIZE='730000'
ARCHIVE_BASE_ORWELL2_0_VERSION='1.1.6717-gog21014'


UNITY3D_NAME_ORWELL1='Orwell'
UNITY3D_NAME_ORWELL2='Ignorance'
UNITY3D_PLUGINS_ORWELL1='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_DOC_DATA_FILES='
*.txt'

## Both games use the same path.
FAKE_HOME_PERSISTENT_DIRECTORIES='
My Games/Orwell'
## Orwell 1 - The original game icon is not provided by the GOG installer for Orwell 1.4.
##            Other archives can use the default icon path for Unity3D games.
APP_MAIN_ICON_ORWELL1_2='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID_ORWELL1="${GAME_ID_ORWELL1}-data"
PKG_DATA_ID_ORWELL2="${GAME_ID_ORWELL2}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
