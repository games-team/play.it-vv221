#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Grim Dawn
###

script_version=20250211.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='grim-dawn'
GAME_NAME='Grim Dawn'

ARCHIVE_BASE_9_NAME='setup_grim_dawn_1.2.1.5a_(64bit)_(79771).exe'
ARCHIVE_BASE_9_MD5='298b7115cb10d141474e3deada484cfa'
ARCHIVE_BASE_9_TYPE='innosetup'
ARCHIVE_BASE_9_PART1_NAME='setup_grim_dawn_1.2.1.5a_(64bit)_(79771)-1.bin'
ARCHIVE_BASE_9_PART1_MD5='f3fd27887478cf9e4b91a9ccec978a5e'
ARCHIVE_BASE_9_PART2_NAME='setup_grim_dawn_1.2.1.5a_(64bit)_(79771)-2.bin'
ARCHIVE_BASE_9_PART2_MD5='8da34220573b78a73028f4aa8945f660'
ARCHIVE_BASE_9_SIZE='4837497'
ARCHIVE_BASE_9_VERSION='1.2.1.5a-gog79771'
ARCHIVE_BASE_9_URL='https://www.gog.com/game/grim_dawn'

ARCHIVE_BASE_8_NAME='setup_grim_dawn_1.2.1.4_hotfix_1_(64bit)_(78646).exe'
ARCHIVE_BASE_8_MD5='f8578ada2efb2b5a8a2342c18b15d9eb'
ARCHIVE_BASE_8_TYPE='innosetup'
ARCHIVE_BASE_8_PART1_NAME='setup_grim_dawn_1.2.1.4_hotfix_1_(64bit)_(78646)-1.bin'
ARCHIVE_BASE_8_PART1_MD5='b43dcb38aafbb74c841a568f58adb44a'
ARCHIVE_BASE_8_PART2_NAME='setup_grim_dawn_1.2.1.4_hotfix_1_(64bit)_(78646)-2.bin'
ARCHIVE_BASE_8_PART2_MD5='b3cfefce65e64effc0d2a3e00a204fec'
ARCHIVE_BASE_8_SIZE='4820603'
ARCHIVE_BASE_8_VERSION='1.2.1.4-gog78646'

ARCHIVE_BASE_7_NAME='setup_grim_dawn_1.2.1.2_(64bit)_(74537).exe'
ARCHIVE_BASE_7_MD5='defc9ce6611401d67b04c3c86252ca9b'
ARCHIVE_BASE_7_TYPE='innosetup'
ARCHIVE_BASE_7_PART1_NAME='setup_grim_dawn_1.2.1.2_(64bit)_(74537)-1.bin'
ARCHIVE_BASE_7_PART1_MD5='46d0391d8757224f8157792839424dcd'
ARCHIVE_BASE_7_SIZE='4782087'
ARCHIVE_BASE_7_VERSION='1.2.1.2-gog74537'

ARCHIVE_BASE_6_NAME='setup_grim_dawn_1.2.1.1_(64bit)_(74365).exe'
ARCHIVE_BASE_6_MD5='419c765ff7d14cc19142774ae44c3eb9'
ARCHIVE_BASE_6_TYPE='innosetup'
ARCHIVE_BASE_6_PART1_NAME='setup_grim_dawn_1.2.1.1_(64bit)_(74365)-1.bin'
ARCHIVE_BASE_6_PART1_MD5='e3f6f66a0aa88c6f7a47e9c6a07c208b'
ARCHIVE_BASE_6_SIZE='4787490'
ARCHIVE_BASE_6_VERSION='1.2.1.1-gog74365'

ARCHIVE_BASE_5_NAME='setup_grim_dawn_1.2.0.5a_(64bit)_(71558).exe'
ARCHIVE_BASE_5_MD5='f232c25a9c69a5776dd4ce65b40df6df'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_PART1_NAME='setup_grim_dawn_1.2.0.5a_(64bit)_(71558)-1.bin'
ARCHIVE_BASE_5_PART1_MD5='c78a59aa61b124030e6d96cabaaabd2d'
ARCHIVE_BASE_5_SIZE='4768460'
ARCHIVE_BASE_5_VERSION='1.2.0.5a-gog71558'

ARCHIVE_BASE_4_NAME='setup_grim_dawn_1.2.0.5_(64bit)_(71516).exe'
ARCHIVE_BASE_4_MD5='0ec082fed3fa62b70979fcf9fd5ed919'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_PART1_NAME='setup_grim_dawn_1.2.0.5_(64bit)_(71516)-1.bin'
ARCHIVE_BASE_4_PART1_MD5='8f7dd61efc9962fddd88dc19da4a8efe'
ARCHIVE_BASE_4_SIZE='4767825'
ARCHIVE_BASE_4_VERSION='1.2.0.5-gog71516'

ARCHIVE_BASE_3_NAME='setup_grim_dawn_1.2.0.3_hotfix_3_(64bit)_(69499).exe'
ARCHIVE_BASE_3_MD5='d407d7f0c13b2e9a09c9d507a6dd9406'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_PART1_NAME='setup_grim_dawn_1.2.0.3_hotfix_3_(64bit)_(69499)-1.bin'
ARCHIVE_BASE_3_PART1_MD5='cb0b21ec562a0023650928cabd3f2aac'
ARCHIVE_BASE_3_SIZE='4716376'
ARCHIVE_BASE_3_VERSION='1.2.0.3-gog69499'

ARCHIVE_BASE_2_NAME='setup_grim_dawn_1.2.0.2_hotfix_2_(64bit)_(69134).exe'
ARCHIVE_BASE_2_MD5='a86614183a27085a644642e73c89dc31'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_grim_dawn_1.2.0.2_hotfix_2_(64bit)_(69134)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='d4088f8591111a2aa1c5af081179ec38'
ARCHIVE_BASE_2_SIZE='4414600'
ARCHIVE_BASE_2_VERSION='1.2.0.2-gog69134'

ARCHIVE_BASE_1_NAME='setup_grim_dawn_1.2.0.1_hotfix_1_(64bit)_(69098).exe'
ARCHIVE_BASE_1_MD5='0b4edacfee791cc6d83af7a2c36cb11c'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_grim_dawn_1.2.0.1_hotfix_1_(64bit)_(69098)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='9450bc1b82d609709356e7b3bb76570f'
ARCHIVE_BASE_1_SIZE='4411128'
ARCHIVE_BASE_1_VERSION='1.2.0.1-gog69098'

ARCHIVE_BASE_0_NAME='setup_grim_dawn_1.1.9.8_(64bit)_(65199).exe'
ARCHIVE_BASE_0_MD5='c0fb173132b21833052d1496f6ab832e'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_grim_dawn_1.1.9.8_(64bit)_(65199)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='d97de2b8cd7ae7aa723e720ec2fa21f5'
ARCHIVE_BASE_0_SIZE='4332472'
ARCHIVE_BASE_0_VERSION='1.1.9.8-gog65199'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
x64
crashreport.dll
d3d9.dll
devil.dll
dinput8.dll
direct3d.dll
direct3d11.dll
directinput.dll
ds8sound.dll
dxgi.dll
engine.dll
freeimage.dll
game.dll
libogg.dll
libvorbis.dll
libvorbisfile.dll
lua51.dll
sourcecontrol.dll
statreport.dll
ucrtbase.dll
widget.dll
xinput1_3.dll
zlibwapi.dll
api-ms-win-*.dll
aifeditor.exe
animationcompiler.exe
archivetool.exe
assetmanager.exe
bitmapcreator.exe
conversationeditor.exe
crashreporter.exe
dbreditor.exe
editor.exe
fontcompiler.exe
grim dawn.exe
mapcompiler.exe
modelcompiler.exe
pseditor.exe
questeditor.exe
repair.exe
shadercompiler.exe
sourceserver.exe
texturecompiler.exe
texviewer.exe
viewer.exe
*.dlo'
## TODO: Check if the Galaxy library are required
CONTENT_GAME0_BIN_FILES='
galaxy.dll
goggalaxyhooks.dll'
CONTENT_GAME_DATA_FILES='
database
resources
video
*.zip
minimap.atn'
CONTENT_DOC_DATA_FILES='
grim dawn modding guide.pdf'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Grim Dawn'
## The following winetricks verb prevents a rendering failure with WINE 9.0,
## with the following error repeated many times if WINE debug is enabled:
## 019c:fixme:d3dcompiler:skip_u32_unknown
WINE_WINETRICKS_VERBS='d3dcompiler_43'

APP_MAIN_EXE='x64/grim dawn.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
