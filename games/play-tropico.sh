#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tropico games:
# - Tropico 1
# - Tropico 2
###

script_version=20241103.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_TROPICO1='tropico-1'
GAME_NAME_TROPICO1='Tropico'

GAME_ID_TROPICO2='tropico-2'
GAME_NAME_TROPICO2='Tropico 2: Pirate Cove'

# Archives

## Tropico 1

ARCHIVE_BASE_TROPICO1_EN_0_NAME='setup_tropico_2.1.0.14.exe'
ARCHIVE_BASE_TROPICO1_EN_0_MD5='1bd761bc4a40a42a9caeb41c70d46465'
ARCHIVE_BASE_TROPICO1_EN_0_TYPE='innosetup'
ARCHIVE_BASE_TROPICO1_EN_0_SIZE='1400000'
ARCHIVE_BASE_TROPICO1_EN_0_VERSION='1.5.3-gog2.1.0.14'
ARCHIVE_BASE_TROPICO1_EN_0_URL='https://www.gog.com/game/tropico_reloaded'

ARCHIVE_BASE_TROPICO1_FR_0_NAME='setup_tropico_french_2.1.0.14.exe'
ARCHIVE_BASE_TROPICO1_FR_0_MD5='aad4ea5a6fe2b2c2f347cfa7aae058b3'
ARCHIVE_BASE_TROPICO1_FR_0_TYPE='innosetup'
ARCHIVE_BASE_TROPICO1_FR_0_SIZE='1400000'
ARCHIVE_BASE_TROPICO1_FR_0_VERSION='1.5.3-gog2.1.0.14'
ARCHIVE_BASE_TROPICO1_FR_0_URL='https://www.gog.com/game/tropico_reloaded'

## Tropico 2

ARCHIVE_BASE_TROPICO2_EN_0_NAME='setup_tropico2_2.1.0.14.exe'
ARCHIVE_BASE_TROPICO2_EN_0_MD5='59a41778988f4b0a45d144f29187ffd8'
ARCHIVE_BASE_TROPICO2_EN_0_TYPE='innosetup'
ARCHIVE_BASE_TROPICO2_EN_0_SIZE='1900000'
ARCHIVE_BASE_TROPICO2_EN_0_VERSION='1.20-gog2.1.0.14'
ARCHIVE_BASE_TROPICO2_EN_0_URL='https://www.gog.com/game/tropico_reloaded'

ARCHIVE_BASE_TROPICO2_FR_0_NAME='setup_tropico2_french_2.1.0.14.exe'
ARCHIVE_BASE_TROPICO2_FR_0_MD5='e9cb36d88a03fd65b7152c815f05a7cc'
ARCHIVE_BASE_TROPICO2_FR_0_TYPE='innosetup'
ARCHIVE_BASE_TROPICO2_FR_0_SIZE='1900000'
ARCHIVE_BASE_TROPICO2_FR_0_VERSION='1.20-gog2.1.0.14'
ARCHIVE_BASE_TROPICO2_FR_0_URL='https://www.gog.com/game/tropico_reloaded'


CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.asi
*.dll
*.exe
*.ini'
CONTENT_GAME_L10N_FILES='
maps
data/soun.{}
data/text.{}
data2/*.cfg
data2/*.lng
data2/*.txt
data2/x1.dap
data2/x2.dap
movies/*.txt
movies/campaigntoexit.bik
movies/desktooutside.bik
movies/s_f2o.bik
movies/s_m2o.bik
movies/s_s2o.bik'
CONTENT_GAME_DATA_FILES='
data
data2
movies
voices'
CONTENT_DOC_L10N_FILES='
*.doc
*.pdf
*.rtf
*.txt'

USER_PERSISTENT_DIRECTORIES='
campaign
games
maps
save'
USER_PERSISTENT_FILES='
*.ini
*.cfg
*.dat'

WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE_TROPICO1='tropico.exe'
APP_MAIN_EXE_TROPICO2='tropico2.exe'

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_DESCRIPTION='data'

## Tropico 1

PKG_L10N_BASE_ID_TROPICO1="${GAME_ID_TROPICO1}-l10n"
PKG_L10N_ID_TROPICO1_EN="${PKG_L10N_BASE_ID_TROPICO1}-en"
PKG_L10N_ID_TROPICO1_FR="${PKG_L10N_BASE_ID_TROPICO1}-fr"
PKG_L10N_PROVIDES_TROPICO1="
$PKG_L10N_BASE_ID_TROPICO1"
PKG_L10N_DESCRIPTION_TROPICO1_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_TROPICO1_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_TROPICO1="${GAME_ID_TROPICO1}-data"

## Tropico 2

PKG_L10N_BASE_ID_TROPICO2="${GAME_ID_TROPICO2}-l10n"
PKG_L10N_ID_TROPICO2_EN="${PKG_L10N_BASE_ID_TROPICO2}-en"
PKG_L10N_ID_TROPICO2_FR="${PKG_L10N_BASE_ID_TROPICO2}-fr"
PKG_L10N_PROVIDES_TROPICO2="
$PKG_L10N_BASE_ID_TROPICO2"
PKG_L10N_DESCRIPTION_TROPICO2_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_TROPICO2_FR="$PKG_L10N_DESCRIPTION_FR"

PKG_DATA_ID_TROPICO2="${GAME_ID_TROPICO2}-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Run extra depencies check based on source archive

## Tropico 2 - Use software rendering by default.
case "$(current_archive)" in
	('ARCHIVE_BASE_TROPICO2_'*)
		REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
		dos2unix"
		requirements_check
	;;
esac

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Tropico 1 - Disable censorship in French version.
	case "$(current_archive)" in
		('ARCHIVE_BASE_TROPICO1_FR_'*)
			localization_file='data2/tropico.lng'
			sed_expression='s/^\( \+406 .\+militaire\) \(.\+\)/\1  \2/'
			## Force standard locale to avoid choking on some characters.
			LANG=C sed --in-place --expression="$sed_expression" "$localization_file"
		;;
	esac

	## Tropico 2 - Use software rendering by default.
	case "$(current_archive)" in
		('ARCHIVE_BASE_TROPICO2_'*)
			ini_file='tropico2.ini'
			ini_field='SoftwareDevice'
			ini_value='1'
			sed_expression="s/^${ini_field}=.*$/${ini_field}=${ini_value}/"
			unix2dos --quiet "$ini_file"
			sed --in-place --expression="$sed_expression" "$ini_file"
			dos2unix --quiet "$ini_file"
		;;
	esac

	## Tropico 2 - Fix a file name encoding, to prevent a crash when selecting this map.
	case "$(current_archive)" in
		('ARCHIVE_BASE_TROPICO2_FR_'*)
			if [ -e 'maps/6-pičges en eaux troubles.{}' ]; then
				mv 'maps/6-pičges en eaux troubles.{}' 'maps/6-pièges en eaux troubles.{}'
				mv 'maps/6-pičges en eaux troubles.txt' 'maps/6-pièges en eaux troubles.txt'
			fi
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
