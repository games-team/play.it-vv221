#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# War for the Overworld expansions:
# - My Pet Dungeon
# - Heart of Gold
# - The Under Games
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='war-for-the-overworld'
GAME_NAME='War for the Overworld'

EXPANSION_ID_PETDUNGEON='my-pet-dungeon'
EXPANSION_NAME_PETDUNGEON='My Pet Dungeon'

EXPANSION_ID_HEARTOFGOLD='heart-of-gold'
EXPANSION_NAME_HEARTOFGOLD='Heart of Gold'

EXPANSION_ID_UNDERGAMES='the-under-games'
EXPANSION_NAME_UNDERGAMES='The Under Games'

# Archives

## My Pet Dungeon

ARCHIVE_BASE_PETDUNGEON_7_NAME='war_for_the_overworld_my_pet_dungeon_v2_1_2_76431.sh'
ARCHIVE_BASE_PETDUNGEON_7_MD5='4561b9cb3f6405c7c576da87e9e74175'
ARCHIVE_BASE_PETDUNGEON_7_SIZE='1048'
ARCHIVE_BASE_PETDUNGEON_7_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_PETDUNGEON_7_URL='https://www.gog.com/game/war_for_the_overworld_my_pet_dungeon'

ARCHIVE_BASE_PETDUNGEON_6_NAME='war_for_the_overworld_my_pet_dungeon_v2_1_1_73576.sh'
ARCHIVE_BASE_PETDUNGEON_6_MD5='435ec9378fbefad46ad4827d870b84e1'
ARCHIVE_BASE_PETDUNGEON_6_SIZE='1048'
ARCHIVE_BASE_PETDUNGEON_6_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_PETDUNGEON_5_NAME='war_for_the_overworld_my_pet_dungeon_v2_1_0f4_55096.sh'
ARCHIVE_BASE_PETDUNGEON_5_MD5='5aadd3ab719d2fd71df604708b9a29e1'
ARCHIVE_BASE_PETDUNGEON_5_SIZE='1400'
ARCHIVE_BASE_PETDUNGEON_5_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_PETDUNGEON_4_NAME='war_for_the_overworld_my_pet_dungeon_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_PETDUNGEON_4_MD5='55fd7104b3c3141bc38a2809ae470c92'
ARCHIVE_BASE_PETDUNGEON_4_SIZE='1400'
ARCHIVE_BASE_PETDUNGEON_4_VERSION='2.0.7f1-gog36563'

ARCHIVE_BASE_PETDUNGEON_3_NAME='war_for_the_overworld_my_pet_dungeon_2_0_7f1_30014.sh'
ARCHIVE_BASE_PETDUNGEON_3_MD5='f9cd5f6fbe46d46c98837410f8cbfeee'
ARCHIVE_BASE_PETDUNGEON_3_SIZE='1400'
ARCHIVE_BASE_PETDUNGEON_3_VERSION='2.0.6f1-gog30014'

ARCHIVE_BASE_PETDUNGEON_2_NAME='war_for_the_overworld_my_pet_dungeon_2_0_6f1_24637.sh'
ARCHIVE_BASE_PETDUNGEON_2_MD5='7788aeeee1e9c7cd365eb595e772ff52'
ARCHIVE_BASE_PETDUNGEON_2_SIZE='1400'
ARCHIVE_BASE_PETDUNGEON_2_VERSION='2.0.6f1-gog24637'

ARCHIVE_BASE_PETDUNGEON_1_NAME='war_for_the_overworld_my_pet_dungeon_2_0_5_24177.sh'
ARCHIVE_BASE_PETDUNGEON_1_MD5='eb45d5ee8c699d9ded7d15b82ad1efa3'
ARCHIVE_BASE_PETDUNGEON_1_SIZE='1400'
ARCHIVE_BASE_PETDUNGEON_1_VERSION='2.0.5-gog24177'

## Heart of Gold

ARCHIVE_BASE_HEARTOFGOLD_4_NAME='war_for_the_overworld_heart_of_gold_v2_1_2_76431.sh'
ARCHIVE_BASE_HEARTOFGOLD_4_MD5='c3acb781eda622962857b70416fc7a54'
ARCHIVE_BASE_HEARTOFGOLD_4_SIZE='1048'
ARCHIVE_BASE_HEARTOFGOLD_4_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_HEARTOFGOLD_4_URL='https://www.gog.com/game/war_for_the_overworld_heart_of_gold'

ARCHIVE_BASE_HEARTOFGOLD_3_NAME='war_for_the_overworld_heart_of_gold_v2_1_1_73576.sh'
ARCHIVE_BASE_HEARTOFGOLD_3_MD5='653ab69838583a231b7cc8128286e183'
ARCHIVE_BASE_HEARTOFGOLD_3_SIZE='1048'
ARCHIVE_BASE_HEARTOFGOLD_3_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_HEARTOFGOLD_2_NAME='war_for_the_overworld_heart_of_gold_v2_1_0f4_55096.sh'
ARCHIVE_BASE_HEARTOFGOLD_2_MD5='c492a072ab257817abeb7c80f76c1eca'
ARCHIVE_BASE_HEARTOFGOLD_2_SIZE='1400'
ARCHIVE_BASE_HEARTOFGOLD_2_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_HEARTOFGOLD_1_NAME='war_for_the_overworld_heart_of_gold_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_HEARTOFGOLD_1_MD5='c512019fa698759ba0a96e79fadcb06e'
ARCHIVE_BASE_HEARTOFGOLD_1_SIZE='1400'
ARCHIVE_BASE_HEARTOFGOLD_1_VERSION='2.0.7f1-gog36563'

ARCHIVE_BASE_HEARTOFGOLD_0_NAME='war_for_the_overworld_heart_of_gold_2_0_7f1_30014.sh'
ARCHIVE_BASE_HEARTOFGOLD_0_MD5='2d3555bfea2aafca9ff9e8eb7a970c0d'
ARCHIVE_BASE_HEARTOFGOLD_0_SIZE='1400'
ARCHIVE_BASE_HEARTOFGOLD_0_VERSION='2.0.6f1-gog30014'

## The Under Games

ARCHIVE_BASE_UNDERGAMES_3_NAME='war_for_the_overworld_the_under_games_v2_1_2_76431.sh'
ARCHIVE_BASE_UNDERGAMES_3_MD5='09378c271fee3fd4eac40d14f6161831'
ARCHIVE_BASE_UNDERGAMES_3_SIZE='1048'
ARCHIVE_BASE_UNDERGAMES_3_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_UNDERGAMES_3_URL='https://www.gog.com/game/war_for_the_overworld_the_under_games'

ARCHIVE_BASE_UNDERGAMES_2_NAME='war_for_the_overworld_the_under_games_v2_1_1_73576.sh'
ARCHIVE_BASE_UNDERGAMES_2_MD5='820594211c64b3fe94716921c1da3f73'
ARCHIVE_BASE_UNDERGAMES_2_SIZE='1048'
ARCHIVE_BASE_UNDERGAMES_2_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_UNDERGAMES_1_NAME='war_for_the_overworld_the_under_games_v2_1_0f4_55096.sh'
ARCHIVE_BASE_UNDERGAMES_1_MD5='8c7bdf2299a2cc5795f92e58c94aa57b'
ARCHIVE_BASE_UNDERGAMES_1_SIZE='1400'
ARCHIVE_BASE_UNDERGAMES_1_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_UNDERGAMES_0_NAME='war_for_the_overworld_the_under_games_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_UNDERGAMES_0_MD5='c63ba259c40ab080f697fe03678d287e'
ARCHIVE_BASE_UNDERGAMES_0_SIZE='1400'
ARCHIVE_BASE_UNDERGAMES_0_VERSION='2.0.7f1-gog36563'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
goggame-*.info'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
