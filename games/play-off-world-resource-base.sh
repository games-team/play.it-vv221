#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Off-World Resource Base
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='off-world-resource-base'
GAME_NAME='O.R.B.: Off-World Resource Base'

ARCHIVE_BASE_0_NAME='setup_orb_2.0.0.5.exe'
ARCHIVE_BASE_0_MD5='b1cab44f6fe398ed008dda23e1538c03'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='651435'
ARCHIVE_BASE_0_VERSION='1.04-gog2.0.0.5'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/orb_offworld_resource_base'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
bobo.dat
binkw32.dll
c4dll-d.dll
dbghelp.dll
mss32.dll
msvcrtd.dll
msvcrt.dll
orb.exe
orbscenarioeditor.exe
orbsetup.exe
lensflares.ini
netcolors.ini
orb.ini
orbsetup.ini
*.asi
*.flt
*.m3d'
CONTENT_GAME_DATA_FILES='
orbdata
cdkey
game.positions
credits.txt
*.bmp
*.flg
*.sup'
CONTENT_DOC_DATA_FILES='
readme.txt
scen_eula.txt
*.pdf'

## The intro video and game menu fail to render
## unless a virtual desktop is used (WINE 9.0).
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='orb.exe'

APP_EDITOR_ID="${GAME_ID}-editor"
APP_EDITOR_NAME="$GAME_NAME - Scenario editor"
APP_EDITOR_EXE='orbscenarioeditor.exe'

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_NAME="$GAME_NAME - Settings"
APP_SETUP_CAT='Settings'
APP_SETUP_EXE='orbsetup.exe'

USER_PERSISTENT_DIRECTORIES='
orbdata/players
orbdata/scenarios'
USER_PERSISTENT_FILES='
orb.ini
orbdata/frontend/players.dat
orbdata/language/current.lang'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
