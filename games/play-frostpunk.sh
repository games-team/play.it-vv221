#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Frostpunk
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='frostpunk'
GAME_NAME='Frostpunk'

ARCHIVE_BASE_8_NAME='setup_frostpunk_1.6.1_51852_59618_(51757).exe'
ARCHIVE_BASE_8_MD5='6d1f0d7c81821c949c5a7eb9c7f26133'
ARCHIVE_BASE_8_TYPE='innosetup'
ARCHIVE_BASE_8_PART1_NAME='setup_frostpunk_1.6.1_51852_59618_(51757)-1.bin'
ARCHIVE_BASE_8_PART1_MD5='43f6adbfdc53299d4591a2abb1c84457'
ARCHIVE_BASE_8_PART2_NAME='setup_frostpunk_1.6.1_51852_59618_(51757)-2.bin'
ARCHIVE_BASE_8_PART2_MD5='6689f6a289b899ddcd69af920f4ecdc4'
ARCHIVE_BASE_8_PART3_NAME='setup_frostpunk_1.6.1_51852_59618_(51757)-3.bin'
ARCHIVE_BASE_8_PART3_MD5='a72c123ddeef82cdebeed57792b02e4c'
ARCHIVE_BASE_8_SIZE='9593582'
ARCHIVE_BASE_8_VERSION='1.6.1-gog51757'
ARCHIVE_BASE_8_URL='https://www.gog.com/game/frostpunk'

ARCHIVE_BASE_7_NAME='setup_frostpunk_1.6.1_51795_59550_(42925).exe'
ARCHIVE_BASE_7_MD5='77bc92a7242dea010d766cf83bbace36'
ARCHIVE_BASE_7_TYPE='innosetup'
ARCHIVE_BASE_7_PART1_NAME='setup_frostpunk_1.6.1_51795_59550_(42925)-1.bin'
ARCHIVE_BASE_7_PART1_MD5='cfdfccd316e3e1b82e285f9a54f3307a'
ARCHIVE_BASE_7_PART2_NAME='setup_frostpunk_1.6.1_51795_59550_(42925)-2.bin'
ARCHIVE_BASE_7_PART2_MD5='ff9cc388079e949bd1ac0f888315282b'
ARCHIVE_BASE_7_PART3_NAME='setup_frostpunk_1.6.1_51795_59550_(42925)-3.bin'
ARCHIVE_BASE_7_PART3_MD5='966cf1daaa31538c2dfa3b2125f86c34'
ARCHIVE_BASE_7_SIZE='9500000'
ARCHIVE_BASE_7_VERSION='1.6.1-gog42925'

ARCHIVE_BASE_6_NAME='setup_frostpunk_1.6.1_51791_59537_(42472).exe'
ARCHIVE_BASE_6_MD5='ec9795eb841cde30cb62d9983517aa68'
ARCHIVE_BASE_6_TYPE='innosetup'
ARCHIVE_BASE_6_PART1_NAME='setup_frostpunk_1.6.1_51791_59537_(42472)-1.bin'
ARCHIVE_BASE_6_PART1_MD5='cb3996ecfa56f3ee102317d21392c034'
ARCHIVE_BASE_6_PART2_NAME='setup_frostpunk_1.6.1_51791_59537_(42472)-2.bin'
ARCHIVE_BASE_6_PART2_MD5='80af46c57e1e2d0fdb4fd97e4ba685f2'
ARCHIVE_BASE_6_PART3_NAME='setup_frostpunk_1.6.1_51791_59537_(42472)-3.bin'
ARCHIVE_BASE_6_PART3_MD5='652fb81dda4ca761d6fa08eba603c268'
ARCHIVE_BASE_6_SIZE='9600000'
ARCHIVE_BASE_6_VERSION='1.6.1-gog42472'

ARCHIVE_BASE_5_NAME='setup_frostpunk_1.6.0_hotfix_candidate_3_(40765).exe'
ARCHIVE_BASE_5_MD5='103d278de0b32670596d48fa0a3e1e7a'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_PART1_NAME='setup_frostpunk_1.6.0_hotfix_candidate_3_(40765)-1.bin'
ARCHIVE_BASE_5_PART1_MD5='fc12ac7dc545219d54c0d547f30f77be'
ARCHIVE_BASE_5_PART2_NAME='setup_frostpunk_1.6.0_hotfix_candidate_3_(40765)-2.bin'
ARCHIVE_BASE_5_PART2_MD5='8393d7ff2b240fa894ad0523f6ffd3a0'
ARCHIVE_BASE_5_PART3_NAME='setup_frostpunk_1.6.0_hotfix_candidate_3_(40765)-3.bin'
ARCHIVE_BASE_5_PART3_MD5='77e9e8c6cfa3953a33d82b5fa822f226'
ARCHIVE_BASE_5_SIZE='9800000'
ARCHIVE_BASE_5_VERSION='1.6.0-gog40765'

ARCHIVE_BASE_4_NAME='setup_frostpunk_1.6.0_(40599).exe'
ARCHIVE_BASE_4_MD5='da7acf8c314c798743645d218567960d'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_PART1_NAME='setup_frostpunk_1.6.0_(40599)-1.bin'
ARCHIVE_BASE_4_PART1_MD5='9ad1b9c0fe5ca877bbda422b465f81fc'
ARCHIVE_BASE_4_PART2_NAME='setup_frostpunk_1.6.0_(40599)-2.bin'
ARCHIVE_BASE_4_PART2_MD5='0aaed0691be9193110e8179e0468cd1c'
ARCHIVE_BASE_4_PART3_NAME='setup_frostpunk_1.6.0_(40599)-3.bin'
ARCHIVE_BASE_4_PART3_MD5='097ab0962232b97bb568350208e84dae'
ARCHIVE_BASE_4_SIZE='9800000'
ARCHIVE_BASE_4_VERSION='1.6.0-gog40599'

ARCHIVE_BASE_3_NAME='setup_frostpunk_1.5.0.51146.56648_(2020-02-14_17-22)_(36204).exe'
ARCHIVE_BASE_3_MD5='cc9bd3aba061dacd4f83e79e6a13d4e8'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_PART1_NAME='setup_frostpunk_1.5.0.51146.56648_(2020-02-14_17-22)_(36204)-1.bin'
ARCHIVE_BASE_3_PART1_MD5='ba31654b9f3b1d24b22df3999e5ff78e'
ARCHIVE_BASE_3_PART2_NAME='setup_frostpunk_1.5.0.51146.56648_(2020-02-14_17-22)_(36204)-2.bin'
ARCHIVE_BASE_3_PART2_MD5='fc560cb127691e6fdcd9e1a82f0e7b26'
ARCHIVE_BASE_3_PART3_NAME='setup_frostpunk_1.5.0.51146.56648_(2020-02-14_17-22)_(36204)-3.bin'
ARCHIVE_BASE_3_PART3_MD5='b82e3dc7bd981c8a30b8da51813acbf6'
ARCHIVE_BASE_3_SIZE='8700000'
ARCHIVE_BASE_3_VERSION='1.5.0-gog36204'

ARCHIVE_BASE_2_NAME='setup_frostpunk_1.5.0.51029.56354_(2020-01-21_1545)_(35558).exe'
ARCHIVE_BASE_2_MD5='c31ecb7aa2497bc12bf83324d52b413f'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_frostpunk_1.5.0.51029.56354_(2020-01-21_1545)_(35558)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='92cc54cc6c91f8435c3e7bc80fb22a20'
ARCHIVE_BASE_2_PART2_NAME='setup_frostpunk_1.5.0.51029.56354_(2020-01-21_1545)_(35558)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='22489f0c14736cd5c473a3e091dfed51'
ARCHIVE_BASE_2_PART3_NAME='setup_frostpunk_1.5.0.51029.56354_(2020-01-21_1545)_(35558)-3.bin'
ARCHIVE_BASE_2_PART3_MD5='24013bb92bf78991e110463579685461'
ARCHIVE_BASE_2_SIZE='8700000'
ARCHIVE_BASE_2_VERSION='1.5.0-gog35558'

ARCHIVE_BASE_1_NAME='setup_frostpunk_1.4.1.50110.53938_(2019-11-05_1825)_(33713).exe'
ARCHIVE_BASE_1_MD5='99b71af138d5fdcb67418392b0a14d62'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_frostpunk_1.4.1.50110.53938_(2019-11-05_1825)_(33713)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='3fbe880a3c3acaf00ede07c998b13c92'
ARCHIVE_BASE_1_PART2_NAME='setup_frostpunk_1.4.1.50110.53938_(2019-11-05_1825)_(33713)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='86267bb5dc870db828c50ccaa45d6091'
ARCHIVE_BASE_1_SIZE='6500000'
ARCHIVE_BASE_1_VERSION='1.4.1-gog33713'

ARCHIVE_BASE_0_NAME='setup_frostpunk_1.4.0.48534.51933_(2019-08-30_1543)_(32102).exe'
ARCHIVE_BASE_0_MD5='08e52207d9385bd5d3d66755facad69a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_frostpunk_1.4.0.48534.51933_(2019-08-30_1543)_(32102)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='60245c2ede7e99f526fa5cb87a660ebe'
ARCHIVE_BASE_0_PART2_NAME='setup_frostpunk_1.4.0.48534.51933_(2019-08-30_1543)_(32102)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='48dcdc8acb8bfd93b5eab09b8695854e'
ARCHIVE_BASE_0_SIZE='6500000'
ARCHIVE_BASE_0_VERSION='1.4.0-gog32102'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
anselsdk64.dll
libcurl.dll
libeay32.dll
libssh2.dll
pctouchhelper.dll
ssleay32.dll
frostpunk.exe
gfxconfig.ini'
## TODO: Check if the GOG Galaxy library is required
CONTENT_GAME0_BIN_FILES='
galaxy64.dll'
CONTENT_GAME_TEXTURES_FILES='
textures-s3.dat
textures-s3.idx'
CONTENT_GAME_DATA_PATH='app'
CONTENT_GAME_DATA_FILES='
custom_localizations.dat
voices.dat'
CONTENT_GAME0_DATA_FILES='
*.dat
*.idx
*.str'
CONTENT_DOC_DATA_FILES='
openfontlicense.txt'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/11bitstudios/Frostpunk'

USER_PERSISTENT_FILES='
gfxconfig.ini'

APP_MAIN_EXE='frostpunk.exe'
APP_MAIN_ICON='frostpunk.exe'
## The game crashes on launch with some SDL settings.
APP_MAIN_PRERUN='
# The game crashes on launch with some SDL settings
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
if [ "${SDL_AUDIODRIVER:-}" = "alsa" ]; then
	unset SDL_AUDIODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_TEXTURES
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_TEXTURES'

PKG_TEXTURES_ID="${GAME_ID}-textures"
PKG_TEXTURES_DESCRIPTION='textures'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
