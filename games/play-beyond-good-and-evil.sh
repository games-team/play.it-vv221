#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# Beyond Good and Evil
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='beyond-good-and-evil'
GAME_NAME='Beyond Good and Evil'

ARCHIVE_BASE_0_NAME='setup_beyond_good_and_evil_2.1.0.9.exe'
ARCHIVE_BASE_0_MD5='fdfa4b94cf02e24523b01c9d54568482'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2195754'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.9'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/beyond_good_and_evil'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
binkw32.dll
bge.exe
checkapplication.exe
run.exe
settingsapplication.exe'
CONTENT_GAME0_BIN_PATH='sys'
CONTENT_GAME0_BIN_FILES='
eax.dll'
CONTENT_GAME_DATA_FILES='
bgemakingof.bik
jade.spe
sally_clean.bf'
CONTENT_DOC_DATA_FILES='
manual.pdf
readme.txt'

USER_PERSISTENT_FILES='
sally.idx
*.sav'

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Ubisoft\Beyond Good & Evil\settingsapplication.INI'

APP_MAIN_EXE='run.exe'
APP_MAIN_ICON='bge.exe'
## Hide EAX library from the game, this helps avoiding some sound issues.
## The library should still be available to the settings application.
APP_MAIN_PRERUN='
# Hide EAX library from the game, this helps avoiding some sound issues.
rm --force eax.dll
'

APP_SETTINGS_ID="${GAME_ID}-settings"
APP_SETTINGS_NAME="${GAME_NAME} - settings"
APP_SETTINGS_CAT='Settings'
APP_SETTINGS_EXE='settingsapplication.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Set install path in registry

install_path="C:\\\\${GAME_ID}"
registry_dump_install_file='registry-dumps/install-path.reg'
registry_dump_install_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Beyond Good & Evil]
"Install path"="'"${install_path}"'"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_install_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_install_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Automatically spawn game settings window on first launch

## Using desktop_field_exec here ensures that we get a path already escaped if required.
settings_cmd=$(desktop_field_exec 'APP_SETTINGS')
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')"'
# Automatically spawn game settings window on first launch
settings_registry_key='\''HKEY_CURRENT_USER\Software\Ubisoft\Beyond Good & Evil\settingsapplication.INI'\''
settings_registry_dump="${REGEDIT_DUMPS_WINEPREFIX_PATH}/$(regedit_convert_key_to_path "$settings_registry_key")"
if [ ! -e "$settings_registry_dump" ]; then
	'"${settings_cmd}"'
	exit 0
fi'

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set install path in registry.
	mkdir --parents "$(dirname "$registry_dump_install_file")"
	printf '%s' "$registry_dump_install_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_install_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
