#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warcraft 3
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warcraft-3'
GAME_NAME='Warcraft Ⅲ: Reign of Chaos'

ARCHIVE_BASE_EN_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_EN_0_MD5='dd5926d8bb6ed10c47617bbc69b0ce1a'
ARCHIVE_BASE_EN_0_SIZE='775512'
ARCHIVE_BASE_EN_0_VERSION='1.27-blizzard1'
ARCHIVE_BASE_EN_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=enUS&product=WAR3'

ARCHIVE_BASE_FR_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_FR_0_MD5='e61e66359b1ceedbf3c3d06b835e6aa1'
ARCHIVE_BASE_FR_0_SIZE='801118'
ARCHIVE_BASE_FR_0_VERSION='1.27-blizzard1'
ARCHIVE_BASE_FR_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=frFR&product=WAR3'

ARCHIVE_BASE_DE_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_DE_0_MD5='d5c1dd8ed66473e6adee36e1f8b528cb'
ARCHIVE_BASE_DE_0_SIZE='795612'
ARCHIVE_BASE_DE_0_VERSION='1.27-blizzard1'
ARCHIVE_BASE_DE_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=deDE&product=WAR3'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_PATH='PC'
## Warcraft III.exe and World Editor.exe are distinct between the English and the French build of the game,
## for now we assume the differences are not critical and one can be used in place of the other.
CONTENT_GAME_BIN_FILES='
redist
blizzard.ax
*.dll
*.exe'
CONTENT_GAME_DATA_PATH='Common'
CONTENT_GAME_DATA_FILES='
Maps
Movies
War3.mpq'
CONTENT_GAME_DATA_SHARED_PATH='Common'
CONTENT_GAME_DATA_SHARED_FILES='
War3Patch.mpq'

USER_PERSISTENT_DIRECTORIES='
Campaigns
Maps
Replay
Save'

APP_MAIN_EXE='Warcraft III.exe'
## Ensure that Reign of Chaos is started, even if The Frozen Throne is installed
APP_MAIN_OPTIONS='-classic -opengl'
APP_MAIN_ICON='PC/War3.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=WAR3.ICO'

PACKAGES_LIST='
PKG_BIN
PKG_DATA
PKG_DATA_SHARED'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA_BASE'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/x-msvideo'

PKG_DATA_BASE_ID="${GAME_ID}-data"
PKG_DATA_ID_EN="${PKG_DATA_BASE_ID}-en"
PKG_DATA_ID_FR="${PKG_DATA_BASE_ID}-fr"
PKG_DATA_ID_DE="${PKG_DATA_BASE_ID}-de"
PKG_DATA_PROVIDES="
$PKG_DATA_BASE_ID"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_SHARED'

PKG_DATA_SHARED_ID="${PKG_DATA_BASE_ID}-shared"
PKG_DATA_SHARED_ID_EN="${PKG_DATA_SHARED_ID}-en"
PKG_DATA_SHARED_ID_FR="${PKG_DATA_SHARED_ID}-fr"
PKG_DATA_SHARED_ID_DE="${PKG_DATA_SHARED_ID}-de"
PKG_DATA_SHARED_PROVIDES="
$PKG_DATA_SHARED_ID"
PKG_DATA_SHARED_DESCRIPTION='data shared between the base game and the expansion'

# Set the list of requirements to extract the archive contents.

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
smpq"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of required extra archives

## Check for the presence of the CD key files.
## See notes/warcraft-3 for details on how to get these files.
ARCHIVE_REQUIRED_CDKEY_1_NAME='font.clh'
ARCHIVE_REQUIRED_CDKEY_2_NAME='font.gid'
archive_initialize_required \
	'ARCHIVE_CDKEY_1' \
	'ARCHIVE_REQUIRED_CDKEY_1'
archive_initialize_required \
	'ARCHIVE_CDKEY_2' \
	'ARCHIVE_REQUIRED_CDKEY_2'

# Extract game data

archive_path=$(archive_path "$(current_archive)")
archive_path_cdkey_1=$(archive_path 'ARCHIVE_CDKEY_1')
archive_path_cdkey_2=$(archive_path 'ARCHIVE_CDKEY_2')
mkdir --parents "${PLAYIT_WORKDIR}/gamedata"
information_archive_data_extraction "$archive_path"
(
	cd "${PLAYIT_WORKDIR}/gamedata"
	smpq --extract "$archive_path"

	# Update the .mpq file
	(
		## Ensure case consistency between the different builds of the game
		cd 'Common'
		if [ -e 'war3.mpq' ]; then
			mv 'war3.mpq' 'War3.mpq'
		fi
	)
	(
		cd 'PC-100'
		smpq --append ../Common/War3.mpq ./*
	)
	(
		cd 'Files'
		cp --dereference \
			"$archive_path_cdkey_1" \
			"$archive_path_cdkey_2" \
			'font'
		smpq --append ../Common/War3.mpq \
			font/font.ccd \
			font/font.clh \
			font/font.gid
		rm --force --recursive 'font'
	)
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
