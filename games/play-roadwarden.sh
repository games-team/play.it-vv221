#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Roadwarden
###

script_version=20241126.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='roadwarden'
GAME_NAME='Roadwarden'

ARCHIVE_BASE_4_NAME='roadwarden_1_1_31_67621.sh'
ARCHIVE_BASE_4_MD5='c5ce0e6b2b92a0ca49ab0bd06e018957'
ARCHIVE_BASE_4_SIZE='867056'
ARCHIVE_BASE_4_VERSION='1.1.31-gog67621'
ARCHIVE_BASE_4_URL='https://www.gog.com/game/roadwarden'

ARCHIVE_BASE_3_NAME='roadwarden_1_1_2_67478.sh'
ARCHIVE_BASE_3_MD5='f62eb9f3027ea9b6fb4f76b87cc71471'
ARCHIVE_BASE_3_SIZE='867052'
ARCHIVE_BASE_3_VERSION='1.1.2-gog67478'

ARCHIVE_BASE_2_NAME='roadwarden_1_0_92_63495.sh'
ARCHIVE_BASE_2_MD5='1808e2f215bdca65324b909f2706574b'
ARCHIVE_BASE_2_SIZE='870000'
ARCHIVE_BASE_2_VERSION='1.0.92-gog63495'

ARCHIVE_BASE_1_NAME='roadwarden_1_0_9_62941.sh'
ARCHIVE_BASE_1_MD5='4fa5f13a40a2ae3e287493298e326e96'
ARCHIVE_BASE_1_SIZE='870000'
ARCHIVE_BASE_1_VERSION='1.0.9-gog62941'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/roadwarden'

ARCHIVE_BASE_0_NAME='roadwarden_1_0_8_61761.sh'
ARCHIVE_BASE_0_MD5='659218d7803632a38f8e72e4f4f83a26'
ARCHIVE_BASE_0_SIZE='870000'
ARCHIVE_BASE_0_VERSION='1.0.8-gog61761'

CONTENT_PATH_DEFAULT='data/noarch/game/game'
CONTENT_FONTS_MAIN_FILES='
munro.ttf
philosopher.ttf'
CONTENT_GAME_MAIN_FILES='
areas
audio
cache
gui
images
tl
script_version.txt
*.png
*.rpy
*.rpyc
*.ttf'

APP_MAIN_TYPE='renpy'
APP_MAIN_ICON='../../support/icon.png'

## Ensure easy upgrades from packages generated with pre-20240427.1 game scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
roadwarden-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

## Link the fonts in the hardcoded path the game engine expects.
fonts_source=$(path_fonts_ttf)
fonts_destination="$(package_path "$(current_package)")$(path_game_data)"
mkdir --parents "$fonts_destination"
for font_file in \
	'munro.ttf' \
	'philosopher.ttf'
do
	ln --symbolic "${fonts_source}/${font_file}" "$fonts_destination"
done

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
