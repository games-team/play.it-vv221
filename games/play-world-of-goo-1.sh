#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2020 BetaRays
set -o errexit

###
# World of Goo 1
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='world-of-goo-1'
GAME_NAME='World of Goo'

# Archives

## Sold from gog.com
ARCHIVE_BASE_GOG_0_NAME='world_of_goo_1_51_29337.sh'
ARCHIVE_BASE_GOG_0_MD5='f3566d859e824862b4dc14f94b15cafa'
ARCHIVE_BASE_GOG_0_SIZE='186520'
ARCHIVE_BASE_GOG_0_VERSION='1.51-gog29337'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/world_of_goo'

## Formerly sold from humblebundle.com, this archive is no longer available for sale.
ARCHIVE_BASE_HUMBLE_0_NAME='WorldOfGoo.Linux.1.53.sh'
ARCHIVE_BASE_HUMBLE_0_MD5='9049f4fccf98ba79ac238a8b414e053a'
ARCHIVE_BASE_HUMBLE_0_SIZE='210000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.53-humble'


CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_GAME_BIN64_RELATIVE_PATH_HUMBLE='x86_64'
CONTENT_GAME_BIN64_FILES='
WorldOfGoo.bin.x86_64'
CONTENT_GAME_BIN32_RELATIVE_PATH_HUMBLE='x86'
CONTENT_GAME_BIN32_FILES='
WorldOfGoo.bin.x86'
CONTENT_GAME_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_GAME_DATA_FILES='
game'
CONTENT_DOC_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_DOC_DATA_PATH_GOG='data/noarch/docs'
CONTENT_DOC_DATA_FILES='
readme.html
linux-issues.txt'

APP_MAIN_EXE_BIN64='WorldOfGoo.bin.x86_64'
APP_MAIN_EXE_BIN32='WorldOfGoo.bin.x86'
APP_MAIN_ICON='game/gooicon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libSDL2-2.0.so.0
libSDL2_mixer-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
## Ensure easy upgrades from packages generated with pre-20240805.1 scripts
PKG_BIN64_PROVIDES="${PKG_BIN64_PROVIDES:-}
world-of-goo"
PKG_BIN32_PROVIDES="${PKG_BIN32_PROVIDES:-}
world-of-goo"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrades from packages generated with pre-20240805.1 scripts
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
world-of-goo-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
