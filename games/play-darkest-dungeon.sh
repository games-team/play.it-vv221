#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Darkest Dungeon
###

script_version=20241103.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='darkest-dungeon'
GAME_NAME='Darkest Dungeon'

ARCHIVE_BASE_7_NAME='darkest_dungeon_24839_28859.sh'
ARCHIVE_BASE_7_MD5='2a04beb04b3129b4bd68b4dd9023e82d'
ARCHIVE_BASE_7_SIZE='2300000'
ARCHIVE_BASE_7_VERSION='24839-gog28859'
ARCHIVE_BASE_7_URL='https://www.gog.com/game/darkest_dungeon'

ARCHIVE_BASE_6_NAME='darkest_dungeon_24788_26004.sh'
ARCHIVE_BASE_6_MD5='be838bdc8e7c971e4d412f833fd348ac'
ARCHIVE_BASE_6_SIZE='2300000'
ARCHIVE_BASE_6_VERSION='24788-gog26004'

ARCHIVE_BASE_5_NAME='darkest_dungeon_en_24358_23005.sh'
ARCHIVE_BASE_5_MD5='3d7dc739665003d48589cdbe6cc472ef'
ARCHIVE_BASE_5_SIZE='2300000'
ARCHIVE_BASE_5_VERSION='24358-gog23005'

ARCHIVE_BASE_4_NAME='darkest_dungeon_en_24154_22522.sh'
ARCHIVE_BASE_4_MD5='361d3e7b117725e8ce3982d183d4810a'
ARCHIVE_BASE_4_SIZE='2300000'
ARCHIVE_BASE_4_VERSION='24154-gog22522'

ARCHIVE_BASE_3_NAME='darkest_dungeon_en_23904_21681.sh'
ARCHIVE_BASE_3_MD5='9ddb131060d0995c4ceb56dd9c846b8f'
ARCHIVE_BASE_3_SIZE='2300000'
ARCHIVE_BASE_3_VERSION='23904-gog21681'

ARCHIVE_BASE_2_NAME='darkest_dungeon_en_23885_21662.sh'
ARCHIVE_BASE_2_MD5='ff449de9cfcdf97fa1a27d1073139463'
ARCHIVE_BASE_2_SIZE='2300000'
ARCHIVE_BASE_2_VERSION='23885-gog21662'

ARCHIVE_BASE_1_NAME='darkest_dungeon_en_21142_16140.sh'
ARCHIVE_BASE_1_MD5='4b43065624dbab74d794c56809170588'
ARCHIVE_BASE_1_SIZE='2200000'
ARCHIVE_BASE_1_VERSION='21142-gog16140'

ARCHIVE_BASE_0_NAME='darkest_dungeon_en_21096_16066.sh'
ARCHIVE_BASE_0_MD5='435905fe6edd911a8645d4feaf94ec34'
ARCHIVE_BASE_0_SIZE='2200000'
ARCHIVE_BASE_0_VERSION='21096-gog16066'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libfmod.so.9
libfmodstudio.so.9'
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_FILES='
darkest.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
darkest.bin.x86'
CONTENT_GAME_DATA_FILES='
audio
video
Icon.bmp
pin
svn_revision.txt
activity_log
campaign
colours
curios
cursors
dungeons
effects
fe_flow
fonts
fx
game_over
heroes
inventory
loading_screen
loot
maps
modes
mods
monsters
overlays
panels
props
raid
raid_results
scripts
scrolls
shaders
shared
trinkets
upgrades
user_information
localization/*.bat
localization/*.csv
localization/*.loc
localization/*.txt
localization/*.xml
localization/pc'
CONTENT_DOC_DATA_FILES='
README.linux'

APP_MAIN_EXE_BIN32='darkest.bin.x86'
APP_MAIN_EXE_BIN64='darkest.bin.x86_64'
APP_MAIN_ICON='Icon.bmp'
## Prevent a game crash when using the Wayland video backend of SDL
APP_MAIN_PRERUN='
# Prevent a game crash when using the Wayland video backend of SDL
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Fix icon background transparency

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
convert"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Fix icon background transparency
	icon_path=$(icon_path 'APP_MAIN_ICON')
	convert "$icon_path" \
		-transparent "#008080" \
		-transparent "#006e6e" \
		"${icon_path%.bmp}_fixed.bmp"
	mv "${icon_path%.bmp}_fixed.bmp" "$icon_path"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
