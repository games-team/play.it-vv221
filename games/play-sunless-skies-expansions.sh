#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sunless Skies expansions:
# - Cyclopean Owl
###

script_version=20241126.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='sunless-skies'
GAME_NAME='Sunless Skies'

EXPANSION_ID_OWL='cyclopean-owl'
EXPANSION_NAME_OWL='Cyclopean Owl'

## Cyclopean Owl

ARCHIVE_BASE_OWL_3_NAME='sunless_skies_cyclopean_owl_2_0_5_6e8c8ff_74416.sh'
ARCHIVE_BASE_OWL_3_MD5='163712d560736a51eb51fda83cee7336'
ARCHIVE_BASE_OWL_3_SIZE='899'
ARCHIVE_BASE_OWL_3_VERSION='2.0.5-gog74416'

ARCHIVE_BASE_OWL_2_NAME='sunless_skies_cyclopean_owl_2_0_4_fcf0af7a_52215.sh'
ARCHIVE_BASE_OWL_2_MD5='c3749871147607f488f58881b30b9600'
ARCHIVE_BASE_OWL_2_SIZE='1300'
ARCHIVE_BASE_OWL_2_VERSION='2.0.4-gog52215'

ARCHIVE_BASE_OWL_0_NAME='sunless_skies_cyclopean_owl_2_0_2_9bcd3d8c_48199.sh'
ARCHIVE_BASE_OWL_0_MD5='9d0dee370093bce46c35dc114c0d6241'
ARCHIVE_BASE_OWL_0_SIZE='1300'
ARCHIVE_BASE_OWL_0_VERSION='2.0.2-gog48199'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
dlc'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Work around directory name case mismatch.
	if [ -e 'DLC' ]; then
		mv 'DLC' 'dlc'
	fi
)

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
