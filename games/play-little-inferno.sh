#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 VA
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Little Inferno
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='little-inferno'
GAME_NAME='Little Inferno'

ARCHIVE_BASE_HUMBLE_0_NAME='LittleInferno_Linux_v2.0.3.sh'
ARCHIVE_BASE_HUMBLE_0_MD5='2d104de7966405f905d36bfbc7722808'
ARCHIVE_BASE_HUMBLE_0_SIZE='274737'
ARCHIVE_BASE_HUMBLE_0_VERSION='2.0.3-humble.2022.12.08'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/little-inferno'

ARCHIVE_BASE_32BIT_HUMBLE_0_NAME='little-inferno_0_20130509-0ubuntu1_i386.deb'
ARCHIVE_BASE_32BIT_HUMBLE_0_MD5='cd7eac96e33de3089c665915b1a7239c'
ARCHIVE_BASE_32BIT_HUMBLE_0_SIZE='200000'
ARCHIVE_BASE_32BIT_HUMBLE_0_VERSION='1.3.20130509-humble1'

ARCHIVE_BASE_32BIT_GOG_0_NAME='gog_little_inferno_2.0.0.2.sh'
ARCHIVE_BASE_32BIT_GOG_0_MD5='29375d30d03f12db44d09a1c32398d33'
ARCHIVE_BASE_32BIT_GOG_0_SIZE='200000'
ARCHIVE_BASE_32BIT_GOG_0_VERSION='1.3.20130509-gog2.0.0.2'
ARCHIVE_BASE_32BIT_GOG_0_URL='https://www.gog.com/game/little_inferno'

CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_PATH_DEFAULT_32BIT_HUMBLE='opt/little-inferno'
CONTENT_PATH_DEFAULT_32BIT_GOG='data/noarch/game'
CONTENT_GAME_BIN64_RELATIVE_PATH_HUMBLE='x86_64'
CONTENT_GAME_BIN64_FILES='
LittleInferno.bin.x86_64'
CONTENT_GAME_BIN32_RELATIVE_PATH_HUMBLE='x86'
CONTENT_GAME_BIN32_FILES='
LittleInferno.bin.x86'
CONTENT_GAME_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_GAME_DATA_FILES='
shaders
.tc-edition
icon.png
*.pak'
CONTENT_DOC_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_DOC_DATA_FILES='
LICENSE.txt
README.linux'
## Include shipped libvorbis.so.0 for game build 1.x,
## the game segfaults when using a system-provided library instead
CONTENT_LIBS_BIN32_RELATIVE_PATH_32BIT='lib'
CONTENT_LIBS_BIN32_FILES_32BIT='
libvorbis.so.0'

APP_MAIN_EXE_BIN64='LittleInferno.bin.x86_64'
APP_MAIN_EXE_BIN32='LittleInferno.bin.x86'
APP_MAIN_ICON='noarch/icon.png'
APP_MAIN_ICONS_LIST_32BIT_HUMBLE='APP_MAIN_ICON_HUMBLE_16 APP_MAIN_ICON_HUMBLE_32 APP_MAIN_ICON_HUMBLE_64 APP_MAIN_ICON_HUMBLE_128'
APP_MAIN_ICON_HUMBLE_16='../../usr/share/icons/hicolor/16x16/apps/little-inferno.png'
APP_MAIN_ICON_HUMBLE_32='../../usr/share/icons/hicolor/32x32/apps/little-inferno.png'
APP_MAIN_ICON_HUMBLE_64='../../usr/share/icons/hicolor/64x64/apps/little-inferno.png'
APP_MAIN_ICON_HUMBLE_128='../../usr/share/icons/hicolor/128x128/apps/little-inferno.png'
APP_MAIN_ICONS_LIST_32BIT_GOG='APP_MAIN_ICON_GOG'
APP_MAIN_ICON_GOG='../support/icon.png'

PACKAGES_LIST='
PKG_BIN32
PKG_BIN64
PKG_DATA'
PACKAGES_LIST_32BIT='
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES_32BIT='
libc.so.6
libcurl.so.4
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libvorbis.so.0
libX11.so.6'
## Include shipped libvorbis.so.0 for game build 1.x,
## the game segfaults when using a system-provided library instead
PKG_BIN32_DEPENDENCIES_LIBRARIES_32BIT="${PKG_BIN32_DEPENDENCIES_LIBRARIES_32BIT:-}
libc.so.6
libm.so.6
libogg.so.0"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_32BIT_'*)
		## Old 1.x builds provide a 32-bit binary only.
		launchers_generation 'PKG_BIN32'
	;;
	(*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
