#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Elder Scrolls 3
###

script_version=20241121.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-elder-scrolls-3'
GAME_NAME='The Elder Scrolls Ⅲ: Morrowind'

ARCHIVE_BASE_EN_1_NAME='setup_the_elder_scrolls_iii_morrowind_goty_1.6.0.1820_gog_0.1_(77582).exe'
ARCHIVE_BASE_EN_1_MD5='c3ea7739b81d457e4c7d91758b762f4e'
## Do not convert file paths to lowercase, as it causes problems when importing morrowind.ini default settings
ARCHIVE_BASE_EN_1_EXTRACTOR='innoextract'
ARCHIVE_BASE_EN_1_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_EN_1_PART1_NAME='setup_the_elder_scrolls_iii_morrowind_goty_1.6.0.1820_gog_0.1_(77582)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='1158ea779b50c704c17bedee0ad2612c'
ARCHIVE_BASE_EN_1_SIZE='2209531'
ARCHIVE_BASE_EN_1_VERSION='1.6.0.1820-gog77582'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition'

ARCHIVE_BASE_FR_1_NAME='setup_the_elder_scrolls_iii_morrowind_goty_1.6.0.1820_gog_0.1_(french)_(77582).exe'
ARCHIVE_BASE_FR_1_MD5='5f56a00e9256d930f63ab677c43c8b75'
## Do not convert file paths to lowercase, as it causes problems when importing morrowind.ini default settings
ARCHIVE_BASE_FR_1_EXTRACTOR='innoextract'
ARCHIVE_BASE_FR_1_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_FR_1_PART1_NAME='setup_the_elder_scrolls_iii_morrowind_goty_1.6.0.1820_gog_0.1_(french)_(77582)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='38571e142c67f9c8edd433902abe1454'
ARCHIVE_BASE_FR_1_SIZE='2239754'
ARCHIVE_BASE_FR_1_VERSION='1.6.0.1820-gog77582'
ARCHIVE_BASE_FR_1_URL='https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition'

ARCHIVE_BASE_EN_0_NAME='setup_tes_morrowind_goty_2.0.0.7.exe'
ARCHIVE_BASE_EN_0_MD5='3a027504a0e4599f8c6b5b5bcc87a5c6'
## Do not convert file paths to lowercase, as it causes problems when importing morrowind.ini default settings
ARCHIVE_BASE_EN_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_EN_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_EN_0_SIZE='2300000'
ARCHIVE_BASE_EN_0_VERSION='1.6.1820-gog2.0.0.7'

ARCHIVE_BASE_FR_0_NAME='setup_tes_morrowind_goty_french_2.0.0.7.exe'
ARCHIVE_BASE_FR_0_MD5='2aee024e622786b2cb5454ff074faf9b'
## Do not convert file paths to lowercase, as it causes problems when importing morrowind.ini default settings
ARCHIVE_BASE_FR_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_FR_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_FR_0_SIZE='2300000'
ARCHIVE_BASE_FR_0_VERSION='1.6.1820-gog2.0.0.7'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_PATH_DEFAULT_FR_0='app'
CONTENT_GAME_L10N_FILES='
Morrowind.ini
Data Files/BookArt
Data Files/Sound/Vo
Data Files/Splash
Data Files/Video
Data Files/*.bsa
Data Files/*.esm'
CONTENT_GAME_DATA_FILES='
Data Files
Knife.ico'
CONTENT_GAME_DATAFILES_DATA_RELATIVE_PATH='_OfficialPlugins/_unpacked_files'
CONTENT_GAME_DATAFILES_DATA_FILES='
Icons
Meshes
Sound
Textures
*.esp
*.txt'
CONTENT_DOC_L10N_FILES='
*.txt'
CONTENT_DOC_DATA_FILES='
*.pdf'

APP_MAIN_TYPE='custom'
APP_MAIN_ICON='Morrowind.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'
PKG_BIN_DEPENDENCIES_COMMANDS='
openmw-iniimporter
openmw-launcher'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Fix .bsa/.esm dates on French version.
	case "$(current_archive)" in
		('ARCHIVE_BASE_FR_'*)
			touch --date='2002-06-21 17:31:46.000000000 +0200' \
				'Data Files/Morrowind.bsa'
			touch --date='2002-07-17 18:59:22.000000000 +0200' \
				'Data Files/Morrowind.esm'
			touch --date='2002-10-29 21:22:06.000000000 +0100' \
				'Data Files/Tribunal.bsa'
			touch --date='2003-06-26 20:05:06.000000000 +0200' \
				'Data Files/Tribunal.esm'
			touch --date='2003-05-01 13:37:30.000000000 +0200' \
				'Data Files/Bloodmoon.bsa'
			touch --date='2003-07-07 17:27:56.000000000 +0200' \
				'Data Files/Bloodmoon.esm'
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion 'GAME_DATAFILES_DATA' 'PKG_DATA' "$(path_game_data)/Data Files"
content_inclusion_default

# Write launchers

custom_launcher() {
	launcher_headers
	cat <<- EOF
	PATH_GAME_DATA="$(path_game_data)"
	EOF
	cat <<- 'OUTEREOF'
	OPENMW_CONFIG_PATH="${XDG_CONFIG_HOME:=${HOME}/.config}/openmw"
	OPENMW_CONFIG_FILE="${OPENMW_CONFIG_PATH}/openmw.cfg"
	OPENMW_CONFIG_LAUNCHER_FILE="${OPENMW_CONFIG_PATH}/launcher.cfg"

	# Initialize OpenMW configuration on first launch
	if [ ! -e "$OPENMW_CONFIG_FILE" ]; then
	    mkdir --parents "$OPENMW_CONFIG_PATH"
	    cat > "$OPENMW_CONFIG_FILE" <<- EOF
	    data="${PATH_GAME_DATA}/Data Files"
	    content=Morrowind.esm
	    EOF
	    openmw-iniimporter --ini "${PATH_GAME_DATA}/Morrowind.ini" --cfg "$OPENMW_CONFIG_FILE"
	    if [ ! -e "$OPENMW_CONFIG_LAUNCHER_FILE" ]; then
	        cat > "$OPENMW_CONFIG_LAUNCHER_FILE" <<- EOF
	        [General]
	        firstrun=false
	        EOF
	    fi
	fi

	openmw-launcher

	exit 0
	OUTEREOF
}
launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
