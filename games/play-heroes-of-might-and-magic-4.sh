#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroes of Might and Magic 4
###

script_version=20241121.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='heroes-of-might-and-magic-4'
GAME_NAME='Heroes of Might and Magic Ⅳ'

ARCHIVE_BASE_EN_4_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_gog_0.2_(76223).exe'
ARCHIVE_BASE_EN_4_MD5='7737a4617848cb701f6577128b17fdc4'
ARCHIVE_BASE_EN_4_TYPE='innosetup'
ARCHIVE_BASE_EN_4_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_gog_0.2_(76223)-1.bin'
ARCHIVE_BASE_EN_4_PART1_MD5='fb953df444f1716b856db53d4c08520a'
ARCHIVE_BASE_EN_4_SIZE='1126546'
ARCHIVE_BASE_EN_4_VERSION='3.0-gog76223'
ARCHIVE_BASE_EN_4_URL='https://www.gog.com/game/heroes_of_might_and_magic_4_complete'

ARCHIVE_BASE_FR_4_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_gog_0.2_(french)_(76223).exe'
ARCHIVE_BASE_FR_4_MD5='d87e74674382d446332cf257ef6cbf74'
ARCHIVE_BASE_FR_4_TYPE='innosetup'
ARCHIVE_BASE_FR_4_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_gog_0.2_(french)_(76223)-1.bin'
ARCHIVE_BASE_FR_4_PART1_MD5='00c24ec39905f65ed033d8dc2a77e7b3'
ARCHIVE_BASE_FR_4_SIZE='1173753'
ARCHIVE_BASE_FR_4_VERSION='3.0-gog76223'
ARCHIVE_BASE_FR_4_URL='https://www.gog.com/game/heroes_of_might_and_magic_4_complete'

ARCHIVE_BASE_EN_3_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_(58571).exe'
ARCHIVE_BASE_EN_3_MD5='4f342558b1f93f4057666bc510127a3b'
ARCHIVE_BASE_EN_3_TYPE='innosetup'
ARCHIVE_BASE_EN_3_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_(58571)-1.bin'
ARCHIVE_BASE_EN_3_PART1_MD5='c82afd73219eb772ce21f5ab43ef34ce'
ARCHIVE_BASE_EN_3_SIZE='1200000'
ARCHIVE_BASE_EN_3_VERSION='3.0-gog58571'

ARCHIVE_BASE_FR_3_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_(french)_(58571).exe'
ARCHIVE_BASE_FR_3_MD5='27d3987a5001beff61176d39199b9284'
ARCHIVE_BASE_FR_3_TYPE='innosetup'
ARCHIVE_BASE_FR_3_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_win11_(french)_(58571)-1.bin'
ARCHIVE_BASE_FR_3_PART1_MD5='588b0ecacc6b91df76042af7a7667d24'
ARCHIVE_BASE_FR_3_SIZE='1200000'
ARCHIVE_BASE_FR_3_VERSION='3.0-gog58571'

ARCHIVE_BASE_EN_2_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(25023).exe'
ARCHIVE_BASE_EN_2_MD5='2694dfef6827142866fb778588966d8e'
ARCHIVE_BASE_EN_2_TYPE='innosetup'
ARCHIVE_BASE_EN_2_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(25023)-1.bin'
ARCHIVE_BASE_EN_2_PART1_MD5='54bfa42a4e23236729f8b0724216ee83'
ARCHIVE_BASE_EN_2_SIZE='1100000'
ARCHIVE_BASE_EN_2_VERSION='3.0-gog25023'

ARCHIVE_BASE_FR_2_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(french)_(25023).exe'
ARCHIVE_BASE_FR_2_MD5='4a90a92d637bc7d1af5354565ae5cd70'
ARCHIVE_BASE_FR_2_TYPE='innosetup'
ARCHIVE_BASE_FR_2_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(french)_(25023)-1.bin'
ARCHIVE_BASE_FR_2_PART1_MD5='46d1239b8491d3adec647a7526f763b2'
ARCHIVE_BASE_FR_2_SIZE='1100000'
ARCHIVE_BASE_FR_2_VERSION='3.0-gog25023'

ARCHIVE_BASE_EN_1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(22812).exe'
ARCHIVE_BASE_EN_1_MD5='d5e0a55e2bba4f0ac643ec1fb2ba17cc'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(22812)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='3457ead5c208a3d40498d6e1f08bf588'
ARCHIVE_BASE_EN_1_SIZE='1100000'
ARCHIVE_BASE_EN_1_VERSION='3.0-gog22812'

ARCHIVE_BASE_FR_1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(french)_(22812).exe'
ARCHIVE_BASE_FR_1_MD5='e15ec7a308ea442bfeeb3410314b39d7'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_PART1_NAME='setup_heroes_of_might_and_magic_4_complete_3.0_(french)_(22812)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='7abff7182f6bed3199d2b71cdd60d926'
ARCHIVE_BASE_FR_1_SIZE='1100000'
ARCHIVE_BASE_FR_1_VERSION='3.0-gog22812'

ARCHIVE_BASE_EN_0_NAME='setup_homm4_complete_2.0.0.12.exe'
ARCHIVE_BASE_EN_0_MD5='74de66eb408bb2916dd0227781ba96dc'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1100000'
ARCHIVE_BASE_EN_0_VERSION='3.0-gog2.0.0.12'

ARCHIVE_BASE_FR_0_NAME='setup_homm4_complete_french_2.1.0.14.exe'
ARCHIVE_BASE_FR_0_MD5='2af96eb28226e563bbbcd62771f3a319'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='1100000'
ARCHIVE_BASE_FR_0_VERSION='3.0-gog2.1.0.14'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_PATH_DEFAULT_FR_0='app'
CONTENT_GAME_BIN_FILES='
mp3dec.asi
binkw32.dll
mss32.dll
data/binkw32.dll
data/mss32.dll
campaign_editor.exe
dxcfg.exe
heroes4.exe'
CONTENT_GAME_DATA_FILES='
data
maps'
CONTENT_DOC_DATA_FILES='
campaign_editor_help.chm
h4gsmanual.pdf
h4manual.pdf
h4qref.pdf
h4towntrees.pdf
h4wwmanual.pdf
multiplayer.txt
readme.txt'

USER_PERSISTENT_DIRECTORIES='
games'
USER_PERSISTENT_FILES='
data/high_scores.dat'

WINE_VIRTUAL_DESKTOP='1280x1024'

APP_MAIN_EXE='heroes4.exe'
APP_MAIN_ICON='heroes4.exe'

APP_EDITOR_ID="${GAME_ID}-campaign-editor"
APP_EDITOR_EXE='campaign_editor.exe'
APP_EDITOR_ICON='campaign_editor.exe'
APP_EDITOR_NAME="$GAME_NAME - campaign editor"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_BASE_ID="$GAME_ID"
PKG_BIN_ID_EN="${PKG_BIN_BASE_ID}-en"
PKG_BIN_ID_FR="${PKG_BIN_BASE_ID}-fr"
PKG_BIN_PROVIDES="
$PKG_BIN_BASE_ID"
PKG_BIN_DESCRIPTION_EN='English version'
PKG_BIN_DESCRIPTION_FR='French version'
## A dependency on the language-specific variant of the data package is set,
## not on the common name provided by both variants of this package.
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_BASE_ID="${GAME_ID}-data"
PKG_DATA_ID_EN="${PKG_DATA_BASE_ID}-en"
PKG_DATA_ID_FR="${PKG_DATA_BASE_ID}-fr"
PKG_DATA_PROVIDES="
$PKG_DATA_BASE_ID"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DESCRIPTION_EN="${PKG_DATA_DESCRIPTION} - English version"
PKG_DATA_DESCRIPTION_FR="${PKG_DATA_DESCRIPTION} - French version"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
