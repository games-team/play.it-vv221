#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Reus
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='reus'
GAME_NAME='Reus'

ARCHIVE_BASE_GOG_1_NAME='reus_en_1_6_5_20844.sh'
ARCHIVE_BASE_GOG_1_MD5='a768dd2347ac7f6be16ffa9e3f0952c4'
ARCHIVE_BASE_GOG_1_SIZE='480000'
ARCHIVE_BASE_GOG_1_VERSION='1.6.5-gog20844'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/reus'

ARCHIVE_BASE_GOG_0_NAME='gog_reus_2.0.0.2.sh'
ARCHIVE_BASE_GOG_0_MD5='25fe7ec93305e804558e4ef8a31fbbf8'
ARCHIVE_BASE_GOG_0_SIZE='480000'
ARCHIVE_BASE_GOG_0_VERSION='1.5.1-gog2.0.0.2'

ARCHIVE_BASE_HUMBLE_1_NAME='reus-linux-1.6.5.tar.gz'
ARCHIVE_BASE_HUMBLE_1_MD5='2b61251f7aa41542db03a1fe637b57dc'
ARCHIVE_BASE_HUMBLE_1_SIZE='480000'
ARCHIVE_BASE_HUMBLE_1_VERSION='1.6.5-humble180612'
ARCHIVE_BASE_HUMBLE_1_URL='https://www.humblebundle.com/store/reus'

ARCHIVE_BASE_HUMBLE_0_NAME='reus_linux_1389636757-bin'
ARCHIVE_BASE_HUMBLE_0_MD5='9914e7fcb5f3b761941169ae13ec205c'
ARCHIVE_BASE_HUMBLE_0_SIZE='380000'
ARCHIVE_BASE_HUMBLE_0_VERSION='0.beta-humble140113'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_PATH_DEFAULT_HUMBLE_0='data'
CONTENT_GAME_MAIN_FILES='
Audio
Cursors
Effects
Fonts
MainMenu
Particles
Settings
Skeletons
Textures
UI
Reus.exe
Reus.bmp
FNA.dll
FNA.dll.config
Game.*.dll
Newtonsoft.Json.dll
Reus.*.dll
SpaceTech.dll
SpaceTech.*.dll
SpaceTech.*.dll.config
SpaceTech2D.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'
## Include shipped libraries that can not be replaced by system ones.
CONTENT_LIBS_FILES='
libFontNative.so
libmojoshader.so
libtheorafile.so'
CONTENT_LIBS_LIBS64_RELATIVE_PATH='lib64'
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES
libfmodevent64-4.44.00.so
libfmodex64-4.44.00.so"
CONTENT_LIBS_LIBS32_RELATIVE_PATH='lib'
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES
libfmodevent-4.44.00.so
libfmodex-4.44.00.so"

APP_MAIN_EXE='Reus.exe'
APP_MAIN_ICON='Reus.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Microsoft.CSharp.dll
Mono.CSharp.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Design.dll
System.Drawing.dll
System.Management.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Transactions.dll
System.Xml.dll
System.Xml.Linq.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfreetype.so.6
libm.so.6
libogg.so.0
libpthread.so.0
libstdc++.so.6
libtheoradec.so.1
libvorbis.so.0'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
