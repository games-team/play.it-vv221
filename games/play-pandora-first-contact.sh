#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pandora: First Contact
###

script_version=20250104.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='pandora-first-contact'
GAME_NAME='Pandora: First Contact'

ARCHIVE_BASE_0_NAME='pandora_first_contact_en_1_6_7_16815.sh'
ARCHIVE_BASE_0_MD5='0d9343d1693fc561823811a0cd3e279c'
ARCHIVE_BASE_0_SIZE='720000'
ARCHIVE_BASE_0_VERSION='1.6.7-gog16815'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/pandora'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='pandora-first-contact_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='66b1d99166b738b2130449a49b9cd58c'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/resources/pandora-first-contact/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
256x256'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_RELATIVE_PATH='Binaries'
CONTENT_LIBS_BIN_FILES='
libavcodec.so.53
libavformat.so.53
libavutil.so.51
libboost_date_time.so.1.58.0
libboost_filesystem.so.1.58.0
libboost_locale.so.1.58.0
libboost_regex.so.1.58.0
libboost_system.so.1.58.0
libboost_thread.so.1.58.0
libGLEW.so.1.6
libjpeg.so.8
libpng15.so.15
libswscale.so.2'
CONTENT_GAME_BIN_FILES='
Binaries/Pandora.bin
Binaries/PandoraServer.bin'
CONTENT_GAME_DATA_FILES='
Data'
CONTENT_DOC_DATA_RELATIVE_PATH='Documents'
CONTENT_DOC_DATA_FILES='
Dutch
English
French
German
Italian
Licenses
PandoraWebsite.url'

APP_MAIN_EXE='Binaries/Pandora.bin'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libbz2.so.1
libc.so.6
libcurl.so.4
libfreetype.so.6
libgcc_s.so.1
libglfw.so.3
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libvorbisfile.so.3
libX11.so.6
libXi.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set the required execution permissions on the server binary
	chmod 755 'Binaries/PandoraServer.bin'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd Binaries
	./Pandora.bin "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
