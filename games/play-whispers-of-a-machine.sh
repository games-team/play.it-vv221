#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Whispers of a Machine
###

script_version=20250110.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='whispers-of-a-machine'
GAME_NAME='Whispers of a Machine'

ARCHIVE_BASE_0_NAME='setup_whispers_of_a_machine_1.0.6d_(35876).exe'
ARCHIVE_BASE_0_MD5='92244849a09e9b47a290618acd9dc9b6'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='768282'
ARCHIVE_BASE_0_VERSION='1.0.6d-gog35876'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/whispers_of_a_machine'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
acsetup.cfg
whispers.exe
*.vox
*.tra'

APP_MAIN_SCUMMID='ags:whispersofamachine'
APP_MAIN_ICON='whispers.exe'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
