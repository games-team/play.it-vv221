#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Stellaris
###

script_version=20250227.2

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='stellaris'
GAME_NAME='Stellaris'

ARCHIVE_BASE_59_NAME='stellaris_3_14_15926_78464.sh'
ARCHIVE_BASE_59_MD5='0e93163e55e8bddbcdfd6c7c9c0307bd'
ARCHIVE_BASE_59_SIZE='20344787'
ARCHIVE_BASE_59_VERSION='3.14.15926-gog78464'
ARCHIVE_BASE_59_URL='https://www.gog.com/game/stellaris'

ARCHIVE_BASE_58_NAME='stellaris_3_13_2_0_76713.sh'
ARCHIVE_BASE_58_MD5='e8a71267c39846f3fa51a7f14f0d2177'
ARCHIVE_BASE_58_SIZE='19165769'
ARCHIVE_BASE_58_VERSION='3.13.2-gog76713'

ARCHIVE_BASE_55_NAME='stellaris_3_12_5_74193.sh'
ARCHIVE_BASE_55_MD5='bb1b18f25732a09a6f257554a1d98375'
ARCHIVE_BASE_55_SIZE='18792782'
ARCHIVE_BASE_55_VERSION='3.12.5-gog74193'

ARCHIVE_BASE_51_NAME='stellaris_3_11_3_0_72561.sh'
ARCHIVE_BASE_51_MD5='a03584bd08d67544607fade70c8cb157'
ARCHIVE_BASE_51_SIZE='16450506'
ARCHIVE_BASE_51_VERSION='3.11.3-gog72561'

ARCHIVE_BASE_48_NAME='stellaris_3_10_4_70670.sh'
ARCHIVE_BASE_48_MD5='8703e439a2588f88987ad7d600674714'
ARCHIVE_BASE_48_SIZE='16449439'
ARCHIVE_BASE_48_VERSION='3.10.4-gog70670'

ARCHIVE_BASE_42_NAME='stellaris_3_9_3_68314.sh'
ARCHIVE_BASE_42_MD5='fb96d68d043643b032328023c09bbb53'
ARCHIVE_BASE_42_SIZE='16222112'
ARCHIVE_BASE_42_VERSION='3.9.3-gog68314'

ARCHIVE_BASE_39_NAME='stellaris_3_8_4_1_65337.sh'
ARCHIVE_BASE_39_MD5='95e375ad5250a5ecc525e9f873edf642'
ARCHIVE_BASE_39_SIZE='17000000'
ARCHIVE_BASE_39_VERSION='3.8.4.1-gog65337'

ARCHIVE_BASE_35_NAME='stellaris_3_7_4_63489.sh'
ARCHIVE_BASE_35_MD5='2c0200a3eac988cf300b259ec0e75745'
ARCHIVE_BASE_35_SIZE='16000000'
ARCHIVE_BASE_35_VERSION='3.7.4-gog63489'

ARCHIVE_BASE_32_NAME='stellaris_3_6_1_60754.sh'
ARCHIVE_BASE_32_MD5='d2a56659e3ff14f1916e54cda1848efb'
ARCHIVE_BASE_32_SIZE='16000000'
ARCHIVE_BASE_32_VERSION='3.6.1-gog60754'

ARCHIVE_BASE_30_NAME='stellaris_3_5_3_59410.sh'
ARCHIVE_BASE_30_MD5='4476050de7a99e8de7dfb9674d770ab4'
ARCHIVE_BASE_30_SIZE='16000000'
ARCHIVE_BASE_30_VERSION='3.5.3-gog59410'

ARCHIVE_BASE_28_NAME='stellaris_3_4_5_56913.sh'
ARCHIVE_BASE_28_MD5='aadcdd3e618635fe0a014ee649ed9204'
ARCHIVE_BASE_28_SIZE='16000000'
ARCHIVE_BASE_28_VERSION='3.4.5-gog56913'

ARCHIVE_BASE_24_NAME='stellaris_3_3_4_54421.sh'
ARCHIVE_BASE_24_MD5='2fe3bf9955468eed06bfaaf6e8e48f89'
ARCHIVE_BASE_24_SIZE='15000000'
ARCHIVE_BASE_24_VERSION='3.3.4-gog54421'

ARCHIVE_BASE_20_NAME='stellaris_3_2_1_1_51520.sh'
ARCHIVE_BASE_20_MD5='91d1173e84573c04b4b84978dfe068e5'
ARCHIVE_BASE_20_SIZE='15000000'
ARCHIVE_BASE_20_VERSION='3.2.1.1-gog51520'

ARCHIVE_BASE_19_NAME='stellaris_3_1_1_50008.sh'
ARCHIVE_BASE_19_MD5='157bbf508ea354ced1cbbf0d06c15b0f'
ARCHIVE_BASE_19_SIZE='12000000'
ARCHIVE_BASE_19_VERSION='3.1.1-gog50008'

ARCHIVE_BASE_18_NAME='stellaris_3_0_3_47193.sh'
ARCHIVE_BASE_18_MD5='3c818f2b540998ddcc9c18dd98e15cba'
ARCHIVE_BASE_18_SIZE='12000000'
ARCHIVE_BASE_18_VERSION='3.0.3-gog47193'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libnakama-cpp.so
libpops_api.so'
CONTENT_GAME_BIN_FILES='
crash_reporter
tools
stellaris'
CONTENT_GAME_DATA_MODELS_SHIPS_FILES='
gfx/models/ships'
CONTENT_GAME_DATA_MODELS_FILES='
gfx/models'
CONTENT_GAME_DATA_FILES='
common
curated_save_games
dlc
dlc_metadata
events
flags
fonts
gfx
interface
licenses
locales
localisation
localisation_synced
map
music
prescripted_countries
previewer_assets
sound
tweakergui_assets
unchecked_defines
ChangeLog.txt
ChangeLogBlank.txt
checksum_manifest.txt
ThirdPartyLicenses.txt'
CONTENT_GAME_MULTIPLAYER_FILES='
launcher-assets
livepp
pdx_launcher
pdx_online_assets
dowser
launcher-installer-linux_2024.14
pdx_core_test
launcher-settings.json'

## TODO: Check why a symlinks farm prefix can not be used
APP_MAIN_PREFIX_TYPE='none'
APP_MAIN_EXE='stellaris'
APP_MAIN_ICON='gfx/exe_icon.bmp'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_MODELS_SHIPS
PKG_DATA_MODELS
PKG_DATA
PKG_MULTIPLAYER'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libresolv.so.2
libSDL2-2.0.so.0
libstdc++.so.6
libuuid.so.1
libX11.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_MODELS'

PKG_DATA_MODELS_ID="${PKG_DATA_ID}-models"
PKG_DATA_MODELS_DESCRIPTION="${PKG_DATA_DESCRIPTION} - models"
PKG_DATA_MODELS_DEPENDENCIES_SIBLINGS='
PKG_DATA_MODELS_SHIPS'

PKG_DATA_MODELS_SHIPS_ID="${PKG_DATA_MODELS_ID}-ships"
PKG_DATA_MODELS_SHIPS_DESCRIPTION="${PKG_DATA_MODELS_DESCRIPTION} - ships"

PKG_MULTIPLAYER_ID="${GAME_ID}-multiplayer"
PKG_MULTIPLAYER_DESCRIPTION='Paradox launcher'
PKG_MULTIPLAYER_ARCH='64'
PKG_MULTIPLAYER_DEPENDENCIES_SIBLINGS='
PKG_BIN'
PKG_MULTIPLAYER_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6
libX11.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the use of system-provided SDL
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(launcher_tweak_sdl_override)"

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
