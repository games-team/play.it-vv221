#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warhammer 40k: Gladius expansions (extra content):
# - Lord of Skulls
###

script_version=20241212.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warhammer-40k-gladius'
GAME_NAME='Warhammer 40,000: Gladius'

EXPANSION_ID_LORDOFSKULLS='lord-of-skulls'
EXPANSION_NAME_LORDOFSKULLS='Lord of Skulls'

# Archives

## Lord of Skulls

ARCHIVE_BASE_LORDOFSKULLS_20_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_1_14_03_77940.sh'
ARCHIVE_BASE_LORDOFSKULLS_20_MD5='d175e20aa6d48a3ee6e06929b4c34ff2'
ARCHIVE_BASE_LORDOFSKULLS_20_SIZE='1086'
ARCHIVE_BASE_LORDOFSKULLS_20_VERSION='1.14.3-gog77940'
ARCHIVE_BASE_LORDOFSKULLS_20_URL='https://www.gog.com/game/warhammer_40000_gladius_relics_of_war_lord_of_skulls'

ARCHIVE_BASE_LORDOFSKULLS_17_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_1_13_03_72014.sh'
ARCHIVE_BASE_LORDOFSKULLS_17_MD5='4e14b2240bb38489dad212e05cbadba3'
ARCHIVE_BASE_LORDOFSKULLS_17_SIZE='1086'
ARCHIVE_BASE_LORDOFSKULLS_17_VERSION='1.13.3-gog72014'

ARCHIVE_BASE_LORDOFSKULLS_14_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_1_12_03_67548.sh'
ARCHIVE_BASE_LORDOFSKULLS_14_MD5='b0481ff1c15b0393bfd7c1b1c8d80709'
ARCHIVE_BASE_LORDOFSKULLS_14_SIZE='1400'
ARCHIVE_BASE_LORDOFSKULLS_14_VERSION='1.12.3-gog67548'

ARCHIVE_BASE_LORDOFSKULLS_11_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_1_11_04_64361.sh'
ARCHIVE_BASE_LORDOFSKULLS_11_MD5='82bf8335b67ecfc66ca0a58abfee40b6'
ARCHIVE_BASE_LORDOFSKULLS_11_SIZE='1400'
ARCHIVE_BASE_LORDOFSKULLS_11_VERSION='1.11.4-gog64361'

ARCHIVE_BASE_LORDOFSKULLS_6_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_1_10_01_00_57375.sh'
ARCHIVE_BASE_LORDOFSKULLS_6_MD5='ad8818e83aec830eb11385cfbf1c3d10'
ARCHIVE_BASE_LORDOFSKULLS_6_SIZE='1400'
ARCHIVE_BASE_LORDOFSKULLS_6_VERSION='1.10.1-gog57375'

ARCHIVE_BASE_LORDOFSKULLS_4_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_v1_09_03_00_54377.sh'
ARCHIVE_BASE_LORDOFSKULLS_4_MD5='a3e949e976d3248ea338a4555454c804'
ARCHIVE_BASE_LORDOFSKULLS_4_SIZE='1400'
ARCHIVE_BASE_LORDOFSKULLS_4_VERSION='1.9.3-gog54377'

ARCHIVE_BASE_LORDOFSKULLS_2_NAME='warhammer_40_000_gladius_relics_of_war_lord_of_skulls_1_08_04_01_49548.sh'
ARCHIVE_BASE_LORDOFSKULLS_2_MD5='a5cad7f6b77e112e10172709f531bb76'
ARCHIVE_BASE_LORDOFSKULLS_2_SIZE='1400'
ARCHIVE_BASE_LORDOFSKULLS_2_VERSION='1.8.4.1-gog49548'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
Data/*.dlc'

PKG_PARENT_ID="$GAME_ID"
PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
