#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2020 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# realMyst: Masterpiece Edition
###

script_version=20250112.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='myst-1-realmyst-masterpiece-edition'
GAME_NAME='realMyst: Masterpiece Edition'

ARCHIVE_BASE_0_NAME='setup_real_myst_masterpiece_edition_2.2_rev_10535_(64bit)_(23829).exe'
ARCHIVE_BASE_0_MD5='fcb23e0256ab826e9a2ba9cad00d9a66'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_real_myst_masterpiece_edition_2.2_rev_10535_(64bit)_(23829)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='038b24ec51a18b325574293d7f2d0ec2'
ARCHIVE_BASE_0_VERSION='2.2.10535-gog23829'
ARCHIVE_BASE_0_SIZE='2800000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/real_myst_masterpiece_edition'

UNITY3D_NAME='realmyst'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Cyan Worlds/realMyst'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
