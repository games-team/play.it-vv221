#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Desperados 3
###

script_version=20241226.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='desperados-3'
GAME_NAME='Desperados Ⅲ'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_0_NAME='setup_desperados_3_1.7_(43311).exe'
ARCHIVE_BASE_0_MD5='27d07bcafb7b0a0c5bd0af5a0b91edb8'
## Do not convert file paths to lowercase
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_desperados_3_1.7_(43311)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='5554579fd67870ad94347b70df2743f6'
ARCHIVE_BASE_0_PART2_NAME='setup_desperados_3_1.7_(43311)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='f7f43119dda91362ec45b122e4ea641d'
ARCHIVE_BASE_0_PART3_NAME='setup_desperados_3_1.7_(43311)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='51948b7e8d6a9e45638b49988f86c5f9'
ARCHIVE_BASE_0_SIZE='24000000'
ARCHIVE_BASE_0_VERSION='1.5.8-gog43311'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/desperados_iii'

ARCHIVE_BASE_DEMO_0_NAME='setup_desperados_3_demo_1.0b_(38752).exe'
ARCHIVE_BASE_DEMO_0_MD5='2a856f6a70fb57b7095d829fe90f0e61'
## Do not convert file paths to lowercase
ARCHIVE_BASE_DEMO_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_DEMO_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_DEMO_0_PART1_NAME='setup_desperados_3_demo_1.0b_(38752)-1.bin'
ARCHIVE_BASE_DEMO_0_PART1_MD5='fa829f953b1183dd8a41c12599c992fe'
ARCHIVE_BASE_DEMO_0_PART2_NAME='setup_desperados_3_demo_1.0b_(38752)-2.bin'
ARCHIVE_BASE_DEMO_0_PART2_MD5='77e764c9d83158abf31b7e98006cc5c9'
ARCHIVE_BASE_DEMO_0_SIZE='7537932'
ARCHIVE_BASE_DEMO_0_VERSION='1.0b-gog38752'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/desperados_iii_demo'

UNITY3D_NAME='Desperados III'

CONTENT_PATH_DEFAULT='.'
## The game fails to start if "THQNOnline" and "thqnocfg.dat" are not included.
CONTENT_GAME0_BIN_FILES='
THQNOnline
MiVersion
thqnocfg.dat'
## The game fails to start if the Steam library is not included.
CONTENT_GAME1_BIN_FILES='
steam_api64.dll'
CONTENT_GAME_DATA_GI_FILES="
${UNITY3D_NAME}_Data/GI"
CONTENT_GAME_DATA_STREAMINGASSETS_FILES="
${UNITY3D_NAME}_Data/StreamingAssets"
CONTENT_GAME_DATA_LEVELS_FILES_DEMO="
${UNITY3D_NAME}_Data/level*"
CONTENT_GAME_DATA_LEVELS_1_FILES="
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/level?.resS
${UNITY3D_NAME}_Data/level??
${UNITY3D_NAME}_Data/level??.resS"
CONTENT_GAME_DATA_LEVELS_2_FILES="
${UNITY3D_NAME}_Data/level???
${UNITY3D_NAME}_Data/level???.resS"

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/Desperados III'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_GI
PKG_DATA_STREAMINGASSETS
PKG_DATA_LEVELS_1
PKG_DATA_LEVELS_2
PKG_DATA'
PACKAGES_LIST_DEMO='
PKG_BIN
PKG_DATA_GI
PKG_DATA_STREAMINGASSETS
PKG_DATA_LEVELS
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_GI
PKG_DATA_STREAMINGASSETS
PKG_DATA_LEVELS_1
PKG_DATA_LEVELS_2'
PKG_DATA_DEPENDENCIES_SIBLINGS_DEMO='
PKG_DATA_GI
PKG_DATA_STREAMINGASSETS
PKG_DATA_LEVELS'

PKG_DATA_GI_ID="${PKG_DATA_ID}-gi"
PKG_DATA_GI_ID_DEMO="${PKG_DATA_ID_DEMO}-gi"
PKG_DATA_GI_DESCRIPTION="$PKG_DATA_DESCRIPTION - gi"

PKG_DATA_STREAMINGASSETS_ID="${PKG_DATA_ID}-streamingassets"
PKG_DATA_STREAMINGASSETS_ID_DEMO="${PKG_DATA_ID_DEMO}-streamingassets"
PKG_DATA_STREAMINGASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - streamingassets"

PKG_DATA_LEVELS_ID="${PKG_DATA_ID}-levels"
PKG_DATA_LEVELS_ID_DEMO="${PKG_DATA_ID_DEMO}-levels"
PKG_DATA_LEVELS_1_ID="${PKG_DATA_LEVELS_ID}-1"
PKG_DATA_LEVELS_2_ID="${PKG_DATA_LEVELS_ID}-2"
PKG_DATA_LEVELS_DESCRIPTION="$PKG_DATA_DESCRIPTION - levels"
PKG_DATA_LEVELS_1_DESCRIPTION="$PKG_DATA_LEVELS_DESCRIPTION - 1"
PKG_DATA_LEVELS_2_DESCRIPTION="$PKG_DATA_LEVELS_DESCRIPTION - 2"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
