#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Song of Farca
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='song-of-farca'
GAME_NAME='Song of Farca'

ARCHIVE_BASE_2_NAME='song_of_farca_1_0_2_15_59133.sh'
ARCHIVE_BASE_2_MD5='3130d206d897c7e41b8349a426fe0a49'
ARCHIVE_BASE_2_SIZE='1820017'
ARCHIVE_BASE_2_VERSION='1.0.2.15-gog59133'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/song_of_farca'

ARCHIVE_BASE_1_NAME='song_of_farca_1_0_2_8_52330.sh'
ARCHIVE_BASE_1_MD5='dd45ac62732f088acc44f60ddbb97724'
ARCHIVE_BASE_1_SIZE='1900000'
ARCHIVE_BASE_1_VERSION='1.0.2.8-gog52330'

ARCHIVE_BASE_0_NAME='song_of_farca_1_0_2_2_49499.sh'
ARCHIVE_BASE_0_MD5='d06f0dd62349c0834f1a8366f5525935'
ARCHIVE_BASE_0_SIZE='1900000'
ARCHIVE_BASE_0_VERSION='1.0.2.2-gog49499'

UNITY3D_NAME='Song of Farca'
UNITY3D_PLUGINS='
libzipw.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"
 
# Run the default initialization actions

initialization_default "$@"

# Extract game data
 
archive_extraction_default

# Include game data
 
content_inclusion_icons 'PKG_DATA'
content_inclusion_default
 
# Write launchers
 
launchers_generation 'PKG_BIN'
 
# Build packages
 
packages_generation
print_instructions
 
# Clean up
 
working_directory_cleanup
 
exit 0
