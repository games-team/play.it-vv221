#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Raji: An Ancient Epic
###

script_version=20241226.3

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='raji-an-ancient-epic'
GAME_NAME='Raji: An Ancient Epic'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

# Archives

## Raji: An Ancient Epic (full game)

ARCHIVE_BASE_1_NAME='setup_raji_an_ancient_epic_1.6.0_(64bit)_(57469).exe'
ARCHIVE_BASE_1_MD5='d05e588d8f7a32b5471a0c193ca10adb'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_raji_an_ancient_epic_1.6.0_(64bit)_(57469)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='84f8c041aba726936ce450c481a1962c'
ARCHIVE_BASE_1_PART2_NAME='setup_raji_an_ancient_epic_1.6.0_(64bit)_(57469)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='3dc293acb794752426baa2e6174b25dc'
ARCHIVE_BASE_1_SIZE='6400000'
ARCHIVE_BASE_1_VERSION='1.6.0-gog57469'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/raji_an_ancient_epic'

ARCHIVE_BASE_0_NAME='setup_raji_an_ancient_epic_1.4.0_(64bit)_(45493).exe'
ARCHIVE_BASE_0_MD5='5defde7fed6a972c5f40f7b58a4631a9'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_raji_an_ancient_epic_1.4.0_(64bit)_(45493)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='db0573d9b6647e12989ef8d46842bdce'
ARCHIVE_BASE_0_PART2_NAME='setup_raji_an_ancient_epic_1.4.0_(64bit)_(45493)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='dfabd86fda6c240335eea1d2338c6b5f'
ARCHIVE_BASE_0_SIZE='5900000'
ARCHIVE_BASE_0_VERSION='1.4.0-gog45493'

## Raji: An Ancient Epic (demo)

ARCHIVE_BASE_DEMO_0_NAME='Raji Demo v11.zip'
ARCHIVE_BASE_DEMO_0_MD5='b021b724963d80b4e4690fe096969a32'
ARCHIVE_BASE_DEMO_0_SIZE='3100000'
ARCHIVE_BASE_DEMO_0_VERSION='11-itch.2020.10.21'
ARCHIVE_BASE_DEMO_0_URL='https://rajithegame.itch.io/raji-an-ancient-epic'

## Optional icons pack

ARCHIVE_OPTIONAL_ICONS_NAME='raji-an-ancient-epic_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='befa06adad5d44a2946cc19f54b0a267'
ARCHIVE_OPTIONAL_ICONS_URL='http://downloads.dotslashplay.it/games/raji-an-ancient-epic/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
256x256'


UNREALENGINE4_NAME='raji'
UNREALENGINE4_NAME_DEMO='Raji'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_DEMO='Raji Demo v11'

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"
APP_MAIN_EXE_DEMO="${UNREALENGINE4_NAME_DEMO}.exe"
## The rendering is done using Direct3D 12 by default starting with game version 1.6.0.
## We force Direct3D 11 instead as it is better supported by current WINE builds.
## TODO: Check if it is still required with current WINE builds
APP_MAIN_OPTIONS='-d3d11'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default
## Convert icon file name for the game demo,
## if it has been provided by the optional icons archive
case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		icon_source="$(package_path 'PKG_DATA')$(path_icons)/256x256/apps/${GAME_ID}.png"
		icon_destination="$(package_path 'PKG_DATA')$(path_icons)/256x256/apps/$(game_id).png"
		if [ -e "$icon_source" ]; then
			mv "$icon_source" "$icon_destination"
		fi
	;;
esac

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
