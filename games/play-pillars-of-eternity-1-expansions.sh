#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pillars of Eternity expansions:
# - Kickstarter item
# - Kickstarter pet
# - Pre-order item and pet
# - The White March, Part 1
# - The White March, Part 2
# - Deadfire Pack
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='pillars-of-eternity-1'
GAME_NAME='Pillars of Eternity'

EXPANSION_ID_TIER1BACKER='kickstarter-item'
EXPANSION_NAME_TIER1BACKER='Kickstarter item'

EXPANSION_ID_TIER2BACKER='kickstarter-pet'
EXPANSION_NAME_TIER2BACKER='Kickstarter pet'

EXPANSION_ID_PREORDER='preorder-content'
EXPANSION_NAME_PREORDER='Pre-order item and pet'

EXPANSION_ID_TWM1='the-white-march-part-1'
EXPANSION_NAME_TWM1='The White March Part Ⅰ'

EXPANSION_ID_TWM2='the-white-march-part-2'
EXPANSION_NAME_TWM2='The White March Part Ⅱ'

EXPANSION_ID_DEADFIRE='deadfire-pack'
EXPANSION_NAME_DEADFIRE='Deadfire Pack'

# Archives

## Kickstarter item

ARCHIVE_BASE_TIER1BACKER_0_NAME='gog_pillars_of_eternity_kickstarter_item_dlc_2.0.0.2.sh'
ARCHIVE_BASE_TIER1BACKER_0_MD5='b4c29ae17c87956471f2d76d8931a4e5'
ARCHIVE_BASE_TIER1BACKER_0_SIZE='924'
ARCHIVE_BASE_TIER1BACKER_0_VERSION='1.0-gog2.0.0.2'

## Kickstarter pet

ARCHIVE_BASE_TIER2BACKER_0_NAME='gog_pillars_of_eternity_kickstarter_pet_dlc_2.0.0.2.sh'
ARCHIVE_BASE_TIER2BACKER_0_MD5='3653fc2a98ef578335f89b607f0b7968'
ARCHIVE_BASE_TIER2BACKER_0_SIZE='924'
ARCHIVE_BASE_TIER2BACKER_0_VERSION='1.0-gog2.0.0.2'

## Pre-order item and pet

ARCHIVE_BASE_PREORDER_0_NAME='gog_pillars_of_eternity_preorder_item_and_pet_dlc_2.0.0.2.sh'
ARCHIVE_BASE_PREORDER_0_MD5='b86ad866acb62937d2127407e4beab19'
ARCHIVE_BASE_PREORDER_0_SIZE='924'
ARCHIVE_BASE_PREORDER_0_VERSION='1.0-gog2.0.0.2'

## The White March, Part 1

ARCHIVE_BASE_TWM1_3_NAME='pillars_of_eternity_white_march_part_1_dlc_en_3_07_0_1318_17464.sh'
ARCHIVE_BASE_TWM1_3_MD5='cc72f59ee20238ff05c47646b4618f01'
ARCHIVE_BASE_TWM1_3_SIZE='5507108'
ARCHIVE_BASE_TWM1_3_VERSION='3.7.0.1318-gog17464'
ARCHIVE_BASE_TWM1_3_URL='https://www.gog.com/game/pillars_of_eternity_the_white_march_part_1'

ARCHIVE_BASE_TWM1_2_NAME='pillars_of_eternity_white_march_part_1_dlc_en_3_07_16598.sh'
ARCHIVE_BASE_TWM1_2_MD5='054b6af430da1ed2635b9c6b4ed56866'
ARCHIVE_BASE_TWM1_2_SIZE='5500000'
ARCHIVE_BASE_TWM1_2_VERSION='3.7.0.1284-gog16598'

ARCHIVE_BASE_TWM1_1_NAME='gog_pillars_of_eternity_white_march_part_1_dlc_2.10.0.12.sh'
ARCHIVE_BASE_TWM1_1_MD5='8fafcb549fffd2de24f381a85e859622'
ARCHIVE_BASE_TWM1_1_SIZE='5500000'
ARCHIVE_BASE_TWM1_1_VERSION='3.06.0.1254-gog2.10.0.12'

ARCHIVE_BASE_TWM1_0_NAME='gog_pillars_of_eternity_white_march_part_1_dlc_2.9.0.11.sh'
ARCHIVE_BASE_TWM1_0_MD5='98424615626c82ed723860d421f187b6'
ARCHIVE_BASE_TWM1_0_SIZE='5500000'
ARCHIVE_BASE_TWM1_0_VERSION='3.05.0.1186-gog2.9.0.11'

## The White March, Part 2

ARCHIVE_BASE_TWM2_3_NAME='pillars_of_eternity_white_march_part_2_dlc_en_3_07_0_1318_17464.sh'
ARCHIVE_BASE_TWM2_3_MD5='03067ebdd878cc16c283f63ddf015e90'
ARCHIVE_BASE_TWM2_3_SIZE='4360158'
ARCHIVE_BASE_TWM2_3_VERSION='3.7.0.1318-gog17464'
ARCHIVE_BASE_TWM2_3_URL='https://www.gog.com/game/pillars_of_eternity_the_white_march_part_2'

ARCHIVE_BASE_TWM2_2_NAME='pillars_of_eternity_white_march_part_2_dlc_en_3_07_16598.sh'
ARCHIVE_BASE_TWM2_2_MD5='db3a345b2b2782e2ad075dd32567f303'
ARCHIVE_BASE_TWM2_2_SIZE='4300000'
ARCHIVE_BASE_TWM2_2_VERSION='3.7.0.1284-gog16598'

ARCHIVE_BASE_TWM2_1_NAME='gog_pillars_of_eternity_white_march_part_2_dlc_2.6.0.7.sh'
ARCHIVE_BASE_TWM2_1_MD5='fdc1446661a358961379fbec24c44680'
ARCHIVE_BASE_TWM2_1_SIZE='4400000'
ARCHIVE_BASE_TWM2_1_VERSION='3.06.1254-gog2.6.0.7'

ARCHIVE_BASE_TWM2_0_NAME='gog_pillars_of_eternity_white_march_part_2_dlc_2.5.0.6.sh'
ARCHIVE_BASE_TWM2_0_MD5='483d4b8cc046a07ec91a6306d3409e23'
ARCHIVE_BASE_TWM2_0_SIZE='4400000'
ARCHIVE_BASE_TWM2_0_VERSION='3.05.1186-gog2.5.0.6'

## Deadfire Pack

ARCHIVE_BASE_DEADFIRE_2_NAME='pillars_of_eternity_deadfire_pack_dlc_en_3_07_0_1318_20099.sh'
ARCHIVE_BASE_DEADFIRE_2_MD5='da315aba26784e55aa51139cebb7f9d2'
ARCHIVE_BASE_DEADFIRE_2_SIZE='922'
ARCHIVE_BASE_DEADFIRE_2_VERSION='3.07.0.1318-gog20099'
ARCHIVE_BASE_DEADFIRE_2_URL='https://www.gog.com/game/pillars_of_eternity_deadfire_pack'

ARCHIVE_BASE_DEADFIRE_1_NAME='pillars_of_eternity_deadfire_pack_dlc_en_3_07_0_1318_17462.sh'
ARCHIVE_BASE_DEADFIRE_1_MD5='021362da5912dc8a3e47473e97726f7f'
ARCHIVE_BASE_DEADFIRE_1_SIZE='1300'
ARCHIVE_BASE_DEADFIRE_1_VERSION='3.07.0.1318-gog17462'

ARCHIVE_BASE_DEADFIRE_0_NAME='pillars_of_eternity_deadfire_pack_dlc_en_3_07_16380.sh'
ARCHIVE_BASE_DEADFIRE_0_MD5='2fc0dc21648953be1c571e28b1e3d002'
ARCHIVE_BASE_DEADFIRE_0_SIZE='1300'
ARCHIVE_BASE_DEADFIRE_0_VERSION='3.07-gog16380'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
PillarsOfEternity_Data'

PKG_PARENT_ID="$GAME_ID"
PKG_TWM1_ID="${GAME_ID}-${EXPANSION_ID_TWM1}"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'
PKG_MAIN_DEPENDENCIES_SIBLINGS_TWM2="$PKG_MAIN_DEPENDENCIES_SIBLINGS
PKG_TWM1"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# The White March, Part 2 - Remove a file already provided by a dependency
	case "$(current_archive)" in
		('ARCHIVE_BASE_TWM2_'*)
			rm --force 'PillarsOfEternity_Data/assetbundles/prefabs/objectbundle/px1_cre_blight_ice_terror.unity3d'
		;;
	esac

	# Deadfire Pack - Fix a typo in a file path
	case "$(current_archive)" in
		('ARCHIVE_BASE_DEADFIRE_'*)
			if [ -e 'PillarsOfEternity_data' ]; then
				mv 'PillarsOfEternity_data' 'PillarsOfEternity_Data'
			fi
		;;
	esac
)

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
