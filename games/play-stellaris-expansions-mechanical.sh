#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Stellaris expansions (mechanical expansions):
# - Cosmic Storms
###

script_version=20241215.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='stellaris'
GAME_NAME='Stellaris'

EXPANSION_ID_STORMS='cosmic-storms'
EXPANSION_NAME_STORMS='Cosmic Storms'

# Archives

## Cosmic Storms

ARCHIVE_BASE_STORMS_3_NAME='stellaris_cosmic_storms_3_14_15926_78464.sh'
ARCHIVE_BASE_STORMS_3_MD5='6cf3fcb0ef883746b11eb3dd55c67a89'
ARCHIVE_BASE_STORMS_3_SIZE='829'
ARCHIVE_BASE_STORMS_3_VERSION='3.14.15926-gog78464'
ARCHIVE_BASE_STORMS_3_URL='https://www.gog.com/game/stellaris_cosmic_storms'

ARCHIVE_BASE_STORMS_2_NAME='stellaris_cosmic_storms_3_13_2_0_76713.sh'
ARCHIVE_BASE_STORMS_2_MD5='a011e1ef5f111c624be7cbbcd5a924fa'
ARCHIVE_BASE_STORMS_2_SIZE='829'
ARCHIVE_BASE_STORMS_2_VERSION='3.13.2-gog76713'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
dlc'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
