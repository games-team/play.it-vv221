#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 BetaRays
set -o errexit

###
# Human Resource Machine
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='human-resource-machine'
GAME_NAME='Human Resource Machine'

ARCHIVE_BASE_HUMBLE_0_NAME='HumanResourceMachine-Linux-2016-03-23.sh'
ARCHIVE_BASE_HUMBLE_0_MD5='ac6013284194203c4732e713dcc0f543'
ARCHIVE_BASE_HUMBLE_0_SIZE='73000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0.8262-humble.2016.03.23'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/human-resource-machine'

ARCHIVE_BASE_GOG_1_NAME='gog_human_resource_machine_2.0.0.3.sh'
ARCHIVE_BASE_GOG_1_MD5='4670105392afc503b880c78d56a2f1ad'
ARCHIVE_BASE_GOG_1_SIZE='74000'
ARCHIVE_BASE_GOG_1_VERSION='1.0.8262-gog2.0.0.3'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/human_resource_machine'

ARCHIVE_BASE_GOG_0_NAME='gog_human_resource_machine_2.0.0.2.sh'
ARCHIVE_BASE_GOG_0_MD5='35bc19839c73ddf4b503c58a0a887f98'
ARCHIVE_BASE_GOG_0_SIZE='74000'
ARCHIVE_BASE_GOG_0_VERSION='1.0.8262-gog2.0.0.2'

CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_GAME_BIN64_RELATIVE_PATH_HUMBLE='x86_64'
CONTENT_GAME_BIN64_FILES='
HumanResourceMachine.bin.x86_64'
CONTENT_GAME_BIN32_RELATIVE_PATH_HUMBLE='x86'
CONTENT_GAME_BIN32_FILES='
HumanResourceMachine.bin.x86'
CONTENT_GAME_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_GAME_DATA_FILES='
shaders
resource.pak
icon.png'
CONTENT_DOC_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_DOC_DATA_FILES='
LICENSE.txt
README.linux'

APP_MAIN_EXE_BIN64='HumanResourceMachine.bin.x86_64'
APP_MAIN_EXE_BIN32='HumanResourceMachine.bin.x86'
APP_MAIN_ICON_HUMBLE='noarch/icon.png'
APP_MAIN_ICON_GOG='icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
