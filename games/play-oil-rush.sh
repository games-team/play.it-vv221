#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Oil Rush
###

script_version=20250104.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='oil-rush'
GAME_NAME='Oil Rush'

## This archive used to be provided by Humble Bundle,
## but it is no longer available for sale.
ARCHIVE_BASE_0_NAME='OilRush_1.35_Linux_1370041755.run'
ARCHIVE_BASE_0_MD5='5a38d0545a64e0d21ddfe4afc15253a8'
ARCHIVE_BASE_0_SIZE='1351677'
ARCHIVE_BASE_0_VERSION='1.35-humble1'

CONTENT_PATH_DEFAULT='.'
CONTENT_LIBS_BIN64_RELATIVE_PATH='bin'
CONTENT_LIBS_BIN64_FILES='
libOilRush_x64.so
libQtCoreUnigine_x64.so.4
libQtGuiUnigine_x64.so.4
libQtNetworkUnigine_x64.so.4
libQtWebKitUnigine_x64.so.4
libQtXmlUnigine_x64.so.4
libRakNet_x64.so
libUnigine_x64.so'
CONTENT_LIBS_BIN32_RELATIVE_PATH='bin'
CONTENT_LIBS_BIN32_FILES='
libOilRush_x86.so
libQtCoreUnigine_x86.so.4
libQtGuiUnigine_x86.so.4
libQtNetworkUnigine_x86.so.4
libQtWebKitUnigine_x86.so.4
libQtXmlUnigine_x86.so.4
libRakNet_x86.so
libUnigine_x86.so'
CONTENT_GAME_BIN64_FILES='
bin/launcher_x64
bin/OilRush_x64'
CONTENT_GAME_BIN32_FILES='
bin/launcher_x86
bin/OilRush_x86'
CONTENT_GAME_DATA_FILES='
data
oilrush.cfg'
CONTENT_DOC_DATA_PATH='documentation'
CONTENT_DOC_DATA_FILES='
user_manual.pdf'

USER_PERSISTENT_FILES='
oilrush.cfg
data/launcher/launcher*.xml'

APP_MAIN_EXE_BIN64='bin/launcher_x64'
APP_MAIN_EXE_BIN32='bin/launcher_x86'
APP_MAIN_OPTIONS='-config ../data/launcher/launcher.xml'
APP_MAIN_ICON='data/launcher/oilrush.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXext.so.6
libXinerama.so.1
libXrandr.so.2
libXrender.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set the execution bit on the game binaries
	chmod 755 'bin/OilRush_x86' 'bin/OilRush_x64'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	local application
	application="$1"

	local application_exe application_options
	application_exe=$(application_exe "$application")
	application_options=$(application_options "$application")
	cat <<- EOF
	cd "$(dirname "$application_exe")"
	"./$(basename "$application_exe")" $application_options "\$@"
	EOF
}

## Copy the real game binary into the game prefix.
## (bin/OilRush_x64 instead of bin/launcher_x64)
native_launcher_binary_copy() {
	cat <<- 'EOF'
	# Copy the game binary into the user prefix

	exe_destination="${PATH_PREFIX}/bin/OilRush_x64"
	if [ -h "$exe_destination" ]; then
	    exe_source=$(realpath "$exe_destination")
	    cp --remove-destination "$exe_source" "$exe_destination"
	fi
	unset exe_destination exe_source

	EOF
}

launchers_generation 'PKG_BIN64'

## Copy the real game binary into the game prefix.
## (bin/OilRush_x86 instead of bin/launcher_x86)
native_launcher_binary_copy() {
	cat <<- 'EOF'
	# Copy the game binary into the user prefix

	exe_destination="${PATH_PREFIX}/bin/OilRush_x86"
	if [ -h "$exe_destination" ]; then
	    exe_source=$(realpath "$exe_destination")
	    cp --remove-destination "$exe_source" "$exe_destination"
	fi
	unset exe_destination exe_source

	EOF
}

launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
