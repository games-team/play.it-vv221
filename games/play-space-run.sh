#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Space Run: Fast and safe delivery
###

script_version=20241120.4

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='space-run'
GAME_NAME='Space Run: Fast and safe delivery'

ARCHIVE_BASE_0_NAME='setup_space_run_2.1.0.2.exe'
ARCHIVE_BASE_0_MD5='e370f25708f12ef1916400a25c2c4e9c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1263910'
ARCHIVE_BASE_0_VERSION='1.11e-gog2.1.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/space_run'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
bass.dll
cg.dll
cggl.dll
chartdir50.dll
crashrpt.dll
dbghelp.dll
devil.dll
fbxsdk-2013.3.dll
fmod_event.dll
fmod_event_net.dll
fmodex.dll
glut32.dll
ilu.dll
ilut.dll
libfbxsdk.dll
libmysql.dll
tbb.dll
tier0_s.dll
vstdlib_s.dll
crashsender.exe
ospacegame.exe
crashrpt_lang.ini
reshacker.ini
spaceresources/enginesettings.ini'
## TODO: Check if the Steam libraries are required
CONTENT_GAME0_BIN_FILES='
steam.dll
steam_api.dll
steamclient.dll'
CONTENT_GAME_DATA_FILES='
spaceresources'

USER_PERSISTENT_DIRECTORIES='
Save'
USER_PERSISTENT_FILES='
reshacker.ini
spaceresources/enginesettings.ini'
## Work around the broken in-game language selection
USER_PERSISTENT_FILES="${USER_PERSISTENT_FILES:-}
localization.cfg"

APP_MAIN_EXE='ospacegame.exe'
## Work around the broken in-game language selection
APP_MAIN_PRERUN='
# Work around the broken in-game language selection
LANG=$(cat localization.cfg)
export LANG
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Work around the broken in-game language selection

config_path="$(package_path 'PKG_BIN')$(path_game_data)/localization.cfg"
cat > "$config_path" << EOF
en
EOF

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
