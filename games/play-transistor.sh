#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# Transistor
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='transistor'
GAME_NAME='Transistor'

ARCHIVE_BASE_2_NAME='transistor_1_50440_8123_23365.sh'
ARCHIVE_BASE_2_MD5='dc89c175267dc1a1f3434a9d4f903cce'
ARCHIVE_BASE_2_SIZE='3580037'
ARCHIVE_BASE_2_VERSION='1.50440.8123-gog23365'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/transistor'

ARCHIVE_BASE_1_NAME='transistor_en_v1_50423_21516.sh'
ARCHIVE_BASE_1_MD5='52d0df1d959b333b17ede106f8e53062'
ARCHIVE_BASE_1_SIZE='3600000'
ARCHIVE_BASE_1_VERSION='1.50423-gog21516'

ARCHIVE_BASE_0_NAME='gog_transistor_2.0.0.3.sh'
ARCHIVE_BASE_0_MD5='53dbaf643471f3b8494548261584dd13'
ARCHIVE_BASE_0_SIZE='3200000'
ARCHIVE_BASE_0_VERSION='1.20140310-gog2.0.0.3'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_FILES='
libBink.so
libFModPlugins.so
libfmod.so.4
libfmodstudio.so.4
liblua52.so'
CONTENT_LIBS_LIBS64_RELATIVE_PATH='lib64'
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_RELATIVE_PATH='lib'
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
Content
monoconfig
monomachineconfig
Transistor.exe
Transistor.exe.config
Engine.dll
Engine.dll.config
Engine.SDL2.dll
Engine.SDL2.dll.config
HostessProtocol.dll
KeraLua.dll
MonoGame.Framework.SDL2.dll
NLua.dll
SDL2-CS.dll
SDL2-CS.dll.config
Newtonsoft.Json.dll
Newtonsoft.Json.pdb
Newtonsoft.Json.xml
*.bmp
*.cfg
*.pdb
*.xml
*.txt'
CONTENT_DOC_MAIN_FILES='
Linux.README'

## TODO: Check why a regular symlinks prefix can not be used
APP_MAIN_PREFIX_TYPE='none'
APP_MAIN_EXE='Transistor.exe'
APP_MAIN_ICON='Transistor.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libGL.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Design.dll
System.Drawing.dll
System.Management.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Transactions.dll
System.Xml.dll
System.Xml.Linq.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
