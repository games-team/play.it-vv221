#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# Desperados: Wanted Dead or Alive
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='desperados-1'
GAME_NAME='Desperados: Wanted Dead or Alive'

ARCHIVE_BASE_1_NAME='desperados_wanted_dead_or_alive_en_1_0_2_thqn_22430.sh'
ARCHIVE_BASE_1_MD5='c4338cd7526dc01eef347408368f6bf4'
ARCHIVE_BASE_1_SIZE='1938955'
ARCHIVE_BASE_1_VERSION='1.0.2-gog22430'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/desperados_wanted_dead_or_alive'

ARCHIVE_BASE_0_NAME='desperados_wanted_dead_or_alive_en_gog_1_22137.sh'
ARCHIVE_BASE_0_MD5='72e623355b7ca5ccdef0c549d0a77192'
ARCHIVE_BASE_0_SIZE='2000000'
ARCHIVE_BASE_0_VERSION='1.0-gog22137'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
desperados32'
CONTENT_GAME_DATA_FILES='
bootmenu
data
demo
localisation
localisation_demo
shaders'
CONTENT_DOC_DATA_FILES='
readme.txt'

APP_MAIN_EXE='desperados32'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
