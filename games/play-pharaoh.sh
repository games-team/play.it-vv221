#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pharaoh
###

script_version=20241226.4

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='pharaoh'
GAME_NAME='Pharaoh'

# Archives

## Pharaoh + Cleopatra

ARCHIVE_BASE_0_NAME='setup_pharaoh_gold_2.1.0.15.exe'
ARCHIVE_BASE_0_MD5='62298f00f1f2268c8d5004f5b2e9fc93'
## innoextract --lowercase option should not be used, or Akhenaten will fail to find some required files.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_SIZE='804849'
ARCHIVE_BASE_0_VERSION='2.1-gog2.1.0.15'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/pharaoh_cleopatra'

## Akhenaten
## cf. https://github.com/dalerank/Akhenaten

### release 1036
ARCHIVE_OPTIONAL_AKHENATEN_3_NAME='akhenaten.linux'
ARCHIVE_OPTIONAL_AKHENATEN_3_MD5='d35934a08420b82d55ed41405d5bea61'
ARCHIVE_OPTIONAL_AKHENATEN_3_SIZE='87467'
ARCHIVE_OPTIONAL_AKHENATEN_3_URL='https://dalerank.itch.io/akhenaten'

### release 784
ARCHIVE_OPTIONAL_AKHENATEN_2_NAME='akhenaten.linux'
ARCHIVE_OPTIONAL_AKHENATEN_2_MD5='eebce8cd3a284399d271b1f9e2b02461'
ARCHIVE_OPTIONAL_AKHENATEN_2_SIZE='64301'

### release 205
ARCHIVE_OPTIONAL_AKHENATEN_1_NAME='akhenaten.linux'
ARCHIVE_OPTIONAL_AKHENATEN_1_MD5='cdd40aa1ab541035b1a2c5e02cbe5064'
ARCHIVE_OPTIONAL_AKHENATEN_1_SIZE='34006'

### release 108 (still called "Ozyamandias")
ARCHIVE_OPTIONAL_AKHENATEN_0_NAME='ozymandias.linux'
ARCHIVE_OPTIONAL_AKHENATEN_0_MD5='7e52e2e90f5b00069cd312507d55c518'
ARCHIVE_OPTIONAL_AKHENATEN_0_SIZE='31820'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_WINE_FILES='
MP3DEC.ASI
BINKW32.DLL
mss16.dll
mss32.dll
SMACKW32.DLL
Pharaoh.exe
Pharaoh.ini
mssb16.tsk
*.M3D'
CONTENT_GAME_DATA_FILES='
AUDIO
BINKS
Data
Maps
cleoicon.ico
auto reason phrases.txt
campaign.txt
eventmsg.txt
Figure_model.txt
Figure_model_*.txt
music.txt
Pharaoh_MM.txt
Pharaoh_Model_*.txt
Pharaoh_Text.txt
Tax_Sentiment_Model_*.txt
trade_recommends.txt
Pharaoh2.emp
Pharaoh_MM.eng
Pharaoh_Text.eng
mission1.pak
*.inf'
CONTENT_DOC_DATA_FILES='
Mission Editor Guide.txt
Readme.txt
*.pdf'

USER_PERSISTENT_DIRECTORIES='
Save'
USER_PERSISTENT_FILES='
*.ini'

## WINE - With the default OpenGL renderer, the game menu is not displayed (WINE 9.0).
WINE_DIRECT3D_RENDERER='wined3d/gdi'
## WINE - The game window fails to render anything unless the game runs in a WINE virtual desktop (WINE 9.0).
WINE_VIRTUAL_DESKTOP='auto'
## WINE - Disable CSMT to avoid degraded performances
## TODO: Check if it is still required with current WINE builds
WINE_WINETRICKS_VERBS='csmt=off'

APP_MAIN_EXE_BIN_LINUX='akhenaten.linux'
APP_MAIN_EXE_BIN_WINE='Pharaoh.exe'
APP_MAIN_ICON='cleoicon.ico'

PACKAGES_LIST='
PKG_BIN_WINE
PKG_DATA'
PACKAGES_LIST_AKHENATEN="
PKG_BIN_LINUX
$PACKAGES_LIST"

PKG_BIN_ID="$GAME_ID"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_BIN_LINUX_ID="${PKG_BIN_ID}-akhenaten"
PKG_BIN_LINUX_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_LINUX_DESCRIPTION='using the Akhenaten engine'
PKG_BIN_LINUX_ARCH='64'
PKG_BIN_LINUX_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_LINUX_DEPENDENCIES_LIBRARIES='
libc.so.6
libm.so.6
libSDL2-2.0.so.0
libSDL2_mixer-2.0.so.0'
## Ensure smooth upgrades from packages generated with pre-20231105.3 game scripts
PKG_BIN_LINUX_PROVIDES="${PKG_BIN_LINUX_PROVIDES:-}
pharaoh-ozymandias"

PKG_BIN_WINE_ID="${PKG_BIN_ID}-wine"
PKG_BIN_WINE_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_WINE_DESCRIPTION='using WINE'
PKG_BIN_WINE_ARCH='32'
PKG_BIN_WINE_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of the Akhenaten native engine binary (optional)

archive_initialize_optional \
	'ARCHIVE_AKHENATEN' \
	'ARCHIVE_OPTIONAL_AKHENATEN_3' \
	'ARCHIVE_OPTIONAL_AKHENATEN_2' \
	'ARCHIVE_OPTIONAL_AKHENATEN_1' \
	'ARCHIVE_OPTIONAL_AKHENATEN_0'
if archive_is_available 'ARCHIVE_AKHENATEN'; then
	export PACKAGES_LIST="$PACKAGES_LIST_AKHENATEN"
fi

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default
if archive_is_available 'ARCHIVE_AKHENATEN'; then
	application_exe=$(
		set_current_package 'PKG_BIN_LINUX'
		application_exe 'APP_MAIN'
	)
	install -D --mode=755 \
		"$(archive_path 'ARCHIVE_AKHENATEN')" \
		"$(package_path 'PKG_BIN_LINUX')$(path_game_data)/${application_exe}"
fi

# Write launchers

launchers_generation 'PKG_BIN_WINE'
if archive_is_available 'ARCHIVE_AKHENATEN'; then
	launchers_generation 'PKG_BIN_LINUX'
fi

# Build packages

packages_generation
printf '\n'
if archive_is_available 'ARCHIVE_AKHENATEN'; then
	printf 'Akhenaten:'
	print_instructions 'PKG_DATA' 'PKG_BIN_LINUX'
	printf 'WINE:'
	print_instructions 'PKG_DATA' 'PKG_BIN_WINE'
else
	print_instructions
fi

# Clean up

working_directory_cleanup

exit 0
