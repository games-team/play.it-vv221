#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Iron Harvest
###

script_version=20241104.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='iron-harvest'
GAME_NAME='Iron Harvest'

ARCHIVE_BASE_1_NAME='setup_iron_harvest_1.4.8.2986_rev._58254_(64bit)_(58453).exe'
ARCHIVE_BASE_1_MD5='1f13da4dc9ce6916075c4f3287feba5a'
ARCHIVE_BASE_1_EXTRACTOR='innoextract'
## Do not convert file paths to lower case.
ARCHIVE_BASE_1_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_1_PART1_NAME='setup_iron_harvest_1.4.8.2986_rev._58254_(64bit)_(58453)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='39064f2c73fc1289d6963d60ad3ed20f'
ARCHIVE_BASE_1_PART2_NAME='setup_iron_harvest_1.4.8.2986_rev._58254_(64bit)_(58453)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='6c7307a3c9fd9762e277efde3f38c408'
ARCHIVE_BASE_1_PART3_NAME='setup_iron_harvest_1.4.8.2986_rev._58254_(64bit)_(58453)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='c8a1219731b4d51bf294937691883bbd'
ARCHIVE_BASE_1_PART4_NAME='setup_iron_harvest_1.4.8.2986_rev._58254_(64bit)_(58453)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='6501face360636344b93183c072b9a10'
ARCHIVE_BASE_1_PART5_NAME='setup_iron_harvest_1.4.8.2986_rev._58254_(64bit)_(58453)-5.bin'
ARCHIVE_BASE_1_PART5_MD5='ead1c35efe7a6883a4204550650f2b0f'
ARCHIVE_BASE_1_SIZE='30858160'
ARCHIVE_BASE_1_VERSION='1.4.8.2986-gog58453'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/iron_harvest'

ARCHIVE_BASE_0_NAME='setup_iron_harvest_1.4.8.2983_rev._58247_(64bit)_(58091).exe'
ARCHIVE_BASE_0_MD5='d89a5f6b4a8c89769af3eaadc75953f2'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
## Do not convert file paths to lower case.
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_iron_harvest_1.4.8.2983_rev._58247_(64bit)_(58091)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='5d90f09c6b65889db5754d97cbdfebf3'
ARCHIVE_BASE_0_PART2_NAME='setup_iron_harvest_1.4.8.2983_rev._58247_(64bit)_(58091)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='80be1a755688797cc7a7a5b099add78a'
ARCHIVE_BASE_0_PART3_NAME='setup_iron_harvest_1.4.8.2983_rev._58247_(64bit)_(58091)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='81e7c85bc9017e8ae157bdea2140d852'
ARCHIVE_BASE_0_PART4_NAME='setup_iron_harvest_1.4.8.2983_rev._58247_(64bit)_(58091)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='3279373222b0dfdc37bd4d882d3bcc63'
ARCHIVE_BASE_0_PART5_NAME='setup_iron_harvest_1.4.8.2983_rev._58247_(64bit)_(58091)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='96b6db50d7167699257c1ab6428c8711'
ARCHIVE_BASE_0_SIZE='31000000'
ARCHIVE_BASE_0_VERSION='1.4.8.2983-gog58091'

UNITY3D_NAME='IronHarvest'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
config
release/MonoBleedingEdge
release/UnityPlayer.dll
release/${UNITY3D_NAME}.exe
release/${UNITY3D_NAME}_Data/Plugins"
CONTENT_GAME_DATA_SCENES_1_FILES='
gamedata/scenes/scene_sp_*'
CONTENT_GAME_DATA_SCENES_2_FILES='
gamedata/scenes'
CONTENT_GAME_DATA_CINEMATICS_FILES="
release/${UNITY3D_NAME}_Data/StreamingAssets/Cinematics"
CONTENT_GAME_DATA_SETTING_FILES='
gamedata/setting'
CONTENT_GAME_DATA_UI_FILES='
gamedata/ui'
CONTENT_GAME_DATA_FILES="
battleplan
content
gamedata
lang
logic
version
release/${UNITY3D_NAME}_Data"
CONTENT_DOC_DATA_FILES='
licenses'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/GOG.com/Galaxy/Applications/49637353028337449'

APP_MAIN_EXE="release/${UNITY3D_NAME}.exe"
APP_MAIN_ICON="$APP_MAIN_EXE"

PACKAGES_LIST='
PKG_BIN
PKG_DATA_SCENES_1
PKG_DATA_SCENES_2
PKG_DATA_CINEMATICS
PKG_DATA_SETTING
PKG_DATA_UI
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_SCENES_1
PKG_DATA_SCENES_2
PKG_DATA_CINEMATICS
PKG_DATA_SETTING
PKG_DATA_UI'

PKG_DATA_SCENES_ID="${PKG_DATA_ID}-scenes"
PKG_DATA_SCENES_1_ID="${PKG_DATA_SCENES_ID}-1"
PKG_DATA_SCENES_2_ID="${PKG_DATA_SCENES_ID}-2"
PKG_DATA_SCENES_DESCRIPTION="$PKG_DATA_DESCRIPTION - scenes"
PKG_DATA_SCENES_1_DESCRIPTION="$PKG_DATA_SCENES_DESCRIPTION - 1"
PKG_DATA_SCENES_2_DESCRIPTION="$PKG_DATA_SCENES_DESCRIPTION - 2"

PKG_DATA_CINEMATICS_ID="${PKG_DATA_ID}-cinematics"
PKG_DATA_CINEMATICS_DESCRIPTION="$PKG_DATA_DESCRIPTION - cinematics"

PKG_DATA_SETTING_ID="${PKG_DATA_ID}-setting"
PKG_DATA_SETTING_DESCRIPTION="$PKG_DATA_DESCRIPTION - setting"

PKG_DATA_UI_ID="${PKG_DATA_ID}-ui"
PKG_DATA_UI_DESCRIPTION="$PKG_DATA_DESCRIPTION - ui"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build package

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
