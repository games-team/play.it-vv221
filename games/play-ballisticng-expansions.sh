#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# BallisticNG expansions:
# - Neon Nights
###

script_version=20241230.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='ballisticng'
GAME_NAME='BallisticNG'

EXPANSION_ID_NEON='neon-nights'
EXPANSION_NAME_NEON='Neon Nights'

ARCHIVE_BASE_NEON_0_NAME='ballisticng_neon_nights_1_3_3_1_69502.sh'
ARCHIVE_BASE_NEON_0_MD5='18897d4477a80a88afe3a05fa52cfee9'
ARCHIVE_BASE_NEON_0_SIZE='100101'
ARCHIVE_BASE_NEON_0_VERSION='1.3.3.1-gog69502'
ARCHIVE_BASE_NEON_0_URL='https://www.gog.com/game/ballisticng_neon_nights'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
DLC'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
