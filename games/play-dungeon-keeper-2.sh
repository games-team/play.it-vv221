#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dungeon Keeper 2
###

script_version=20241123.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='dungeon-keeper-2'
GAME_NAME='Dungeon Keeper Ⅱ'

ARCHIVE_BASE_EN_2_NAME='setup_dungeon_keepertm_2_1.7_alttab_hotfix_(77775).exe'
ARCHIVE_BASE_EN_2_MD5='250ba86776e434e2fc65534a97e68e41'
ARCHIVE_BASE_EN_2_TYPE='innosetup'
ARCHIVE_BASE_EN_2_PART1_NAME='setup_dungeon_keepertm_2_1.7_alttab_hotfix_(77775)-1.bin'
ARCHIVE_BASE_EN_2_PART1_MD5='4cf9a2db8dfba58d6e869e2b124edbe0'
ARCHIVE_BASE_EN_2_SIZE='514101'
ARCHIVE_BASE_EN_2_VERSION='1.7-gog77775'
ARCHIVE_BASE_EN_2_URL='https://www.gog.com/game/dungeon_keeper_2'

ARCHIVE_BASE_FR_2_NAME='setup_dungeon_keepertm_2_1.7_alttab_hotfix_(french)_(77775).exe'
ARCHIVE_BASE_FR_2_MD5='1ce059082cddb831ea25e571c571eaef'
ARCHIVE_BASE_FR_2_TYPE='innosetup'
ARCHIVE_BASE_FR_2_PART1_NAME='setup_dungeon_keepertm_2_1.7_alttab_hotfix_(french)_(77775)-1.bin'
ARCHIVE_BASE_FR_2_PART1_MD5='5cd909d382a35f22ad93470e5281fa2e'
ARCHIVE_BASE_FR_2_SIZE='512981'
ARCHIVE_BASE_FR_2_VERSION='1.7-gog77775'
ARCHIVE_BASE_FR_2_URL='https://www.gog.com/game/dungeon_keeper_2'

ARCHIVE_BASE_EN_1_NAME='setup_dungeon_keeper_2_1.7_(22280).exe'
ARCHIVE_BASE_EN_1_MD5='eeb2229ee49518504884a9ac65f611ad'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_SIZE='500537'
ARCHIVE_BASE_EN_1_VERSION='1.7-gog22280'

ARCHIVE_BASE_EN_0_NAME='setup_dungeon_keeper2_2.0.0.32.exe'
ARCHIVE_BASE_EN_0_MD5='92d04f84dd870d9624cd18449d3622a5'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='510000'
ARCHIVE_BASE_EN_0_VERSION='1.7-gog2.0.0.32'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
aweman32.dll
patch.dll
qmixer.dll
sfman32.dll
weanetr.dll
dkii.exe
dkii-dx.exe'
CONTENT_GAME_L10N_FILES='
data/sound/sfx/speech_*
data/text/default'
CONTENT_GAME_DATA_FILES='
data
dk2texturecache'
CONTENT_DOC_L10N_FILES='
eamseula.txt
readme.txt'
CONTENT_DOC_DATA_FILES='
manual.pdf
reference_card.pdf'

USER_PERSISTENT_DIRECTORIES='
data/settings
data/save'

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Bullfrog Productions Ltd\Dungeon Keeper II'
WINE_VIRTUAL_DESKTOP='auto'
## Work around poor performances using hardware rendering with Intel+Mesa.
WINE_WINETRICKS_VERBS='csmt=off'

APP_MAIN_EXE='dkii-dx.exe'
APP_MAIN_ICON='dkii.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_PROVIDES_EN="$PKG_L10N_PROVIDES"
PKG_L10N_PROVIDES_FR="$PKG_L10N_PROVIDES"
PKG_L10N_DESCRIPTION_EN='English localisation'
PKG_L10N_DESCRIPTION_FR='French localisation'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
