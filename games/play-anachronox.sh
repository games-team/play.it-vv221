#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2016 Mopi
set -o errexit

###
# Anachronox
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='anachronox'
GAME_NAME='Anachronox'

ARCHIVE_BASE_1_NAME='setup_anachronox_1.02_(22258).exe'
ARCHIVE_BASE_1_MD5='4e23d4f7637f6914a7cd6c13feb7ad7d'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1074901'
ARCHIVE_BASE_1_VERSION='1.02-gog22258'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/anachronox'

ARCHIVE_BASE_0_NAME='setup_anachronox_2.0.0.28.exe'
ARCHIVE_BASE_0_MD5='a9e148972e51a4980a2531d12a85dfc0'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1100000'
ARCHIVE_BASE_0_VERSION='1.02-gog2.0.0.28'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
anoxdata/miles
anoxdata/plugins
anoxaux.dll
anoxgfx.dll
gamex86.dll
ijl15.dll
libpng13a.dll
metagl.dll
mss32.dll
msvcp60.dll
msvcrt.dll
patch.dll
ref_gl.dll
zlib.dll
afscmd.exe
anox.exe
autorun.exe
dparse.exe
gct setup.exe
particleman.exe
setupanox.exe
autorun.inf
gct setup.ini
mscomctl.ocx'
CONTENT_GAME_DATA_FILES='
anoxdata
anox.ico
anachronox_word.jpg'
CONTENT_DOC_DATA_FILES='
manual.pdf
readme.htm
anachronox patch 1_02.rtf
*readme.txt'

USER_PERSISTENT_DIRECTORIES='
anoxdata/save'
USER_PERSISTENT_FILES='
*.ini
anoxdata/nokill.*'

APP_MAIN_EXE='anox.exe'
APP_MAIN_ICON='anox.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
