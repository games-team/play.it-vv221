#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2020 macaron
set -o errexit

###
# Sam and Max Hit the Road
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='sam-and-max-hit-the-road'
GAME_NAME='Sam & Max Hit the Road'

ARCHIVE_BASE_EN_1_NAME='sam_and_max_hit_the_road_en_gog_2_20100.sh'
ARCHIVE_BASE_EN_1_MD5='0771889c051c7e1cc6e6c8e8ca8fbe1f'
ARCHIVE_BASE_EN_1_SIZE='390000'
ARCHIVE_BASE_EN_1_VERSION='1.0-gog20100'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/sam_max_hit_the_road'

ARCHIVE_BASE_FR_1_NAME='sam_and_max_hit_the_road_fr_gog_2_20100.sh'
ARCHIVE_BASE_FR_1_MD5='52b35282832b477c7f1bb06688ba3b95'
ARCHIVE_BASE_FR_1_SIZE='280000'
ARCHIVE_BASE_FR_1_VERSION='1.0-gog20100'
ARCHIVE_BASE_FR_1_URL='https://www.gog.com/game/sam_max_hit_the_road'

ARCHIVE_BASE_EN_0_NAME='gog_sam_max_hit_the_road_2.0.0.8.sh'
ARCHIVE_BASE_EN_0_MD5='00e6de62115b581f01f49354212ce545'
ARCHIVE_BASE_EN_0_SIZE='270000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog2.0.0.1'

ARCHIVE_BASE_FR_0_NAME='gog_sam_max_hit_the_road_french_2.0.0.8.sh'
ARCHIVE_BASE_FR_0_MD5='127be643ebaa9af24ddd9f2618e4433e'
ARCHIVE_BASE_FR_0_SIZE='160000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog2.0.0.1'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
samnmax.000
samnmax.001
monster.so[3fgu]'
CONTENT_DOC_MAIN_PATH_EN='data/noarch/docs/english'
CONTENT_DOC_MAIN_PATH_FR='data/noarch/docs/french'
CONTENT_DOC_MAIN_FILES='
*.pdf'

APP_MAIN_SCUMMID='scumm:samnmax'
APP_MAIN_ICON='../support/icon.png'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${PKG_MAIN_ID}-en"
PKG_MAIN_ID_FR="${PKG_MAIN_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
