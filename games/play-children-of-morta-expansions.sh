#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Children of Morta expansions:
# - Ancient Spirits
# - Paws and Claws
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='children-of-morta'
GAME_NAME='Children of Morta'

EXPANSION_ID_ANCIENT='ancient-spirits'
EXPANSION_NAME_ANCIENT='Ancient Spirits'

EXPANSION_ID_PAWS='paws-and-claws'
EXPANSION_NAME_PAWS='Paws and Claws'

# Archives

## Ancient Spirits

ARCHIVE_BASE_ANCIENT_0_NAME='backers_dlc_1_2_63_48061.sh'
ARCHIVE_BASE_ANCIENT_0_MD5='931cfdfd304b909efbe61ac4d6c10b34'
ARCHIVE_BASE_ANCIENT_0_SIZE='1026'
ARCHIVE_BASE_ANCIENT_0_VERSION='1.2.63-gog48061'
ARCHIVE_BASE_ANCIENT_0_URL='https://www.gog.com/en/game/children_of_morta_ancient_spirits'

## Paws and Claws

ARCHIVE_BASE_PAWS_0_NAME='children_of_morta_paws_and_claws_1_2_63_48061.sh'
ARCHIVE_BASE_PAWS_0_MD5='09efef01a9bd0c1a5172e1fe7fd19530'
ARCHIVE_BASE_PAWS_0_SIZE='1026'
ARCHIVE_BASE_PAWS_0_VERSION='1.2.63-gog48061'
ARCHIVE_BASE_PAWS_0_URL='https://www.gog.com/en/game/children_of_morta_paws_and_claws'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
goggame-*.info'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
