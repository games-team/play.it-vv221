#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 macaron
set -o errexit

###
# Goblins series:
# - Goblins 1
# - Goblins 2
# - Goblins 3
###

script_version=20241123.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_GOB1='goblins-1'
GAME_NAME_GOB1='Gobliiins'

GAME_ID_GOB2='goblins-2'
GAME_NAME_GOB2='Gobliins 2: The Prince Buffoon'

GAME_ID_GOB3='goblins-3'
GAME_NAME_GOB3='Goblins 3'

# Archives

## Goblins 1

ARCHIVE_BASE_GOB1_0_NAME='setup_gobliiins_1.02_(20270).exe'
ARCHIVE_BASE_GOB1_0_MD5='dd4fa52b7ed43b964d82f2056c18b681'
ARCHIVE_BASE_GOB1_0_TYPE='innosetup'
ARCHIVE_BASE_GOB1_0_SIZE='99000'
ARCHIVE_BASE_GOB1_0_VERSION='1.02-gog20270'
ARCHIVE_BASE_GOB1_0_URL='https://www.gog.com/game/gobliiins_pack'

## Goblins 2

ARCHIVE_BASE_GOB2_0_NAME='setup_gobliins_2_-_the_prince_buffoon_1.02_(20270).exe'
ARCHIVE_BASE_GOB2_0_MD5='3607f4ab042fea51e3b6544775955701'
ARCHIVE_BASE_GOB2_0_TYPE='innosetup'
ARCHIVE_BASE_GOB2_0_SIZE='110000'
ARCHIVE_BASE_GOB2_0_VERSION='1.02-gog20270'
ARCHIVE_BASE_GOB2_0_URL='https://www.gog.com/game/gobliiins_pack'

## Goblins 3

ARCHIVE_BASE_GOB3_EN_0_NAME='setup_goblins_quest_3_1.02_(20270).exe'
ARCHIVE_BASE_GOB3_EN_0_MD5='9d98b9f643dad9c793416d50bcbd9f17'
ARCHIVE_BASE_GOB3_EN_0_TYPE='innosetup'
ARCHIVE_BASE_GOB3_EN_0_SIZE='210000'
ARCHIVE_BASE_GOB3_EN_0_VERSION='1.02-gog20270'
ARCHIVE_BASE_GOB3_EN_0_URL='https://www.gog.com/game/gobliiins_pack'

ARCHIVE_BASE_GOB3_FR_0_NAME='setup_goblins_quest_3_1.02_(french)_(20270).exe'
ARCHIVE_BASE_GOB3_FR_0_MD5='52649e08b57d8edfdbb4b72bc032e625'
ARCHIVE_BASE_GOB3_FR_0_TYPE='innosetup'
ARCHIVE_BASE_GOB3_FR_0_SIZE='200000'
ARCHIVE_BASE_GOB3_FR_0_VERSION='1.02-gog20270'
ARCHIVE_BASE_GOB3_FR_0_URL='https://www.gog.com/game/gobliiins_pack'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_L10N_FILES_GOB3='
??gob3.itk'
CONTENT_GAME_FLOPPY_RELATIVE_PATH='fdd'
CONTENT_GAME_FLOPPY_FILES='
*.stk'
CONTENT_GAME_CDROM_FILES='
*.itk
*.lic
*.stk
*.mp3'
CONTENT_DOC_COMMON_FILES='
*.pdf'

# Applications

## Goblins 1

APP_MAIN_SCUMMID_GOB1='gob:gob1'
APP_MAIN_ICON_GOB1='goggame-1207662273.ico'

## Goblins 2

APP_MAIN_SCUMMID_GOB2='gob:gob2'
APP_MAIN_ICON_GOB2='goggame-1207662293.ico'

## Goblins 3

APP_MAIN_SCUMMID_GOB3='gob:gob3'
APP_MAIN_ICON_GOB3='goggame-1207662313.ico'

# Packages

## Common

PACKAGES_LIST='
PKG_COMMON
PKG_FLOPPY
PKG_CDROM'

PKG_FLOPPY_DEPENDENCIES_SIBLINGS='
PKG_COMMON'
PKG_CDROM_DEPENDENCIES_SIBLINGS='
PKG_COMMON'

## Goblins 1

PKG_COMMON_ID_GOB1="${GAME_ID_GOB1}-common"

PKG_MAIN_ID_GOB1="$GAME_ID_GOB1"

PKG_FLOPPY_ID_GOB1="${PKG_MAIN_ID_GOB1}-floppy"
PKG_FLOPPY_PROVIDES_GOB1="
$PKG_MAIN_ID_GOB1"

PKG_CDROM_ID_GOB1="${PKG_MAIN_ID_GOB1}-cdrom"
PKG_CDROM_PROVIDES_GOB1="
$PKG_MAIN_ID_GOB1"

## Goblins 2

PKG_COMMON_ID_GOB2="${GAME_ID_GOB2}-common"

PKG_MAIN_ID_GOB2="$GAME_ID_GOB2"

PKG_FLOPPY_ID_GOB2="${PKG_MAIN_ID_GOB2}-floppy"
PKG_FLOPPY_PROVIDES_GOB2="
$PKG_MAIN_ID_GOB2"

PKG_CDROM_ID_GOB2="${PKG_MAIN_ID_GOB2}-cdrom"
PKG_CDROM_PROVIDES_GOB2="
$PKG_MAIN_ID_GOB2"

## Goblins 3

PACKAGES_LIST_GOB3="
PKG_L10N
$PACKAGES_LIST"

PKG_COMMON_ID_GOB3="${GAME_ID_GOB3}-common"

PKG_L10N_BASE_ID="${GAME_ID_GOB3}-l10n"
PKG_L10N_ID_GOB3_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_GOB3_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES_GOB3="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_GOB3_EN='English localization'
PKG_L10N_DESCRIPTION_GOB3_FR='French localization'

PKG_MAIN_ID_GOB3="$GAME_ID_GOB3"

PKG_FLOPPY_ID_GOB3="${PKG_MAIN_ID_GOB3}-floppy"
PKG_FLOPPY_PROVIDES_GOB3="
$PKG_MAIN_ID_GOB3"

PKG_CDROM_ID_GOB3="${PKG_MAIN_ID_GOB3}-cdrom"
PKG_CDROM_PROVIDES_GOB3="
$PKG_MAIN_ID_GOB3"
PKG_CDROM_DEPENDENCIES_SIBLINGS_GOB3='
PKG_COMMON
PKG_L10N_BASE'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Incude game data

content_inclusion_icons 'PKG_COMMON'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_FLOPPY'

## Goblins 3 - Set game language based on the available localization.
case "$(current_archive)" in
	('ARCHIVE_BASE_GOB3_'*)
		game_exec_line() {
			cat <<- 'EOF'
			if [ -e "${PATH_GAME_DATA}/frgob3.itk" ]; then
			    scummvm --language=fr --path="$PATH_GAME_DATA" "$@" "$SCUMMVM_ID"
			elif [ -e "${PATH_GAME_DATA}/usgob3.itk" ]; then
			    scummvm --language=us --path="$PATH_GAME_DATA" "$@" "$SCUMMVM_ID"
			fi
			EOF
		}
	;;
esac
launchers_generation 'PKG_CDROM'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		version_string='version %s :'
		version_floppy='disquette'
		version_cdrom='CD-ROM'
	;;
	('en'|*)
		version_string='%s version:'
		version_floppy='Floppy'
		version_cdrom='CD-ROM'
	;;
esac
printf '\n'
printf "$version_string" "$version_floppy"
print_instructions 'PKG_FLOPPY' 'PKG_COMMON'
printf "$version_string" "$version_cdrom"
case "$(current_archive)" in
	('ARCHIVE_BASE_GOB3_'*)
		print_instructions 'PKG_CDROM' 'PKG_L10N' 'PKG_COMMON'
	;;
	(*)
		print_instructions 'PKG_CDROM' 'PKG_COMMON'
	;;
esac

# Clean up

working_directory_cleanup

exit 0
