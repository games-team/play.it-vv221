#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kingdom Rush series:
# - Kingdom Rush
# - Kingdom Rush: Frontiers
# - Kingdom Rush: Origins
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_ORIGINAL='kingdom-rush'
GAME_NAME_ORIGINAL='Kingdom Rush'

GAME_ID_FRONTIERS='kingdom-rush-frontiers'
GAME_NAME_FRONTIERS='Kingdom Rush: Frontiers'

GAME_ID_ORIGINS='kingdom-rush-origins'
GAME_NAME_ORIGINS='Kingdom Rush: Origins'

# Archives

## Kingdom Rush (original game)

ARCHIVE_BASE_ORIGINAL_2_NAME='kingdom_rush_5_6_12_52189.sh'
ARCHIVE_BASE_ORIGINAL_2_MD5='ca0bd3ede3d5768d51b66c366d4c68cb'
ARCHIVE_BASE_ORIGINAL_2_SIZE='378545'
ARCHIVE_BASE_ORIGINAL_2_VERSION='5.6.12-gog52189'
ARCHIVE_BASE_ORIGINAL_2_URL='https://www.gog.com/game/kingdom_rush'

ARCHIVE_BASE_ORIGINAL_1_NAME='kingdom_rush_4_2_33_43930.sh'
ARCHIVE_BASE_ORIGINAL_1_MD5='1423a27078042c5fc9d7ee45d06a5744'
ARCHIVE_BASE_ORIGINAL_1_SIZE='380000'
ARCHIVE_BASE_ORIGINAL_1_VERSION='4.2.33-gog43930'

ARCHIVE_BASE_ORIGINAL_0_NAME='kingdom_rush_4_2_15_40260.sh'
ARCHIVE_BASE_ORIGINAL_0_MD5='025031489b81493e3816e077ab62214a'
ARCHIVE_BASE_ORIGINAL_0_SIZE='380000'
ARCHIVE_BASE_ORIGINAL_0_VERSION='4.2.15-gog40260'

## Kingdom Rush: Frontiers

ARCHIVE_BASE_FRONTIERS_0_NAME='kingdom_rush_frontiers_4_2_33_43930.sh'
ARCHIVE_BASE_FRONTIERS_0_MD5='6e00968ec07cec1bbafd8c43ed4416cb'
ARCHIVE_BASE_FRONTIERS_0_SIZE='449652'
ARCHIVE_BASE_FRONTIERS_0_VERSION='4.2.33-gog43930'
ARCHIVE_BASE_FRONTIERS_0_URL='https://www.gog.com/game/kingdom_rush_frontiers'

## Kingdom Rush: Origins

ARCHIVE_BASE_ORIGINS_0_NAME='kingdom_rush_origins_4_2_15_40260.sh'
ARCHIVE_BASE_ORIGINS_0_MD5='864fb8a387081304df1d0674c40d718e'
ARCHIVE_BASE_ORIGINS_0_SIZE='497827'
ARCHIVE_BASE_ORIGINS_0_VERSION='4.2.15-gog40260'
ARCHIVE_BASE_ORIGINS_0_URL='https://www.gog.com/game/kingdom_rush_origins'


CONTENT_PATH_DEFAULT='data/noarch/game'
## System-provided love2d runtime can not be used, it crashes on launch with the following error:
## Error: [love "boot.lua"]:276: Syntax error: conf.lua: cannot load incompatible bytecode
CONTENT_LIBS_MAIN_FILES='
liblove.so.0'
## System-provided libluajit-5.1.so.2 can not be used, as it triggers a crash on launch.
CONTENT_LIBS0_MAIN_FILES='
libluajit-5.1.so.2'
CONTENT_GAME_MAIN_FILES='
icon.png
Kingdom Rush
Kingdom Rush Frontiers
Kingdom Rush Origins'
CONTENT_DOC_MAIN_PATH='
license-kr-desktop.txt'

APP_MAIN_EXE_ORIGINAL='Kingdom Rush'
APP_MAIN_EXE_FRONTIERS='Kingdom Rush Frontiers'
APP_MAIN_EXE_ORIGINS='Kingdom Rush Origins'
APP_MAIN_ICON='icon.png'

PKG_MAIN_ARCH='64'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfreetype.so.6
libgcc_s.so.1
libmodplug.so.1
libmpg123.so.0
libm.so.6
libogg.so.0
libopenal.so.1
libphysfs.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libtheoradec.so.1
libvorbisfile.so.3
libvorbis.so.0
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
