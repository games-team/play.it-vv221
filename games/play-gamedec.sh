#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gamedec
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='gamedec'
GAME_NAME='Gamedec'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_0_NAME='setup_gamedec_-_definitive_edition_20221128_1.7.1.r70100_shipping_(64bit)_(60562).exe'
ARCHIVE_BASE_0_MD5='81ce6923488911399dbd209cc574334a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_gamedec_-_definitive_edition_20221128_1.7.1.r70100_shipping_(64bit)_(60562)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='e069d6764e368cd141d98ba93d286b50'
ARCHIVE_BASE_0_PART2_NAME='setup_gamedec_-_definitive_edition_20221128_1.7.1.r70100_shipping_(64bit)_(60562)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='2e4507b72e2e47a9ccd658365fc8347e'
ARCHIVE_BASE_0_PART3_NAME='setup_gamedec_-_definitive_edition_20221128_1.7.1.r70100_shipping_(64bit)_(60562)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='feee3d36bb8198f2d4e86eddaf0efd6e'
ARCHIVE_BASE_0_SIZE='16408534'
ARCHIVE_BASE_0_VERSION='1.7.1-gog60562'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/gamedec'

ARCHIVE_BASE_DEMO_0_NAME='setup_gamedec_demo_20211126_1.3.0.r47441_(64bit)_(51758).exe'
ARCHIVE_BASE_DEMO_0_MD5='cc801764f1ebf5e9c6c044ca7429dd4c'
ARCHIVE_BASE_DEMO_0_TYPE='innosetup'
ARCHIVE_BASE_DEMO_0_PART1_NAME='setup_gamedec_demo_20211126_1.3.0.r47441_(64bit)_(51758)-1.bin'
ARCHIVE_BASE_DEMO_0_PART1_MD5='8d45c7abe34aef48ad40178230940713'
ARCHIVE_BASE_DEMO_0_PART2_NAME='setup_gamedec_demo_20211126_1.3.0.r47441_(64bit)_(51758)-2.bin'
ARCHIVE_BASE_DEMO_0_PART2_MD5='edf41425796878607f5c0574695b9ee8'
ARCHIVE_BASE_DEMO_0_SIZE='10215245'
ARCHIVE_BASE_DEMO_0_VERSION='1.3.0-gog51758'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/gamedec_demo'

UNREALENGINE4_NAME='gamedec'

CONTENT_PATH_DEFAULT='.'

HUGE_FILES_DATA="
${UNREALENGINE4_NAME}/content/paks/${UNREALENGINE4_NAME}-windowsnoeditor.pak"

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/${UNREALENGINE4_NAME}-win64-shipping.exe"
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=123'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
