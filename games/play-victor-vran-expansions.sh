#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Victor Vran expansions:
# - Motörhead - Through the Ages
# - Fractured Worlds
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='victor-vran'
GAME_NAME='Victor Vran'

EXPANSION_ID_MOTORHEAD='motorhead-through-the-ages'
EXPANSION_NAME_MOTORHEAD='Motörhead - Through the Ages'

EXPANSION_ID_FRACTURED='fractured-worlds'
EXPANSION_NAME_FRACTURED='Fractured Worlds'

# Archives

## Motörhead - Through the Ages

ARCHIVE_BASE_MOTORHEAD_0_NAME='victor_vran_mot_rhead_through_the_ages_2_07_20181005_24296.sh'
ARCHIVE_BASE_MOTORHEAD_0_MD5='e84685034d2d90782a3ab7c83901bc11'
ARCHIVE_BASE_MOTORHEAD_0_SIZE='868392'
ARCHIVE_BASE_MOTORHEAD_0_VERSION='2.07.20181005-gog24296'
ARCHIVE_BASE_MOTORHEAD_0_URL='https://www.gog.com/game/victor_vran_motorhead_through_the_ages'

## Fractured Worlds

ARCHIVE_BASE_FRACTURED_0_NAME='victor_vran_fractured_worlds_2_07_20181005_24296.sh'
ARCHIVE_BASE_FRACTURED_0_MD5='94a8bd9f2ca09e3c4e2547a0d5a7bc8c'
ARCHIVE_BASE_FRACTURED_0_SIZE='213785'
ARCHIVE_BASE_FRACTURED_0_VERSION='2.07.20181005-gog24296'
ARCHIVE_BASE_FRACTURED_0_URL='https://www.gog.com/game/victor_vran_fractured_worlds'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
DLC'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
