#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 macaron
set -o errexit

###
# Day of the Tentacle
###

script_version=20250227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='day-of-the-tentacle'
GAME_NAME='Day of the Tentacle'

ARCHIVE_BASE_0_NAME='gog_day_of_the_tentacle_remastered_2.1.0.2.sh'
ARCHIVE_BASE_0_MD5='612c59c5cbdbf4d73322b46527a2d502'
ARCHIVE_BASE_0_SIZE='2677592'
ARCHIVE_BASE_0_VERSION='1.4.1-gog2.1.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/day_of_the_tentacle_remastered'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_RELATIVE_PATH='lib'
CONTENT_LIBS_BIN_FILES='
libfmod.so.8'
CONTENT_GAME_BIN_FILES='
Dott
controllerdef.txt'
CONTENT_GAME_DATA_FILES='
tenta.cle'
CONTENT_DOC_DATA_FILES='
readme.txt'

APP_MAIN_EXE='Dott'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the use of system-provided SDL
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(launcher_tweak_sdl_override)"

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
