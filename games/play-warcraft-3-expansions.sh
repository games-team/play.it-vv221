#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warcraft 3 expansions:
# - The Frozen Throne
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warcraft-3'
GAME_NAME='Warcraft Ⅲ'

EXPANSION_ID='the-frozen-throne'
EXPANSION_NAME='The Frozen Throne'

ARCHIVE_BASE_EN_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_EN_0_MD5='04f1a75841df58a839ea3c2ea9d46a7e'
ARCHIVE_BASE_EN_0_SIZE='612802'
ARCHIVE_BASE_EN_0_VERSION='1.27-blizzard1'
ARCHIVE_BASE_EN_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=enUS&product=W3XP'

ARCHIVE_BASE_FR_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_FR_0_MD5='050d7b179b1c8c784f3ff40fae077f6e'
ARCHIVE_BASE_FR_0_SIZE='661687'
ARCHIVE_BASE_FR_0_VERSION='1.27-blizzard1'
ARCHIVE_BASE_FR_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=frFR&product=W3XP'

ARCHIVE_BASE_DE_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_DE_0_MD5='fdb12d1362e846c189e10b24ef3feee6'
ARCHIVE_BASE_DE_0_SIZE='654348'
ARCHIVE_BASE_DE_0_VERSION='1.27-blizzard1'
ARCHIVE_BASE_DE_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=deDE&product=W3XP'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_PATH='PC'
## Frozen Throne.exe is distinct between the English and the French build of the game,
## for now we assume the differences are not critical and one can be used in place of the other.
CONTENT_GAME_BIN_FILES='
Frozen Throne.exe'
CONTENT_GAME_DATA_PATH='Common'
CONTENT_GAME_DATA_FILES='
Maps/FrozenThrone
Movies/IntroX.mpq
Movies/OutroX.mpq
War3x.mpq
War3xLocal.mpq'
CONTENT_GAME_DATA_SHARED_PATH='Common'
CONTENT_GAME_DATA_SHARED_FILES='
War3Patch.mpq'

USER_PERSISTENT_DIRECTORIES='
Campaigns
Maps
Replay
Save'

APP_MAIN_ID="${GAME_ID}-${EXPANSION_ID}"
APP_MAIN_NAME="${GAME_NAME}: ${EXPANSION_NAME}"
APP_MAIN_EXE='Frozen Throne.exe'
APP_MAIN_OPTIONS='-opengl'
APP_MAIN_ICON='PC/Frozen Throne.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA
PKG_DATA_SHARED'

PKG_PARENT_ID="$GAME_ID"

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA_BASE
PKG_PARENT'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/x-msvideo'

PKG_DATA_BASE_ID="${GAME_ID}-${EXPANSION_ID}-data"
PKG_DATA_ID_EN="${PKG_DATA_BASE_ID}-en"
PKG_DATA_ID_FR="${PKG_DATA_BASE_ID}-fr"
PKG_DATA_ID_DE="${PKG_DATA_BASE_ID}-de"
PKG_DATA_PROVIDES="
$PKG_DATA_BASE_ID"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_SHARED'

PKG_DATA_SHARED_ID="${PKG_DATA_BASE_ID}-shared"
PKG_DATA_SHARED_ID_EN="${PKG_DATA_SHARED_ID}-en"
PKG_DATA_SHARED_ID_FR="${PKG_DATA_SHARED_ID}-fr"
PKG_DATA_SHARED_ID_DE="${PKG_DATA_SHARED_ID}-de"
PKG_DATA_SHARED_PROVIDES="
$PKG_DATA_SHARED_ID
${GAME_ID}-data-shared"
PKG_DATA_SHARED_PROVIDES_EN="$PKG_DATA_SHARED_PROVIDES
${GAME_ID}-data-shared-en"
PKG_DATA_SHARED_PROVIDES_FR="$PKG_DATA_SHARED_PROVIDES
${GAME_ID}-data-shared-fr"
PKG_DATA_SHARED_PROVIDES_DE="$PKG_DATA_SHARED_PROVIDES
${GAME_ID}-data-shared-de"
PKG_DATA_SHARED_DESCRIPTION='data shared between the base game and the expansion'

# Set the list of requirements to extract the archive contents.

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
smpq"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of required extra archives

## Check for the presence of the CD key files.
## See notes/warcraft-3 for details on how to get these files.
ARCHIVE_REQUIRED_CDKEY_NAME='font.exp'
archive_initialize_required \
	'ARCHIVE_CDKEY' \
	'ARCHIVE_REQUIRED_CDKEY'

# Extract game data

archive_path=$(archive_path "$(current_archive)")
archive_path_cdkey=$(archive_path 'ARCHIVE_CDKEY')
mkdir --parents "${PLAYIT_WORKDIR}/gamedata"
information_archive_data_extraction "$archive_path"
(
	cd "${PLAYIT_WORKDIR}/gamedata"
	smpq --extract "$archive_path"

	# Update the .mpq file
	(
		cd 'PC-100x'
		smpq --append --overwrite ../Common/War3x.mpq ./*
	)
	(
		cd 'Files'
		cp --dereference \
			"$archive_path_cdkey" \
			'font'
		smpq --append ../Common/War3x.mpq \
			font/font.ccd \
			font/font.exp
		rm --force --recursive 'font'
	)
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
