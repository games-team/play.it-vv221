#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Lamplighters League
###

script_version=20241229.2

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='the-lamplighters-league'
GAME_NAME='The Lamplighters League'

ARCHIVE_BASE_0_NAME='setup_the_lamplighters_league_1.3.1-67360_(75947).exe'
ARCHIVE_BASE_0_MD5='bc06890044afe8cd3395249a4553c1f7'
## Do not convert the file paths to lowercase,
## otherwise the engine would fail to fetch localized strings.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_the_lamplighters_league_1.3.1-67360_(75947)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='6a217c893d0cc2b93ec561ad0880eb73'
ARCHIVE_BASE_0_PART2_NAME='setup_the_lamplighters_league_1.3.1-67360_(75947)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='2a56f3cdc2bd2898fb7bc3eacedefec7'
ARCHIVE_BASE_0_PART3_NAME='setup_the_lamplighters_league_1.3.1-67360_(75947)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='e398fc1ae766a81f1374f971be55929e'
ARCHIVE_BASE_0_SIZE='15634809'
ARCHIVE_BASE_0_VERSION='1.3.1-gog75947'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_lamplighters_league'

UNITY3D_NAME='LamplightersLeague'

CONTENT_PATH_DEFAULT='.'
## The game fails to start if this GOG Galaxy setting file is not included,
## as it then expects to be running from Steam.
CONTENT_GAME0_BIN_FILES='
galaxyconfig.json'
CONTENT_GAME_DATA_BUNDLES_1_FILES="
${UNITY3D_NAME}_data/streamingassets/aa/standalonewindows64/[0-7]*.bundle"
CONTENT_GAME_DATA_BUNDLES_2_FILES="
${UNITY3D_NAME}_data/streamingassets/aa/standalonewindows64/[8,9a-f]*.bundle"

## Prevent rendering problems, including flashing bright colours (WINE 8.0)
## Due to the flashes being potentially harmful, this should not be removed
## at least until a WINE build not triggering them reaches Debian oldstable.
WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/HarebrainedSchemes/LamplightersLeague'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_BUNDLES_1
PKG_DATA_BUNDLES_2
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_BUNDLES_1
PKG_DATA_BUNDLES_2'

PKG_DATA_BUNDLES_ID="${PKG_DATA_ID}-bundles"
PKG_DATA_BUNDLES_1_ID="${PKG_DATA_BUNDLES_ID}-1"
PKG_DATA_BUNDLES_2_ID="${PKG_DATA_BUNDLES_ID}-2"
PKG_DATA_BUNDLES_DESCRIPTION="$PKG_DATA_DESCRIPTION - bundles"
PKG_DATA_BUNDLES_1_DESCRIPTION="$PKG_BUNDLES_DESCRIPTION - part 1"
PKG_DATA_BUNDLES_2_DESCRIPTION="$PKG_BUNDLES_DESCRIPTION - part 2"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
