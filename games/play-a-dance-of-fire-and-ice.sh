#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Dance of Fire and Ice
###

script_version=20241021.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='a-dance-of-fire-and-ice'
GAME_NAME='A Dance of Fire and Ice'

ARCHIVE_BASE_1_NAME='a-dance-of-fire-and-ice-linux.zip'
ARCHIVE_BASE_1_MD5='adcffda56ec76b5ae8c3ba89e9b9bb93'
ARCHIVE_BASE_1_SIZE='2069741'
ARCHIVE_BASE_1_VERSION='2.6.3-itch.2023.12.16'
ARCHIVE_BASE_1_URL='https://fizzd.itch.io/a-dance-of-fire-and-ice'

ARCHIVE_BASE_0_NAME='a-dance-of-fire-and-ice-linux.zip'
ARCHIVE_BASE_0_MD5='caf90b5416730395963e26aeb76c1155'
ARCHIVE_BASE_0_SIZE='965475'
ARCHIVE_BASE_0_VERSION='2.5.0-itch.2023.06.20'

UNITY3D_NAME='ADanceOfFireAndIce'
## The game will crash on launch if libsteam_api.so is not available.
UNITY3D_PLUGINS='
libadofaipulse.so
libStandaloneFileBrowser.so
libsteam_api.so'

CONTENT_PATH_DEFAULT='.'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libpulse.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
