#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Painkiller
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='painkiller'
GAME_NAME='Painkiller'

ARCHIVE_BASE_EN_0_NAME='setup_painkiller_black_1.64_lang_update_(24538).exe'
ARCHIVE_BASE_EN_0_MD5='328381c6d874b5c6822cdcf596b6c00e'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_painkiller_black_1.64_lang_update_(24538)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='26cf8f833c5b75efb006eac0e731a0af'
ARCHIVE_BASE_EN_0_SIZE='3934610'
ARCHIVE_BASE_EN_0_VERSION='1.6.4-gog24538'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/painkiller'

ARCHIVE_BASE_FR_0_NAME='setup_painkiller_black_1.64_lang_update_(french)_(24538).exe'
ARCHIVE_BASE_FR_0_MD5='9a4301af374c500ac7d00b5e7ad70b65'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_painkiller_black_1.64_lang_update_(french)_(24538)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='09e9614cadf2973ead1f826c24ebf2fe'
ARCHIVE_BASE_FR_0_SIZE='3612122'
ARCHIVE_BASE_FR_0_VERSION='1.6.0-gog24538'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/painkiller'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bin/miles
bin/*.dll
bin/*.exe
bin/*.ini'
CONTENT_GAME_MOVIES_FILES='
data/movies'
CONTENT_GAME_DATA_FILES='
data/models
data/music
data/*.pak
data/*.pkm'
CONTENT_DOC_DATA_RELATIVE_PATH='docs'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES='
savegames'
USER_PERSISTENT_FILES='
bin/config.ini'

APP_MAIN_EXE='bin/painkiller.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=128'

PACKAGES_LIST='
PKG_BIN
PKG_MOVIES
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_MOVIES
PKG_DATA'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_ID_EN="${PKG_MOVIES_ID}-en"
PKG_MOVIES_ID_FR="${PKG_MOVIES_ID}-fr"
PKG_MOVIES_PROVIDES="
$PKG_MOVIES_ID"
PKG_MOVIES_DESCRIPTION_EN='English movies'
PKG_MOVIES_DESCRIPTION_FR='French movies'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
