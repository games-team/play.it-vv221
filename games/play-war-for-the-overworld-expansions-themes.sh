#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# War for the Overworld themes:
# - The Cynical Imp
# - Founders Theme
# - Kickstarter Theme
# - Underlord Edition
# - Seasonal Worker Skins
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='war-for-the-overworld'
GAME_NAME='War for the Overworld'

EXPANSION_ID_UNDERLORD='underlord-edition'
EXPANSION_NAME_UNDERLORD='Underlord Edition'

EXPANSION_ID_FOUNDERS='founders-theme'
EXPANSION_NAME_FOUNDERS='Founders Theme'

EXPANSION_ID_KICKSTARTER='kickstarter-theme'
EXPANSION_NAME_KICKSTARTER='Kickstarter Theme'

EXPANSION_ID_CYNICAL='the-cynical-imp'
EXPANSION_NAME_CYNICAL='The Cynical Imp'

EXPANSION_ID_SEASONALWORKER='seasonal-worker-skins'
EXPANSION_NAME_SEASONALWORKER='Seasonal Worker Skins'

# Archives

## Underlord Edition

ARCHIVE_BASE_UNDERLORD_8_NAME='war_for_the_overworld_underlord_edition_upgrade_v2_1_2_76431.sh'
ARCHIVE_BASE_UNDERLORD_8_MD5='c214676919d7475a5dc2558ab8481b2e'
ARCHIVE_BASE_UNDERLORD_8_SIZE='1048'
ARCHIVE_BASE_UNDERLORD_8_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_UNDERLORD_8_URL='https://www.gog.com/game/war_for_the_overworld_underlord_edition_upgrade'

ARCHIVE_BASE_UNDERLORD_7_NAME='war_for_the_overworld_underlord_edition_upgrade_v2_1_1_73576.sh'
ARCHIVE_BASE_UNDERLORD_7_MD5='066e110e2b85ffca512ce8c8d3d67ca3'
ARCHIVE_BASE_UNDERLORD_7_SIZE='1048'
ARCHIVE_BASE_UNDERLORD_7_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_UNDERLORD_6_NAME='war_for_the_overworld_underlord_edition_upgrade_v2_1_0f4_55096.sh'
ARCHIVE_BASE_UNDERLORD_6_MD5='b2b81108728fe2bbadd51c7897f9269c'
ARCHIVE_BASE_UNDERLORD_6_SIZE='1400'
ARCHIVE_BASE_UNDERLORD_6_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_UNDERLORD_5_NAME='war_for_the_overworld_underlord_edition_upgrade_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_UNDERLORD_5_MD5='7cd85994986cf75f9fd81d4be0df82fe'
ARCHIVE_BASE_UNDERLORD_5_SIZE='1300'
ARCHIVE_BASE_UNDERLORD_5_VERSION='2.0.7f1-gog36563'

ARCHIVE_BASE_UNDERLORD_4_NAME='war_for_the_overworld_underlord_edition_upgrade_2_0_7f1_30014.sh'
ARCHIVE_BASE_UNDERLORD_4_MD5='9d67c88dd331703315e9056dc6abb78c'
ARCHIVE_BASE_UNDERLORD_4_SIZE='1300'
ARCHIVE_BASE_UNDERLORD_4_VERSION='2.0.6f1-gog30014'

ARCHIVE_BASE_UNDERLORD_3_NAME='war_for_the_overworld_underlord_edition_upgrade_2_0_6f1_24637.sh'
ARCHIVE_BASE_UNDERLORD_3_MD5='0be12c1160fdba4f180dc3776f1bb21e'
ARCHIVE_BASE_UNDERLORD_3_SIZE='1300'
ARCHIVE_BASE_UNDERLORD_3_VERSION='2.0.6f1-gog24637'

ARCHIVE_BASE_UNDERLORD_2_NAME='war_for_the_overworld_underlord_edition_upgrade_2_0_5_24177.sh'
ARCHIVE_BASE_UNDERLORD_2_MD5='97857939a158c470d04936bc580838c2'
ARCHIVE_BASE_UNDERLORD_2_SIZE='1300'
ARCHIVE_BASE_UNDERLORD_2_VERSION='2.0.5-gog24177'

## Founders Theme

ARCHIVE_BASE_FOUNDERS_3_NAME='war_for_the_overworld_founders_theme_charity_v2_1_2_76431.sh'
ARCHIVE_BASE_FOUNDERS_3_MD5='5d3a4a047ff40d4d6aede45bc92241a5'
ARCHIVE_BASE_FOUNDERS_3_SIZE='1048'
ARCHIVE_BASE_FOUNDERS_3_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_FOUNDERS_3_URL='http://buy.wftogame.com/charity/'

ARCHIVE_BASE_FOUNDERS_2_NAME='war_for_the_overworld_founders_theme_charity_v2_1_1_73576.sh'
ARCHIVE_BASE_FOUNDERS_2_MD5='1b12880acd1ddbd352ff8321e6c0b6c8'
ARCHIVE_BASE_FOUNDERS_2_SIZE='1048'
ARCHIVE_BASE_FOUNDERS_2_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_FOUNDERS_1_NAME='war_for_the_overworld_founders_theme_charity_v2_1_0f4_55096.sh'
ARCHIVE_BASE_FOUNDERS_1_MD5='0313b90d995d5f527feea040995677f7'
ARCHIVE_BASE_FOUNDERS_1_SIZE='1400'
ARCHIVE_BASE_FOUNDERS_1_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_FOUNDERS_0_NAME='war_for_the_overworld_founders_theme_charity_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_FOUNDERS_0_MD5='a8fec55ffb2c9eda265f7f346c8998e6'
ARCHIVE_BASE_FOUNDERS_0_SIZE='1300'
ARCHIVE_BASE_FOUNDERS_0_VERSION='2.0.7f1-gog36563'

## Kickstarter Theme

ARCHIVE_BASE_KICKSTARTER_3_NAME='war_for_the_overworld_kickstarter_theme_charity_v2_1_2_76431.sh'
ARCHIVE_BASE_KICKSTARTER_3_MD5='e5f937fe8b8b2b323d06e7b77c579d45'
ARCHIVE_BASE_KICKSTARTER_3_SIZE='1048'
ARCHIVE_BASE_KICKSTARTER_3_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_KICKSTARTER_3_URL='http://buy.wftogame.com/charity/'

ARCHIVE_BASE_KICKSTARTER_2_NAME='war_for_the_overworld_kickstarter_theme_charity_v2_1_1_73576.sh'
ARCHIVE_BASE_KICKSTARTER_2_MD5='d63028e7708e7bbf167d4ea44db42ab6'
ARCHIVE_BASE_KICKSTARTER_2_SIZE='1048'
ARCHIVE_BASE_KICKSTARTER_2_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_KICKSTARTER_1_NAME='war_for_the_overworld_kickstarter_theme_charity_v2_1_0f4_55096.sh'
ARCHIVE_BASE_KICKSTARTER_1_MD5='194826ab3072cd5007a8214fe96bc071'
ARCHIVE_BASE_KICKSTARTER_1_SIZE='1400'
ARCHIVE_BASE_KICKSTARTER_1_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_KICKSTARTER_0_NAME='war_for_the_overworld_kickstarter_theme_charity_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_KICKSTARTER_0_MD5='9ee5fad56ab9dd6676ab4718fa7e5e2e'
ARCHIVE_BASE_KICKSTARTER_0_SIZE='1300'
ARCHIVE_BASE_KICKSTARTER_0_VERSION='2.0.7f1-gog36563'

## The Cynical Imp

ARCHIVE_BASE_CYNICAL_3_NAME='war_for_the_overworld_cynical_imp_charity_v2_1_2_76431.sh'
ARCHIVE_BASE_CYNICAL_3_MD5='39148177572cbb5a953eede826816cf7'
ARCHIVE_BASE_CYNICAL_3_SIZE='1048'
ARCHIVE_BASE_CYNICAL_3_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_CYNICAL_3_URL='http://buy.wftogame.com/charity/'

ARCHIVE_BASE_CYNICAL_2_NAME='war_for_the_overworld_cynical_imp_charity_v2_1_1_73576.sh'
ARCHIVE_BASE_CYNICAL_2_MD5='a82cbdad5ac4320dac813d5d588af23d'
ARCHIVE_BASE_CYNICAL_2_SIZE='1048'
ARCHIVE_BASE_CYNICAL_2_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_CYNICAL_1_NAME='war_for_the_overworld_cynical_imp_charity_v2_1_0f4_55096.sh'
ARCHIVE_BASE_CYNICAL_1_MD5='7941c2d30434ec481056ca668d8901d6'
ARCHIVE_BASE_CYNICAL_1_SIZE='1400'
ARCHIVE_BASE_CYNICAL_1_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_CYNICAL_0_NAME='war_for_the_overworld_cynical_imp_charity_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_CYNICAL_0_MD5='301e40e80942c2382dbf892fd3801216'
ARCHIVE_BASE_CYNICAL_0_SIZE='1300'
ARCHIVE_BASE_CYNICAL_0_VERSION='2.0.7f1-gog36563'

## Seasonal Worker Skins

ARCHIVE_BASE_SEASONALWORKER_4_NAME='war_for_the_overworld_seasonal_worker_skins_v2_1_2_76431.sh'
ARCHIVE_BASE_SEASONALWORKER_4_MD5='941ee4e1fd7a37a9862f79054bbb8b00'
ARCHIVE_BASE_SEASONALWORKER_4_SIZE='1048'
ARCHIVE_BASE_SEASONALWORKER_4_VERSION='2.1.2-gog76431'
ARCHIVE_BASE_SEASONALWORKER_4_URL='https://www.gog.com/game/war_for_the_overworld_worker_skin_collection'

ARCHIVE_BASE_SEASONALWORKER_3_NAME='war_for_the_overworld_seasonal_worker_skins_v2_1_1_73576.sh'
ARCHIVE_BASE_SEASONALWORKER_3_MD5='a74c7dc94c79c37a328a15d9cc4a8385'
ARCHIVE_BASE_SEASONALWORKER_3_SIZE='1048'
ARCHIVE_BASE_SEASONALWORKER_3_VERSION='2.1.1-gog73576'

ARCHIVE_BASE_SEASONALWORKER_2_NAME='war_for_the_overworld_seasonal_worker_skins_v2_1_0f4_55096.sh'
ARCHIVE_BASE_SEASONALWORKER_2_MD5='874a21a49ba212ef716e35e0a5b98357'
ARCHIVE_BASE_SEASONALWORKER_2_SIZE='1400'
ARCHIVE_BASE_SEASONALWORKER_2_VERSION='2.1.0f4-gog55096'

ARCHIVE_BASE_SEASONALWORKER_1_NAME='war_for_the_overworld_seasonal_worker_skins_2_0_7f1_gog_36563.sh'
ARCHIVE_BASE_SEASONALWORKER_1_MD5='ccc02cab316dff3f6a9351217f38f029'
ARCHIVE_BASE_SEASONALWORKER_1_SIZE='1400'
ARCHIVE_BASE_SEASONALWORKER_1_VERSION='2.0.7f1-gog36563'

ARCHIVE_BASE_SEASONALWORKER_0_NAME='war_for_the_overworld_seasonal_worker_skins_2_0_7f1_30014.sh'
ARCHIVE_BASE_SEASONALWORKER_0_MD5='6b4c2c19a901547b1fe9a8a51edab522'
ARCHIVE_BASE_SEASONALWORKER_0_SIZE='1400'
ARCHIVE_BASE_SEASONALWORKER_0_VERSION='2.0.6f1-gog30014'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
goggame-*.info'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
