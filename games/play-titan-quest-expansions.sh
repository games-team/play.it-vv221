#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Titan Quest expansions:
# - Ragnarök
# - Atlantis
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='titan-quest'
GAME_NAME='Titan Quest'

EXPANSION_ID_RAGNAROK='ragnarok'
EXPANSION_NAME_RAGNAROK='Ragnarök'

EXPANSION_ID_ATLANTIS='atlantis'
EXPANSION_NAME_ATLANTIS='Atlantis'

# Archives

## Ragnarök

ARCHIVE_BASE_RAGNAROK_0_NAME='setup_titan_quest_ragnarok_2.10.6_(59562).exe'
ARCHIVE_BASE_RAGNAROK_0_MD5='cb8f5a3f5567d9f84ad3add23347ad07'
ARCHIVE_BASE_RAGNAROK_0_TYPE='innosetup'
ARCHIVE_BASE_RAGNAROK_0_PART1_NAME='setup_titan_quest_ragnarok_2.10.6_(59562)-1.bin'
ARCHIVE_BASE_RAGNAROK_0_PART1_MD5='e4a5d3bb1264c1a3881e2263cd3d6af3'
ARCHIVE_BASE_RAGNAROK_0_SIZE='2553284'
ARCHIVE_BASE_RAGNAROK_0_VERSION='2.10.6-gog59562'
ARCHIVE_BASE_RAGNAROK_0_URL='https://www.gog.com/game/titan_quest_ragnarok'

## Atlantis

ARCHIVE_BASE_ATLANTIS_0_NAME='setup_titan_quest_atlantis_2.10.6_(59562).exe'
ARCHIVE_BASE_ATLANTIS_0_MD5='6924de3dc1dd15eec6f1e4e6d465f33c'
ARCHIVE_BASE_ATLANTIS_0_TYPE='innosetup'
ARCHIVE_BASE_ATLANTIS_0_PART1_NAME='setup_titan_quest_atlantis_2.10.6_(59562)-1.bin'
ARCHIVE_BASE_ATLANTIS_0_PART1_MD5='2ad964c3cf35c13a0e05b7680b62d718'
ARCHIVE_BASE_ATLANTIS_0_SIZE='2265041'
ARCHIVE_BASE_ATLANTIS_0_VERSION='2.10.6-gog59562'
ARCHIVE_BASE_ATLANTIS_0_URL='https://www.gog.com/game/titan_quest_atlantis'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES_RAGNAROK='
resources/quests.arc
resources/xpack2/dialog.arc
resources/xpack2/dialog_de.arc
resources/xpack2/dialog_fr.arc
resources/xpack2/dialog_ru.arc
resources/xpack2/menu.arc
resources/xpack2/quests.arc'
CONTENT_GAME_MAIN_FILES_ATLANTIS='
resources/xpack3/dialog.arc
resources/xpack3/dialog_de.arc
resources/xpack3/dialog_fr.arc
resources/xpack3/dialog_ru.arc
resources/xpack3/menu.arc
resources/xpack3/quests.arc
resources/xpack3/system.arc'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'
PKG_MAIN_PROVIDES_RAGNAROK="
${GAME_ID}-data-xpack2"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
