#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Jade Empire
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='jade-empire'
GAME_NAME='Jade Empire'

ARCHIVE_BASE_EN_1_NAME='setup_jade_empire_1.00_(15538).exe'
ARCHIVE_BASE_EN_1_MD5='e68f17f59bde2254ab1e9b70c078e9f1'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_PART1_NAME='setup_jade_empire_1.00_(15538)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='6470aa8dac5486d7c66336686e2e442d'
ARCHIVE_BASE_EN_1_PART2_NAME='setup_jade_empire_1.00_(15538)-2.bin'
ARCHIVE_BASE_EN_1_PART2_MD5='57f4931e55373a9c994b67d14f43dc1c'
ARCHIVE_BASE_EN_1_SIZE='7454315'
ARCHIVE_BASE_EN_1_VERSION='1.00-gog15538'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/jade_empire_special_edition'

ARCHIVE_BASE_FR_1_NAME='setup_jade_empire_french_1.00_(15538).exe'
ARCHIVE_BASE_FR_1_MD5='872f400a6af8bae9af9bf0b2025d29f4'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_PART1_NAME='setup_jade_empire_french_1.00_(15538)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='39182b7e8651b92b1703e6c2b89c783c'
ARCHIVE_BASE_FR_1_PART2_NAME='setup_jade_empire_french_1.00_(15538)-2.bin'
ARCHIVE_BASE_FR_1_PART2_MD5='428bf4eba51fde69fa6fe6fb05aadb96'
ARCHIVE_BASE_FR_1_SIZE='7608886'
ARCHIVE_BASE_FR_1_VERSION='1.00-gog15538'
ARCHIVE_BASE_FR_1_URL='https://www.gog.com/game/jade_empire_special_edition'

ARCHIVE_BASE_EN_0_NAME='setup_jade_empire_2.0.0.4.exe'
ARCHIVE_BASE_EN_0_MD5='8f9db8c43a9cab6cd00de3d6e69fbda5'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_jade_empire_2.0.0.4-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='9fbfbc9b047288ebcbac9551a5f27ae8'
ARCHIVE_BASE_EN_0_PART2_NAME='setup_jade_empire_2.0.0.4-2.bin'
ARCHIVE_BASE_EN_0_PART2_MD5='94af70b645c525b7263258c91d95cd92'
ARCHIVE_BASE_EN_0_PART3_NAME='setup_jade_empire_2.0.0.4-3.bin'
ARCHIVE_BASE_EN_0_PART3_MD5='3efd05ca48fc9d2dfe79b2fab2456df0'
ARCHIVE_BASE_EN_0_PART4_NAME='setup_jade_empire_2.0.0.4-4.bin'
ARCHIVE_BASE_EN_0_PART4_MD5='a480e87364cc8ab2a519c1f09a2da2c9'
ARCHIVE_BASE_EN_0_PART5_NAME='setup_jade_empire_2.0.0.4-5.bin'
ARCHIVE_BASE_EN_0_PART5_MD5='081042ad8561b599add7b2f366cf3da8'
ARCHIVE_BASE_EN_0_SIZE='7800000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog2.0.0.4'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
binkw32.dll
d3d9.dll
ogg.dll
vorbis.dll
vorbisfile.dll
jadeempire.exe
jadeempireconfig.exe
jadeempirelauncher.exe'
CONTENT_GAME_L10N_FILES='
sound
data/bips
movies/attract.bik
movies/c01_cutzu.bik
movies/c04_princisfox.bik
movies/c06_partycall.bik
movies/cut_c3escape*.bik
movies/j00_cut_open_c1.bik
movies/j00_cut_open_c6.bik
movies/j01_jiahand_01.bik
movies/j04_cut_lotfin*.bik
movies/j04_pop_*.bik
movies/j06_recover_01.bik
movies/j07_cut_drop01.bik
movies/j07_cut_final06b.bik
movies/j07_cut_final06.bik
movies/j07_cut_final06c.bik
movies/j07_cut_final06d.bik
movies/j07_cut_final06e.bik
movies/j07_cut_final06f.bik
movies/j07_cut_final06g.bik
movies/j08_cut_ending3.bik
movies/j08_cut_final_01.bik
movies/j08_cut_stone_01.bik
movies/j08_ending3_*.bik
movies/j08_final_01_*.bik
movies/j08_stone_01_*.bik
*.tlk'
CONTENT_GAME_DATA_FILES='
data
fonts
movies
override
shaderpc
*.key'
CONTENT_DOC_L10N_RELATIVE_PATH='docs'
## FIXME: An explicit list of files should be set
CONTENT_DOC_L10N_FILES='
*'
CONTENT_DOC_DATA_FILES='
*.txt'

USER_PERSISTENT_DIRECTORIES='
logs
persistent
save
scratch'
USER_PERSISTENT_FILES='
*.ini
data/*.xml'

APP_MAIN_EXE='jadeempire.exe'

APP_CONFIG_EXE='jadeempireconfig.exe'
APP_CONFIG_CAT='Settings'
APP_CONFIG_ID="${GAME_ID}-config"
APP_CONFIG_GAME="$GAME_NAME - Configuration"

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Automatically spawn game settings window on first launch
## Using desktop_field_exec here ensures that we get a path already escaped if required.
settings_cmd=$(desktop_field_exec 'APP_CONFIG')
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')"'
# Automatically spawn game settings window on first launch
settings_file="JadeEmpire.ini"
if [ ! -e "$settings_file" ]; then
	'"${settings_cmd}"'
fi
'

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
