#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tyranny
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='tyranny'
GAME_NAME='Tyranny'

ARCHIVE_BASE_0_NAME='tyranny_v1_2_1_160_v2_25169.sh'
ARCHIVE_BASE_0_MD5='72bdb7c9f6966ac6f1ccfbbad9fb29e6'
ARCHIVE_BASE_0_SIZE='15136872'
ARCHIVE_BASE_0_VERSION='1.2.1.0160-gog25169'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/tyranny_commander_edition'

ARCHIVE_BASE_1_NAME='tyranny_en_1_2_1_0158_15398.sh'
ARCHIVE_BASE_1_MD5='664cba00a861611fb155f65b8d83d9e9'
ARCHIVE_BASE_1_SIZE='15000000'
ARCHIVE_BASE_1_VERSION='1.2.1.0158-gog15398'

UNITY3D_NAME='Tyranny'
UNITY3D_PLUGINS='
libpops_api.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_AREAS_FILES="
${UNITY3D_NAME}_Data/bundles/st_ar_*"

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_AREAS
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
libresolv.so.2
librt.so.1
libstdc++.so.6
libuuid.so.1
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_AREAS'

PKG_AREAS_ID="${GAME_ID}-areas"
PKG_AREAS_DESCRIPTION='areas'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
