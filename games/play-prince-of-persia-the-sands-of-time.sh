#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Prince of Persia: The Sands of Time
###

script_version=20241125.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='prince-of-persia-the-sands-of-time'
GAME_NAME='Prince of Persia: The Sands of Time'

ARCHIVE_BASE_FR_0_NAME='setup_prince_of_persia_-_the_sands_of_time_181_(french)_(28548).exe'
ARCHIVE_BASE_FR_0_MD5='b71ed96b13461f56c8a47e16fbecadbb'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_prince_of_persia_-_the_sands_of_time_181_(french)_(28548)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='691e5144e34bf71ab15d45de11089b34'
ARCHIVE_BASE_FR_0_SIZE='1408358'
ARCHIVE_BASE_FR_0_VERSION='1.00.181-gog28548'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/prince_of_persia_the_sands_of_time'

ARCHIVE_BASE_EN_0_NAME='setup_prince_of_persia_-_the_sands_of_time_181_(28548).exe'
ARCHIVE_BASE_EN_0_MD5='008b4359cafb21dd4e437860dc23e2cf'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_prince_of_persia_-_the_sands_of_time_181_(28548)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='639fd428677dc8fb52e864422cbfe26d'
ARCHIVE_BASE_EN_0_SIZE='1413339'
ARCHIVE_BASE_EN_0_VERSION='1.00.181-gog28548'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/prince_of_persia_the_sands_of_time'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
binkw32.dll
detectionapi.dll
directx8tests.dll
directx9tests.dll
eax.dll
mfc71.dll
msvcp71.dll
msvcr71.dll
pop.exe
princeofpersia.exe
directxtests.tst
systemtests.tst'
CONTENT_GAME0_BIN_PATH='__support/app'
## "Hardware.ini" instead of "hardware.ini" is not a typo.
CONTENT_GAME0_BIN_FILES='
Hardware.ini
sound/dare.ini'
CONTENT_GAME_L10N_FILES='
poplauncherres.dll
popdata.bf
sound/soundlocal.big
sound/soundlocal.fat
video/loading.int'
CONTENT_GAME_DATA_FILES='
profiles
sound
video
prince.bf'
CONTENT_DOC_L10N_FILES='
manual.pdf
licence.txt
readme.txt'

USER_PERSISTENT_FILES='
Hardware.ini
sound/dare.ini'
USER_PERSISTENT_DIRECTORIES='
profiles'

APP_MAIN_EXE='princeofpersia.exe'
## Work around fog rendering problems
APP_MAIN_PRERUN='
# Work around fog rendering problems
config_file="Hardware.ini"
dos2unix --quiet "$config_file"
sed \
	--in-place \
	--expression="s/InvertFogRange=.*/InvertFogRange=0/" \
	"$config_file"
unix2dos --quiet "$config_file"
'
## Work around an override of configuration file
APP_MAIN_POSTRUN='
# Work around an override of configuration file
config_file="Hardware.ini"
config_file_persistent="${PATH_PERSISTENT}/${config_file}"
if \
	[ -f "$config_file" ] \
	&& [ ! -h "$config_file" ]
then
	cp --remove-destination "$config_file" "$config_file_persistent"
	rm "$config_file"
	ln --symbolic "$config_file_persistent" "$config_file"
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'
## Work around fog rendering problems
PKG_BIN_DEPENDENCIES_COMMANDS="${PKG_BIN_DEPENDENCIES_COMMANDS:-}
dos2unix"
## Ensure a smooth upgrade from packages generated with pre-20231018.1 scripts.
PKG_BIN_PROVIDES="${PKG_BIN_PROVIDES:-}
prince-of-persia"

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'
## Ensure a smooth upgrade from packages generated with pre-20231018.1 scripts.
PKG_L10N_PROVIDES="${PKG_L10N_PROVIDES:-}
prince-of-persia-l10n"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure a smooth upgrade from packages generated with pre-20231018.1 scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
prince-of-persia-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME0_BIN')"

	# Work around case-related problems caused by file deletion-creation instead of in-place edition
	mv 'hardware.ini' 'Hardware.ini'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
