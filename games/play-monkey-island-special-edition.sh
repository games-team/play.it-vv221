#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Monkey Island Special Edition:
# - Monkey Island 1
# - Monkey Island 2
###

script_version=20241121.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_MONKEY1='monkey-island-1-special-edition'
GAME_NAME_MONKEY1='Monkey Island 1 Special Edition: The Secret of Monkey Island'

GAME_ID_MONKEY2='monkey-island-2-special-edition'
GAME_NAME_MONKEY2='Monkey Island 2 Special Edition: LeChuckʼs Revenge'

# Archives

## Monkey Island 1

ARCHIVE_BASE_MONKEY1_1_NAME='setup_the_secret_of_monkey_islandtm_special_edition_1.0_(18587).exe'
ARCHIVE_BASE_MONKEY1_1_MD5='dc463e4d640e417df53684afb9921dd1'
ARCHIVE_BASE_MONKEY1_1_TYPE='innosetup'
ARCHIVE_BASE_MONKEY1_1_SIZE='2594611'
ARCHIVE_BASE_MONKEY1_1_VERSION='1.0-gog18587'
ARCHIVE_BASE_MONKEY1_1_URL='https://www.gog.com/game/the_secret_of_monkey_island_special_edition'

ARCHIVE_BASE_MONKEY1_0_NAME='setup_monkey_island_1_se_1.0_(18587).exe'
ARCHIVE_BASE_MONKEY1_0_MD5='ff2eaa21af8f59371583b007b439b873'
ARCHIVE_BASE_MONKEY1_0_TYPE='innosetup'
ARCHIVE_BASE_MONKEY1_0_PART1_NAME='setup_monkey_island_1_se_1.0_(18587)-1.bin'
ARCHIVE_BASE_MONKEY1_0_PART1_MD5='6a3ca78328b99ae0d9d0a3d7a4fb3cd9'
ARCHIVE_BASE_MONKEY1_0_SIZE='2600000'
ARCHIVE_BASE_MONKEY1_0_VERSION='1.0-gog18587'

## Monkey Island 2

ARCHIVE_BASE_MONKEY2_0_NAME='setup_monkey_island2_se_2.0.0.10.exe'
ARCHIVE_BASE_MONKEY2_0_MD5='20a0bc39dcf543856f0d463649c482c4'
ARCHIVE_BASE_MONKEY2_0_TYPE='innosetup'
ARCHIVE_BASE_MONKEY2_0_SIZE='2300000'
ARCHIVE_BASE_MONKEY2_0_VERSION='1.0-gog2.0.0.10'
ARCHIVE_BASE_MONKEY2_0_URL='https://www.gog.com/game/monkey_island_2_special_edition_lechucks_revenge'

# Archive contents

## Monkey Island 1

CONTENT_PATH_DEFAULT_MONKEY1='.'
CONTENT_PATH_DEFAULT_MONKEY1_0='app'
CONTENT_GAME_BIN_FILES_MONKEY1='
mise.exe'
CONTENT_GAME_DATA_FILES_MONKEY1='
audio
localization
monkey1.pak'
CONTENT_DOC_DATA_FILES_MONKEY1='
*.pdf'

## Monkey Island 2

CONTENT_PATH_DEFAULT_MONKEY2='app'
CONTENT_GAME_BIN_FILES_MONKEY2='
monkey2.exe
lang.ini'
CONTENT_GAME_DATA_FILES_MONKEY2='
audio
ui
monkey2.pak'

USER_PERSISTENT_FILES_MONKEY2='
monkey2.bin'

WINE_PERSISTENT_DIRECTORIES_MONKEY1='
users/${USER}/AppData/Roaming/LucasArts/The Secret of Monkey Island Special Edition'
WINE_PERSISTENT_DIRECTORIES_MONKEY2='
users/${USER}/AppData/Roaming/LucasArts/Monkey Island 2 Special Edition'
## Native d3dcompiler_47 is required to prevent a crash on launch:
## 0024:err:d3dcompiler:D3DCompile2 Failed to compile shader, vkd3d result -4.
## 0024:err:d3dcompiler:D3DCompile2 Shader log:
## 0024:err:d3dcompiler:D3DCompile2     <anonymous>:7:1: E5000: syntax error, unexpected KW_SAMPLER_STATE
## 0024:err:d3dcompiler:D3DCompile2
## TODO: Check if it is still required with current WINE
WINE_WINETRICKS_VERBS='d3dcompiler_47'

APP_MAIN_EXE_MONKEY1='mise.exe'
APP_MAIN_EXE_MONKEY2='monkey2.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
audioconvert
audio/x-wma, wmaversion=(int)1'

PKG_DATA_ID_MONKEY1="${GAME_ID_MONKEY1}-data"
PKG_DATA_ID_MONKEY2="${GAME_ID_MONKEY2}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
