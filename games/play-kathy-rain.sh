#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kathy Rain
###

script_version=20241111.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='kathy-rain'
GAME_NAME='Kathy Rain'

ARCHIVE_BASE_0_NAME='kathy_rain_director_s_cut_v_1_0_3_5225_53868.sh'
ARCHIVE_BASE_0_MD5='0bcbe5aa3508431c685b8ecd1617d560'
ARCHIVE_BASE_0_SIZE='842486'
ARCHIVE_BASE_0_VERSION='1.0.3.5225-gog53868'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/kathy_rain_directors_cut'

UNITY3D_NAME='KathyRainDirectorsCut'

CONTENT_PATH_DEFAULT='data/noarch/game'
## This game use a non-standard binary name
CONTENT_GAME0_BIN_FILES="
${UNITY3D_NAME}.x64"

APP_MAIN_EXE="${UNITY3D_NAME}.x64"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Prevent the inclusion of the shipped Steam library
	rm --recursive "$(unity3d_name)_Data/Plugins"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
