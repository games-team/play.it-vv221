#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warcraft 1
###

script_version=20241123.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warcraft-1'
GAME_NAME='Warcraft: Orcs & Humans'

ARCHIVE_BASE_1_NAME='setup_warcraft_orcs__humans_1.2_(28330).exe'
ARCHIVE_BASE_1_MD5='3ec0ea59f7a1c4413792da43cf9affe7'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='642566'
ARCHIVE_BASE_1_VERSION='1.2-gog28330'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/warcraft_orcs_and_humans'

ARCHIVE_BASE_0_NAME='setup_warcraft_orcs__humans_1.2_(28330).exe'
ARCHIVE_BASE_0_MD5='79d30dbb24395d32f77156a2e2b4639c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='642266'
ARCHIVE_BASE_0_VERSION='1.2-gog28330'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
data
drivers
*.bin
*.cue
*.exe
*.war'
CONTENT_DOC_MAIN_PATH='
*.txt'

GAME_IMAGE='war1.cue'

USER_PERSISTENT_FILES='
*.SAV
*.war'

APP_MAIN_EXE='war.exe'
APP_MAIN_ICON='app/goggame-1706049527.ico'
## Use fixed cpu cycles, to avoid scrolling issues.
APP_MAIN_DOSBOX_PRERUN='
config -set cpu cycles=fixed 30000'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build package

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
