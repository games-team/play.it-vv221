#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Songs of Silence
###

script_version=20241220.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='songs-of-silence'
GAME_NAME='Songs of Silence'

ARCHIVE_BASE_2_NAME='setup_songs_of_silence_1.1.0-d.7846_(64bit)_(78613).exe'
ARCHIVE_BASE_2_MD5='1da2bbeff582b8e21507c96e8f6d979a'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_songs_of_silence_1.1.0-d.7846_(64bit)_(78613)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='7478d544e00335d80dfd2320955b8dd6'
ARCHIVE_BASE_2_PART2_NAME='setup_songs_of_silence_1.1.0-d.7846_(64bit)_(78613)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='79ac2108a4a5afd81b77c550a7c8a25c'
ARCHIVE_BASE_2_SIZE='6083070'
ARCHIVE_BASE_2_VERSION='1.1.0-gog78613'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/songs_of_silence'

ARCHIVE_BASE_1_NAME='setup_songs_of_silence_1.0.3-d.7778_(64bit)_(78393).exe'
ARCHIVE_BASE_1_MD5='6fc46c01c88a258da47c6c767aaf667e'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_songs_of_silence_1.0.3-d.7778_(64bit)_(78393)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='0cce99726df83cc685744ad2de501282'
ARCHIVE_BASE_1_PART2_NAME='setup_songs_of_silence_1.0.3-d.7778_(64bit)_(78393)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='d221248b7e86a42b570eec2045c48f53'
ARCHIVE_BASE_1_SIZE='6092548'
ARCHIVE_BASE_1_VERSION='1.0.3-gog78393'

ARCHIVE_BASE_0_NAME='setup_songs_of_silence_1.0.2-d.7719_(64bit)_(78109).exe'
ARCHIVE_BASE_0_MD5='44adde224499bc5f07b83d58637e1b68'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_songs_of_silence_1.0.2-d.7719_(64bit)_(78109)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='b14dd73296a314c2a02e72c957001472'
ARCHIVE_BASE_0_PART2_NAME='setup_songs_of_silence_1.0.2-d.7719_(64bit)_(78109)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='26a256d9d57b6df627db84ed9cf95df3'
ARCHIVE_BASE_0_SIZE='6660310'
ARCHIVE_BASE_0_VERSION='1.0.2-gog78109'

UNITY3D_NAME='songsofsilence'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Chimera Entertainment GmbH/SongsOfSilence'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

## A warning is triggered during the extraction, but despite the hash mismatch
## the file size is the expected one and it does not seem to cause problems:
## Warning: Output checksum mismatch for songsofsilence_data/resources.assets:
##  ├─ actual:   MD5 34a78c4bd457d0af0f6be739c84f1de7
##  └─ expected: MD5 cf5d0fdd0140118e253c735df3d3059c
archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
