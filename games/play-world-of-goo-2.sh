#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# World of Goo 2
###

script_version=20250227.2

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='world-of-goo-2'
GAME_NAME='World of Goo 2'

ARCHIVE_BASE_1_NAME='World_of_Goo_2-x86_64-1.0.12478.15.AppImage'
ARCHIVE_BASE_1_MD5='3748d37805bd4bc96f1b19d108d10f9d'
ARCHIVE_BASE_1_SIZE='903954'
ARCHIVE_BASE_1_VERSION='1.0.12478-2dboy'
ARCHIVE_BASE_1_URL='https://worldofgoo2.com/#getitnow'

ARCHIVE_BASE_0_NAME='World_of_Goo_2-x86_64.12329.171.AppImage'
ARCHIVE_BASE_0_MD5='011faf15abea1c51837e6e2683c80042'
ARCHIVE_BASE_0_SIZE='896550'
ARCHIVE_BASE_0_VERSION='1.0.12329-2dboy'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
WorldOfGoo2'
CONTENT_GAME_DATA_FILES='
game
WorldOfGoo2.png'

APP_MAIN_EXE='WorldOfGoo2'
APP_MAIN_ICON='WorldOfGoo2.png'
## Prevent a crash on launch when the wayland backend of SDL is used
## This crash happens even when forcing the use of system-provided SDL
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Prevent a crash on launch when the wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'
## The game will segfault when loading the world selector if started through a symlinks farm
APP_MAIN_PREFIX_TYPE='none'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl-gnutls.so.4
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libX11.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the use of system-provided SDL
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(launcher_tweak_sdl_override)"

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
