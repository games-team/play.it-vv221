#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mask of the Rose
###

script_version=20241141.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='mask-of-the-rose'
GAME_NAME='Mask of the Rose'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

# Archives

## Full game

ARCHIVE_BASE_5_NAME='mask_of_the_rose_1_6_1043_linux_70837.sh'
ARCHIVE_BASE_5_MD5='f7fc5f6adb0621cc0c3560658de3f372'
ARCHIVE_BASE_5_SIZE='2674740'
ARCHIVE_BASE_5_VERSION='1.6.1043-gog70837'
ARCHIVE_BASE_5_URL='https://www.gog.com/game/mask_of_the_rose'

ARCHIVE_BASE_4_NAME='mask_of_the_rose_1_6_1024_linux_70649.sh'
ARCHIVE_BASE_4_MD5='6342815fe4de7b9e2998d6563e27ec6f'
ARCHIVE_BASE_4_SIZE='2674723'
ARCHIVE_BASE_4_VERSION='1.6.1024-gog70649'

ARCHIVE_BASE_3_NAME='mask_of_the_rose_1_5_943_linux_68850.sh'
ARCHIVE_BASE_3_MD5='950ad3b9a759c4c21fd31b155cb85f2c'
ARCHIVE_BASE_3_SIZE='2676548'
ARCHIVE_BASE_3_VERSION='1.5.943-gog68850'

ARCHIVE_BASE_2_NAME='mask_of_the_rose_1_4_835_linux_66472.sh'
ARCHIVE_BASE_2_MD5='2b132223cfe6558d100b1edc410be00a'
ARCHIVE_BASE_2_SIZE='2700000'
ARCHIVE_BASE_2_VERSION='1.4.835-gog66472'

ARCHIVE_BASE_1_NAME='mask_of_the_rose_1_3_765_linux_65488.sh'
ARCHIVE_BASE_1_MD5='d793ec177ee72e9ea04c025775896842'
ARCHIVE_BASE_1_SIZE='2700000'
ARCHIVE_BASE_1_VERSION='1.3.765-gog65488'

ARCHIVE_BASE_0_NAME='mask_of_the_rose_1_2_666_linux_65143.sh'
ARCHIVE_BASE_0_MD5='5c9b396ead2343237ff588f9ad784cff'
ARCHIVE_BASE_0_SIZE='2700000'
ARCHIVE_BASE_0_VERSION='1.2.666-gog65143'

## Demo

## This game demo is no longer available from GOG.
ARCHIVE_BASE_DEMO_0_NAME='mask_of_the_rose_demo_0_1_94_56552.sh'
ARCHIVE_BASE_DEMO_0_MD5='81da92a188178423523e0d1f1251e094'
ARCHIVE_BASE_DEMO_0_SIZE='1500000'
ARCHIVE_BASE_DEMO_0_VERSION='0.1.94-gog56552'


UNITY3D_NAME='Mask of the Rose'
UNITY3D_NAME_DEMO='Mask of the Rose Demo'
## TODO: Check if this library is actually required
UNITY3D_PLUGINS='
lib_burst_generated.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
