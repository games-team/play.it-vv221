#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Werewolf: The Apocalypse - Forest Stories series:
# - Werewolf: The Apocalypse - Heart of the Forest
# - Werewolf: The Apocalypse - Purgatory
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_HEART='werewolf-the-apocalypse-heart-of-the-forest'
GAME_NAME_HEART='Werewolf: The Apocalypse - Heart of the Forest'

GAME_ID_PURGATORY='werewolf-the-apocalypse-purgatory'
GAME_NAME_PURGATORY='Werewolf: The Apocalypse - Purgatory'

ARCHIVE_BASE_HEART_0_NAME='werewolf_the_apocalypse_heart_of_the_forest_1_0_13_2010191537_42086.sh'
ARCHIVE_BASE_HEART_0_MD5='8f8655786c9eed74746c9cc151b5543b'
ARCHIVE_BASE_HEART_0_SIZE='902811'
ARCHIVE_BASE_HEART_0_VERSION='1.0.13-gog42086'
ARCHIVE_BASE_HEART_0_URL='https://www.gog.com/game/werewolf_the_apocalypse_heart_of_the_forest'

ARCHIVE_BASE_PURGATORY_0_NAME='purgatory_1_0_13_2406131256_74723.sh'
ARCHIVE_BASE_PURGATORY_0_MD5='eac63ba42adc2614e66e0cd733c019fb'
ARCHIVE_BASE_PURGATORY_0_SIZE='1069791'
ARCHIVE_BASE_PURGATORY_0_VERSION='1.0.13-gog74723'
ARCHIVE_BASE_PURGATORY_0_URL='https://www.gog.com/game/werewolf_the_apocalypse_purgatory'

UNITY3D_NAME_HEART='Heart of the Forest'
UNITY3D_NAME_PURGATORY='Purgatory'
UNITY3D_PLUGINS_HEART='
libfmodL.so
libfmod.so
libfmodstudioL.so
libfmodstudio.so
libgvraudio.so
libresonanceaudio.so'
UNITY3D_PLUGINS_PURGATORY='
libfmodstudio.so
libresonanceaudio.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES_HEART='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libresolv.so.2
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_PURGATORY='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID_HEART="${GAME_ID_HEART}-data"
PKG_DATA_ID_PURGATORY="${GAME_ID_PURGATORY}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
