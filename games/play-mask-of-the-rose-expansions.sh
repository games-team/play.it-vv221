#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mask of the Rose expansions:
# - The Murder Crow
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='mask-of-the-rose'
GAME_NAME='Mask of the Rose'

EXPANSION_ID_CROW='murder-crow'
EXPANSION_NAME_CROW='The Murder Crow'

# Archives

## The Murder Crow

ARCHIVE_BASE_CROW_5_NAME='mask_of_the_rose_the_murder_crow_1_6_1043_linux_70837.sh'
ARCHIVE_BASE_CROW_5_MD5='1e492aad8f1b246055c485c924fb607b'
ARCHIVE_BASE_CROW_5_SIZE='999'
ARCHIVE_BASE_CROW_5_VERSION='1.6.1043-gog70837'

ARCHIVE_BASE_CROW_4_NAME='mask_of_the_rose_the_murder_crow_1_6_1024_linux_70649.sh'
ARCHIVE_BASE_CROW_4_MD5='f6b9cd36e7c57a775170d6953eeff7e2'
ARCHIVE_BASE_CROW_4_SIZE='999'
ARCHIVE_BASE_CROW_4_VERSION='1.6.1024-gog70649'

ARCHIVE_BASE_CROW_3_NAME='mask_of_the_rose_the_murder_crow_1_5_943_linux_68850.sh'
ARCHIVE_BASE_CROW_3_MD5='d528ab03cbfb76463b5c49774de1be0e'
ARCHIVE_BASE_CROW_3_SIZE='1300'
ARCHIVE_BASE_CROW_3_VERSION='1.5.943-gog68850'

ARCHIVE_BASE_CROW_2_NAME='mask_of_the_rose_the_murder_crow_1_4_835_linux_66472.sh'
ARCHIVE_BASE_CROW_2_MD5='1141e6eb61e618486cfe01d78bc2db90'
ARCHIVE_BASE_CROW_2_SIZE='1300'
ARCHIVE_BASE_CROW_2_VERSION='1.4.835-gog66472'

ARCHIVE_BASE_CROW_1_NAME='mask_of_the_rose_the_murder_crow_1_3_765_linux_65488.sh'
ARCHIVE_BASE_CROW_1_MD5='93ea92a56956663cd42b66ea9b04820a'
ARCHIVE_BASE_CROW_1_SIZE='1300'
ARCHIVE_BASE_CROW_1_VERSION='1.3.765-gog65488'

ARCHIVE_BASE_CROW_0_NAME='mask_of_the_rose_the_murder_crow_1_2_666_linux_65143.sh'
ARCHIVE_BASE_CROW_0_MD5='43b3b6e5025d8b2521f4d72c337aff3c'
ARCHIVE_BASE_CROW_0_SIZE='1300'
ARCHIVE_BASE_CROW_0_VERSION='1.2.666-gog65143'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
goggame-1879903382.info'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
