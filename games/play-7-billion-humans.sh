#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# 7 Billion Humans
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='7-billion-humans'
GAME_NAME='7 Billion Humans'

ARCHIVE_BASE_0_NAME='7_billion_humans_1_0_32487_23774.sh'
ARCHIVE_BASE_0_MD5='3d42952a6b9329f1c64bda00b05c4afb'
ARCHIVE_BASE_0_SIZE='160000'
ARCHIVE_BASE_0_VERSION='1.0.32487-gog23774'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/7_billion_humans'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN64_FILES='
7BillionHumans.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
7BillionHumans.bin.x86'
CONTENT_GAME_DATA_FILES='
shaders
resource.pak
icon.png'
CONTENT_DOC_DATA_FILES='
LICENSE.txt
README.linux'

APP_MAIN_EXE_BIN64='7BillionHumans.bin.x86_64'
APP_MAIN_EXE_BIN32='7BillionHumans.bin.x86'
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
