#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# StarCraft
###

script_version=20241108.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='starcraft'
GAME_NAME='StarCraft'

ARCHIVE_BASE_EN_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_EN_0_MD5='209b1757b3d10aad3e5055e7d11d7599'
ARCHIVE_BASE_EN_0_PART1_NAME='Installer Tome 2.mpq'
ARCHIVE_BASE_EN_0_PART1_MD5='d3d1bb36d4d256693a1df430d853cfe9'
ARCHIVE_BASE_EN_0_SIZE='1294777'
ARCHIVE_BASE_EN_0_VERSION='1.15.2-blizzard1'
ARCHIVE_BASE_EN_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=enUS&product=STAR'

ARCHIVE_BASE_FR_0_NAME='Installer Tome.mpq'
ARCHIVE_BASE_FR_0_MD5='5f94ac518b98829cc5a8078fc13f0b6f'
ARCHIVE_BASE_FR_0_PART1_NAME='Installer Tome 2.mpq'
ARCHIVE_BASE_FR_0_PART1_MD5='57f632256a23caaf19457773832027f7'
ARCHIVE_BASE_FR_0_SIZE='1314699'
ARCHIVE_BASE_FR_0_VERSION='1.15.2-blizzard1'
ARCHIVE_BASE_FR_0_URL='https://eu.battle.net/download/getLegacy?os=win&locale=frF&product=STAR'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_PATH='PC'
CONTENT_GAME_BIN_FILES='
*.snp
*.dll
*.exe
*.loc
*.mpq'
CONTENT_GAME_DATA_PATH='Common'
CONTENT_GAME_DATA_FILES='
Maps
*.mpq'
CONTENT_DOC_DATA_PATH='PC'
CONTENT_DOC_DATA_FILES='
*.cnt
*.hlp
*.pdf
*.txt
*.url'

APP_MAIN_EXE='StarCraft.exe'
APP_MAIN_ICON='PC/StarCraft.exe'

USER_PERSISTENT_DIRECTORIES='
Characters
Maps'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA_BASE'

PKG_DATA_BASE_ID="${GAME_ID}-data"
PKG_DATA_ID_EN="${PKG_DATA_BASE_ID}-en"
PKG_DATA_ID_FR="${PKG_DATA_BASE_ID}-fr"
PKG_DATA_PROVIDES="
$PKG_DATA_BASE_ID"
PKG_DATA_DESCRIPTION='data'

# Set list of requirements to extract the archive data

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
smpq"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of the CD key files
# See notes/starcraft for details on how to get these files.

ARCHIVE_REQUIRED_CDKEY_1_NAME='font.clh'
ARCHIVE_REQUIRED_CDKEY_2_NAME='font.gid'

archive_initialize_required \
	'ARCHIVE_CDKEY_1' \
	'ARCHIVE_REQUIRED_CDKEY_1'
archive_initialize_required \
	'ARCHIVE_CDKEY_2' \
	'ARCHIVE_REQUIRED_CDKEY_2'

# Extract game data

mkdir --parents "${PLAYIT_WORKDIR}/gamedata"
(
	cd "${PLAYIT_WORKDIR}/gamedata"

	# Extract the content from the installer
	information_archive_data_extraction "$(archive_name "$(current_archive)")"
	smpq --extract "$(archive_path "$(current_archive)")"
	information_archive_data_extraction "$(archive_name "$(current_archive)_PART1")"
	smpq --extract --overwrite "$(archive_path "$(current_archive)_PART1")"

	# Include the CD key files
	install -D --mode=644 \
		"$(archive_path 'ARCHIVE_CDKEY_1')" \
		"${PLAYIT_WORKDIR}/gamedata/Files/font/font.clh"
	install -D --mode=644 \
		"$(archive_path 'ARCHIVE_CDKEY_2')" \
		"${PLAYIT_WORKDIR}/gamedata/Files/font/font.gid"

	# Update the main .mpq file
	(
		cd 'PC'
		smpq --append ../Common/StarDat.mpq \
			StarEdit.cnt \
			EditLocal.dll \
			Local.dll \
			Riched20.dll \
			Smackw32.dll
	)
	(
		cd 'PC-100'
		smpq --append ../Common/StarDat.mpq \
			Readme.cnt \
			storm.dll \
			StarCraft.exe \
			StarEdit.exe \
			battle.snp \
			standard.snp \
			License.txt \
			./*.hlp
		## This file is only included in the English build of the game
		case "$(current_archive)" in
			('ARCHIVE_BASE_EN_'*)
				smpq --append ../Common/StarDat.mpq \
					rez/License.txt
			;;
		esac
		## Compression must be disabled when adding this file to the .mpq archive, to prevent a crash on launch
		smpq --append --compression none ../Common/StarDat.mpq \
			Smk/Blizzard.smk
	)
	(
		cd 'Files'
		## Compression must be disabled when adding these files to the .mpq archive, to prevent a crash on launch
		smpq --append --compression none ../Common/StarDat.mpq \
			font/*.fnt \
			font/font.ccd \
			font/font.clh \
			font/font.gid
	)
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
