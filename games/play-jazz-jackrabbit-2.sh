#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Jazz Jackrabbit 2
###

script_version=20241123.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='jazz-jackrabbit-2'
GAME_NAME='Jazz Jackrabbit 2'

GAME_ID_JJ2PLUS="${GAME_ID}-jj2plus"
GAME_NAME_JJ2PLUS="$GAME_NAME - JJ2+"

GAME_ID_CHRISTMAS="${GAME_ID}-the-christmas-chronicles"
GAME_NAME_CHRISTMAS="$GAME_NAME - The Christmas Chronicles"

# Game archives

## Jazz Jackrabbit 2 (including The Secret Files)

ARCHIVE_BASE_2_NAME='setup_jazz_jackrabbit_2_1.24hf_(16886).exe'
ARCHIVE_BASE_2_MD5='25a730c0813eb006555e6bbaf9613487'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='66354'
ARCHIVE_BASE_2_VERSION='1.24-gog16886'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/jazz_jackrabbit_2_collection'

ARCHIVE_BASE_1_NAME='setup_jazz_jackrabbit_2_1.24hf_(16886).exe'
ARCHIVE_BASE_1_MD5='45be80bad040ea821bc6096abe6f3196'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='66986'
ARCHIVE_BASE_1_VERSION='1.24-gog16886'

ARCHIVE_BASE_0_NAME='setup_jazz_jackrabbit_2_1.24hf_(16886).exe'
ARCHIVE_BASE_0_MD5='48a48258ed60b24068cbbb2f110b049b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='67000'
ARCHIVE_BASE_0_VERSION='1.24-gog16886'

## Jazz Jackrabbit 2 (including The Secret Files) + JJ2+ expansion

ARCHIVE_BASE_JJ2PLUS_1_NAME='setup_jazz_jackrabbit_2_1.24_jj2_(5.12)_(66703).exe'
ARCHIVE_BASE_JJ2PLUS_1_MD5='e76181591367d1ad986a6dec22fa6f9f'
ARCHIVE_BASE_JJ2PLUS_1_TYPE='innosetup'
ARCHIVE_BASE_JJ2PLUS_1_SIZE='76618'
ARCHIVE_BASE_JJ2PLUS_1_VERSION='1.24-gog66703'
ARCHIVE_BASE_JJ2PLUS_1_URL='https://www.gog.com/game/jazz_jackrabbit_2_collection'

ARCHIVE_BASE_JJ2PLUS_0_NAME='setup_jazz_jackrabbit_2_1.24_jj2_(5.9)_(46861).exe'
ARCHIVE_BASE_JJ2PLUS_0_MD5='94f8477d51e35b4cdc6b3d999b502580'
ARCHIVE_BASE_JJ2PLUS_0_TYPE='innosetup'
ARCHIVE_BASE_JJ2PLUS_0_SIZE='78000'
ARCHIVE_BASE_JJ2PLUS_0_VERSION='1.24-gog46861'

## Jazz Jackrabbit 2 - The Christmas Chronicles

ARCHIVE_BASE_CHRISTMAS_1_NAME='setup_jazz_jackrabbit_2_the_christmas_chronicles_1.2x_(16742).exe'
ARCHIVE_BASE_CHRISTMAS_1_MD5='7ef514ecfdbbe321665b507b47f55fc5'
ARCHIVE_BASE_CHRISTMAS_1_TYPE='innosetup'
ARCHIVE_BASE_CHRISTMAS_1_SIZE='71308'
ARCHIVE_BASE_CHRISTMAS_1_VERSION='1.2x-gog16742'
ARCHIVE_BASE_CHRISTMAS_1_URL='https://www.gog.com/game/jazz_jackrabbit_2_collection'

ARCHIVE_BASE_CHRISTMAS_0_NAME='setup_jazz_jackrabbit_2_cc_1.2x_(16742).exe'
ARCHIVE_BASE_CHRISTMAS_0_MD5='3289263ea6bad8bc35f02176e22109f2'
ARCHIVE_BASE_CHRISTMAS_0_TYPE='innosetup'
ARCHIVE_BASE_CHRISTMAS_0_SIZE='70000'
ARCHIVE_BASE_CHRISTMAS_0_VERSION='1.2x-gog16742'

## Jazz² Resurrection (native engine)

ARCHIVE_JAZZ2NATIVE_3_NAME='Jazz2_2.9.1_Linux.zip'
ARCHIVE_JAZZ2NATIVE_3_MD5='7a0b286248e54ab652f4f0e437912961'
ARCHIVE_JAZZ2NATIVE_3_SIZE='30115'
ARCHIVE_JAZZ2NATIVE_3_VERSION='2.9.1'
ARCHIVE_JAZZ2NATIVE_3_URL='http://deat.tk/jazz2/'

ARCHIVE_JAZZ2NATIVE_2_NAME='Jazz2_2.7.0_Linux.zip'
ARCHIVE_JAZZ2NATIVE_2_MD5='50fd07614c030c2e86498894f67cf203'
ARCHIVE_JAZZ2NATIVE_2_SIZE='29747'
ARCHIVE_JAZZ2NATIVE_2_VERSION='2.7.0'

ARCHIVE_JAZZ2NATIVE_1_NAME='Jazz2_2.6.0_Linux.zip'
ARCHIVE_JAZZ2NATIVE_1_MD5='ff2ff3acc2213f4133ca8138603feb67'
ARCHIVE_JAZZ2NATIVE_1_SIZE='29553'
ARCHIVE_JAZZ2NATIVE_1_VERSION='2.6.0'

# Archives content

## Jazz Jackrabbit 2

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_PATH_DEFAULT_CHRISTMAS_0='app'
CONTENT_GAME_DATA_FILES='
html
jcshelp
tiles
userlevels
*.asc
*.it
*.j2?
*.j2as
*.mo3
*.mod
*.mut
*.pal
*.s3m
*.wav'
CONTENT_DOC_DATA_FILES='
*.doc
*.html
*.pdf
*.txt'

## Jazz² Resurrection

CONTENT_GAME0_DATA_PATH='x64'
CONTENT_GAME0_DATA_FILES='
Content'

CONTENT_GAME_BIN_DEFAULT_PATH='x64'
CONTENT_GAME_BIN_DEFAULT_FILES='
jazz2'

CONTENT_GAME_BIN_SDL_PATH='x64'
CONTENT_GAME_BIN_SDL_FILES='
jazz2_sdl2'


APP_MAIN_EXE_BIN_DEFAULT='jazz2'
APP_MAIN_EXE_BIN_SDL='jazz2_sdl2'
APP_MAIN_ICON='jazz2.exe'

PACKAGES_LIST='
PKG_BIN_DEFAULT
PKG_BIN_SDL
PKG_DATA'

PKG_BIN_ID="${GAME_ID}-bin"
PKG_BIN_ID_JJ2PLUS="${GAME_ID_JJ2PLUS}-bin"
PKG_BIN_ID_CHRISTMAS="${GAME_ID_CHRISTMAS}-bin"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_PROVIDES_JJ2PLUS="
$PKG_BIN_ID_JJ2PLUS"
PKG_BIN_PROVIDES_CHRISTMAS="
$PKG_BIN_ID_CHRISTMAS"

PKG_BIN_DEFAULT_ID="${PKG_BIN_ID}-default"
PKG_BIN_DEFAULT_ID_JJ2PLUS="${PKG_BIN_ID_JJ2PLUS}-default"
PKG_BIN_DEFAULT_ID_CHRISTMAS="${PKG_BIN_ID_CHRISTMAS}-default"
PKG_BIN_DEFAULT_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_DEFAULT_PROVIDES_JJ2PLUS="$PKG_BIN_PROVIDES_JJ2PLUS"
PKG_BIN_DEFAULT_PROVIDES_CHRISTMAS="$PKG_BIN_PROVIDES_CHRISTMAS"
PKG_BIN_DEFAULT_ARCH='64'
PKG_BIN_DEFAULT_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEFAULT_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libglfw.so.3
libm.so.6
libopenal.so.1
libOpenGL.so.0
libopenmpt.so.0
libstdc++.so.6
libvorbisfile.so.3
libz.so.1'
## Ensure smooth upgrades from packages generated with pre-20241123.3 game scripts
PKG_BIN_DEFAULT_PROVIDES="${PKG_BIN_DEFAULT_PROVIDES:-}
jazz-jackrabbit-2
jazz-jackrabbit-2-bin"
PKG_BIN_DEFAULT_PROVIDES_CHRISTMAS="${PKG_BIN_DEFAULT_PROVIDES_CHRISTMAS:-}
jazz-jackrabbit-2-the-christmas-chronicles
jazz-jackrabbit-2-the-christmas-chronicles-bin"
### This one is not actually required, but prevents the JJ2+ package from conflicting with the regular one.
PKG_BIN_DEFAULT_PROVIDES_JJ2PLUS="${PKG_BIN_DEFAULT_PROVIDES_JJ2PLUS:-}
jazz-jackrabbit-2-jj2plus
jazz-jackrabbit-2-jj2plus-bin"

PKG_BIN_SDL_ID="${PKG_BIN_ID}-sdl"
PKG_BIN_SDL_ID_JJ2PLUS="${PKG_BIN_ID_JJ2PLUS}-sdl"
PKG_BIN_SDL_ID_CHRISTMAS="${PKG_BIN_ID_CHRISTMAS}-sdl"
PKG_BIN_SDL_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_SDL_PROVIDES_JJ2PLUS="$PKG_BIN_PROVIDES_JJ2PLUS"
PKG_BIN_SDL_PROVIDES_CHRISTMAS="$PKG_BIN_PROVIDES_CHRISTMAS"
PKG_BIN_SDL_ARCH='64'
PKG_BIN_SDL_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_SDL_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libOpenGL.so.0
libopenmpt.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libvorbisfile.so.3
libz.so.1'
## Ensure smooth upgrades from packages generated with pre-20241123.3 game scripts
PKG_BIN_SDL_PROVIDES="${PKG_BIN_SDL_PROVIDES:-}
jazz-jackrabbit-2
jazz-jackrabbit-2-bin"
PKG_BIN_SDL_PROVIDES_CHRISTMAS="${PKG_BIN_SDL_PROVIDES_CHRISTMAS:-}
jazz-jackrabbit-2-the-christmas-chronicles
jazz-jackrabbit-2-the-christmas-chronicles-bin"
### This one is not actually required, but prevents the JJ2+ package from conflicting with the regular one.
PKG_BIN_SDL_PROVIDES_JJ2PLUS="${PKG_BIN_SDL_PROVIDES_JJ2PLUS:-}
jazz-jackrabbit-2-jj2plus
jazz-jackrabbit-2-jj2plus-bin"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_JJ2PLUS="${GAME_ID_JJ2PLUS}-data"
PKG_DATA_ID_CHRISTMAS="${GAME_ID_CHRISTMAS}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of required extra archives

archive_initialize_required \
	'ARCHIVE_JAZZ2NATIVE' \
	'ARCHIVE_JAZZ2NATIVE_3' \
	'ARCHIVE_JAZZ2NATIVE_2' \
	'ARCHIVE_JAZZ2NATIVE_1'
installer_release=$(package_version | cut --delimiter='-' --fields=2)
## TODO: The following test could be avoided if the library did set ARCHIVE_xxx_VERSION through archive_initialize_required
##       cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/550
case "$(archive_name 'ARCHIVE_JAZZ2NATIVE')" in
	("$(archive_name 'ARCHIVE_JAZZ2NATIVE_3')")
		export "$(current_archive)_VERSION=${ARCHIVE_JAZZ2NATIVE_3_VERSION}-${installer_release}"
	;;
	("$(archive_name 'ARCHIVE_JAZZ2NATIVE_2')")
		export "$(current_archive)_VERSION=${ARCHIVE_JAZZ2NATIVE_2_VERSION}-${installer_release}"
	;;
	("$(archive_name 'ARCHIVE_JAZZ2NATIVE_1')")
		export "$(current_archive)_VERSION=${ARCHIVE_JAZZ2NATIVE_1_VERSION}-${installer_release}"
	;;
esac

# Extract game data

archive_extraction_default
archive_extraction 'ARCHIVE_JAZZ2NATIVE'

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion 'GAME_DATA' 'PKG_DATA' "$(path_game_data)/Source"
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN_DEFAULT'
launchers_generation 'PKG_BIN_SDL'

# Build packages

packages_generation
printf '\n'
printf 'Default binaries:'
print_instructions 'PKG_BIN_DEFAULT' 'PKG_DATA'
printf 'SDL build:'
print_instructions 'PKG_BIN_SDL' 'PKG_DATA'

# Clean up

working_directory_cleanup

exit 0
