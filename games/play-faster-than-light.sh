#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 BetaRays
set -o errexit

###
# Faster Than Light
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='faster-than-light'
GAME_NAME='FTL: Faster Than Light'

# Archives

## Installers sold from gog.com

ARCHIVE_BASE_GOG_6_NAME='ftl_advanced_edition_1_6_12_2_35269.sh'
ARCHIVE_BASE_GOG_6_MD5='fc012e9ac7515f0b7b119a73ccfd7190'
ARCHIVE_BASE_GOG_6_SIZE='401336'
ARCHIVE_BASE_GOG_6_VERSION='1.6.12.2-gog35269'
ARCHIVE_BASE_GOG_6_URL='https://www.gog.com/game/faster_than_light'

ARCHIVE_BASE_GOG_5_NAME='ftl_advanced_edition_1_6_12_2_34795.sh'
ARCHIVE_BASE_GOG_5_MD5='d62355fc9339cd901242fc1828e8c248'
ARCHIVE_BASE_GOG_5_SIZE='410000'
ARCHIVE_BASE_GOG_5_VERSION='1.6.12.2-gog34795'

ARCHIVE_BASE_GOG_4_NAME='ftl_advanced_edition_1_6_9_25330.sh'
ARCHIVE_BASE_GOG_4_MD5='c3598ab0c07d1f038eb1642da066b6a5'
ARCHIVE_BASE_GOG_4_SIZE='230000'
ARCHIVE_BASE_GOG_4_VERSION='1.6.9-gog25330'

ARCHIVE_BASE_GOG_3_NAME='ftl_advanced_edition_1_6_8_24110.sh'
ARCHIVE_BASE_GOG_3_MD5='4d654aeca32de557c109fa5c642ff455'
ARCHIVE_BASE_GOG_3_SIZE='230000'
ARCHIVE_BASE_GOG_3_VERSION='1.6.8-gog24110'

ARCHIVE_BASE_GOG_2_NAME='ftl_advanced_edition_1_6_7_24012.sh'
ARCHIVE_BASE_GOG_2_MD5='43392da0d11548b1c16f1263fc5fad65'
ARCHIVE_BASE_GOG_2_SIZE='230000'
ARCHIVE_BASE_GOG_2_VERSION='1.6.8-gog24012'

ARCHIVE_BASE_GOG_1_NAME='ftl_advanced_edition_en_1_6_7_18662.sh'
ARCHIVE_BASE_GOG_1_MD5='2c5254547639b7718dac7a06dabd1d82'
ARCHIVE_BASE_GOG_1_SIZE='210000'
ARCHIVE_BASE_GOG_1_VERSION='1.6.7-gog18662'

ARCHIVE_BASE_GOG_0_NAME='ftl_advanced_edition_en_1_6_3_17917.sh'
ARCHIVE_BASE_GOG_0_MD5='b64692d5302a1ab60d912c5eb5fbc5e4'
ARCHIVE_BASE_GOG_0_SIZE='210000'
ARCHIVE_BASE_GOG_0_VERSION='1.6.3-gog17917'

# Installers sold from humblebundle.com

ARCHIVE_BASE_HUMBLE_2_NAME='FTL.1.6.12.Linux.zip'
ARCHIVE_BASE_HUMBLE_2_MD5='4ee7ea561d7753c8a003570364e15311'
ARCHIVE_BASE_HUMBLE_2_SIZE='400104'
ARCHIVE_BASE_HUMBLE_2_VERSION='1.6.12-humble191220'
ARCHIVE_BASE_HUMBLE_2_URL='https://www.humblebundle.com/store/ftl-faster-than-light'

ARCHIVE_BASE_HUMBLE_1_NAME='FTL.1.6.9.tar.gz'
ARCHIVE_BASE_HUMBLE_1_MD5='c70d9cbc55217a5f83e0d51189240ec2'
ARCHIVE_BASE_HUMBLE_1_SIZE='230000'
ARCHIVE_BASE_HUMBLE_1_VERSION='1.6.9-humble181120'

ARCHIVE_BASE_HUMBLE_0_NAME='FTL-linux-1.6.8.tar.gz'
ARCHIVE_BASE_HUMBLE_0_MD5='5898d476dae289dae20d93ecfc1b8390'
ARCHIVE_BASE_HUMBLE_0_SIZE='230000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.6.8-humble180928'


CONTENT_PATH_DEFAULT_GOG='data/noarch/game/data'
CONTENT_PATH_DEFAULT_HUMBLE='FTL-linux/data'
CONTENT_PATH_DEFAULT_HUMBLE_2='FTL.1.6.12.Linux/data'
CONTENT_GAME_BIN64_FILES='
FTL.amd64'
CONTENT_GAME_BIN32_FILES='
FTL.x86'
CONTENT_GAME_DATA_FILES='
exe_icon.bmp
ftl.dat'
CONTENT_DOC_DATA_FILES='
licenses'
CONTENT_DOC0_DATA_RELATIVE_PATH='..'
CONTENT_DOC0_DATA_FILES='
FTL_README.html'

APP_MAIN_EXE_BIN64='FTL.amd64'
APP_MAIN_EXE_BIN32='FTL.x86'
APP_MAIN_ICON='exe_icon.bmp'
## Work around problems with non-US locales.
APP_MAIN_PRERUN='
# Work around problems with non-US locales
export LANG=C
'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libX11.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
