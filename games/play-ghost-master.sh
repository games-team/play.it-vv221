#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ghost Master
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='ghost-master'
GAME_NAME='Ghost Master'

ARCHIVE_BASE_1_NAME='setup_ghost_master_20171020_(15806).exe'
ARCHIVE_BASE_1_MD5='bbc7b8d6ed9b08c54cba6f2b1048a0fd'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='658670'
ARCHIVE_BASE_1_VERSION='1.1-gog15806'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/ghost_master'

ARCHIVE_BASE_0_NAME='setup_ghost_master_2.0.0.3.exe'
ARCHIVE_BASE_0_MD5='f581e0e08d7d9dfc89838c3ac892611a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='650000'
ARCHIVE_BASE_0_VERSION='1.1-gog2.0.0.3'

CONTENT_PATH_DEFAULT='app/ghostdata'
CONTENT_GAME_BIN_FILES='
spdrv.cfg
ogg.dll
vorbis.dll
vorbisfile.dll
ghost.exe'
CONTENT_GAME_DATA_FILES='
characters
cursors
fonts
icons
levels
movies
music
new_animations
otherobjects
psparams
pstextures
scenarios
screenshots
scripts
sound
text
ui
voice
lsize.txt'
CONTENT_DOC_DATA_PATH='app'
CONTENT_DOC_DATA_FILES='
manual.pdf
readme.txt'

WINE_PERSISTENT_DIRECTORIES='
users/Public/Documents/Ghost Master/SaveGames'

USER_PERSISTENT_DIRECTORIES='
screenshots'
USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE='ghost.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
## Even with all the required decoders available,
## the introduction movie only shows still frames.
## cf. https://bugs.winehq.org/show_bug.cgi?id=53734
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
deinterlace
video/x-ms-asf
audio/x-wma, wmaversion=(int)1
video/x-wmv, wmvversion=(int)1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
