#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Titan Quest
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='titan-quest'
GAME_NAME='Titan Quest'

ARCHIVE_BASE_0_NAME='setup_titan_quest_-_anniversary_edition_2.10.6_(59562).exe'
ARCHIVE_BASE_0_MD5='a8b9399e2f968ca819c7138ff2ce5ab1'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_titan_quest_-_anniversary_edition_2.10.6_(59562)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='7164d6f1cf600aaa17101b4a263d4a70'
ARCHIVE_BASE_0_PART2_NAME='setup_titan_quest_-_anniversary_edition_2.10.6_(59562)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='b8b5821c04bfed43068e02b5a1431511'
ARCHIVE_BASE_0_PART3_NAME='setup_titan_quest_-_anniversary_edition_2.10.6_(59562)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='bb6bcb03b2ef688ef57d46d12d4b1233'
ARCHIVE_BASE_0_PART4_NAME='setup_titan_quest_-_anniversary_edition_2.10.6_(59562)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='2a61ebe44e9424cdbad29f14e072c743'
ARCHIVE_BASE_0_SIZE='13753628'
ARCHIVE_BASE_0_VERSION='2.10.21415-gog59562'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/titan_quest_anniversary_edition'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
milesredist
settings
binkw32.dll
concrt140.dll
devil.dll
direct3d11.dll
direct3d.dll
engine.dll
game.dll
gfsdk_ssao_d3d11.win32.dll
libcurl.dll
libeay32.dll
libfbxsdk.dll
memorymgr.dll
mss32.dll
msvcp140.dll
msvcp71.dll
msvcr71.dll
natlib.dll
pathengine.dll
sourcecontrol.dll
ssleay32.dll
thqgdscore.dll
thqno_api.dll
vcruntime140.dll
widget.dll
zlib1.dll
maxplugins.dlo
aifeditor.exe
animationcompiler.exe
archivetool.exe
artmanager.exe
bitmapcreator.exe
editor.exe
fbxexporter.exe
fontcompiler.exe
mapcompiler.exe
modelcompiler.exe
pseditor.exe
questeditor.exe
resourcetree.exe
shadercompiler.exe
sourceserver.exe
texturecompiler.exe
tq.exe
viewer.exe'
## The game crashes on launch if some store-specific libraries are missing:
##
## [2024-06-06-23:20:08][ERROR]: LoadPlugin: Unable to load plugin 'gog' from THQNOnline\gog\
## [2024-06-06-23:20:08][ERROR]: LoadPlugin: Unable to load plugin 'gog' from THQNOnline\gog\
CONTENT_GAME0_BIN_FILES='
thqnonline/gog'
CONTENT_GAME_DATA_XPACK2_FILES='
resources/quests.arc'
CONTENT_GAME_DATA_RESOURCES_FILES='
resources'
CONTENT_GAME_DATA_FILES='
audio
database
text
toolset
videos
thqnocfg.dat'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Titan Quest - Immortal Throne'

APP_MAIN_EXE='tq.exe'
APP_MAIN_OPTIONS='/dx11'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_XPACK2
PKG_DATA_RESOURCES
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_XPACK2
PKG_DATA_RESOURCES'

PKG_DATA_XPACK2_ID="${PKG_DATA_ID}-xpack2"
PKG_DATA_XPACK2_DESCRIPTION='resources shared between the base game and the Raganarök expansion'

PKG_DATA_RESOURCES_ID="${PKG_DATA_ID}-resources"
PKG_DATA_RESOURCES_DESCRIPTION="$PKG_DATA_DESCRIPTION - resources"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
