#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Return to Monkey Island
###

script_version=20241126.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='return-to-monkey-island'
GAME_NAME='Return to Monkey Island'

ARCHIVE_BASE_0_NAME='return_to_monkey_island_1_5_linux_60417.sh'
ARCHIVE_BASE_0_MD5='ef405e58f99c56b69bdd1da811b79580'
ARCHIVE_BASE_0_SIZE='4477001'
ARCHIVE_BASE_0_VERSION='1.5-gog60417'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/return_to_monkey_island'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libfmod.so.13
libfmodstudio.so.13'
CONTENT_GAME_BIN_FILES='
Return to Monkey Island'
CONTENT_GAME_DATA_FILES='
Resources
ReturnToMonkeyIsland.png
Weird.ggpack??'

APP_MAIN_EXE='Return to Monkey Island'
APP_MAIN_ICON='ReturnToMonkeyIsland.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libEGL.so.1
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libuuid.so.1
libX11.so.6
libXi.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
