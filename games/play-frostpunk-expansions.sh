#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Frostpunk expansions:
# - The Rifts
# - The Last Autumn
# - On The Edge
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='frostpunk'
GAME_NAME='Frostpunk'

EXPANSION_ID_RIFTS='the-rifts'
EXPANSION_NAME_RIFTS='The Rifts'

EXPANSION_ID_AUTUMN='the-last-autumn'
EXPANSION_NAME_AUTUMN='The Last Autumn'

EXPANSION_ID_EDGE='on-the-edge'
EXPANSION_NAME_EDGE='On The Edge'

# Archives

## The Rifts

ARCHIVE_BASE_RIFTS_0_NAME='setup_frostpunk_-_szczeliny_1.6.1_51852_59618_(51757).exe'
ARCHIVE_BASE_RIFTS_0_MD5='28b7855428687f3e819e141e78f0efce'
ARCHIVE_BASE_RIFTS_0_TYPE='innosetup'
ARCHIVE_BASE_RIFTS_0_SIZE='7457'
ARCHIVE_BASE_RIFTS_0_VERSION='1.6.1-gog51757'
ARCHIVE_BASE_RIFTS_0_URL='https://www.gog.com/game/frostpunk_the_rifts'

## The Last Autumn

ARCHIVE_BASE_AUTUMN_0_NAME='setup_frostpunk_the_last_autumn_1.6.1_51852_59618_(51757).exe'
ARCHIVE_BASE_AUTUMN_0_MD5='caf40e3e2fc0354b4a4ac130abba1f9e'
ARCHIVE_BASE_AUTUMN_0_TYPE='innosetup'
ARCHIVE_BASE_AUTUMN_0_SIZE='7132'
ARCHIVE_BASE_AUTUMN_0_VERSION='1.6.1-gog51757'
ARCHIVE_BASE_AUTUMN_0_URL='https://www.gog.com/game/frostpunk_the_last_autumn'

## On The Edge

ARCHIVE_BASE_EDGE_0_NAME='setup_frostpunk_on_the_edge_1.6.1_51852_59618_(51757).exe'
ARCHIVE_BASE_EDGE_0_MD5='ac91f06f1e421f0ae8061eefaaf42859'
ARCHIVE_BASE_EDGE_0_TYPE='innosetup'
ARCHIVE_BASE_EDGE_0_SIZE='7926'
ARCHIVE_BASE_EDGE_0_VERSION='1.6.1-gog51757'
ARCHIVE_BASE_EDGE_0_URL='https://www.gog.com/game/frostpunk_on_the_edge'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
goggame-*.info'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
