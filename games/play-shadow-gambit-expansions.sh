#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shadow Gambit expansions:
# - Yuki's Wish
# - Zagan's Ritual
###

script_version=20241023.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='shadow-gambit'
GAME_NAME='Shadow Gambit: The Cursed Crew'

EXPANSION_ID_YUKI='yukis-wish'
EXPANSION_NAME_YUKI='Yuki’s Wish'

EXPANSION_ID_ZAGAN='zagans-ritual'
EXPANSION_NAME_ZAGAN='Zagan’s Ritual'

# Archives

## Yuki's Wish

ARCHIVE_BASE_YUKI_0_NAME='setup_shadow_gambit_the_cursed_crew__dlctrp_1.2.133.f.r40893_(64bit)_(69829).exe'
ARCHIVE_BASE_YUKI_0_MD5='da9c4f3c92dc2eec996592f3e96acf33'
ARCHIVE_BASE_YUKI_0_TYPE='innosetup'
ARCHIVE_BASE_YUKI_0_SIZE='7198'
ARCHIVE_BASE_YUKI_0_VERSION='1.2.133.f-gog69829'
ARCHIVE_BASE_YUKI_0_URL='https://www.gog.com/game/shadow_gambit_yukis_wish'

## Zagan's Ritual

ARCHIVE_BASE_ZAGAN_0_NAME='setup_shadow_gambit_the_cursed_crew__dlcchc_1.2.133.f.r40893_(64bit)_(69829).exe'
ARCHIVE_BASE_ZAGAN_0_MD5='a715770254fe1f451c616503c19ecb2e'
ARCHIVE_BASE_ZAGAN_0_TYPE='innosetup'
ARCHIVE_BASE_ZAGAN_0_SIZE='7058'
ARCHIVE_BASE_ZAGAN_0_VERSION='1.2.133.f-gog69829'
ARCHIVE_BASE_ZAGAN_0_URL='https://www.gog.com/game/shadow_gambit_zagans_ritual'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
goggame-*.info'

## TODO: This could automatically be set by the library when EXPANSION_ID is set
PKG_PARENT_ID="$GAME_ID"
PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
