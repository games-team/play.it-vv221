#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# Shadowrun trilogy:
# - Shadowrun Returns
# - Shadowrun: Dragonfall
# - Shadowrun: Hong Kong
###

script_version=20241226.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_RETURNS='shadowrun-returns'
GAME_NAME_RETURNS='Shadowrun Returns'

GAME_ID_DRAGONFALL='shadowrun-dragonfall'
GAME_NAME_DRAGONFALL='Shadowrun: Dragonfall'

GAME_ID_HONGKONG='shadowrun-hong-kong'
GAME_NAME_HONGKONG='Shadowrun: Hong Kong'

# Archives

## Shadowrun Returns

ARCHIVE_BASE_RETURNS_GOG_1_NAME='gog_shadowrun_returns_2.0.0.7.sh'
ARCHIVE_BASE_RETURNS_GOG_1_MD5='61c12b14c7e6040cb1465390320a61da'
ARCHIVE_BASE_RETURNS_GOG_1_SIZE='3000000'
ARCHIVE_BASE_RETURNS_GOG_1_VERSION='1.2.7-gog2.0.0.7'
ARCHIVE_BASE_RETURNS_GOG_1_URL='https://www.gog.com/game/shadowrun_returns'

ARCHIVE_BASE_RETURNS_GOG_0_NAME='gog_shadowrun_returns_2.0.0.5.sh'
ARCHIVE_BASE_RETURNS_GOG_0_MD5='feb59e116eb3fd7a12f484a135e37fa4'
ARCHIVE_BASE_RETURNS_GOG_0_SIZE='3000000'
ARCHIVE_BASE_RETURNS_GOG_0_VERSION='1.2.7-gog2.0.0.5'

ARCHIVE_BASE_RETURNS_HUMBLE_0_NAME='shadowrun-returns-linux127.tar.gz'
ARCHIVE_BASE_RETURNS_HUMBLE_0_MD5='ff3146b1ad046f81bf8f3deba277e472'
ARCHIVE_BASE_RETURNS_HUMBLE_0_SIZE='3000000'
ARCHIVE_BASE_RETURNS_HUMBLE_0_VERSION='1.2.7-humble140311'
ARCHIVE_BASE_RETURNS_HUMBLE_0_URL='https://www.humblebundle.com/store/shadowrun-returns'

## Shadowrun: Dragonfall

ARCHIVE_BASE_DRAGONFALL_0_NAME='gog_shadowrun_dragonfall_director_s_cut_2.6.0.11.sh'
ARCHIVE_BASE_DRAGONFALL_0_MD5='ee3db5bc8554852337b063b993f66012'
ARCHIVE_BASE_DRAGONFALL_0_SIZE='7200000'
ARCHIVE_BASE_DRAGONFALL_0_VERSION='2.0.9-gog2.6.0.11'
ARCHIVE_BASE_DRAGONFALL_0_URL='https://www.gog.com/game/shadowrun_dragonfall_directors_cut'

## Shadowrun: Hong Kong

ARCHIVE_BASE_HONGKONG_0_NAME='gog_shadowrun_hong_kong_extended_edition_2.8.0.11.sh'
ARCHIVE_BASE_HONGKONG_0_MD5='643ba68e47c309d391a6482f838e46af'
ARCHIVE_BASE_HONGKONG_0_SIZE='12000000'
ARCHIVE_BASE_HONGKONG_0_VERSION='3.1.2-gog2.8.0.11'
ARCHIVE_BASE_HONGKONG_0_URL='https://www.gog.com/game/shadowrun_hong_kong_extended_edition'


UNITY3D_NAME_RETURNS='Shadowrun'
UNITY3D_NAME_DRAGONFALL='Dragonfall'
UNITY3D_NAME_HONGKONG='SRHK'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_RETURNS_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_RETURNS_HUMBLE='Shadowrun Returns'
CONTENT_PATH_DEFAULT_DRAGONFALL='data/noarch/game'
CONTENT_PATH_DEFAULT_HONGKONG='data/noarch/game'
CONTENT_GAME_DATA_STANDALONE_FILES='
*_Data/StreamingAssets/standalone'
CONTENT_GAME0_DATA_FILES_HONGKONG='
dictionary'

FAKE_HOME_PERSISTENT_DIRECTORIES_RETURNS='
Documents/Shadowrun Returns'
FAKE_HOME_PERSISTENT_DIRECTORIES_DRAGONFALL='
Documents/Shadowrun Dragonfall'
FAKE_HOME_PERSISTENT_DIRECTORIES_HONGKONG='
Documents/Shadowrun Hong Kong'

## Create required writable directory
APP_MAIN_PRERUN='
# Create required writable directory
mkdir --parents DumpBox
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_STANDALONE
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES_RETURNS='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'
PKG_BIN_DEPENDENCIES_LIBRARIES_DRAGONFALL='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'
PKG_BIN_DEPENDENCIES_LIBRARIES_HONGKONG='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

PKG_DATA_ID_RETURNS="${GAME_ID_RETURNS}-data"
PKG_DATA_ID_DRAGONFALL="${GAME_ID_DRAGONFALL}-data"
PKG_DATA_ID_HONGKONG="${GAME_ID_HONGKONG}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_STANDALONE'

PKG_DATA_STANDALONE_ID_RETURNS="${PKG_DATA_ID_RETURNS}-standalone"
PKG_DATA_STANDALONE_ID_DRAGONFALL="${PKG_DATA_ID_DRAGONFALL}-standalone"
PKG_DATA_STANDALONE_ID_HONGKONG="${PKG_DATA_ID_HONGKONG}-standalone"
PKG_DATA_STANDALONE_DESCRIPTION="$PKG_DATA_DESCRIPTION - standalone"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
