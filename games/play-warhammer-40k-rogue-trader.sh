#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warhammer 40k: Rogue Trader
###

script_version=20250212.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='warhammer-40k-rogue-trader'
GAME_NAME='Warhammer 40,000: Rogue Trader'

ARCHIVE_BASE_5_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404).exe'
ARCHIVE_BASE_5_MD5='71407e5f100c9afb88d8a876acbe51b7'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_PART1_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-1.bin'
ARCHIVE_BASE_5_PART1_MD5='451f0a5aac0a3da0c9e8fa3979eff824'
ARCHIVE_BASE_5_PART2_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-2.bin'
ARCHIVE_BASE_5_PART2_MD5='14fb662df8cd6819ddfce0450c8ec36a'
ARCHIVE_BASE_5_PART3_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-3.bin'
ARCHIVE_BASE_5_PART3_MD5='f80074c63d24b5520ec88654285c5468'
ARCHIVE_BASE_5_PART4_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-4.bin'
ARCHIVE_BASE_5_PART4_MD5='48c37e52d4c3427aed8b6bd91620bcf9'
ARCHIVE_BASE_5_PART5_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-5.bin'
ARCHIVE_BASE_5_PART5_MD5='bc344f584140b3f6d667d9e879cc8fcc'
ARCHIVE_BASE_5_PART6_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-6.bin'
ARCHIVE_BASE_5_PART6_MD5='621b0d6aa656fb9a9d4e996b11f36d3e'
ARCHIVE_BASE_5_PART7_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-7.bin'
ARCHIVE_BASE_5_PART7_MD5='f4824d1c931dab3d9e03d18fda2154c1'
ARCHIVE_BASE_5_PART8_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.11_release_(79404)-8.bin'
ARCHIVE_BASE_5_PART8_MD5='f7bb783d1b27594b3628838afe9db6d2'
ARCHIVE_BASE_5_SIZE='37048733'
ARCHIVE_BASE_5_VERSION='1.3.1.11-gog79404'
ARCHIVE_BASE_5_URL='https://www.gog.com/game/warhammer_40000_rogue_trader'

ARCHIVE_BASE_4_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738).exe'
ARCHIVE_BASE_4_MD5='13a67e2d41eaa13be8b5b029f5689960'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_PART1_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-1.bin'
ARCHIVE_BASE_4_PART1_MD5='dd82d3952855dccbacea503ecb474d29'
ARCHIVE_BASE_4_PART2_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-2.bin'
ARCHIVE_BASE_4_PART2_MD5='7e9d9ec0ed8b3464b3bc69b1b4da9cac'
ARCHIVE_BASE_4_PART3_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-3.bin'
ARCHIVE_BASE_4_PART3_MD5='cea9076d2ca033c8c19d0ae430e1e360'
ARCHIVE_BASE_4_PART4_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-4.bin'
ARCHIVE_BASE_4_PART4_MD5='511ea6681bc8bdc30b2fbcecebc0eb93'
ARCHIVE_BASE_4_PART5_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-5.bin'
ARCHIVE_BASE_4_PART5_MD5='a14376472e60896a5df65ee827f6b44e'
ARCHIVE_BASE_4_PART6_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-6.bin'
ARCHIVE_BASE_4_PART6_MD5='dde090355aa02688b2221654d28d578d'
ARCHIVE_BASE_4_PART7_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-7.bin'
ARCHIVE_BASE_4_PART7_MD5='ce048a3df557b10ec1b9aed3829d4a01'
ARCHIVE_BASE_4_PART8_NAME='setup_warhammer_40000_rogue_trader_windows_1.3.1.6_release_(78738)-8.bin'
ARCHIVE_BASE_4_PART8_MD5='2c92fb4be98fd9b1dd1df64d1054aa92'
ARCHIVE_BASE_4_SIZE='37053756'
ARCHIVE_BASE_4_VERSION='1.3.1.6-gog78738'

ARCHIVE_BASE_3_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703).exe'
ARCHIVE_BASE_3_MD5='02a4073b04dbbf73724860397b66af7f'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_PART1_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-1.bin'
ARCHIVE_BASE_3_PART1_MD5='dc8e9b260bbc886a11f6994d61bdbb46'
ARCHIVE_BASE_3_PART2_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-2.bin'
ARCHIVE_BASE_3_PART2_MD5='65edfab770311c2d7b840f374628b3da'
ARCHIVE_BASE_3_PART3_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-3.bin'
ARCHIVE_BASE_3_PART3_MD5='6627554f24158c5b0650c58f9a95395e'
ARCHIVE_BASE_3_PART4_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-4.bin'
ARCHIVE_BASE_3_PART4_MD5='4a30eeb23fc592041127e6615ce65bcf'
ARCHIVE_BASE_3_PART5_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-5.bin'
ARCHIVE_BASE_3_PART5_MD5='d249a8e0769661359b5d0467a7fb065b'
ARCHIVE_BASE_3_PART6_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-6.bin'
ARCHIVE_BASE_3_PART6_MD5='067db90eec212272951f04324b5883f4'
ARCHIVE_BASE_3_PART7_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.1.26_release_(77703)-7.bin'
ARCHIVE_BASE_3_PART7_MD5='ded5b6136305429254afcbc8f82db455'
ARCHIVE_BASE_3_SIZE='36237881'
ARCHIVE_BASE_3_VERSION='1.2.1.26-gog77703'

ARCHIVE_BASE_2_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506).exe'
ARCHIVE_BASE_2_MD5='28277602abf5481ed6ef4f58c5769d17'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='2ac1b1e04feacc93d734a4f8d409d549'
ARCHIVE_BASE_2_PART2_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='93d73c1420af6a771773c2be6ca52d16'
ARCHIVE_BASE_2_PART3_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-3.bin'
ARCHIVE_BASE_2_PART3_MD5='66435951336c29903538ac094f9b1e3d'
ARCHIVE_BASE_2_PART4_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-4.bin'
ARCHIVE_BASE_2_PART4_MD5='0256c674ed680b53b0afd83c58ad803d'
ARCHIVE_BASE_2_PART5_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-5.bin'
ARCHIVE_BASE_2_PART5_MD5='e9b1a1a80da7efa72dd98ed0c526c3c9'
ARCHIVE_BASE_2_PART6_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-6.bin'
ARCHIVE_BASE_2_PART6_MD5='f0ff7ff188125429a042d8a867c1811a'
ARCHIVE_BASE_2_PART7_NAME='setup_warhammer_40000_rogue_trader_windows_1.2.0.30_release_(74506)-7.bin'
ARCHIVE_BASE_2_PART7_MD5='4ec080aace5f303e6232464617807820'
ARCHIVE_BASE_2_SIZE='36502107'
ARCHIVE_BASE_2_VERSION='1.2.0.30-gog74506'

ARCHIVE_BASE_1_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681).exe'
ARCHIVE_BASE_1_MD5='4dacb9055aefe3aba3c0b9d69f1e7bbe'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='0992b9315611a76db8eb3ec16af01437'
ARCHIVE_BASE_1_PART2_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='8390e0d884653541edf3d489cf069aab'
ARCHIVE_BASE_1_PART3_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='0c8dcda2d01a9db6d48750213454ae46'
ARCHIVE_BASE_1_PART4_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='3857bdce47b4d311c1712eb8bf96e76d'
ARCHIVE_BASE_1_PART5_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-5.bin'
ARCHIVE_BASE_1_PART5_MD5='498fd37ca55b4082a5620e0892ef7e85'
ARCHIVE_BASE_1_PART6_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-6.bin'
ARCHIVE_BASE_1_PART6_MD5='c471fa41f77b39003e4cbcc5177636e2'
ARCHIVE_BASE_1_PART7_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-7.bin'
ARCHIVE_BASE_1_PART7_MD5='1d9ee45fd389fac0a34c9154f52068cf'
ARCHIVE_BASE_1_PART8_NAME='setup_warhammer_40000_rogue_trader_windows_1.1.58.505_release_(72681)-8.bin'
ARCHIVE_BASE_1_PART8_MD5='d36f057b4f24c2c4f0a6fe8341d258f4'
ARCHIVE_BASE_1_SIZE='37037198'
ARCHIVE_BASE_1_VERSION='1.1.58.505-gog72681'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/warhammer_40000_rogue_trader'

ARCHIVE_BASE_0_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073).exe'
ARCHIVE_BASE_0_MD5='adecb272b3abcafcdadcc661fd0fa16a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='750087be34c3e067e8f117cb75abf01b'
ARCHIVE_BASE_0_PART2_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='3ac14fc89b7770aaa91ace86b5d77fa0'
ARCHIVE_BASE_0_PART3_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='9d75b578aff00669425099c77d39c946'
ARCHIVE_BASE_0_PART4_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='4873f86957ee03b4c6d5a9b467300d43'
ARCHIVE_BASE_0_PART5_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='e63da686a49e98310fdc26a426d94fd2'
ARCHIVE_BASE_0_PART6_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='71395b57d293fabd7e2e930523cbf909'
ARCHIVE_BASE_0_PART7_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-7.bin'
ARCHIVE_BASE_0_PART7_MD5='e172106a1819cf3ff50229d01986ebbd'
ARCHIVE_BASE_0_PART8_NAME='setup_warhammer_40000_rogue_trader_windows_1.0.89.426_modded_(70073)-8.bin'
ARCHIVE_BASE_0_PART8_MD5='cd37f515a987c38b8bbef29c073f29f9'
ARCHIVE_BASE_0_SIZE='37034784'
ARCHIVE_BASE_0_VERSION='1.0.89.426-gog70073'

UNITY3D_NAME='wh40krt'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_BUNDLES_SCENES_1_FILES='
bundles/[a-p]*.scenes'
CONTENT_GAME_DATA_BUNDLES_SCENES_2_FILES='
bundles/*.scenes'
CONTENT_GAME_DATA_BUNDLES_UNIT_FILES='
bundles/*.unit'
CONTENT_GAME_DATA_BUNDLES_FILES='
bundles'
CONTENT_GAME0_DATA_FILES='
whrtmodificationtemplate-release.rar'

## While the game works with the default wined3d renderer,
## performances are much better when using dxvk instead.
WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Owlcat Games/Warhammer 40000 Rogue Trader'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_BUNDLES_SCENES_1
PKG_DATA_BUNDLES_SCENES_2
PKG_DATA_BUNDLES_UNIT
PKG_DATA_BUNDLES
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_BUNDLES'

PKG_DATA_BUNDLES_ID="${PKG_DATA_ID}-bundles"
PKG_DATA_BUNDLES_DESCRIPTION="$PKG_DATA_DESCRIPTION - bundles"
PKG_DATA_BUNDLES_DEPENDENCIES_SIBLINGS='
PKG_DATA_BUNDLES_SCENES_1
PKG_DATA_BUNDLES_SCENES_2
PKG_DATA_BUNDLES_UNIT'

PKG_DATA_BUNDLES_SCENES_ID="${PKG_DATA_BUNDLES_ID}-scenes"
PKG_DATA_BUNDLES_SCENES_1_ID="${PKG_DATA_BUNDLES_SCENES_ID}-1"
PKG_DATA_BUNDLES_SCENES_2_ID="${PKG_DATA_BUNDLES_SCENES_ID}-2"
PKG_DATA_BUNDLES_SCENES_DESCRIPTION="$PKG_DATA_BUNDLES_DESCRIPTION - scenes"
PKG_DATA_BUNDLES_SCENES_1_DESCRIPTION="$PKG_DATA_BUNDLES_SCENES_DESCRIPTION - 1"
PKG_DATA_BUNDLES_SCENES_2_DESCRIPTION="$PKG_DATA_BUNDLES_SCENES_DESCRIPTION - 2"

PKG_DATA_BUNDLES_UNIT_ID="${PKG_DATA_BUNDLES_ID}-unit"
PKG_DATA_BUNDLES_UNIT_DESCRIPTION="$PKG_DATA_BUNDLES_DESCRIPTION - unit"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Drop Steam-specific stuff.
	## --force is used because it is not included in all releases.
	rm --force --recursive 'steam workshop tool'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
