#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Star Wars: Galactic Battlegrounds
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='star-wars-galactic-battlegrounds'
GAME_NAME='Star Wars: Galactic Battlegrounds'

ARCHIVE_BASE_EN_0_NAME='setup_sw_galactic_battlegrounds_saga_2.0.0.4.exe'
ARCHIVE_BASE_EN_0_MD5='6af25835c5f240914cb04f7b4f741813'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='819518'
ARCHIVE_BASE_EN_0_VERSION='1.1-gog2.0.0.4'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/star_wars_galactic_battlegrounds_saga'

ARCHIVE_BASE_FR_0_NAME='setup_sw_galactic_battlegrounds_saga_french_2.0.0.4.exe'
ARCHIVE_BASE_FR_0_MD5='b30458033e825ad252e2d5b3dc8a7845'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='807572'
ARCHIVE_BASE_FR_0_VERSION='1.1-gog2.0.0.4'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/star_wars_galactic_battlegrounds_saga'

CONTENT_PATH_DEFAULT='app/game'
CONTENT_GAME_BIN_FILES='
libogg-0.dll
libvorbis-0.dll
libvorbisfile-3.dll
win32.dll
*.exe'
CONTENT_GAME_L10N_FILES='
history
taunt
sound/campaign
sound/scenario
campaign/media/1c2s6_end.mm
data/gamedata_x1.drs
scenario/default0.scx
data/list*.crx
data/genie*.dat
language*.dll
data/sounds.*drs'
CONTENT_GAME_DATA_FILES='
ai
campaign
data
extras
music
random
savegame
scenario
sound
*.avi'
CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/.."
CONTENT_DOC_DATA_FILES='
*.pdf'

USER_PERSISTENT_DIRECTORIES='
ai
campaign
random
savegame
scenario'
USER_PERSISTENT_FILES='
data/*.dat
player.nf*'

## Without a WINE virtual desktop, only a black screen is rendered.
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='battlegrounds.exe'

APP_ADDON_ID="${GAME_ID}-clone-wars"
APP_ADDON_NAME="$GAME_NAME - Clone Wars"
APP_ADDON_EXE='battlegrounds_x1.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Work around CD check

registry_dump_nocd_file='registry-dumps/no-cd-check.reg'
registry_dump_nocd_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\LucasArts Entertainment Company LLC\Star Wars Galactic Battlegrounds\1.0]
"CDPath"="C:"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_nocd_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_nocd_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Work around CD check.
	mkdir --parents "$(dirname "$registry_dump_nocd_file")"
	printf '%s' "$registry_dump_nocd_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_nocd_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
