#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Loop Hero
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='loop-hero'
GAME_NAME='Loop Hero'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

# Archives

## Full game (gog.com)

ARCHIVE_BASE_2_NAME='loop_hero_1_1054_55873.sh'
ARCHIVE_BASE_2_MD5='28197c34cb00548770b6878f95f69b89'
ARCHIVE_BASE_2_SIZE='220000'
ARCHIVE_BASE_2_VERSION='1.1054-gog55873'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/loop_hero'

ARCHIVE_BASE_1_NAME='loop_hero_1_105_50560.sh'
ARCHIVE_BASE_1_MD5='7ce0680d3dc6d945b0db0e3719addb21'
ARCHIVE_BASE_1_SIZE='160000'
ARCHIVE_BASE_1_VERSION='1.105-gog50560'

ARCHIVE_BASE_0_NAME='loop_hero_1_102_48813.sh'
ARCHIVE_BASE_0_MD5='fc860d85d59d02360d2d6aeb57cc8538'
ARCHIVE_BASE_0_SIZE='160000'
ARCHIVE_BASE_0_VERSION='1.102-gog48813'

## Demo (itch.io)

ARCHIVE_BASE_DEMO_0_NAME='LooPatHerO_demo_linux.zip'
ARCHIVE_BASE_DEMO_0_MD5='8698b73828300da7a519b026cd85d4aa'
ARCHIVE_BASE_DEMO_0_SIZE='23000'
ARCHIVE_BASE_DEMO_0_VERSION='1.0-itch1'
ARCHIVE_BASE_DEMO_0_URL='https://fourquarters.itch.io/loopathero-demo'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_PATH_DEFAULT_DEMO='.'
CONTENT_GAME_BIN_FILES='
Loop_Hero'
CONTENT_GAME_BIN_FILES_DEMO='
LooPatHerO'
CONTENT_GAME_DATA_FILES='
assets/*.dat
assets/*.ini
assets/*.png
assets/*.unx
assets/fonts
assets/local'

APP_MAIN_EXE='Loop_Hero'
## The demo crashes on launch.
##
## Thread 1 "LooPatHerO" received signal SIGFPE, Arithmetic exception.
##
## gdb trace:
## #0  0x000000000064bf80 in ?? ()
## #1  0x000000000064be69 in ?? ()
## #2  0x000000000064b80f in ?? ()
## #3  0x000000000072db2e in ?? ()
## #4  0x0000000000744415 in ?? ()
## #5  0x00000000005ad6da in ?? ()
## #6  0x00007ffff6efcc8a in __libc_start_call_main (main=main@entry=0x5acfc0, argc=argc@entry=1, argv=argv@entry=0x7fffffffde38) at ../sysdeps/nptl/libc_start_call_main.h:58
## #7  0x00007ffff6efcd45 in __libc_start_main_impl (main=0x5acfc0, argc=1, argv=0x7fffffffde38, init=<optimized out>, fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffffffde28) at ../csu/libc-start.c:360
## #8  0x00000000004098ea in ?? ()
##
## cf. https://forge.dotslashplay.it/vv221/games/-/issues/32
APP_MAIN_EXE_DEMO='LooPatHerO'
APP_MAIN_ICON='assets/icon.png'
## Work around broken support for non-US locales
APP_MAIN_PRERUN='
# Work around broken support for non-US locales
export LC_NUMERIC=C
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl.so.4
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libssl.so.1.1
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
