#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shogo: Mobile Armor Division
###

script_version=20241230.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='shogo-mobile-armor-division'
GAME_NAME='Shogo: Mobile Armor Division'

ARCHIVE_BASE_EN_0_NAME='setup_shogo_-_mobile_armor_division_2.2.14_(19498).exe'
ARCHIVE_BASE_EN_0_MD5='371cd8bbd32355f78ee7f52e4d34e94a'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_VERSION='2.2.14-gog19498'
ARCHIVE_BASE_EN_0_SIZE='481454'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/shogo_mobile_armor_division'

ARCHIVE_BASE_FR_0_NAME='setup_shogo_-_mobile_armor_division_2.2.14_(french)_(19498).exe'
ARCHIVE_BASE_FR_0_MD5='ca3029d8c6efe0d273a174b73d95e920'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_VERSION='2.2.14-gog19498'
ARCHIVE_BASE_FR_0_SIZE='487166'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/shogo_mobile_armor_division'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
am18.dll
cdaudio.dll
de_msg.dll
ima.dll
imrt1625.dll
imrt25.dll
imrt3225.dll
imusic25.dll
launch.dll
mss16.dll
mss32.dll
msynth25.dll
server.dll
smackw32.dll
softsnd.dll
client.exe
ct.exe
shogo.exe
shogosrv.exe
start.exe
ct.ini
*.m3d
*.pcx
*.ren
*.tsk'
CONTENT_GAME0_BIN_PATH='__support/app'
CONTENT_GAME0_BIN_FILES='
*.cfg'
CONTENT_GAME_DATA_FILES='
custom
joystick
music
save
*.msk
*.rez'
CONTENT_DOC_DATA_FILES='
*.cnt
*.hlp
*.gid
*.pdf
readme.txt
shogosrv.txt'

USER_PERSISTENT_DIRECTORIES='
save'
USER_PERSISTENT_FILES='
*.cfg'

## Without a WINE virtual desktop,
## mouselook can lead to erratic camera behaviour.
## TODO: Check if it is still happening with current WINE builds
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='shogo.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_ID="$GAME_ID"
PKG_BIN_ID_EN="${PKG_BIN_ID}-en"
PKG_BIN_ID_FR="${PKG_BIN_ID}-fr"
PKG_BIN_PROVIDES="
$GAME_ID"
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DESCRIPTION_EN='English version'
PKG_BIN_DESCRIPTION_FR='French version'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_EN="${PKG_DATA_ID}-en"
PKG_DATA_ID_FR="${PKG_DATA_ID}-fr"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DESCRIPTION_EN="$PKG_DATA_DESCRIPTION - English version"
PKG_DATA_DESCRIPTION_FR="$PKG_DATA_DESCRIPTION - French version"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
