#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sunless Skies
###

script_version=20241126.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='sunless-skies'
GAME_NAME='Sunless Skies'

ARCHIVE_BASE_2_NAME='sunless_skies_sovereign_edition_2_0_5_6e8c8ff_74416.sh'
ARCHIVE_BASE_2_MD5='3405edfa656734537b13b253748fc556'
ARCHIVE_BASE_2_SIZE='3276986'
ARCHIVE_BASE_2_VERSION='2.0.5-gog74416'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/sunless_skies'

ARCHIVE_BASE_1_NAME='sunless_skies_sovereign_edition_2_0_4_fcf0af7a_52215.sh'
ARCHIVE_BASE_1_MD5='0d92a8c235d610e6ec560c25179c529a'
ARCHIVE_BASE_1_SIZE='3300000'
ARCHIVE_BASE_1_VERSION='2.0.4-gog52215'

ARCHIVE_BASE_0_NAME='sunless_skies_sovereign_edition_2_0_2_9bcd3d8c_48199.sh'
ARCHIVE_BASE_0_MD5='f99de4c6a893be6f7fcb8170c94478f4'
ARCHIVE_BASE_0_SIZE='3300000'
ARCHIVE_BASE_0_VERSION='2.0.2-gog48199'

UNITY3D_NAME='Sunless Skies'

CONTENT_PATH_DEFAULT='data/noarch/game'

## Without write access to the "dlc" directory, the game fails to load properly.
USER_PERSISTENT_DIRECTORIES='
dlc'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Drop empty "Plugins" directory.
	rmdir "$(unity3d_name)_Data/Plugins"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
