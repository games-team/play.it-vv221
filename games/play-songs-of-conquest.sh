#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Songs of Conquest
###

script_version=20250211.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='songs-of-conquest'
GAME_NAME='Songs of Conquest'

ARCHIVE_BASE_5_NAME='setup_songs_of_conquest_1.4.13_2d9731f566_3063_(79474).exe'
ARCHIVE_BASE_5_MD5='ff47f9178c22a9804a4e513185ce6122'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_PART1_NAME='setup_songs_of_conquest_1.4.13_2d9731f566_3063_(79474)-1.bin'
ARCHIVE_BASE_5_PART1_MD5='d080b22508bcafe99f81b63dca1471db'
ARCHIVE_BASE_5_SIZE='2238908'
ARCHIVE_BASE_5_VERSION='1.4.13-gog79474'
ARCHIVE_BASE_5_URL='https://www.gog.com/game/songs_of_conquest'

ARCHIVE_BASE_4_NAME='setup_songs_of_conquest_1.4.12_a5624b455d_3033_(79217).exe'
ARCHIVE_BASE_4_MD5='d5e6bbfb4a7a6e27696f22f2915b1269'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_PART1_NAME='setup_songs_of_conquest_1.4.12_a5624b455d_3033_(79217)-1.bin'
ARCHIVE_BASE_4_PART1_MD5='8d25598cf45e67b54faaa76708e1a642'
ARCHIVE_BASE_4_SIZE='2240483'
ARCHIVE_BASE_4_VERSION='1.4.12-gog79217'

ARCHIVE_BASE_3_NAME='setup_songs_of_conquest_1.4.11_a56e2e9ed1_3013_(78972).exe'
ARCHIVE_BASE_3_MD5='af79080c297f067b66a45625abd50104'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_PART1_NAME='setup_songs_of_conquest_1.4.11_a56e2e9ed1_3013_(78972)-1.bin'
ARCHIVE_BASE_3_PART1_MD5='7d35955cb78e863fa90db91fb690859b'
ARCHIVE_BASE_3_SIZE='2238284'
ARCHIVE_BASE_3_VERSION='1.4.11-gog78972'

ARCHIVE_BASE_2_NAME='setup_songs_of_conquest_1.4.10_967940f7ee_3002_(78614).exe'
ARCHIVE_BASE_2_MD5='3f7c811f0a316df65446cbce9f294ba6'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_songs_of_conquest_1.4.10_967940f7ee_3002_(78614)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='87bffffbb31fec05c4deba26a40ce42a'
ARCHIVE_BASE_2_SIZE='2239689'
ARCHIVE_BASE_2_VERSION='1.4.10-gog78614'

ARCHIVE_BASE_1_NAME='setup_songs_of_conquest_1.4.8_b1dfeaadfa_2989_(78547).exe'
ARCHIVE_BASE_1_MD5='f3d225f934b63608e707bbd9b9821c78'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_songs_of_conquest_1.4.8_b1dfeaadfa_2989_(78547)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='ecb65717e71b6a8a2f28911512a1f0eb'
ARCHIVE_BASE_1_SIZE='2237801'
ARCHIVE_BASE_1_VERSION='1.4.8-gog78547'

ARCHIVE_BASE_0_NAME='setup_songs_of_conquest_1.3.2_ab33a56fa6_2566_(76712).exe'
ARCHIVE_BASE_0_MD5='ea01d32299258ee757fc349e5d4f9b1e'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_songs_of_conquest_1.3.2_ab33a56fa6_2566_(76712)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='e1a0a815f1f9500d7b5cfdfdf65223f1'
ARCHIVE_BASE_0_SIZE='2544156'
ARCHIVE_BASE_0_VERSION='1.3.2-gog76712'

UNITY3D_NAME='songsofconquest'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Lavapotion/SongsOfConquest'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
