#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2021 Mopi
set -o errexit

###
# Zachtronics games:
# - SpaceChem
# - Opus Magnum
# - Exapunks
# - Eliza
# - Molek-Syntez
# - Möbius Front '83
# - Last Call BBS
# - Zachtronics Solitaire Collection
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_SPACECHEM='spacechem'
GAME_NAME_SPACECHEM='SpaceChem'

GAME_ID_OPUSMAGNUM='opus-magnum'
GAME_NAME_OPUSMAGNUM='Opus Magnum'

GAME_ID_EXAPUNKS='exapunks'
GAME_NAME_EXAPUNKS='Exapunks'

GAME_ID_ELIZA='eliza'
GAME_NAME_ELIZA='Eliza'

GAME_ID_MOLEKSYNTEZ='molek-syntez'
GAME_NAME_MOLEKSYNTEZ='Molek-Syntez'

GAME_ID_MOBIUS='mobius-front-83'
GAME_NAME_MOBIUS="Möbius Front '83"

GAME_ID_LASTCALL='last-call-bbs'
GAME_NAME_LASTCALL='Last Call BBS'

GAME_ID_SOLITAIRE='zachtronics-solitaire-collection'
GAME_NAME_SOLITAIRE='The Zachtronics Solitaire Collection'

# Archives

## SpaceChem

ARCHIVE_BASE_SPACECHEM_0_NAME='spacechem_1016_40617.sh'
ARCHIVE_BASE_SPACECHEM_0_MD5='aec2809166dbc60a7cfa3f7c22db9a07'
ARCHIVE_BASE_SPACECHEM_0_SIZE='310000'
ARCHIVE_BASE_SPACECHEM_0_VERSION='1016-gog40617'
ARCHIVE_BASE_SPACECHEM_0_URL='https://www.gog.com/game/spacechem'

## Opus Magnum

ARCHIVE_BASE_OPUSMAGNUM_2_NAME='opus_magnum_26_03_2023_63471.sh'
ARCHIVE_BASE_OPUSMAGNUM_2_MD5='0313ec8451cac5445818f143a7c2d79d'
ARCHIVE_BASE_OPUSMAGNUM_2_SIZE='520000'
ARCHIVE_BASE_OPUSMAGNUM_2_VERSION='2023.03.26-gog63471'
ARCHIVE_BASE_OPUSMAGNUM_2_URL='https://www.gog.com/game/opus_magnum'

ARCHIVE_BASE_OPUSMAGNUM_1_NAME='opus_magnum_11_14_2020_43144.sh'
ARCHIVE_BASE_OPUSMAGNUM_1_MD5='2defb1198c5a5778eb9ac20ff201f086'
ARCHIVE_BASE_OPUSMAGNUM_1_SIZE='520000'
ARCHIVE_BASE_OPUSMAGNUM_1_VERSION='2020.11.14-gog43144'

ARCHIVE_BASE_OPUSMAGNUM_0_NAME='opus_magnum_en_17_08_2018_update_23270.sh'
ARCHIVE_BASE_OPUSMAGNUM_0_MD5='dbe5137d4b7e2edd21f4117a80756872'
ARCHIVE_BASE_OPUSMAGNUM_0_SIZE='460000'
ARCHIVE_BASE_OPUSMAGNUM_0_VERSION='2018.08.17-gog23270'

## Exapunks

ARCHIVE_BASE_EXAPUNKS_1_NAME='exapunks_gog_26_03_2023_63466.sh'
ARCHIVE_BASE_EXAPUNKS_1_MD5='f2e0fda51c0ebc94345379ffc56d188f'
ARCHIVE_BASE_EXAPUNKS_1_SIZE='730000'
ARCHIVE_BASE_EXAPUNKS_1_VERSION='2023.03.26-gog63466'
ARCHIVE_BASE_EXAPUNKS_1_URL='https://www.gog.com/game/exapunks'

ARCHIVE_BASE_EXAPUNKS_0_NAME='exapunks_gog_11_14_2020_43140.sh'
ARCHIVE_BASE_EXAPUNKS_0_MD5='46222cde2bdac46c80b3c64a976c9f78'
ARCHIVE_BASE_EXAPUNKS_0_SIZE='730000'
ARCHIVE_BASE_EXAPUNKS_0_VERSION='2020.11.14-gog43140'

## Eliza

ARCHIVE_BASE_ELIZA_2_NAME='eliza_march_2023_update_63854.sh'
ARCHIVE_BASE_ELIZA_2_MD5='7ea156a0a553e1f44fbf1f4aee5a3aac'
ARCHIVE_BASE_ELIZA_2_SIZE='1900000'
ARCHIVE_BASE_ELIZA_2_VERSION='2023.03-gog63854'
ARCHIVE_BASE_ELIZA_2_URL='https://www.gog.com/game/eliza'

ARCHIVE_BASE_ELIZA_1_NAME='eliza_11_14_2020_43139.sh'
ARCHIVE_BASE_ELIZA_1_MD5='0508915315f0c2b5b102bfd80cd252ef'
ARCHIVE_BASE_ELIZA_1_SIZE='1900000'
ARCHIVE_BASE_ELIZA_1_VERSION='2020.11.14-gog43139'

ARCHIVE_BASE_ELIZA_0_NAME='eliza_03_09_2019_32218.sh'
ARCHIVE_BASE_ELIZA_0_MD5='7751b93498ad637a79f84cbbc7370f0a'
ARCHIVE_BASE_ELIZA_0_SIZE='1100000'
ARCHIVE_BASE_ELIZA_0_VERSION='2019.03.09-gog32218'

## Molek-Syntez

ARCHIVE_BASE_MOLEKSYNTEZ_1_NAME='molek_syntez_26_03_2023_63470.sh'
ARCHIVE_BASE_MOLEKSYNTEZ_1_MD5='bd13a99c3365d22cabb68d88c0216b2d'
ARCHIVE_BASE_MOLEKSYNTEZ_1_SIZE='200000'
ARCHIVE_BASE_MOLEKSYNTEZ_1_VERSION='2023.03.26-gog63470'
ARCHIVE_BASE_MOLEKSYNTEZ_1_URL='https://www.gog.com/game/moleksyntez'

ARCHIVE_BASE_MOLEKSYNTEZ_0_NAME='molek_syntez_11_14_2020_43139.sh'
ARCHIVE_BASE_MOLEKSYNTEZ_0_MD5='95735bb467813bbcd10c049bf1861c0c'
ARCHIVE_BASE_MOLEKSYNTEZ_0_SIZE='200000'
ARCHIVE_BASE_MOLEKSYNTEZ_0_VERSION='2020.11.14-gog43139'
ARCHIVE_BASE_MOLEKSYNTEZ_0_URL='https://www.gog.com/game/moleksyntez'

## Möbius Front '83

ARCHIVE_BASE_MOBIUS_1_NAME='m_bius_front_83_march_2023_update_63923.sh'
ARCHIVE_BASE_MOBIUS_1_MD5='2be4eb51833c3d72b5dd7f0857e3fbf4'
ARCHIVE_BASE_MOBIUS_1_SIZE='1700000'
ARCHIVE_BASE_MOBIUS_1_VERSION='2023.03-gog63923'
ARCHIVE_BASE_MOBIUS_1_URL='https://www.gog.com/game/mobius_front_83'

ARCHIVE_BASE_MOBIUS_0_NAME='m_bius_front_83_03_01_2021_45297.sh'
ARCHIVE_BASE_MOBIUS_0_MD5='e66950193c5308abd7b4fe2bec610d37'
ARCHIVE_BASE_MOBIUS_0_SIZE='1700000'
ARCHIVE_BASE_MOBIUS_0_VERSION='2021.03.01-gog45297'

## Last Call BBS

ARCHIVE_BASE_LASTCALL_0_NAME='last_call_bbs_1_0_57778.sh'
ARCHIVE_BASE_LASTCALL_0_MD5='3dc1e94645cc3ea871cb48b930056699'
ARCHIVE_BASE_LASTCALL_0_SIZE='230000'
ARCHIVE_BASE_LASTCALL_0_VERSION='1.0-gog57778'
ARCHIVE_BASE_LASTCALL_0_URL='https://www.gog.com/game/last_call_bbs'

## Zachtronics Solitaire Collection

ARCHIVE_BASE_SOLITAIRE_3_NAME='the_zachtronics_solitaire_collection_march_2023_update_63924.sh'
ARCHIVE_BASE_SOLITAIRE_3_MD5='8965f467c0243ead5cf51468771d59fd'
ARCHIVE_BASE_SOLITAIRE_3_SIZE='640000'
ARCHIVE_BASE_SOLITAIRE_3_VERSION='2023.03-gog63924'
ARCHIVE_BASE_SOLITAIRE_3_URL='https://www.gog.com/game/the_zachtronics_solitaire_collection'

ARCHIVE_BASE_SOLITAIRE_2_NAME='the_zachtronics_solitaire_collection_1_2_59266.sh'
ARCHIVE_BASE_SOLITAIRE_2_MD5='8202afdd9c32244da3ea632f86597766'
ARCHIVE_BASE_SOLITAIRE_2_SIZE='640000'
ARCHIVE_BASE_SOLITAIRE_2_VERSION='1.2-gog59266'

ARCHIVE_BASE_SOLITAIRE_1_NAME='the_zachtronics_solitaire_collection_1_1_58740.sh'
ARCHIVE_BASE_SOLITAIRE_1_MD5='33190fa2731ce09d04e028585061c3e4'
ARCHIVE_BASE_SOLITAIRE_1_SIZE='640000'
ARCHIVE_BASE_SOLITAIRE_1_VERSION='1.1-gog58740'

ARCHIVE_BASE_SOLITAIRE_0_NAME='the_zachtronics_solitaire_collection_1_0_58571.sh'
ARCHIVE_BASE_SOLITAIRE_0_MD5='1b33a67b53c34150b8795c9680874bba'
ARCHIVE_BASE_SOLITAIRE_0_SIZE='640000'
ARCHIVE_BASE_SOLITAIRE_0_VERSION='1.0-gog58571'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES_COMMON='
Ionic.Zip.Reduced.dll
MoonSharp.Interpreter.dll
System.Speech.dll
Content
PackedContent
monoconfig
monomachineconfig'
## TODO: Maybe Spacechem could be moved to a dedicated game script,
##       as it is the only game requiring specific code
CONTENT_GAME_MAIN_FILES_SPACECHEM="$CONTENT_GAME_MAIN_FILES_COMMON
SpaceChem.exe
SpaceChem.exe.config
Ionic.Zip.dll
Newtonsoft.Json.dll
System.Data.SQLite.dll
rgb2theora
template.locals
template.user
fonts
images
lang
music
sounds
text
*.json"
CONTENT_GAME_MAIN_FILES_OPUSMAGNUM="$CONTENT_GAME_MAIN_FILES_COMMON
Lightning.exe
Lightning.exe.config"
CONTENT_GAME_MAIN_FILES_EXAPUNKS="$CONTENT_GAME_MAIN_FILES_COMMON
EXAPUNKS.exe
EXAPUNKS.exe.config"
CONTENT_GAME_MAIN_FILES_ELIZA="$CONTENT_GAME_MAIN_FILES_COMMON
Eliza.exe
Eliza.exe.config"
CONTENT_GAME_MAIN_FILES_MOLEKSYNTEZ="$CONTENT_GAME_MAIN_FILES_COMMON
MOLEK-SYNTEZ.exe
MOLEK-SYNTEZ.exe.config"
CONTENT_GAME_MAIN_FILES_MOBIUS="$CONTENT_GAME_MAIN_FILES_COMMON
MobiusFront83.exe
MobiusFront83.exe.config"
CONTENT_GAME_MAIN_FILES_LASTCALL="$CONTENT_GAME_MAIN_FILES_COMMON
LastCallBBS.exe
LastCallBBS.exe.config
System.Speech.dll"
CONTENT_GAME_MAIN_FILES_SOLITAIRE="$CONTENT_GAME_MAIN_FILES_COMMON
TheZachtronicsSolitaireCollection.exe
TheZachtronicsSolitaireCollection.exe.config"
CONTENT_DOC_MAIN_FILES='
LICENSE.txt'

APP_MAIN_EXE_SPACECHEM='SpaceChem.exe'
APP_MAIN_EXE_OPUSMAGNUM='Lightning.exe'
APP_MAIN_EXE_EXAPUNKS='EXAPUNKS.exe'
APP_MAIN_EXE_ELIZA='Eliza.exe'
APP_MAIN_EXE_MOLEKSYNTEZ='MOLEK-SYNTEZ.exe'
APP_MAIN_EXE_MOBIUS='MobiusFront83.exe'
APP_MAIN_EXE_LASTCALL='LastCallBBS.exe'
APP_MAIN_EXE_SOLITAIRE='TheZachtronicsSolitaireCollection.exe'
APP_MAIN_ICON='Content/icon.png'
APP_MAIN_ICON_SPACECHEM='SpaceChem.exe'

PKG_MAIN_DEPENDENCIES_LIBRARIES='
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_mixer-2.0.so.0
libvorbisfile.so.3'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
I18N.dll
I18N.West.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.ComponentModel.DataAnnotations.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Transactions.dll
System.Web.dll
System.Web.Extensions.dll
System.Web.Http.dll
System.Web.Services.dll
System.Xml.dll
System.Xml.Linq.dll'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# SpaceChem - Set required execution permission on the rgb2theora binary
	# This binary is used to generate .ogv video clips from puzzle solutions
	case "$(current_archive)" in
		('ARCHIVE_BASE_SPACECHEM_'*)
			chmod 755 'rgb2theora'
		;;
	esac
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
