#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2019 BetaRays
set -o errexit

###
# Star Wars: Knights of the Old Republic series:
# - Star Wars: Knights of the Old Republic
# - Star Wars: Knights of the Old Republic 2
###

script_version=20250102.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_KOTOR1='star-wars-knights-of-the-old-republic-1'
GAME_NAME_KOTOR1='Star Wars: Knights of the Old Republic'

GAME_ID_KOTOR2='star-wars-knights-of-the-old-republic-2'
GAME_NAME_KOTOR2='Star Wars: Knights of the Old Republic Ⅱ - The Sith Lords'

# Archives

## Star Wars: Knights of the Old Republic

ARCHIVE_BASE_KOTOR1_EN_0_NAME='setup_star_wars_-_knights_of_the_old_republic_1.03_(29871).exe'
ARCHIVE_BASE_KOTOR1_EN_0_MD5='6ea3df208a9cb3c8ca54eac2d0e2e4a9'
ARCHIVE_BASE_KOTOR1_EN_0_TYPE='innosetup'
ARCHIVE_BASE_KOTOR1_EN_0_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_1.03_(29871)-1.bin'
ARCHIVE_BASE_KOTOR1_EN_0_PART1_MD5='51d4eea9a76df9b99fba114c40005cfe'
ARCHIVE_BASE_KOTOR1_EN_0_SIZE='3800000'
ARCHIVE_BASE_KOTOR1_EN_0_VERSION='1.03-gog29871'
ARCHIVE_BASE_KOTOR1_EN_0_URL='https://www.gog.com/game/star_wars_knights_of_the_old_republic'

ARCHIVE_BASE_KOTOR1_FR_0_NAME='setup_star_wars_-_knights_of_the_old_republic_1.03_(french)_(29871).exe'
ARCHIVE_BASE_KOTOR1_FR_0_MD5='8db7abdf7dc05e8f65ea2599c9486b8d'
ARCHIVE_BASE_KOTOR1_FR_0_TYPE='innosetup'
ARCHIVE_BASE_KOTOR1_FR_0_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_1.03_(french)_(29871)-1.bin'
ARCHIVE_BASE_KOTOR1_FR_0_PART1_MD5='010bce761719c5e4570e136092a075fe'
ARCHIVE_BASE_KOTOR1_FR_0_SIZE='3800000'
ARCHIVE_BASE_KOTOR1_FR_0_VERSION='1.03-gog29871'
ARCHIVE_BASE_KOTOR1_FR_0_URL='https://www.gog.com/game/star_wars_knights_of_the_old_republic'

ARCHIVE_BASE_KOTOR1_DE_0_NAME='setup_star_wars_-_knights_of_the_old_republic_1.03_(german)_(29871).exe'
ARCHIVE_BASE_KOTOR1_DE_0_MD5='ba963a9d4e61aabd7f654437b1f6a69e'
ARCHIVE_BASE_KOTOR1_DE_0_TYPE='innosetup'
ARCHIVE_BASE_KOTOR1_DE_0_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_1.03_(german)_(29871)-1.bin'
ARCHIVE_BASE_KOTOR1_DE_0_PART1_MD5='ac11ebefb89767bc38d3521ba048ec31'
ARCHIVE_BASE_KOTOR1_DE_0_SIZE='3900000'
ARCHIVE_BASE_KOTOR1_DE_0_VERSION='1.03-gog29871'
ARCHIVE_BASE_KOTOR1_DE_0_URL='https://www.gog.com/game/star_wars_knights_of_the_old_republic'

## Star Wars: Knights of the Old Republic 2

ARCHIVE_BASE_KOTOR2_EN_1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_update_(56101).exe'
ARCHIVE_BASE_KOTOR2_EN_1_MD5='8ebaa4234c475ef66f8fcf7aa8835d80'
ARCHIVE_BASE_KOTOR2_EN_1_TYPE='innosetup'
ARCHIVE_BASE_KOTOR2_EN_1_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_update_(56101)-1.bin'
ARCHIVE_BASE_KOTOR2_EN_1_PART1_MD5='9ba032cb1cdd08f07c0968ed7c4efa50'
ARCHIVE_BASE_KOTOR2_EN_1_SIZE='4900000'
ARCHIVE_BASE_KOTOR2_EN_1_VERSION='1.0b-gog56101'
ARCHIVE_BASE_KOTOR2_EN_1_URL='https://www.gog.com/game/star_wars_knights_of_the_old_republic_ii_the_sith_lords'

ARCHIVE_BASE_KOTOR2_FR_1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_update_(french)_(56101).exe'
ARCHIVE_BASE_KOTOR2_FR_1_MD5='17f853ecf017b408bee397a4f127dc37'
ARCHIVE_BASE_KOTOR2_FR_1_TYPE='innosetup'
ARCHIVE_BASE_KOTOR2_FR_1_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_update_(french)_(56101)-1.bin'
ARCHIVE_BASE_KOTOR2_FR_1_PART1_MD5='7958318830a662b45fa3b79456713b40'
ARCHIVE_BASE_KOTOR2_FR_1_SIZE='4800000'
ARCHIVE_BASE_KOTOR2_FR_1_VERSION='1.0b-gog56101'
ARCHIVE_BASE_KOTOR2_FR_1_URL='https://www.gog.com/game/star_wars_knights_of_the_old_republic_ii_the_sith_lords'

ARCHIVE_BASE_KOTOR2_DE_1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_update_(german)_(56101).exe'
ARCHIVE_BASE_KOTOR2_DE_1_MD5='f67334a28b571238993d85ab9fdba3ef'
ARCHIVE_BASE_KOTOR2_DE_1_TYPE='innosetup'
ARCHIVE_BASE_KOTOR2_DE_1_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_update_(german)_(56101)-1.bin'
ARCHIVE_BASE_KOTOR2_DE_1_PART1_MD5='64a734389a26611b8bec1d597c6efb52'
ARCHIVE_BASE_KOTOR2_DE_1_SIZE='4800000'
ARCHIVE_BASE_KOTOR2_DE_1_VERSION='1.0b-gog56101'
ARCHIVE_BASE_KOTOR2_DE_1_URL='https://www.gog.com/game/star_wars_knights_of_the_old_republic_ii_the_sith_lords'

ARCHIVE_BASE_KOTOR2_EN_0_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_(29869).exe'
ARCHIVE_BASE_KOTOR2_EN_0_MD5='7f7a2e14e5ebadf14c0cdbb1ee807521'
ARCHIVE_BASE_KOTOR2_EN_0_TYPE='innosetup'
ARCHIVE_BASE_KOTOR2_EN_0_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_(29869)-1.bin'
ARCHIVE_BASE_KOTOR2_EN_0_PART1_MD5='8092cf5da5fa165f88d67e172c610c5e'
ARCHIVE_BASE_KOTOR2_EN_0_SIZE='4700000'
ARCHIVE_BASE_KOTOR2_EN_0_VERSION='1.0b-gog29869'

ARCHIVE_BASE_KOTOR2_FR_0_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_(french)_(29869).exe'
ARCHIVE_BASE_KOTOR2_FR_0_MD5='a16a80f377111ec4152e5d1b196f64f5'
ARCHIVE_BASE_KOTOR2_FR_0_TYPE='innosetup'
ARCHIVE_BASE_KOTOR2_FR_0_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_(french)_(29869)-1.bin'
ARCHIVE_BASE_KOTOR2_FR_0_PART1_MD5='e68c85d7f0ad6212c9841276526aa5d3'
ARCHIVE_BASE_KOTOR2_FR_0_SIZE='4600000'
ARCHIVE_BASE_KOTOR2_FR_0_VERSION='1.0b-gog29869'

ARCHIVE_BASE_KOTOR2_DE_0_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_(german)_(29869).exe'
ARCHIVE_BASE_KOTOR2_DE_0_MD5='fa7e9e961dfe14d730c4ec571ebe6f0e'
ARCHIVE_BASE_KOTOR2_DE_0_TYPE='innosetup'
ARCHIVE_BASE_KOTOR2_DE_0_PART1_NAME='setup_star_wars_-_knights_of_the_old_republic_ii_1.0b_(german)_(29869)-1.bin'
ARCHIVE_BASE_KOTOR2_DE_0_PART1_MD5='26ebb49d133a564538be2d3a5b35c3d2'
ARCHIVE_BASE_KOTOR2_DE_0_SIZE='4600000'
ARCHIVE_BASE_KOTOR2_DE_0_VERSION='1.0b-gog29869'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
miles
utils
binkw32.dll
mss32.dll
mss32midi.dll
patchw32.dll
swconfig.exe
swkotor.exe
swkotor2.exe'
CONTENT_GAME_L10N_FILES='
patch.erf
lips
override
streamwaves
streamsounds
streamvoice
movies/01a.bik
movies/02.bik
movies/09.bik
movies/31a.bik
movies/50.bik
movies/56b.bik
movies/kre*
movies/leclogo.bik
movies/legal.bik
movies/permov01.bik
movies/scn*
movies/trailer.bik
*.tlk'
CONTENT_GAME_DATA_FILES='
chitin.key
data
modules
rims
streammusic
texturepacks
movies'
CONTENT_GAME0_DATA_PATH='__support/app'
CONTENT_GAME0_DATA_FILES='
*.ini'
CONTENT_DOC_L10N_FILES='
docs
*.pdf
*.txt'

# Applications

USER_PERSISTENT_DIRECTORIES='
saves'
USER_PERSISTENT_FILES='
*.ini'

APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='swconfig.exe'

## Star Wars: Knights of the Old Republic

APP_MAIN_EXE_KOTOR1='swkotor.exe'

APP_CONFIG_ID_KOTOR1="${GAME_ID_KOTOR1}-config"
APP_CONFIG_NAME_KOTOR1="$GAME_NAME_KOTOR1 - Configuration"

## Star Wars: Knights of the Old Republic 2

APP_MAIN_EXE_KOTOR2='swkotor2.exe'

APP_CONFIG_ID_KOTOR2="${GAME_ID_KOTOR2}-config"
APP_CONFIG_NAME_KOTOR2="$GAME_NAME_KOTOR2 - configuration"

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'
PKG_L10N_DESCRIPTION_DE='German localization'

PKG_DATA_DESCRIPTION='data'

## Star Wars: Knights of the Old Republic

PKG_L10N_BASE_ID_KOTOR1="${GAME_ID_KOTOR1}-l10n"
PKG_L10N_ID_KOTOR1_EN="${PKG_L10N_BASE_ID_KOTOR1}-en"
PKG_L10N_ID_KOTOR1_FR="${PKG_L10N_BASE_ID_KOTOR1}-fr"
PKG_L10N_ID_KOTOR1_DE="${PKG_L10N_BASE_ID_KOTOR1}-de"
PKG_L10N_PROVIDES_KOTOR1="
$PKG_L10N_BASE_ID_KOTOR1"
PKG_L10N_DESCRIPTION_KOTOR1_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_KOTOR1_FR="$PKG_L10N_DESCRIPTION_FR"
PKG_L10N_DESCRIPTION_KOTOR1_DE="$PKG_L10N_DESCRIPTION_DE"

PKG_DATA_ID_KOTOR1="${GAME_ID_KOTOR1}-data"

## Star Wars: Knights of the Old Republic 2

PKG_L10N_BASE_ID_KOTOR2="${GAME_ID_KOTOR2}-l10n"
PKG_L10N_ID_KOTOR2_EN="${PKG_L10N_BASE_ID_KOTOR2}-en"
PKG_L10N_ID_KOTOR2_FR="${PKG_L10N_BASE_ID_KOTOR2}-fr"
PKG_L10N_ID_KOTOR2_DE="${PKG_L10N_BASE_ID_KOTOR2}-de"
PKG_L10N_PROVIDES_KOTOR2="
$PKG_L10N_BASE_ID_KOTOR2"
PKG_L10N_DESCRIPTION_KOTOR2_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_KOTOR2_FR="$PKG_L10N_DESCRIPTION_FR"
PKG_L10N_DESCRIPTION_KOTOR2_DE="$PKG_L10N_DESCRIPTION_DE"

PKG_DATA_ID_KOTOR2="${GAME_ID_KOTOR2}-data"

# Ensure ability fo fully control the camera with the mouse

registry_dump_mouse_grab_file='registry-dumps/mouse-grab.reg'
registry_dump_mouse_grab_content='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Wine\X11 Driver]
"GrabFullscreen"="Y"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_mouse_grab_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_mouse_grab_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Ensure ability fo fully control the camera with the mouse
	mkdir --parents "$(dirname "$registry_dump_mouse_grab_file")"
	printf '%s' "$registry_dump_mouse_grab_content" |
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_mouse_grab_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
