#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shadow Tactics:
# - Shadow Tactics: Blades of the Shogun
# - Aiko's Choice
###

script_version=20241227.5

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_BLADES='shadow-tactics'
GAME_NAME_BLADES='Shadow Tactics: Blades of the Shogun'

GAME_ID_BLADES_DEMO="${GAME_ID_BLADES}-demo"
GAME_NAME_BLADES_DEMO="$GAME_NAME_BLADES (demo)"

GAME_ID_AIKO='shadow-tactics-aikos-choice'
GAME_NAME_AIKO='Shadow Tactics: Blades of the Shogun - Aikoʼs Choice'

ARCHIVE_BASE_BLADES_1_NAME='shadow_tactics_blades_of_the_shogun_en_2_2_10_f_21297.sh'
ARCHIVE_BASE_BLADES_1_MD5='e7772e7a5f4fee760e9311a9a899dbb3'
ARCHIVE_BASE_BLADES_1_SIZE='7642154'
ARCHIVE_BASE_BLADES_1_VERSION='2.2.10.f-gog21297'
ARCHIVE_BASE_BLADES_1_URL='https://www.gog.com/game/shadow_tactics_blades_of_the_shogun'

ARCHIVE_BASE_BLADES_0_NAME='shadow_tactics_blades_of_the_shogun_en_1_4_4_f_14723.sh'
ARCHIVE_BASE_BLADES_0_MD5='93faa090d5bcaa22f0faabd1e32c5909'
ARCHIVE_BASE_BLADES_0_SIZE='9600000'
ARCHIVE_BASE_BLADES_0_VERSION='1.4.4.f-gog14723'

ARCHIVE_BASE_BLADES_DEMO_0_NAME='shadow_tactics_blades_of_the_shogun_demo_en_1_4_4_d_14915.sh'
ARCHIVE_BASE_BLADES_DEMO_0_MD5='9238093a25ce9c911d9f19789ca18878'
ARCHIVE_BASE_BLADES_DEMO_0_SIZE='2764963'
ARCHIVE_BASE_BLADES_DEMO_0_VERSION='1.4.4.d-gog14915'
ARCHIVE_BASE_BLADES_DEMO_0_URL='https://www.gog.com/game/shadow_tactics_demo'

ARCHIVE_BASE_AIKO_0_NAME='setup_shadow_tactics_blades_of_the_shogun_-_aikos_choice_3.2.25.f.r4769a_(51855).exe'
ARCHIVE_BASE_AIKO_0_MD5='5e0cfcc243f249fded7906fea04f6544'
ARCHIVE_BASE_AIKO_0_TYPE='innosetup'
ARCHIVE_BASE_AIKO_0_PART1_NAME='setup_shadow_tactics_blades_of_the_shogun_-_aikos_choice_3.2.25.f.r4769a_(51855)-1.bin'
ARCHIVE_BASE_AIKO_0_PART1_MD5='8bdc4420659b2382d742165fddb09705'
ARCHIVE_BASE_AIKO_0_SIZE='4569291'
ARCHIVE_BASE_AIKO_0_VERSION='3.2.25.f.r4769a-gog51855'
ARCHIVE_BASE_AIKO_0_URL='https://www.gog.com/game/shadow_tactics_blades_of_the_shogun_aikos_choice'

UNITY3D_NAME_BLADES='Shadow Tactics'
UNITY3D_NAME_AIKO='shadow tactics'
UNITY3D_PLUGINS_BLADES='
libRenderingPlugin.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT_BLADES='data/noarch/game'
CONTENT_PATH_DEFAULT_AIKO='.'
CONTENT_GAME_LIGHTING_FILES_BLADES="
${UNITY3D_NAME_BLADES}_Data/GI"

## Aiko's Choice
WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/Daedalic Entertainment GmbH/Shadow Tactics Blades of the Shogun Aikos Choice'

PACKAGES_LIST_BLADES='
PKG_BIN
PKG_LIGHTING
PKG_DATA'
PACKAGES_LIST_AIKO='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS_BLADES='
PKG_LIGHTING
PKG_DATA'
PKG_BIN_DEPENDENCIES_SIBLINGS_AIKO='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES_BLADES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

## Blades of the Shogun
PKG_LIGHTING_ID_BLADES="${GAME_ID_BLADES}-lighting"
PKG_LIGHTING_ID_BLADES_DEMO="${GAME_ID_BLADES_DEMO}-lighting"
PKG_LIGHTING_DESCRIPTION='lighting'

PKG_DATA_ID_BLADES="${GAME_ID_BLADES}-data"
PKG_DATA_ID_BLADES_DEMO="${GAME_ID_BLADES_DEMO}-data"
PKG_DATA_ID_AIKO="${GAME_ID_AIKO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Blades of the Shogun - Link libRenderingPlugin.so to the harcoded path expected by the game engine
case "$(current_archive)" in
	('ARCHIVE_BASE_BLADES_'*)
		file_destination="$(package_path 'PKG_BIN')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86/libRenderingPlugin.so"
		mkdir --parents "$(dirname "$file_destination")"
		ln --symbolic "$(path_libraries)/libRenderingPlugin.so" "$file_destination"
	;;
esac

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
