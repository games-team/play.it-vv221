#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2020 Hoël Bézier
set -o errexit

###
# Bastion
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='bastion'
GAME_NAME='Bastion'

ARCHIVE_BASE_GOG_3_NAME='bastion_1_0_1747_initial_test_33876.sh'
ARCHIVE_BASE_GOG_3_MD5='b4edf5e95e952a35bfde0c12959134ab'
ARCHIVE_BASE_GOG_3_SIZE='1285709'
ARCHIVE_BASE_GOG_3_VERSION='1.50436-gog33876'
ARCHIVE_BASE_GOG_3_URL='https://www.gog.com/game/bastion'

ARCHIVE_BASE_GOG_2_NAME='bastion_1_50436_29_08_2018_23317.sh'
ARCHIVE_BASE_GOG_2_MD5='73c6b33c23232597bec30f211a46f73d'
ARCHIVE_BASE_GOG_2_SIZE='1400000'
ARCHIVE_BASE_GOG_2_VERSION='1.50436.20180829-gog23317'

ARCHIVE_BASE_GOG_1_NAME='bastion_en_1_50436_23291.sh'
ARCHIVE_BASE_GOG_1_MD5='59c2bbcf43cd9ba243d5fa1baa4a4b48'
ARCHIVE_BASE_GOG_1_SIZE='1400000'
ARCHIVE_BASE_GOG_1_VERSION='1.50436-gog23291'

ARCHIVE_BASE_GOG_0_NAME='gog_bastion_2.0.0.1.sh'
ARCHIVE_BASE_GOG_0_MD5='e5e6eefb4885b67abcfa201b1b3a9c48'
ARCHIVE_BASE_GOG_0_SIZE='1300000'
ARCHIVE_BASE_GOG_0_VERSION='1.2.20161020-gog2.0.0.1'

ARCHIVE_BASE_HUMBLE_0_NAME='bastion-10162016-bin'
ARCHIVE_BASE_HUMBLE_0_MD5='19fea173ff2da0f990f60bd5e7c3b237'
## This is a MojoSetup installer, not relying on Makeself.
ARCHIVE_BASE_HUMBLE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_HUMBLE_0_SIZE='1253966'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.2.20161020-humble161019'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_GAME_MAIN_FILES='
Bastion.bmp
Bastion.exe
gamecontrollerdb.txt
mono
monomachineconfig
monoconfig
Content'
CONTENT_DOC_MAIN_FILES='
Linux.README'
## Some Mono libraries are not provided by system packages.
CONTENT_GAME0_MAIN_FILES='
FMOD.dll
FNA.dll
FNA.dll.config
Lidgren.Network.dll
MojoShader.dll
MonoGame.Framework.Net.dll
SDL2.dll'
## System-provided libmojoshader.so can not be used, as it triggers the following error:
## System.NullReferenceException: Object reference not set to an instance of an object
## cf. https://forge.dotslashplay.it/play.it/games/-/issues/653
CONTENT_LIBS_FILES='
libfmodex.so
libmojoshader.so'
CONTENT_LIBS_LIBS64_RELATIVE_PATH='lib64'
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_RELATIVE_PATH='lib'
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
## libSteamWrapper.so is required to prevent a crash of the Humble Bundle build,
## libsteam_api.so is required by libSteamWrapper.so.
CONTENT_LIBS0_FILES='
libSteamWrapper.so
libsteam_api.so'
CONTENT_LIBS0_LIBS64_RELATIVE_PATH='lib64'
CONTENT_LIBS0_LIBS64_FILES_HUMBLE="$CONTENT_LIBS0_FILES"
CONTENT_LIBS0_LIBS32_RELATIVE_PATH='lib'
CONTENT_LIBS0_LIBS32_FILES_HUMBLE="$CONTENT_LIBS0_FILES"

APP_MAIN_EXE='Bastion.exe'
APP_MAIN_ICON='Bastion.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS32
PKG_LIBS64'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libGL.so.1
libSDL2-2.0.so.0
libudev.so.1'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
