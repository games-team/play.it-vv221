#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dark Reign 2
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='dark-reign-2'
GAME_NAME='Dark Reign 2'

ARCHIVE_BASE_1_NAME='setup_dark_reign_2_1.3_(56283).exe'
ARCHIVE_BASE_1_MD5='23b441f1a8ffd5302e7cde3c1099cb73'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_VERSION='1.3.882-gog56283'
ARCHIVE_BASE_1_SIZE='572562'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/dark_reign_2'

ARCHIVE_BASE_0_NAME='setup_dark_reign2_2.0.0.11.exe'
ARCHIVE_BASE_0_MD5='9a3d10825507b73c4db178f9caea2406'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_VERSION='1.3.882-gog2.0.0.11'
ARCHIVE_BASE_0_SIZE='450000'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
library
settings.cfg
_inmm.dll
binkw32.dll
ddraw.dll
getinfo.dll
libogg.dll
libvorbis.dll
libvorbisfile.dll
mss32.dll
msvcp90.dll
msvcr90.dll
dr2.exe
dxcfg.exe
anet.inf
dxcfg.ini'
## On first launch, register the game CD-key.
## This file is generated later in this script.
CONTENT_GAME0_BIN_FILES='
cdkey'
## launcher.exe is only provided with the 2.0.0.11 GOG build.
CONTENT_GAME1_BIN_FILES_0='
launcher.exe'
CONTENT_GAME_DATA_FILES='
missions
mods
music
packs
sides
worlds'
CONTENT_DOC_DATA_FILES='
customer_support.htm
manual.pdf
readme.rtf
license.txt'

USER_PERSISTENT_DIRECTORIES='
mods
users'
USER_PERSISTENT_FILES='
settings.cfg'

WINE_VIRTUAL_DESKTOP='auto'
WINE_WINETRICKS_VERBS='win98'

APP_MAIN_EXE='dr2.exe'
APP_MAIN_EXE_0='launcher.exe'
APP_MAIN_ICON='dr2.exe'
## On first launch, register the game CD-key.
APP_MAIN_PRERUN='
# On first launch, register the game CD-key
if [ ! -e .cdkey-registered ]; then
	cdkey=$(cat cdkey)
	$(wine_command) reg add "HKLM\\Software\\WON\\CDKeys" /v "DarkReign2" /t REG_BINARY /d "$cdkey" /f
	touch .cdkey-registered
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# On first launch, register the game CD-key

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
base64
xxd"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## On first launch, register the game CD-key.
	## The source is a base64-encoded string, that must be converted to an hexadecimal representation.
	sed --silent '71s/.*"valueData": "\(.*\)",/\1/p' goggame-1207658911.script | \
		base64 --decode | \
		xxd -plain \
		> 'cdkey'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
