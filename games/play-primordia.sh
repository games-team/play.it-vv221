#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Primordia
###

script_version=20250211.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='primordia'
GAME_NAME='Primordia'

ARCHIVE_BASE_13_NAME='primordia_5_2a_79719.sh'
ARCHIVE_BASE_13_MD5='ce24004eaf34646660a01eaf8b3315ee'
ARCHIVE_BASE_13_SIZE='1426939'
ARCHIVE_BASE_13_VERSION='5.2a-gog79719'
ARCHIVE_BASE_13_URL='https://www.gog.com/game/primordia'

ARCHIVE_BASE_12_NAME='primordia_4_0b_79076.sh'
ARCHIVE_BASE_12_MD5='34d1bb696c4ff865a76ba0d06f978def'
ARCHIVE_BASE_12_SIZE='1426936'
ARCHIVE_BASE_12_VERSION='4.0b-gog79076'

ARCHIVE_BASE_11_NAME='primordia_4_0a_77194.sh'
ARCHIVE_BASE_11_MD5='75efd6a2e3344a3bc22cbf69459ea79f'
ARCHIVE_BASE_11_SIZE='1426936'
ARCHIVE_BASE_11_VERSION='4.0a-gog77194'

ARCHIVE_BASE_10_NAME='primordia_4_0_75697.sh'
ARCHIVE_BASE_10_MD5='cbe5608e47f6bf70364208f686e55161'
ARCHIVE_BASE_10_SIZE='1426935'
ARCHIVE_BASE_10_VERSION='4.0-gog75697'

ARCHIVE_BASE_9_NAME='primordia_3_9_75550.sh'
ARCHIVE_BASE_9_MD5='d28470e41889897e45d0f6f400517499'
ARCHIVE_BASE_9_SIZE='1426932'
ARCHIVE_BASE_9_VERSION='3.9-gog75550'

ARCHIVE_BASE_8_NAME='primordia_3_8_75071.sh'
ARCHIVE_BASE_8_MD5='6f184b62fb442f7e6f0f791bc51717f0'
ARCHIVE_BASE_8_SIZE='1426930'
ARCHIVE_BASE_8_VERSION='3.8-gog75071'

ARCHIVE_BASE_7_NAME='primordia_3_7_73438.sh'
ARCHIVE_BASE_7_MD5='8bae6792e72a9ac69263b6eb4f0e03e6'
ARCHIVE_BASE_7_SIZE='1426929'
ARCHIVE_BASE_7_VERSION='3.7-gog73438'

ARCHIVE_BASE_6_NAME='primordia_3_6_70667.sh'
ARCHIVE_BASE_6_MD5='dc254ed674114e4776a91454e35d3741'
ARCHIVE_BASE_6_SIZE='1426928'
ARCHIVE_BASE_6_VERSION='3.6-gog70667'

ARCHIVE_BASE_5_NAME='primordia_3_6_70159.sh'
ARCHIVE_BASE_5_MD5='d1f9eeec2084933463d1b43b5466f5f1'
ARCHIVE_BASE_5_SIZE='1426928'
ARCHIVE_BASE_5_VERSION='3.6-gog70159'

ARCHIVE_BASE_4_NAME='primordia_3_5b_69802.sh'
ARCHIVE_BASE_4_MD5='b3ca4bfa8341f68ec6eb2d6c36f36e0f'
ARCHIVE_BASE_4_SIZE='1426833'
ARCHIVE_BASE_4_VERSION='3.5b-gog69802'

ARCHIVE_BASE_3_NAME='primordia_3_5a_69504.sh'
ARCHIVE_BASE_3_MD5='62d64492018c9b7fe020303040c5dbe9'
ARCHIVE_BASE_3_SIZE='1427316'
ARCHIVE_BASE_3_VERSION='3.5a-gog69504'

ARCHIVE_BASE_2_NAME='primordia_3_0a_65125.sh'
ARCHIVE_BASE_2_MD5='5b096e36b814c16c0ce067046e194c94'
ARCHIVE_BASE_2_SIZE='1500000'
ARCHIVE_BASE_2_VERSION='3.0a-gog65125'

ARCHIVE_BASE_1_NAME='primordia_3_58594.sh'
ARCHIVE_BASE_1_MD5='3cd92a7767d561f2d982014117ee0576'
ARCHIVE_BASE_1_SIZE='1500000'
ARCHIVE_BASE_1_VERSION='3.0-gog58594'

ARCHIVE_BASE_0_NAME='primordia_italian_53699.sh'
ARCHIVE_BASE_0_MD5='812917de017fde37b1c448f156e4ae7d'
ARCHIVE_BASE_0_SIZE='1500000'
ARCHIVE_BASE_0_VERSION='2.6-gog53699'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='primordia_icons.tar.xz'
ARCHIVE_OPTIONAL_ICONS_MD5='66c1d186075708574ee870a9015b4ff6'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/primordia/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
24x24
32x32
48x48
256x256'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
ENGV.tmp
Music
Sounds
*.ags
*.cfg
*.exe
*.ogv
*.tra
*.vox'
CONTENT_DOC_MAIN_FILES='
licenses'

APP_MAIN_SCUMMID='ags:primordia'
APP_MAIN_ICON='../support/icon.png'
APP_MAIN_ICON_2='Primordia.exe'
APP_MAIN_ICON_1='Primordia.exe'
APP_MAIN_ICON_0='Primordia.exe'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
