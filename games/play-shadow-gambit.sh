#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shadow Gambit
###

script_version=20241023.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='shadow-gambit'
GAME_NAME='Shadow Gambit: The Cursed Crew'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_0_NAME='setup_shadow_gambit_the_cursed_crew_1.2.133.f.r40893_(64bit)_(69829).exe'
ARCHIVE_BASE_0_MD5='b394c7866b3e2106b8659f76dcf3b814'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
## Do not convert the paths to lower case.
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_shadow_gambit_the_cursed_crew_1.2.133.f.r40893_(64bit)_(69829)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='33d84cef13cae8cb1a27e03101cda2cc'
ARCHIVE_BASE_0_PART2_NAME='setup_shadow_gambit_the_cursed_crew_1.2.133.f.r40893_(64bit)_(69829)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='aa475cf51a8a6b9f151a63e7f8acd226'
ARCHIVE_BASE_0_PART3_NAME='setup_shadow_gambit_the_cursed_crew_1.2.133.f.r40893_(64bit)_(69829)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='8019f8a20edf33e894a49ccc84fdf867'
ARCHIVE_BASE_0_PART4_NAME='setup_shadow_gambit_the_cursed_crew_1.2.133.f.r40893_(64bit)_(69829)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='d499dd2ab3f31e1ddb7c6b6c6f9d35c7'
ARCHIVE_BASE_0_SIZE='27989056'
ARCHIVE_BASE_0_VERSION='1.2.133.f-gog69829'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/shadow_gambit_the_cursed_crew'

ARCHIVE_BASE_DEMO_0_NAME='setup_shadow_gambit_the_cursed_crew_demo_1.0.46.r37840.d_(64bit)_(66564).exe'
ARCHIVE_BASE_DEMO_0_MD5='ad6c5d562cb5166301b82311495eb4fc'
ARCHIVE_BASE_DEMO_0_EXTRACTOR='innoextract'
## Do not convert the paths to lower case.
ARCHIVE_BASE_DEMO_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_DEMO_0_PART1_NAME='setup_shadow_gambit_the_cursed_crew_demo_1.0.46.r37840.d_(64bit)_(66564)-1.bin'
ARCHIVE_BASE_DEMO_0_PART1_MD5='2ed0f7c0fe3bcba3cfe562ff687743cc'
ARCHIVE_BASE_DEMO_0_PART2_NAME='setup_shadow_gambit_the_cursed_crew_demo_1.0.46.r37840.d_(64bit)_(66564)-2.bin'
ARCHIVE_BASE_DEMO_0_PART2_MD5='cf8746aaa4ff9b40366c562367155876'
ARCHIVE_BASE_DEMO_0_PART3_NAME='setup_shadow_gambit_the_cursed_crew_demo_1.0.46.r37840.d_(64bit)_(66564)-3.bin'
ARCHIVE_BASE_DEMO_0_PART3_MD5='190c8d547cd6f3217a10038079431c9c'
ARCHIVE_BASE_DEMO_0_SIZE='15729374'
ARCHIVE_BASE_DEMO_0_VERSION='1.0.46-gog66564'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/shadow_gambit_the_cursed_crew_demo'

UNITY3D_NAME='ShadowGambit_TCC'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_EXPANSION_YUKI_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/aa/StandaloneWindows64/dlctrp_content_00"
CONTENT_GAME_DATA_EXPANSION_ZAGAN_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/aa/StandaloneWindows64/dlcchc_content_00"
CONTENT_GAME_DATA_BUNDLES1_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/aa/StandaloneWindows64/mis_*"
CONTENT_GAME_DATA_BUNDLES2_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/aa/StandaloneWindows64/main_*"
CONTENT_GAME_DATA_BUNDLES3_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/aa/StandaloneWindows64"

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/ShadowGambit_TCC'
WINE_PERSISTENT_DIRECTORIES_DEMO='
users/${USER}/AppData/Local/ShadowGambit_TCC_Demo'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_EXPANSION_YUKI
PKG_DATA_EXPANSION_ZAGAN
PKG_DATA_BUNDLES1
PKG_DATA_BUNDLES2
PKG_DATA_BUNDLES3
PKG_DATA'
PACKAGES_LIST_DEMO='
PKG_BIN
PKG_DATA_BUNDLES1
PKG_DATA_BUNDLES2
PKG_DATA_BUNDLES3
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_EXPANSION_YUKI
PKG_DATA_EXPANSION_ZAGAN
PKG_DATA_BUNDLES1
PKG_DATA_BUNDLES2
PKG_DATA_BUNDLES3'
PKG_DATA_DEPENDENCIES_SIBLINGS_DEMO='
PKG_DATA_BUNDLES1
PKG_DATA_BUNDLES2
PKG_DATA_BUNDLES3'

PKG_DATA_EXPANSION_ID="${PKG_DATA_ID}-expansion"
PKG_DATA_EXPANSION_YUKI_ID="${PKG_DATA_EXPANSION_ID}-yuki"
PKG_DATA_EXPANSION_ZAGAN_ID="${PKG_DATA_EXPANSION_ID}-zagan"
PKG_DATA_EXPANSION_DESCRIPTION="$PKG_DATA_DESCRIPTION - expansion"
PKG_DATA_EXPANSION_YUKI_DESCRIPTION="$PKG_DATA_EXPANSION_DESCRIPTION - Yuki's Wish"
PKG_DATA_EXPANSION_ZAGAN_DESCRIPTION="$PKG_DATA_EXPANSION_DESCRIPTION - Zagan's Ritual"

PKG_DATA_BUNDLES_ID="${PKG_DATA_ID}-bundles"
PKG_DATA_BUNDLES1_ID="${PKG_DATA_BUNDLES_ID}-1"
PKG_DATA_BUNDLES2_ID="${PKG_DATA_BUNDLES_ID}-2"
PKG_DATA_BUNDLES3_ID="${PKG_DATA_BUNDLES_ID}-3"
PKG_DATA_BUNDLES_ID_DEMO="${PKG_DATA_ID_DEMO}-bundles"
PKG_DATA_BUNDLES1_ID_DEMO="${PKG_DATA_BUNDLES_ID_DEMO}-1"
PKG_DATA_BUNDLES2_ID_DEMO="${PKG_DATA_BUNDLES_ID_DEMO}-2"
PKG_DATA_BUNDLES3_ID_DEMO="${PKG_DATA_BUNDLES_ID_DEMO}-3"
PKG_DATA_BUNDLES_DESCRIPTION="$PKG_DATA_DESCRIPTION - bundles"
PKG_DATA_BUNDLES1_DESCRIPTION="$PKG_DATA_BUNDLES_DESCRIPTION - 1"
PKG_DATA_BUNDLES2_DESCRIPTION="$PKG_DATA_BUNDLES_DESCRIPTION - 2"
PKG_DATA_BUNDLES3_DESCRIPTION="$PKG_DATA_BUNDLES_DESCRIPTION - 3"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Store the game prefix into the WINE prefix, instead of using a symbolic link
## This is required for the expansions to be correctly detected
## The generated launchers will ignore $PLAYIT_PREFIX_PATH
launcher_path_prefix() {
	local game_id
	game_id=$(game_id)
	cat <<- EOF
	## Set the path to the volatile game prefix
	## This launcher ignores \$PLAYIT_PREFIX_PATH
	path_prefix() {
	    # Store the game prefix into the WINE prefix, instead of using a symbolic link
	    # This is required for the expansions to be correctly detected
	    printf '%s/drive_c/%s' \\
	        "\$PATH_WINEPREFIX" \\
	        '$(game_id)'
	}
	PATH_PREFIX=\$(path_prefix)

	EOF
}
wine_wineprefix_init_actions() {
	cat <<- 'EOF'
	    ## Do not link the game prefix into the WINE prefix, as its real path is already there

	    ## Remove most links pointing outside of the WINE prefix
	    rm "$WINEPREFIX/dosdevices/z:"
	    find "$WINEPREFIX/drive_c/users/$(whoami)" -type l | while read -r directory; do
	        rm "$directory"
	        mkdir "$directory"
	    done
	    unset directory

	    ## Set symbolic links to the legacy paths
	    wineprefix_legacy_link 'AppData/Roaming' 'Application Data'
	    wineprefix_legacy_link 'AppData/Local' 'Local Settings/Application Data'
	    wineprefix_legacy_link 'Documents' 'My Documents'

	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
