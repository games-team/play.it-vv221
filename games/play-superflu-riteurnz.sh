#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Superflu Riteurnz
###

script_version=20241021.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='superflu-riteurnz'
GAME_NAME='Superflu Riteurnz'

ARCHIVE_BASE_1_NAME='superfluous-returnz-1.6.0-d13-gnunux.tar.gz'
ARCHIVE_BASE_1_MD5='bbefabb57d0525a6e219251db7937e50'
ARCHIVE_BASE_1_SIZE='256197'
ARCHIVE_BASE_1_VERSION='1.6.0-itch.2023.12.20'
ARCHIVE_BASE_1_URL='https://ptilouk.itch.io/superfluous-returnz'

ARCHIVE_BASE_0_NAME='superfluous-returnz-1.5.5-d12-gnunux.tar.gz'
ARCHIVE_BASE_0_MD5='a147cebc30b9439321b5d6c8b8f04539'
ARCHIVE_BASE_0_SIZE='353012'
ARCHIVE_BASE_0_VERSION='1.5.5-itch.2023.11.22'

ARCHIVE_BASE_MONOARCH_7_NAME='superfluous-returnz-1.5.5-d12-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_7_MD5='6410e666d262dc32b4a143e5670d1805'
ARCHIVE_BASE_MONOARCH_7_SIZE='315764'
ARCHIVE_BASE_MONOARCH_7_VERSION='1.5.5-itch.2023.10.09'

ARCHIVE_BASE_MONOARCH_6_NAME='superfluous-returnz-1.5.4-d11-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_6_MD5='8f1f27107d8195b252a4a95be4e49961'
ARCHIVE_BASE_MONOARCH_6_SIZE='315924'
ARCHIVE_BASE_MONOARCH_6_VERSION='1.5.4-itch.2023.09.14'

ARCHIVE_BASE_MONOARCH_5_NAME='superfluous-returnz-1.5.3-d10-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_5_MD5='bf0989ee6fc571c5dfb1546700630a9a'
ARCHIVE_BASE_MONOARCH_5_SIZE='315888'
ARCHIVE_BASE_MONOARCH_5_VERSION='1.5.3-itch1'

ARCHIVE_BASE_MONOARCH_4_NAME='superfluous-returnz-1.5.2-d10-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_4_MD5='8b07a4ee3c36e12335bfdad784aefcdf'
ARCHIVE_BASE_MONOARCH_4_SIZE='320000'
ARCHIVE_BASE_MONOARCH_4_VERSION='1.5.2-itch1'

ARCHIVE_BASE_MONOARCH_3_NAME='superfluous-returnz-1.5.1-d10-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_3_MD5='34ca505142784f66a30f1c0d387aa920'
ARCHIVE_BASE_MONOARCH_3_SIZE='320000'
ARCHIVE_BASE_MONOARCH_3_VERSION='1.5.1-itch.2023.06.14'

ARCHIVE_BASE_MONOARCH_2_NAME='superfluous-returnz-1.5.0-d10-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_2_MD5='0a243baf9fcc7dbe83a18394eb87a37f'
ARCHIVE_BASE_MONOARCH_2_SIZE='320000'
ARCHIVE_BASE_MONOARCH_2_VERSION='1.5.0-itch.2023.06.08'

ARCHIVE_BASE_MONOARCH_1_NAME='superfluous-returnz-1.4.2-d7-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_1_MD5='a2086175774ea0038e5ea0f952971560'
ARCHIVE_BASE_MONOARCH_1_SIZE='310000'
ARCHIVE_BASE_MONOARCH_1_VERSION='1.4.2-itch.2023.05.16'

ARCHIVE_BASE_MONOARCH_0_NAME='superfluous-returnz-1.4.1-d6-gnunux.tar.gz'
ARCHIVE_BASE_MONOARCH_0_MD5='93a5592d1d10f4a73047ec46d4cc966b'
ARCHIVE_BASE_MONOARCH_0_SIZE='310000'
ARCHIVE_BASE_MONOARCH_0_VERSION='1.4.1-itch.2023.05.15'

CONTENT_PATH_DEFAULT_1='superfluous-returnz-1.6.0-d13'
CONTENT_PATH_DEFAULT_0='superfluous-returnz-1.5.5-d12'
CONTENT_PATH_DEFAULT_MONOARCH_7='superfluous-returnz-1.5.5-d12'
CONTENT_PATH_DEFAULT_MONOARCH_6='superfluous-returnz-1.5.4-d11'
CONTENT_PATH_DEFAULT_MONOARCH_5='superfluous-returnz-1.5.3-d10'
CONTENT_PATH_DEFAULT_MONOARCH_4='superfluous-returnz-1.5.2-d10'
CONTENT_PATH_DEFAULT_MONOARCH_3='superfluous-returnz-1.5.1-d10'
CONTENT_PATH_DEFAULT_MONOARCH_2='superfluous-returnz-1.5.0-d10'
CONTENT_PATH_DEFAULT_MONOARCH_1='superfluous-returnz-1.4.2-d7'
CONTENT_PATH_DEFAULT_MONOARCH_0='superfluous-returnz-1.4.1-d6'
CONTENT_GAME_BIN_FILES='
bin64/superfluous-returnz'
CONTENT_GAME_BIN_FILES_MONOARCH='
bin/superfluous-returnz'
CONTENT_GAME_DATA_FILES='
share/superfluous-returnz/*.data'
CONTENT_DOC_DATA_FILES='
LICENSE.md'

APP_MAIN_EXE='bin64/superfluous-returnz'
APP_MAIN_EXE_MONOARCH='bin/superfluous-returnz'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
liblz4.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_ttf-2.0.so.0
libstdc++.so.6
libyaml-0.so.2'
## libSDL2_image-2.0.so.0 is required by < 1.6 game builds.
PKG_BIN_DEPENDENCIES_LIBRARIES_PRE16='
libc.so.6
libgcc_s.so.1
liblz4.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_ttf-2.0.so.0
libstdc++.so.6
libyaml-0.so.2'
PKG_BIN_DEPENDENCIES_LIBRARIES_0="$PKG_BIN_DEPENDENCIES_LIBRARIES_PRE16"
PKG_BIN_DEPENDENCIES_LIBRARIES_MONOARCH="$PKG_BIN_DEPENDENCIES_LIBRARIES_PRE16"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

## An alternative icon path is used for < 1.6 game builds.
case "$(current_archive)" in
	('ARCHIVE_BASE_0'|'ARCHIVE_BASE_MONOARCH_'*)
		icon_path_source="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/share/icons/hicolor/scalable/apps/superfluous-returnz.svg"
	;;
	(*)
		icon_path_source="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/share/icons/superfluous-returnz.svg"
	;;
esac
install -D --mode=644 --no-target-directory \
	"$icon_path_source" \
	"$(package_path 'PKG_DATA')$(path_icons)/scalable/apps/$(game_id).svg"
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
