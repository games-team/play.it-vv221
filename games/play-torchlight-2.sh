#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Torchlight 2
###

script_version=20241027.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='torchlight-2'
GAME_NAME='Torchlight Ⅱ'

ARCHIVE_BASE_GOG_0_NAME='gog_torchlight_2_2.0.0.2.sh'
ARCHIVE_BASE_GOG_0_MD5='e107f6d4c6d4cecea37ade420a8d4892'
ARCHIVE_BASE_GOG_0_SIZE='1673501'
ARCHIVE_BASE_GOG_0_VERSION='1.25.9.7-gog2.0.0.2'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/torchlight_ii'

ARCHIVE_BASE_HUMBLE_0_NAME='Torchlight2-linux-2015-04-01.sh'
ARCHIVE_BASE_HUMBLE_0_MD5='730a5d08c8f1cd4a65afbc0ca631d85c'
ARCHIVE_BASE_HUMBLE_0_SIZE='1672909'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.25.2.4-humble150402'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/torchlight-ii'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_LIBS_BIN_FILES='
libCEGUIBase.so.1
libCEGUIExpatParser.so
libCEGUIFalagardWRBase.so
libCEGUIFreeImageImageCodec.so
libfmodex.so
libOgreMain.so.1
Plugin_OctreeSceneManager.so
RenderSystem_GL.so'
CONTENT_LIBS_BIN64_RELATIVE_PATH_GOG='lib64'
CONTENT_LIBS_BIN64_RELATIVE_PATH_HUMBLE='x86_64/lib64'
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_RELATIVE_PATH_GOG='lib'
CONTENT_LIBS_BIN32_RELATIVE_PATH_HUMBLE='x86/lib'
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_RELATIVE_PATH_HUMBLE='x86_64'
CONTENT_GAME_BIN64_FILES='
Torchlight2.bin.x86_64
ModLauncher.bin.x86_64'
CONTENT_GAME_BIN32_RELATIVE_PATH_HUMBLE='x86'
CONTENT_GAME_BIN32_FILES='
Torchlight2.bin.x86
ModLauncher.bin.x86'
CONTENT_GAME_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_GAME_DATA_FILES='
icons
movies
music
PAKS
porting
programs
*.bmp
*.cfg
*.png'
CONTENT_DOC_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_DOC_DATA_FILES='
licenses'

APP_MAIN_EXE_BIN64='Torchlight2.bin.x86_64'
APP_MAIN_EXE_BIN32='Torchlight2.bin.x86'
APP_MAIN_ICON='Delvers.png'
APP_MAIN_ICON_HUMBLE='noarch/Delvers.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libexpat.so.1
libfontconfig.so.1
libfreeimage.so.3
libfreetype.so.6
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libICE.so.6
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libSM.so.6
libstdc++.so.6
libuuid.so.1
libX11.so.6
libXext.so.6
libXft.so.2
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_BIN32')"

	## Add required execution bit on extra binaries.
	chmod 755 'ModLauncher.bin.x86'
)
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_BIN64')"

	## Add required execution bit on extra binaries.
	chmod 755 'ModLauncher.bin.x86_64'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
