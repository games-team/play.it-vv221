#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2020 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# Myst series:
# - Myst (Masterpiece Edition)
# - Riven: The Sequel to Myst
# - Myst 3: Exile
###

script_version=20250114.4

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_MYST1='myst-1'
GAME_NAME_MYST1='Myst'

GAME_ID_RIVEN='myst-2-riven'
GAME_NAME_RIVEN='Riven: The Sequel to Myst'

GAME_ID_EXILE='myst-3-exile'
GAME_NAME_EXILE='Myst Ⅲ: Exile'

# Archives

## Myst (Masterpiece Edition)

ARCHIVE_BASE_MYST1_2_NAME='setup_myst_masterpiece_edition_1.0_svm_update_4_(22597).exe'
ARCHIVE_BASE_MYST1_2_MD5='cee43afef96ec781a18c578a1cc8bdcc'
ARCHIVE_BASE_MYST1_2_TYPE='innosetup'
ARCHIVE_BASE_MYST1_2_PART1_NAME='setup_myst_masterpiece_edition_1.0_svm_update_4_(22597)-1.bin'
ARCHIVE_BASE_MYST1_2_PART1_MD5='1417de6ceaaaac67064529c3afab6792'
ARCHIVE_BASE_MYST1_2_SIZE='1409531'
ARCHIVE_BASE_MYST1_2_VERSION='1.0.4-gog22597'
ARCHIVE_BASE_MYST1_2_URL='https://www.gog.com/game/myst_masterpiece_edition'

ARCHIVE_BASE_MYST1_1_NAME='setup_myst_masterpiece_edition_1.0_svm_update_4_(22598).exe'
ARCHIVE_BASE_MYST1_1_MD5='e3c62eeb19abd2c9a947aee8300e995d'
ARCHIVE_BASE_MYST1_1_TYPE='innosetup'
ARCHIVE_BASE_MYST1_1_PART1_NAME='setup_myst_masterpiece_edition_1.0_svm_update_4_(22598)-1.bin'
ARCHIVE_BASE_MYST1_1_PART1_MD5='4b84a68ec57e55bcc9b522c6333c669c'
ARCHIVE_BASE_MYST1_1_SIZE='1500000'
ARCHIVE_BASE_MYST1_1_VERSION='1.0.4-gog22598'

## Riven: The Sequel to Myst

ARCHIVE_BASE_RIVEN_2_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114).exe'
ARCHIVE_BASE_RIVEN_2_MD5='e3bb9372a059a7284b0dc02f39aba929'
ARCHIVE_BASE_RIVEN_2_TYPE='innosetup'
ARCHIVE_BASE_RIVEN_2_PART1_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-1.bin'
ARCHIVE_BASE_RIVEN_2_PART1_MD5='99921a49f1834af00e5f6dda7227d456'
ARCHIVE_BASE_RIVEN_2_PART2_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-2.bin'
ARCHIVE_BASE_RIVEN_2_PART2_MD5='2db5912fc46865a8992cb32417674fca'
ARCHIVE_BASE_RIVEN_2_SIZE='6946550'
ARCHIVE_BASE_RIVEN_2_VERSION='1.2-gog55114'
ARCHIVE_BASE_RIVEN_2_URL='https://www.gog.com/game/riven_the_sequel_to_myst'

ARCHIVE_BASE_RIVEN_1_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114).exe'
ARCHIVE_BASE_RIVEN_1_MD5='879b8aaa724f4f422661634186ee2534'
ARCHIVE_BASE_RIVEN_1_TYPE='innosetup'
ARCHIVE_BASE_RIVEN_1_PART1_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-1.bin'
ARCHIVE_BASE_RIVEN_1_PART1_MD5='dd91f1256d819d6f26d9d0bcaa81548f'
ARCHIVE_BASE_RIVEN_1_PART2_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_no_launcher_(55114)-2.bin'
ARCHIVE_BASE_RIVEN_1_PART2_MD5='2d03d82b784666472e1a3f436ed20924'
ARCHIVE_BASE_RIVEN_1_SIZE='6947081'
ARCHIVE_BASE_RIVEN_1_VERSION='1.2-gog55114'

ARCHIVE_BASE_RIVEN_0_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_update3_(22594).exe'
ARCHIVE_BASE_RIVEN_0_MD5='0520fac1f2ae966aa5c490850b51930f'
ARCHIVE_BASE_RIVEN_0_TYPE='innosetup'
ARCHIVE_BASE_RIVEN_0_PART1_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_update3_(22594)-1.bin'
ARCHIVE_BASE_RIVEN_0_PART1_MD5='4ee339ad50203020e717a78ff95bb590'
ARCHIVE_BASE_RIVEN_0_PART2_NAME='setup_riven_-_the_sequel_to_myst_1.2_svm_update3_(22594)-2.bin'
ARCHIVE_BASE_RIVEN_0_PART2_MD5='2ff2d7d25f2e8c2995c17d8b122d672c'
ARCHIVE_BASE_RIVEN_0_SIZE='6954413'
ARCHIVE_BASE_RIVEN_0_VERSION='1.2-gog22594'

## Myst 3: Exile

ARCHIVE_BASE_EXILE_0_NAME='setup_myst_3_exile_1.27_rvm_(21807).exe'
ARCHIVE_BASE_EXILE_0_MD5='e2b8e962326b8802009d3e30b9e4c8ff'
ARCHIVE_BASE_EXILE_0_TYPE='innosetup'
ARCHIVE_BASE_EXILE_0_PART1_NAME='setup_myst_3_exile_1.27_rvm_(21807)-1.bin'
ARCHIVE_BASE_EXILE_0_PART1_MD5='c69e2780274f2830ab210d230889e5bb'
ARCHIVE_BASE_EXILE_0_SIZE='2422163'
ARCHIVE_BASE_EXILE_0_VERSION='1.27-gog21807'
ARCHIVE_BASE_EXILE_0_URL='https://www.gog.com/game/myst_3_exile'

# Archives content

CONTENT_PATH_DEFAULT='.'

## Myst (Masterpiece Edition)

CONTENT_GAME_MAIN_FILES_MYST1='
qtw/channel/holoalgh.mov
qtw/channel/holoamth.mov
qtw/channel/holoasir.mov
qtw/channel/holosmsg.mov
qtw/channel/monalgh.mov
qtw/channel/monamth.mov
qtw/channel/monasirs.mov
qtw/channel/monsmsg.mov
qtw/dunny/atr1nopg.mov
qtw/dunny/atr1page.mov
qtw/dunny/atrmidc2.mov
qtw/dunny/atrus2.mov
qtw/intro/broder.mov
qtw/intro/intro.mov
qtw/mech/holoon.mov
qtw/myst/aextra.mov
qtw/myst/alose.mov
qtw/myst/apage1.mov
qtw/myst/apage2.mov
qtw/myst/apage3.mov
qtw/myst/apage4.mov
qtw/myst/apage5.mov
qtw/myst/atrusbk1.mov
qtw/myst/atrusbk2.mov
qtw/myst/sextra.mov
qtw/myst/slose.mov
qtw/myst/spage1.mov
qtw/myst/spage2.mov
qtw/myst/spage3.mov
qtw/myst/spage4.mov
qtw/myst/spage5.mov
qtw/myst/vltatrus.mov'
CONTENT_GAME_MAIN_FR_PATH_MYST1='french'
CONTENT_GAME_MAIN_FR_FILES_MYST1="$CONTENT_GAME_MAIN_FILES_MYST1"
CONTENT_GAME_MAIN_EN_FILES_MYST1="$CONTENT_GAME_MAIN_FILES_MYST1"
CONTENT_GAME0_MAIN_FR_FILES_MYST1='
channel_french.dat
credits_french.dat
help_french.dat
mechan_french.dat
myst_french.dat
selen_french.dat
stone_french.dat'
CONTENT_GAME_COMMON_FILES_MYST1='
channel.dat
credits.dat
dunny.dat
help.dat
intro.dat
mechan.dat
menu.dat
myst.dat
selen.dat
stone.dat
qtw'
CONTENT_DOC_COMMON_FILES_MYST1='
manual.pdf
readme.txt'

## Riven: The Sequel to Myst

CONTENT_GAME_MAIN_FR_FILES_RIVEN='
b_data_french.mhk
j_data_french.mhk
o_data_french.mhk
p_data_french.mhk
r_data_french.mhk
t_data_french.mhk'
## TODO: System-provided fonts should be used instead, ScummVM expects to find them in the game directory.
CONTENT_FONTS_COMMON_FILES_RIVEN='
freesans.ttf
mplus-2c-regular.ttf'
CONTENT_GAME_COMMON_FILES_RIVEN='
riven.exe
a_data.mhk
b_data.mhk
b2_data.mhk
g_data.mhk
j_data1.mhk
j_data2.mhk
o_data.mhk
p_data.mhk
r_data.mhk
t_data1.mhk
t_data2.mhk
a_sounds.mhk
b_sounds.mhk
g_sounds.mhk
j_sounds.mhk
o_sounds.mhk
p_sounds.mhk
r_sounds.mhk
t_sounds.mhk
extras.mhk'
## The following localization files must be included, or ScummVM fails to detect the correct game version.
CONTENT_GAME0_COMMON_FILES_RIVEN='
a_data_french.mhk
a_data_german.mhk
a_data_italian.mhk
a_data_japanese.mhk
a_data_polish.mhk
a_data_russian.mhk
a_data_spanish.mhk'
CONTENT_DOC_COMMON_FILES_RIVEN='
manual.pdf
readme.txt'

## Myst 3: Exile

CONTENT_GAME_MAIN_FILES_EXILE='
data
m3data'
CONTENT_DOC_MAIN_FILES_EXILE='
manual.pdf'


APP_MAIN_SCUMMID_MYST1='mohawk:myst'
APP_MAIN_SCUMMID_RIVEN='mohawk:riven'
APP_MAIN_SCUMMID_EXILE='myst3:myst3'
APP_MAIN_ICON_MYST1='app/goggame-1207658818.ico'
APP_MAIN_ICON_RIVEN='app/goggame-1207658819.ico'
APP_MAIN_ICON_EXILE='app/goggame-1766899243.ico'
APP_MAIN_OPTIONS_MAIN_EN_MYST1='--language=en'
APP_MAIN_OPTIONS_MAIN_EN_RIVEN='--language=en'
APP_MAIN_OPTIONS_MAIN_FR_MYST1='--language=fr'
APP_MAIN_OPTIONS_MAIN_FR_RIVEN='--language=fr'

# Packages

PACKAGES_LIST='
PKG_MAIN_EN
PKG_MAIN_FR
PKG_COMMON'

## Myst (Masterpiece Edition)

PKG_MAIN_ID_MYST1="$GAME_ID_MYST1"
PKG_MAIN_EN_ID_MYST1="${PKG_MAIN_ID_MYST1}-en"
PKG_MAIN_FR_ID_MYST1="${PKG_MAIN_ID_MYST1}-fr"
PKG_MAIN_PROVIDES_MYST1="
$PKG_MAIN_ID_MYST1"
PKG_MAIN_EN_PROVIDES_MYST1="$PKG_MAIN_PROVIDES_MYST1"
PKG_MAIN_FR_PROVIDES_MYST1="$PKG_MAIN_PROVIDES_MYST1"
PKG_MAIN_EN_DESCRIPTION_MYST1='English localization'
PKG_MAIN_EN_DESCRIPTION_MYST1='French localization'
PKG_MAIN_DEPENDENCIES_SIBLINGS_MYST1='
PKG_COMMON'
PKG_MAIN_EN_DEPENDENCIES_SIBLINGS_MYST1="$PKG_MAIN_DEPENDENCIES_SIBLINGS_MYST1"
PKG_MAIN_FR_DEPENDENCIES_SIBLINGS_MYST1="$PKG_MAIN_DEPENDENCIES_SIBLINGS_MYST1"

PKG_COMMON_ID_MYST1="${GAME_ID_MYST1}-common"
PKG_COMMON_DESCRIPTION_MYST1='Common data'

## Riven: The Sequel to Myst

PKG_MAIN_ID_RIVEN="$GAME_ID_RIVEN"
PKG_MAIN_EN_ID_RIVEN="${PKG_MAIN_ID_RIVEN}-en"
PKG_MAIN_FR_ID_RIVEN="${PKG_MAIN_ID_RIVEN}-fr"
PKG_MAIN_PROVIDES_RIVEN="
$PKG_MAIN_ID_RIVEN"
PKG_MAIN_EN_PROVIDES_RIVEN="$PKG_MAIN_PROVIDES_RIVEN"
PKG_MAIN_FR_PROVIDES_RIVEN="$PKG_MAIN_PROVIDES_RIVEN"
PKG_MAIN_EN_DESCRIPTION_RIVEN='English localization'
PKG_MAIN_EN_DESCRIPTION_RIVEN='French localization'
PKG_MAIN_DEPENDENCIES_SIBLINGS_RIVEN='
PKG_COMMON'
PKG_MAIN_EN_DEPENDENCIES_SIBLINGS_RIVEN="$PKG_MAIN_DEPENDENCIES_SIBLINGS_RIVEN"
PKG_MAIN_FR_DEPENDENCIES_SIBLINGS_RIVEN="$PKG_MAIN_DEPENDENCIES_SIBLINGS_RIVEN"

PKG_COMMON_ID_RIVEN="${GAME_ID_RIVEN}-common"
PKG_COMMON_DESCRIPTION_RIVEN='Common data'
## Ensure easy upgrades from packages generated with pre-20250114.1 game scripts
PKG_COMMON_PROVIDES_RIVEN="${PKG_COMMON_PROVIDES_RIVEN:-}
myst-2-riven-data"

## Myst 3: Exile

PACKAGES_LIST_EXILE='
PKG_MAIN'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

case "$(current_archive)" in
	('ARCHIVE_BASE_EXILE_'*)
		content_inclusion_icons
	;;
	(*)
		content_inclusion_icons 'PKG_COMMON'
	;;
esac
content_inclusion_default

## Riven - Link the fonts in the hardcoded path the game engine expects
case "$(current_archive)" in
	('ARCHIVE_BASE_RIVEN_'*)
		fonts_source=$(path_fonts_ttf)
		fonts_destination="$(package_path 'PKG_COMMON')$(path_game_data)"
		mkdir --parents "$fonts_destination"
		for font_file in \
			'freesans.ttf' \
			'mplus-2c-regular.ttf'
		do
			ln --symbolic "${fonts_source}/${font_file}" "$fonts_destination"
		done
	;;
esac

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_EXILE_'*)
		launchers_generation
	;;
	(*)
		APP_MAIN_OPTIONS_MAIN_EN=$(context_value 'APP_MAIN_OPTIONS_MAIN_EN')
		APP_MAIN_OPTIONS_MAIN_FR=$(context_value 'APP_MAIN_OPTIONS_MAIN_FR')
		launchers_generation 'PKG_MAIN_EN'
		launchers_generation 'PKG_MAIN_FR'
	;;
esac

# Build packages

packages_generation
case "$(current_archive)" in
	('ARCHIVE_BASE_EXILE_'*)
		print_instructions
	;;
	(*)
		case "$(messages_language)" in
			('fr')
				lang_string='version %s :'
				lang_en='anglaise'
				lang_fr='française'
			;;
			('en'|*)
				lang_string='%s version:'
				lang_en='English'
				lang_fr='French'
			;;
		esac
		printf '\n'
		printf "$lang_string" "$lang_en"
		print_instructions 'PKG_MAIN_EN' 'PKG_COMMON'
		printf "$lang_string" "$lang_fr"
		print_instructions 'PKG_MAIN_FR' 'PKG_COMMON'
	;;
esac

# Clean up

working_directory_cleanup

exit 0
