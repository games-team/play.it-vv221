#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warhammer 40k: Gladius
###

script_version=20241212.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='warhammer-40k-gladius'
GAME_NAME='Warhammer 40,000: Gladius - Relics of War'

ARCHIVE_BASE_3_NAME='warhammer_40_000_gladius_relics_of_war_1_14_03_77940.sh'
ARCHIVE_BASE_3_MD5='3d47d62522615fb42e876c630c5bbbb0'
ARCHIVE_BASE_3_SIZE='3558407'
ARCHIVE_BASE_3_VERSION='1.14.3-gog77940'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/warhammer_40000_gladius_relics_of_war'

ARCHIVE_BASE_0_NAME='warhammer_40_000_gladius_relics_of_war_1_13_03_72014.sh'
ARCHIVE_BASE_0_MD5='6ff6665c0a2ead9b1807fcddb28a9a08'
ARCHIVE_BASE_0_SIZE='3489255'
ARCHIVE_BASE_0_VERSION='1.13.3-gog72014'

ARCHIVE_BASE_OLDLIBS_9_NAME='warhammer_40_000_gladius_relics_of_war_1_12_03_67548.sh'
ARCHIVE_BASE_OLDLIBS_9_MD5='9bbcc5e63c4793fddceea5d8b8fbe1d8'
ARCHIVE_BASE_OLDLIBS_9_SIZE='3254164'
ARCHIVE_BASE_OLDLIBS_9_VERSION='1.12.3-gog67548'

ARCHIVE_BASE_OLDLIBS_8_NAME='warhammer_40_000_gladius_relics_of_war_1_11_04_64361.sh'
ARCHIVE_BASE_OLDLIBS_8_MD5='eb7bcdf1a43f47dbc7407e309fd209a3'
ARCHIVE_BASE_OLDLIBS_8_SIZE='3200000'
ARCHIVE_BASE_OLDLIBS_8_VERSION='1.11.4-gog64361'

ARCHIVE_BASE_OLDLIBS_7_NAME='warhammer_40_000_gladius_relics_of_war_1_10_01_00_57375.sh'
ARCHIVE_BASE_OLDLIBS_7_MD5='924bba1e75fdc323d72f24eccc2856a7'
ARCHIVE_BASE_OLDLIBS_7_SIZE='2900000'
ARCHIVE_BASE_OLDLIBS_7_VERSION='1.10.1-gog57375'

ARCHIVE_BASE_OLDLIBS_6_NAME='warhammer_40_000_gladius_relics_of_war_v1_09_03_00_54377.sh'
ARCHIVE_BASE_OLDLIBS_6_MD5='f3271281015c34f1ce3c6974998d886f'
ARCHIVE_BASE_OLDLIBS_6_SIZE='2900000'
ARCHIVE_BASE_OLDLIBS_6_VERSION='1.9.3-gog54377'

ARCHIVE_BASE_OLDLIBS_5_NAME='warhammer_40_000_gladius_relics_of_war_1_08_04_01_49548.sh'
ARCHIVE_BASE_OLDLIBS_5_MD5='d2e88289b1b7b1ebe1037d999f14578e'
ARCHIVE_BASE_OLDLIBS_5_SIZE='2600000'
ARCHIVE_BASE_OLDLIBS_5_VERSION='1.8.4.1-gog49548'

ARCHIVE_BASE_OLDLIBS_4_NAME='warhammer_40_000_gladius_relics_of_war_1_07_04_44200.sh'
ARCHIVE_BASE_OLDLIBS_4_MD5='b8534d0956159736ee49f207da516ba6'
ARCHIVE_BASE_OLDLIBS_4_SIZE='2600000'
ARCHIVE_BASE_OLDLIBS_4_VERSION='1.7.4-gog44200'

ARCHIVE_BASE_OLDLIBS_3_NAME='warhammer_40_000_gladius_relics_of_war_1_06_4b_41966.sh'
ARCHIVE_BASE_OLDLIBS_3_MD5='dbdcdd7450f009ffd2c5feae2fbc9fd2'
ARCHIVE_BASE_OLDLIBS_3_SIZE='2300000'
ARCHIVE_BASE_OLDLIBS_3_VERSION='1.6.4b-gog41966'

ARCHIVE_BASE_OLDLIBS_2_NAME='warhammer_40_000_gladius_relics_of_war_1_05_01_36614.sh'
ARCHIVE_BASE_OLDLIBS_2_MD5='faf0d5df1a800d8102bdf20309aa9b6c'
ARCHIVE_BASE_OLDLIBS_2_SIZE='2200000'
ARCHIVE_BASE_OLDLIBS_2_VERSION='1.5.1-gog36614'

ARCHIVE_BASE_OLDLIBS_1_NAME='warhammer_40_000_gladius_relics_of_war_1_04_07_36064.sh'
ARCHIVE_BASE_OLDLIBS_1_MD5='d42c72ef55cb2a42487f8173d26b2260'
ARCHIVE_BASE_OLDLIBS_1_SIZE='2300000'
ARCHIVE_BASE_OLDLIBS_1_VERSION='1.4.7-gog36064'

ARCHIVE_BASE_OLDLIBS_0_NAME='warhammer_40_000_gladius_relics_of_war_1_03_08_32868.sh'
ARCHIVE_BASE_OLDLIBS_0_MD5='0739ede31aa10db01b2afef9f66c5e12'
ARCHIVE_BASE_OLDLIBS_0_SIZE='2100000'
ARCHIVE_BASE_OLDLIBS_0_VERSION='1.3.8-gog32868'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_RELATIVE_PATH='Binaries/Linux-x86_64'
CONTENT_LIBS_BIN_FILES='
libavcodec.so.58
libavformat.so.58
libavutil.so.56
libcrypto.so.1.1
libEOSSDK-Linux-Shipping.so
libjemalloc.so.1
libjpeg.so.8
libnettle.so.6
libswresample.so.3'
## Include libraries shipped with old game builds.
CONTENT_LIBS_BIN_FILES_OLDLIBS='
libavcodec.so.58
libavformat.so.58
libavutil.so.56
libcrypto.so.1.1
libboost_locale.so.1.65.1
libboost_system.so.1.65.1
libboost_thread.so.1.65.1
libEOSSDK-Linux-Shipping.so
libicudata.so.60
libicui18n.so.60
libicuuc.so.60
libjemalloc.so.1
libjpeg.so.8
libswresample.so.3'
## The game binary is linked against libsteam_api.so.
CONTENT_LIBS0_BIN_RELATIVE_PATH='Binaries/Linux-x86_64'
CONTENT_LIBS0_BIN_FILES='
libsteam_api.so'
CONTENT_GAME_BIN_FILES='
Binaries/Linux-x86_64/Gladius.bin'
CONTENT_GAME_DATA_FILES='
Data
Documents
Manuals
Resources
*.doc
*.pdf'

## TODO: Check why a regular symlinks farm prefix can not be used
APP_MAIN_PREFIX_TYPE='none'
APP_MAIN_EXE='Binaries/Linux-x86_64/Gladius.bin'
APP_MAIN_ICON='Data/Video/Textures/Icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libboost_locale.so.1.74.0
libc.so.6
libcurl.so.4
libdl.so.2
libfreetype.so.6
libgcc_s.so.1
libglfw.so.3
libminiupnpc.so.17
libm.so.6
libopenal.so.1
libpng16.so.16
libpthread.so.0
librt.so.1
libstdc++.so.6
libvorbisfile.so.3
libvulkan.so.1
libz.so.1'
## Set dependencies for old game builds.
PKG_BIN_DEPENDENCIES_LIBRARIES_OLDLIBS='
libc.so.6
libcurl.so.4
libdl.so.2
libfreetype.so.6
libgcc_s.so.1
libglfw.so.3
libminiupnpc.so.17
libm.so.6
libopenal.so.1
libpng16.so.16
libpthread.so.0
librt.so.1
libstdc++.so.6
libvorbisfile.so.3
libvulkan.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd Binaries/Linux-x86_64
	./Gladius.bin "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
