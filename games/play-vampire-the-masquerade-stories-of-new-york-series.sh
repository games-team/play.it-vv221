#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Vampire: the Masquerade - Stories of New York series:
# - Vampire: the Masquerade - Coteries of New York
# - Vampire: The Masquerade - Shadows of New York
###

script_version=20250103.2

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID_COTERIES='vampire-the-masquerade-coteries-of-new-york'
GAME_NAME_COTERIES='Vampire: The Masquerade - Coteries of New York'

GAME_ID_SHADOWS='vampire-the-masquerade-shadows-of-new-york'
GAME_NAME_SHADOWS='Vampire: The Masquerade - Shadows of New York'

ARCHIVE_BASE_COTERIES_2_NAME='vampire_the_masquerade_coteries_of_new_york_1_0_12_52098.sh'
ARCHIVE_BASE_COTERIES_2_MD5='035735d7a7717c4c31cce156511ec8b0'
ARCHIVE_BASE_COTERIES_2_SIZE='3300000'
ARCHIVE_BASE_COTERIES_2_VERSION='1.0.12-gog52098'
ARCHIVE_BASE_COTERIES_2_URL='https://www.gog.com/game/vampire_the_masquerade_coteries_of_new_york'

ARCHIVE_BASE_COTERIES_1_NAME='vampire_the_masquerade_coteries_of_new_york_1_0_9_45543.sh'
ARCHIVE_BASE_COTERIES_1_MD5='6310c15fabd1768d00faa57f83f2de5e'
ARCHIVE_BASE_COTERIES_1_SIZE='3300000'
ARCHIVE_BASE_COTERIES_1_VERSION='1.0.9-gog45543'

ARCHIVE_BASE_COTERIES_0_NAME='vampire_the_masquerade_coteries_of_new_york_1_0_7_40980.sh'
ARCHIVE_BASE_COTERIES_0_MD5='146113ea6b7295104413d8ce2b1fbf4e'
ARCHIVE_BASE_COTERIES_0_SIZE='3200000'
ARCHIVE_BASE_COTERIES_0_VERSION='1.0.7-gog40980'

ARCHIVE_BASE_SHADOWS_1_NAME='vampire_the_masquerade_shadows_of_new_york_1_0_1_51032.sh'
ARCHIVE_BASE_SHADOWS_1_MD5='0ec3a59f889b30e7d632f65dcdf0ac6b'
ARCHIVE_BASE_SHADOWS_1_SIZE='3319144'
ARCHIVE_BASE_SHADOWS_1_VERSION='1.0.1-gog51032'
ARCHIVE_BASE_SHADOWS_1_URL='https://www.gog.com/game/vampire_the_masquerade_shadows_of_new_york'

ARCHIVE_BASE_SHADOWS_0_NAME='vampire_the_masquerade_shadows_of_new_york_1_0_0_41075.sh'
ARCHIVE_BASE_SHADOWS_0_MD5='e7880d6e417f81f47b079c9cdc4fb907'
ARCHIVE_BASE_SHADOWS_0_SIZE='3300000'
ARCHIVE_BASE_SHADOWS_0_VERSION='1.0.0-gog41075'

UNITY3D_NAME_COTERIES='VtM Coteries of New York'
UNITY3D_NAME_SHADOWS='Vampire the Masquerade - Shadows of New York'
UNITY3D_PLUGINS='
libfmod.so
libfmodstudio.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

## Default to windowed mode on first launch
APP_MAIN_PRERUN_COTERIES='
# Default to windowed mode on first launch
config_file="${XDG_CONFIG_HOME:="$HOME/.config"}/unity3d/DrawDistance/VtM Coteries of New York/Saves/Settings.save"
if [ ! -e "$config_file" ]; then
	mkdir --parents "$(dirname "$config_file")"
	cat > "$config_file" <<- EOF
	{"FullScreenMode":false}
	EOF
fi
'
APP_MAIN_PRERUN_SHADOWS='
# Default to windowed mode on first launch
config_file="${XDG_CONFIG_HOME:="$HOME/.config"}/unity3d/DrawDistance/VtM Shadows of New York/Saves/Settings.save"
if [ ! -e "$config_file" ]; then
	mkdir --parents "$(dirname "$config_file")"
	cat > "$config_file" <<- EOF
	{"FullScreenMode":false}
	EOF
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID_COTERIES="${GAME_ID_COTERIES}-data"
PKG_DATA_ID_SHADOWS="${GAME_ID_SHADOWS}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
