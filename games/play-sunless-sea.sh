#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 BetaRays
set -o errexit

###
# Sunless Sea
###

script_version=20241021.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='sunless-sea'
GAME_NAME='Sunless Sea'

# Archives

## Zubmariner (stand-alone expansion, gog.com)

ARCHIVE_BASE_GOG_ZUBMARINER_5_NAME='sunless_sea_zubmariner_2_2_11_3212_64426.sh'
ARCHIVE_BASE_GOG_ZUBMARINER_5_MD5='35dfc1ccb8a152e7eae5e1468d33098e'
ARCHIVE_BASE_GOG_ZUBMARINER_5_VERSION='2.2.11.3212-gog64426'
ARCHIVE_BASE_GOG_ZUBMARINER_5_SIZE='928276'
ARCHIVE_BASE_GOG_ZUBMARINER_5_URL='https://www.gog.com/game/sunless_sea_zubmariner'

ARCHIVE_BASE_GOG_ZUBMARINER_4_NAME='sunless_sea_zubmariner_v2_2_7_3165_a_53438.sh'
ARCHIVE_BASE_GOG_ZUBMARINER_4_MD5='2d160c6c91b3b7e603447714b3112fba'
ARCHIVE_BASE_GOG_ZUBMARINER_4_VERSION='2.2.7.3165-gog53438'
ARCHIVE_BASE_GOG_ZUBMARINER_4_SIZE='930000'

ARCHIVE_BASE_GOG_ZUBMARINER_3_NAME='sunless_sea_zubmariner_2_2_7_3165_29003.sh'
ARCHIVE_BASE_GOG_ZUBMARINER_3_MD5='7527c8f7a87c6d8f04a0cf2d75f30f4c'
ARCHIVE_BASE_GOG_ZUBMARINER_3_VERSION='2.2.7.3165-gog29003'
ARCHIVE_BASE_GOG_ZUBMARINER_3_SIZE='930000'

ARCHIVE_BASE_GOG_ZUBMARINER_2_NAME='sunless_sea_zubmariner_2_2_6_3150_24613.sh'
ARCHIVE_BASE_GOG_ZUBMARINER_2_MD5='427440ff2f5e57e5e556bbaa1ffcfa7c'
ARCHIVE_BASE_GOG_ZUBMARINER_2_VERSION='2.2.6.3150-gog24613'
ARCHIVE_BASE_GOG_ZUBMARINER_2_SIZE='930000'

ARCHIVE_BASE_GOG_ZUBMARINER_1_NAME='sunless_sea_zubmariner_en_v2_2_4_3141_21326.sh'
ARCHIVE_BASE_GOG_ZUBMARINER_1_MD5='438471f35119ca0131971082f6eb805c'
ARCHIVE_BASE_GOG_ZUBMARINER_1_VERSION='2.2.4.3141-gog21326'
ARCHIVE_BASE_GOG_ZUBMARINER_1_SIZE='930000'

ARCHIVE_BASE_GOG_ZUBMARINER_0_NAME='gog_sunless_sea_zubmariner_2.5.0.6.sh'
ARCHIVE_BASE_GOG_ZUBMARINER_0_MD5='692cd0dac832d5254bd38d7e1a05b918'
ARCHIVE_BASE_GOG_ZUBMARINER_0_VERSION='2.2.2.3130-gog2.5.0.6'
ARCHIVE_BASE_GOG_ZUBMARINER_0_SIZE='870000'

## Sunless Sea (base game, gog.com)

ARCHIVE_BASE_GOG_5_NAME='sunless_sea_2_2_11_3212_64426.sh'
ARCHIVE_BASE_GOG_5_MD5='f5ab5552b618f33c5ddf0d952aa1294c'
ARCHIVE_BASE_GOG_5_VERSION='2.2.11.3212-gog64426'
ARCHIVE_BASE_GOG_5_SIZE='756028'
ARCHIVE_BASE_GOG_5_URL='https://www.gog.com/game/sunless_sea'

ARCHIVE_BASE_GOG_4_NAME='sunless_sea_v2_2_7_3165_a_53438.sh'
ARCHIVE_BASE_GOG_4_MD5='e7cdef68377026c0f61327342a6b68b4'
ARCHIVE_BASE_GOG_4_VERSION='2.2.7.3165-gog53438'
ARCHIVE_BASE_GOG_4_SIZE='760000'

ARCHIVE_BASE_GOG_3_NAME='sunless_sea_2_2_7_3165_29003.sh'
ARCHIVE_BASE_GOG_3_MD5='0feebd54ae67c772d189e19d2042fe91'
ARCHIVE_BASE_GOG_3_VERSION='2.2.7.3165-gog29003'
ARCHIVE_BASE_GOG_3_SIZE='760000'

ARCHIVE_BASE_GOG_2_NAME='sunless_sea_2_2_6_3150_24613.sh'
ARCHIVE_BASE_GOG_2_MD5='1ea56377e7636f354656c1f791f29a5c'
ARCHIVE_BASE_GOG_2_VERSION='2.2.6.3150-gog24613'
ARCHIVE_BASE_GOG_2_SIZE='760000'

ARCHIVE_BASE_GOG_1_NAME='sunless_sea_en_v2_2_4_3141_21326.sh'
ARCHIVE_BASE_GOG_1_MD5='df453a83ac1fb2767bdeafafb40f037a'
ARCHIVE_BASE_GOG_1_VERSION='2.2.4.3141-gog21326'
ARCHIVE_BASE_GOG_1_SIZE='760000'

ARCHIVE_BASE_GOG_0_NAME='gog_sunless_sea_2.8.0.11.sh'
ARCHIVE_BASE_GOG_0_MD5='1cf6bb7a440ce796abf8e7afcb6f7a54'
ARCHIVE_BASE_GOG_0_VERSION='2.2.2.3129-gog2.8.0.11'
ARCHIVE_BASE_GOG_0_SIZE='700000'

## Sunless Sea (base game, humblebundle.com)

ARCHIVE_BASE_HUMBLE_1_NAME='Sunless_Sea_Setup_V2.2.4.3141_LINUX.zip'
ARCHIVE_BASE_HUMBLE_1_MD5='076c6784bb96e4189f675f114c98ae85'
ARCHIVE_BASE_HUMBLE_1_VERSION='2.2.4.3141-humble180606'
ARCHIVE_BASE_HUMBLE_1_SIZE='760000'
ARCHIVE_BASE_HUMBLE_1_URL='https://www.humblebundle.com/store/sunless-sea'

ARCHIVE_BASE_HUMBLE_0_NAME='Sunless_Sea_Setup_V2.2.2.3129_LINUX.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='bdb37932e56fd0655a2e4263631e2582'
ARCHIVE_BASE_HUMBLE_0_VERSION='2.2.2.3129-humble170131'
ARCHIVE_BASE_HUMBLE_0_SIZE='700000'


UNITY3D_NAME='Sunless Sea'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data/noarch'
CONTENT_GAME_BIN64_PATH_HUMBLE='data/x86_64'
CONTENT_GAME_BIN32_PATH_HUMBLE='data/x86'
CONTENT_DOC_DATA_FILES='
README.linux'

## Application type can not be automatically guessed when using the Humble Bundle archive, because the game binary is not stored under CONTENT_PATH_DEFAULT.
## This application type is required to automatically generate the default files list for archive contents inclusion.
APP_MAIN_TYPE_HUMBLE='native'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_GOG_ZUBMARINER="${PKG_DATA_ID}-zubmariner"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DESCRIPTION_GOG_ZUBMARINER="$PKG_DATA_DESCRIPTION (including Zubmariner DLC)"
## Ensure smooth upgrades from packages generated with pre-20231020.1 scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
sunless-sea-zubmariner-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# humblebundle.com - Check that tools required to handle the inner archive are available.

case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_'*)
		REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
		$(archive_requirements_mojosetup_list)"
		requirements_check
	;;
esac

# Extract game data

archive_extraction_default
case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_'*)
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/Sunless Sea.sh"
		archive_extraction 'ARCHIVE_INNER'
		rm "$(archive_path 'ARCHIVE_INNER')"
	;;
esac
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## humblebundle.com - Some arch-specific files in the archive are stored in the arch-independent path.
	case "$(current_archive)" in
		('ARCHIVE_BASE_HUMBLE_'*)
			cp --link --parents --recursive \
				"$(unity3d_name)_Data/Mono/x86_64" \
				"$(unity3d_name)_Data/Plugins/x86_64" \
				"${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_BIN64')"
			cp --link --parents --recursive \
				"$(unity3d_name)_Data/Mono/x86" \
				"$(unity3d_name)_Data/Plugins/x86" \
				"${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_BIN32')"
			rm --recursive \
				"$(unity3d_name)_Data/Mono/x86_64" \
				"$(unity3d_name)_Data/Mono/x86" \
				"$(unity3d_name)_Data/Plugins/x86_64" \
				"$(unity3d_name)_Data/Plugins/x86"
			rmdir "$(unity3d_name)_Data/Plugins"
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
