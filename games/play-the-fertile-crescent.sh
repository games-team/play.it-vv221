#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Fertile Crescent
###

script_version=20241120.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-fertile-crescent'
GAME_NAME='The Fertile Crescent'

ARCHIVE_BASE_3_NAME='tfc_the_fertile_crescent_1_1001_77568.sh'
ARCHIVE_BASE_3_MD5='f6a3809b43dd63f226fe47c756e915f5'
ARCHIVE_BASE_3_SIZE='524869'
ARCHIVE_BASE_3_VERSION='1.1001-gog77568'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/tfc_the_fertile_crescent'

ARCHIVE_BASE_2_NAME='tfc_the_fertile_crescent_1_1000_75581.sh'
ARCHIVE_BASE_2_MD5='2410c776006d758dfcdfb0d961bb906f'
ARCHIVE_BASE_2_SIZE='524869'
ARCHIVE_BASE_2_VERSION='1.1000-gog75581'

ARCHIVE_BASE_1_NAME='tfc_the_fertile_crescent_1_0014_74208.sh'
ARCHIVE_BASE_1_MD5='9255deccb43f9eda5062614d0d6cf9d7'
ARCHIVE_BASE_1_SIZE='524857'
ARCHIVE_BASE_1_VERSION='1.0014-gog74208'

ARCHIVE_BASE_0_NAME='tfc_the_fertile_crescent_1_0010b_74071.sh'
ARCHIVE_BASE_0_MD5='619d9bbccea8d3b05037dacde8684533'
ARCHIVE_BASE_0_SIZE='525376'
ARCHIVE_BASE_0_VERSION='1.0010b-gog74071'

UNITY3D_NAME='game'
UNITY3D_PLUGINS='
libfmodL.so
libfmod.so
libfmodstudio.so
libresonanceaudio.so'
## The game crashes on launch if the Steam library is missing.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
