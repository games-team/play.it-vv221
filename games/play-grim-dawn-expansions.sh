#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Grim Dawn expansions:
# - Ashes of Malmouth
# - Forgotten Gods
###

script_version=20250211.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='grim-dawn'
GAME_NAME='Grim Dawn'

EXPANSION_ID_ASHES='ashes-of-malmouth'
EXPANSION_NAME_ASHES='Ashes of Malmouth'

EXPANSION_ID_GODS='forgotten-gods'
EXPANSION_NAME_GODS='Forgotten Gods'

# Archives

## Ashes of Malmouth

ARCHIVE_BASE_ASHES_9_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.5a_(79771).exe'
ARCHIVE_BASE_ASHES_9_MD5='978c79a533de6d7553044534d6085bdc'
ARCHIVE_BASE_ASHES_9_TYPE='innosetup'
ARCHIVE_BASE_ASHES_9_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.5a_(79771)-1.bin'
ARCHIVE_BASE_ASHES_9_PART1_MD5='d4f7ef400d31a460d0c671b6d0fdd9fe'
ARCHIVE_BASE_ASHES_9_SIZE='2511022'
ARCHIVE_BASE_ASHES_9_VERSION='1.2.1.5a-gog79771'
ARCHIVE_BASE_ASHES_9_URL='https://www.gog.com/game/grim_dawn_ashes_of_malmouth'

ARCHIVE_BASE_ASHES_8_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.4_hotfix_1_(78646).exe'
ARCHIVE_BASE_ASHES_8_MD5='e4b981d5a960f465fedd6c25e47434a5'
ARCHIVE_BASE_ASHES_8_TYPE='innosetup'
ARCHIVE_BASE_ASHES_8_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.4_hotfix_1_(78646)-1.bin'
ARCHIVE_BASE_ASHES_8_PART1_MD5='8f0261ab3eabece7ef4a6667964ba1d0'
ARCHIVE_BASE_ASHES_8_SIZE='2510794'
ARCHIVE_BASE_ASHES_8_VERSION='1.2.1.4-gog78646'

ARCHIVE_BASE_ASHES_7_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.2_(74537).exe'
ARCHIVE_BASE_ASHES_7_MD5='24e439773a3b6935e766110b0ec80fff'
ARCHIVE_BASE_ASHES_7_TYPE='innosetup'
ARCHIVE_BASE_ASHES_7_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.2_(74537)-1.bin'
ARCHIVE_BASE_ASHES_7_PART1_MD5='164321d7ecee354a62dc77b57a9088a0'
ARCHIVE_BASE_ASHES_7_SIZE='2510434'
ARCHIVE_BASE_ASHES_7_VERSION='1.2.1.2-gog74537'

ARCHIVE_BASE_ASHES_6_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.1_(74365).exe'
ARCHIVE_BASE_ASHES_6_MD5='01617f6a1fae6a2d8cd68061a4b7ed71'
ARCHIVE_BASE_ASHES_6_TYPE='innosetup'
ARCHIVE_BASE_ASHES_6_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.1.1_(74365)-1.bin'
ARCHIVE_BASE_ASHES_6_PART1_MD5='d687516b6ab638161aa0f8bd47416b66'
ARCHIVE_BASE_ASHES_6_SIZE='2508370'
ARCHIVE_BASE_ASHES_6_VERSION='1.2.1.1-gog74365'

ARCHIVE_BASE_ASHES_5_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.5a_(71558).exe'
ARCHIVE_BASE_ASHES_5_MD5='459923324f4f32779885fe03f192fe3a'
ARCHIVE_BASE_ASHES_5_TYPE='innosetup'
ARCHIVE_BASE_ASHES_5_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.5a_(71558)-1.bin'
ARCHIVE_BASE_ASHES_5_PART1_MD5='11d3e5371bb3b9ecceb0a72b99671a24'
ARCHIVE_BASE_ASHES_5_SIZE='2508049'
ARCHIVE_BASE_ASHES_5_VERSION='1.2.0.5a-gog71558'

ARCHIVE_BASE_ASHES_4_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.5_(71516).exe'
ARCHIVE_BASE_ASHES_4_MD5='c692033a7f8a57cfc83fe0caecf763e9'
ARCHIVE_BASE_ASHES_4_TYPE='innosetup'
ARCHIVE_BASE_ASHES_4_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.5_(71516)-1.bin'
ARCHIVE_BASE_ASHES_4_PART1_MD5='270e20c090ddf4a172b3e4c7a869222b'
ARCHIVE_BASE_ASHES_4_SIZE='2508622'
ARCHIVE_BASE_ASHES_4_VERSION='1.2.0.5-gog71516'

ARCHIVE_BASE_ASHES_3_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.3_hotfix_3_(69499).exe'
ARCHIVE_BASE_ASHES_3_MD5='921796d60998467c4c753d80f99a6584'
ARCHIVE_BASE_ASHES_3_TYPE='innosetup'
ARCHIVE_BASE_ASHES_3_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.3_hotfix_3_(69499)-1.bin'
ARCHIVE_BASE_ASHES_3_PART1_MD5='618cfba9ba745a2d3fd6e5b9ae744cb2'
ARCHIVE_BASE_ASHES_3_SIZE='2510332'
ARCHIVE_BASE_ASHES_3_VERSION='1.2.0.3-gog69499'

ARCHIVE_BASE_ASHES_2_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.2_hotfix_2_(69134).exe'
ARCHIVE_BASE_ASHES_2_MD5='49194993f6649141ac9f118d61eb8f90'
ARCHIVE_BASE_ASHES_2_TYPE='innosetup'
ARCHIVE_BASE_ASHES_2_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.2_hotfix_2_(69134)-1.bin'
ARCHIVE_BASE_ASHES_2_PART1_MD5='798f63fd270edfdac9938fd77d73752c'
ARCHIVE_BASE_ASHES_2_SIZE='2509140'
ARCHIVE_BASE_ASHES_2_VERSION='1.2.0.2-gog69134'

ARCHIVE_BASE_ASHES_1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.1_hotfix_1_(69098).exe'
ARCHIVE_BASE_ASHES_1_MD5='f8935c6d6ca9c354a9336d51efd823f9'
ARCHIVE_BASE_ASHES_1_TYPE='innosetup'
ARCHIVE_BASE_ASHES_1_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.2.0.1_hotfix_1_(69098)-1.bin'
ARCHIVE_BASE_ASHES_1_PART1_MD5='c2e7bd56299023afa261d43fb89a43f7'
ARCHIVE_BASE_ASHES_1_SIZE='2507692'
ARCHIVE_BASE_ASHES_1_VERSION='1.2.0.1-gog69098'

ARCHIVE_BASE_ASHES_0_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.1.9.8_(65199).exe'
ARCHIVE_BASE_ASHES_0_MD5='0754f567005ecc7d758d905992e5d7f7'
ARCHIVE_BASE_ASHES_0_TYPE='innosetup'
ARCHIVE_BASE_ASHES_0_PART1_NAME='setup_grim_dawn_-_ashes_of_malmouth_1.1.9.8_(65199)-1.bin'
ARCHIVE_BASE_ASHES_0_PART1_MD5='49bc0c617d4ab9415298994ba4a085b8'
ARCHIVE_BASE_ASHES_0_SIZE='2506744'
ARCHIVE_BASE_ASHES_0_VERSION='1.1.9.8-gog65199'

## Forgotten Gods

ARCHIVE_BASE_GODS_9_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.5a_(79771).exe'
ARCHIVE_BASE_GODS_9_MD5='e8850233a15d8e898c53e393f6de3226'
ARCHIVE_BASE_GODS_9_TYPE='innosetup'
ARCHIVE_BASE_GODS_9_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.5a_(79771)-1.bin'
ARCHIVE_BASE_GODS_9_PART1_MD5='6f5c139cd8416882627d1536e83c1ac8'
ARCHIVE_BASE_GODS_9_SIZE='3223745'
ARCHIVE_BASE_GODS_9_VERSION='1.2.1.5a-gog79771'
ARCHIVE_BASE_GODS_9_URL='https://www.gog.com/game/grim_dawn_forgotten_gods'

ARCHIVE_BASE_GODS_8_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.4_hotfix_1_(78646).exe'
ARCHIVE_BASE_GODS_8_MD5='e1718e47e7e3efb5b7e30720237d7ca0'
ARCHIVE_BASE_GODS_8_TYPE='innosetup'
ARCHIVE_BASE_GODS_8_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.4_hotfix_1_(78646)-1.bin'
ARCHIVE_BASE_GODS_8_PART1_MD5='e2d2f437f6cabf93953d280443a37fed'
ARCHIVE_BASE_GODS_8_SIZE='3222704'
ARCHIVE_BASE_GODS_8_VERSION='1.2.1.4-gog78646'

ARCHIVE_BASE_GODS_7_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.2_(74537).exe'
ARCHIVE_BASE_GODS_7_MD5='2d71e98f2aafdd7096b7f1b30279a7d5'
ARCHIVE_BASE_GODS_7_TYPE='innosetup'
ARCHIVE_BASE_GODS_7_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.2_(74537)-1.bin'
ARCHIVE_BASE_GODS_7_PART1_MD5='430063cc53a07cd77ac491bb22c85e3b'
ARCHIVE_BASE_GODS_7_SIZE='3220867'
ARCHIVE_BASE_GODS_7_VERSION='1.2.1.2-gog74537'

ARCHIVE_BASE_GODS_6_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.1_(74365).exe'
ARCHIVE_BASE_GODS_6_MD5='45e52392fb4f503b2078ddf6af6a3411'
ARCHIVE_BASE_GODS_6_TYPE='innosetup'
ARCHIVE_BASE_GODS_6_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.1.1_(74365)-1.bin'
ARCHIVE_BASE_GODS_6_PART1_MD5='195f5f735715d9ae59080774d0a98bd0'
ARCHIVE_BASE_GODS_6_SIZE='3221131'
ARCHIVE_BASE_GODS_6_VERSION='1.2.1.1-gog74365'

ARCHIVE_BASE_GODS_5_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.5a_(71558).exe'
ARCHIVE_BASE_GODS_5_MD5='f4e80fca2102e244a47a218171c0f336'
ARCHIVE_BASE_GODS_5_TYPE='innosetup'
ARCHIVE_BASE_GODS_5_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.5a_(71558)-1.bin'
ARCHIVE_BASE_GODS_5_PART1_MD5='3199f7442939194091a2d534553b8007'
ARCHIVE_BASE_GODS_5_SIZE='3220483'
ARCHIVE_BASE_GODS_5_VERSION='1.2.0.5a-gog71558'

ARCHIVE_BASE_GODS_4_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.5_(71516).exe'
ARCHIVE_BASE_GODS_4_MD5='cef5d93af87d3a4690bca2f78eb6e172'
ARCHIVE_BASE_GODS_4_TYPE='innosetup'
ARCHIVE_BASE_GODS_4_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.5_(71516)-1.bin'
ARCHIVE_BASE_GODS_4_PART1_MD5='8079bd9c7dfbf5234355bd9ecb56d524'
ARCHIVE_BASE_GODS_4_SIZE='3220210'
ARCHIVE_BASE_GODS_4_VERSION='1.2.0.5-gog71516'

ARCHIVE_BASE_GODS_3_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.3_hotfix_3_(69499).exe'
ARCHIVE_BASE_GODS_3_MD5='75b0ed8db6996ae3b5fb4df2177b8507'
ARCHIVE_BASE_GODS_3_TYPE='innosetup'
ARCHIVE_BASE_GODS_3_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.3_hotfix_3_(69499)-1.bin'
ARCHIVE_BASE_GODS_3_PART1_MD5='a0c5c5a18b150c8349d247c593fb71d5'
ARCHIVE_BASE_GODS_3_SIZE='3220424'
ARCHIVE_BASE_GODS_3_VERSION='1.2.0.3-gog69499'

ARCHIVE_BASE_GODS_2_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.2_hotfix_2_(69134).exe'
ARCHIVE_BASE_GODS_2_MD5='3506b7cfcbb6b475b8c182024932d332'
ARCHIVE_BASE_GODS_2_TYPE='innosetup'
ARCHIVE_BASE_GODS_2_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.2_hotfix_2_(69134)-1.bin'
ARCHIVE_BASE_GODS_2_PART1_MD5='40b9f598aa048680c9f7dc4651b99d71'
ARCHIVE_BASE_GODS_2_SIZE='3220892'
ARCHIVE_BASE_GODS_2_VERSION='1.2.0.2-gog691341'

ARCHIVE_BASE_GODS_1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.1_hotfix_1_(69098).exe'
ARCHIVE_BASE_GODS_1_MD5='f0809d0007e3e3634b24dcdbf2bf25fd'
ARCHIVE_BASE_GODS_1_TYPE='innosetup'
ARCHIVE_BASE_GODS_1_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.2.0.1_hotfix_1_(69098)-1.bin'
ARCHIVE_BASE_GODS_1_PART1_MD5='82c069ab14fcec774fb075706fa3cce1'
ARCHIVE_BASE_GODS_1_SIZE='3218872'
ARCHIVE_BASE_GODS_1_VERSION='1.2.0.1-gog69098'

ARCHIVE_BASE_GODS_0_NAME='setup_grim_dawn_-_forgotten_gods_1.1.9.8_(65199).exe'
ARCHIVE_BASE_GODS_0_MD5='6615eb32a355092c4d0173457f701e31'
ARCHIVE_BASE_GODS_0_TYPE='innosetup'
ARCHIVE_BASE_GODS_0_PART1_NAME='setup_grim_dawn_-_forgotten_gods_1.1.9.8_(65199)-1.bin'
ARCHIVE_BASE_GODS_0_PART1_MD5='55ac480dee30aa6cfa6a8b466f521406'
ARCHIVE_BASE_GODS_0_SIZE='3220672'
ARCHIVE_BASE_GODS_0_VERSION='1.1.9.8-gog65199'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
gdx?
survivalmode?'

PKG_PARENT_ID="$GAME_ID"

PKG_ASH_ID="${GAME_ID}-${EXPANSION_ID_ASHES}"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'
PKG_MAIN_DEPENDENCIES_SIBLINGS_GODS="$PKG_MAIN_DEPENDENCIES_SIBLINGS
PKG_ASH"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
