#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Freelancer
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='freelancer'
GAME_NAME='Freelancer'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_DEMO_EN_0_NAME='freelancer_demo.exe'
ARCHIVE_BASE_DEMO_EN_0_MD5='1af0a4cc730a64de9f6a6ecde30edc11'
ARCHIVE_BASE_DEMO_EN_0_TYPE='cabinet'
ARCHIVE_BASE_DEMO_EN_0_VERSION='1.0-archiveorg1'
ARCHIVE_BASE_DEMO_EN_0_SIZE='450000'
ARCHIVE_BASE_DEMO_EN_0_URL='https://archive.org/details/freelancer_demo'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_RELATIVE_PATH='game'
CONTENT_GAME_BIN_FILES='
exe/freelancer.exe'
CONTENT_GAME0_BIN_PATH='cab1'
CONTENT_GAME0_BIN_FILES='
dlls
exe/1033
exe/alchemy.dll
exe/common.dll
exe/dacom.dll
exe/dalib.dll
exe/debuglib.dll
exe/deformable2.dll
exe/ebueula.dll
exe/engbase.dll
exe/flmaterials.dll
exe/gundll.dll
exe/imeui.dll
exe/luaprofile.dll
exe/movie.dll
exe/msxml3a.dll
exe/msxml3.dll
exe/msxml3r.dll
exe/readfile.dll
exe/remoteclient.dll
exe/remoteserver.dll
exe/rendcomp.dll
exe/rp8.dll
exe/rpclocal.dll
exe/server.dll
exe/shading.dll
exe/soundmanager.dll
exe/soundstreamer.dll
exe/system.dll
exe/thorn.dll
exe/unicows.dll
exe/x86math.dll
exe/ximage.dll
exe/zlib.dll
exe/*.bmp
exe/*.exe
exe/*.fl
exe/*.hta
exe/*.ini
exe/*.txt'
CONTENT_GAME_L10N_PATH='cab1'
CONTENT_GAME_L10N_FILES='
data/audio/dialogue
data/audio/mixes
data/audio/sounds/ui/news_vendor_open.wav
data/audio/*.ini
data/audio/*.utf
exe/equipresources.dll
exe/infocards.dll
exe/misctext.dll
exe/misctextinfo2.dll
exe/nameresources.dll
exe/offerbriberesources.dll
exe/resources.dll
exe/serverresources.dll'
CONTENT_GAME0_L10N_PATH='cab2'
CONTENT_GAME0_L10N_FILES='
data/audio/dialogue
data/audio/mixes
data/audio/sounds/ui/news_vendor_open.wav
data/audio/*.ini
data/audio/*.utf'
CONTENT_FONTS_DATA_PATH='fonts'
CONTENT_FONTS_DATA_FILES='
agencyb.ttf
agencyr.ttf
arialuni.ttf'
CONTENT_GAME_DATA_FILES='
fl.ico'
CONTENT_GAME0_DATA_PATH='cab1'
CONTENT_GAME0_DATA_FILES='
data'
CONTENT_GAME1_DATA_PATH='cab2'
CONTENT_GAME1_DATA_FILES='
data'
CONTENT_DOC_DATA_FILES='
eula.rtf
readme.rtf'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Freelancer Trial'

APP_MAIN_EXE='exe/freelancer.exe'
APP_MAIN_ICON='fl.ico'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
deinterlace
video/x-ms-asf'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_BASE_ID_DEMO="${GAME_ID_DEMO}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_DEMO_EN="${PKG_L10N_BASE_ID_DEMO}-en"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_PROVIDES_DEMO="
$PKG_L10N_BASE_ID_DEMO"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_DEMO_EN='English localization'

## TODO: Prevent the game from messing up with the desktop gamma values.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
ARCHIVE_INNER1_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/cab1.cab"
archive_extraction 'ARCHIVE_INNER1'
rm "$(archive_path 'ARCHIVE_INNER1')"
ARCHIVE_INNER2_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/cab2.cab"
archive_extraction 'ARCHIVE_INNER2'
rm "$(archive_path 'ARCHIVE_INNER2')"
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_BIN')"

	## Rename files with truncated names.
	mv 'exe/freela_1.exe' 'exe/freelancer.exe'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
