#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Cultist Simulator
###

script_version=20241123.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='cultist-simulator'
GAME_NAME='Cultist Simulator'

ARCHIVE_BASE_19_NAME='cultist_simulator_2024_7_b_3_77913.sh'
ARCHIVE_BASE_19_MD5='7dec8c03b9f2ec9136ead667cfd4fb73'
ARCHIVE_BASE_19_SIZE='707969'
ARCHIVE_BASE_19_VERSION='2024.07.b.3-gog77913'
ARCHIVE_BASE_19_URL='https://www.gog.com/game/cultist_simulator'

ARCHIVE_BASE_18_NAME='cultist_simulator_2024_5_a_3_74582.sh'
ARCHIVE_BASE_18_MD5='4a6d4118f5545f95119a0f6fedcf1e16'
ARCHIVE_BASE_18_SIZE='707816'
ARCHIVE_BASE_18_VERSION='2024.05.a.3-gog74582'

ARCHIVE_BASE_17_NAME='cultist_simulator_2023_12_s_5_71509.sh'
ARCHIVE_BASE_17_MD5='7e5fd05666c22c1b44401525e2fd24f1'
ARCHIVE_BASE_17_SIZE='707815'
ARCHIVE_BASE_17_VERSION='2023.12.s.5-gog71509'

ARCHIVE_BASE_16_NAME='cultist_simulator_2023_12_s_4_71175.sh'
ARCHIVE_BASE_16_MD5='b34cef2cea237aff1b4ff33fe6acdece'
ARCHIVE_BASE_16_SIZE='707816'
ARCHIVE_BASE_16_VERSION='2023.12.s.4-gog71175'

ARCHIVE_BASE_15_NAME='cultist_simulator_2023_10_r_10_68985.sh'
ARCHIVE_BASE_15_MD5='4c9a58da5701edb9af42b44dedff55d6'
ARCHIVE_BASE_15_SIZE='711252'
ARCHIVE_BASE_15_VERSION='2023.10.r.10-gog68985'

ARCHIVE_BASE_14_NAME='cultist_simulator_2023_5_p_12_67501.sh'
ARCHIVE_BASE_14_MD5='9b849e52ae2c13a0911f2ff78b474264'
ARCHIVE_BASE_14_SIZE='709880'
ARCHIVE_BASE_14_VERSION='2023.05.p.12-gog67501'

ARCHIVE_BASE_13_NAME='cultist_simulator_2023_4_o_4_64959.sh'
ARCHIVE_BASE_13_MD5='4f3a1b1fa787e3b72fd22538051bc064'
ARCHIVE_BASE_13_SIZE='710000'
ARCHIVE_BASE_13_VERSION='2023.04.o.4-gog64959'

ARCHIVE_BASE_12_NAME='cultist_simulator_2022_12_n_2_61763.sh'
ARCHIVE_BASE_12_MD5='852b96c2da6b07d15fa533b3dbb0f316'
ARCHIVE_BASE_12_SIZE='780000'
ARCHIVE_BASE_12_VERSION='2022.12.n.2-gog61763'

ARCHIVE_BASE_11_NAME='cultist_simulator_2022_3_p_1_54545.sh'
ARCHIVE_BASE_11_MD5='6009cfa53c5be9da1e02380d5bcaf712'
ARCHIVE_BASE_11_SIZE='750000'
ARCHIVE_BASE_11_VERSION='2022.03.p.1-gog54545'

ARCHIVE_BASE_10_NAME='cultist_simulator_2022_3_n_1_54418.sh'
ARCHIVE_BASE_10_MD5='0c8e9dedb7b2e5112f7172c9a9095c18'
ARCHIVE_BASE_10_SIZE='750000'
ARCHIVE_BASE_10_VERSION='2022.03.n.1-gog54418'

ARCHIVE_BASE_9_NAME='cultist_simulator_2021_1_b_2_44604.sh'
ARCHIVE_BASE_9_MD5='cbe92b1dee271416ffa4ad29ef4b9123'
ARCHIVE_BASE_9_SIZE='540000'
ARCHIVE_BASE_9_VERSION='2021.01.b.2-gog44604'

ARCHIVE_BASE_8_NAME='cultist_simulator_2020_11_a_1_42424.sh'
ARCHIVE_BASE_8_MD5='76781c87835954da4a97ec7e0efccfe2'
ARCHIVE_BASE_8_SIZE='540000'
ARCHIVE_BASE_8_VERSION='2020.11.a.1-gog42424'

ARCHIVE_BASE_7_NAME='cultist_simulator_2020_10_e_2_42258.sh'
ARCHIVE_BASE_7_MD5='a2a2de8222b9993fc0ae5ef0eaed2b66'
ARCHIVE_BASE_7_SIZE='540000'
ARCHIVE_BASE_7_VERSION='2020.10.e.2-gog42258'

ARCHIVE_BASE_6_NAME='cultist_simulator_2020_10_e_1_42177.sh'
ARCHIVE_BASE_6_MD5='1a16264b6ce868c0f741b6614d84d684'
ARCHIVE_BASE_6_SIZE='540000'
ARCHIVE_BASE_6_VERSION='2020.10.e.1-gog42177'

ARCHIVE_BASE_5_NAME='cultist_simulator_2020_10_b_1_42011.sh'
ARCHIVE_BASE_5_MD5='672409328bd154b4430826c09b58dd74'
ARCHIVE_BASE_5_SIZE='530000'
ARCHIVE_BASE_5_VERSION='2020.10.b.1-gog42011'

ARCHIVE_BASE_4_NAME='cultist_simulator_2020_9_p_1_bis_41797.sh'
ARCHIVE_BASE_4_MD5='9683f0726cba2b116db4134aec863382'
ARCHIVE_BASE_4_SIZE='520000'
ARCHIVE_BASE_4_VERSION='2020.09.p.1-gog41797'

ARCHIVE_BASE_3_NAME='cultist_simulator_2020_9_n_5_41650.sh'
ARCHIVE_BASE_3_MD5='251b5e2d58faaea7132b57ad97495057'
ARCHIVE_BASE_3_SIZE='510000'
ARCHIVE_BASE_3_VERSION='2020.09.n.5-gog41650'

ARCHIVE_BASE_2_NAME='cultist_simulator_2020_6_b_1_38747.sh'
ARCHIVE_BASE_2_MD5='22980acaa3f825d0712621624277d4ed'
ARCHIVE_BASE_2_SIZE='500000'
ARCHIVE_BASE_2_VERSION='2020.06.b.1-gog38747'

ARCHIVE_BASE_1_NAME='cultist_simulator_2020_6_a_1_38655.sh'
ARCHIVE_BASE_1_MD5='099e8a6ed1ae7a9bd654ba63ab89c4a7'
ARCHIVE_BASE_1_SIZE='500000'
ARCHIVE_BASE_1_VERSION='2020.06.a.1-gog38655'

ARCHIVE_BASE_0_NAME='cultist_simulator_2020_3_b_1_37119.sh'
ARCHIVE_BASE_0_MD5='4390c258dcce415d61f96eac66a728ca'
ARCHIVE_BASE_0_SIZE='470000'
ARCHIVE_BASE_0_VERSION='2020.03.b.1-gog37119'

UNITY3D_NAME='CS'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_DATA_FILES='
version.txt'
CONTENT_DOC_DATA_FILES='
README'

## Write access to some game files is required.
USER_PERSISTENT_DIRECTORIES="
${UNITY3D_NAME}_Data/StreamingAssets/content/core/cultures"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
