#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Unavowed
###

script_version=20241209.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='unavowed'
GAME_NAME='Unavowed'

ARCHIVE_BASE_9_NAME='unavowed_2_2_78264.sh'
ARCHIVE_BASE_9_MD5='6a60648441f57342e92a6bb0a4dc462a'
ARCHIVE_BASE_9_SIZE='2310225'
ARCHIVE_BASE_9_VERSION='2.2-gog78264'
ARCHIVE_BASE_9_URL='https://www.gog.com/game/unavowed'

ARCHIVE_BASE_8_NAME='unavowed_2_1_75524.sh'
ARCHIVE_BASE_8_MD5='07926f2dd86460eae439d2d16f87e76d'
ARCHIVE_BASE_8_SIZE='2310225'
ARCHIVE_BASE_8_VERSION='2.1-gog75524'

ARCHIVE_BASE_7_NAME='unavowed_2_0_2_70667.sh'
ARCHIVE_BASE_7_MD5='00c9f35c3e3e506f8238de49c8ac781a'
ARCHIVE_BASE_7_SIZE='2310224'
ARCHIVE_BASE_7_VERSION='2.0.2-gog70667'

ARCHIVE_BASE_6_NAME='unavowed_2_0_2_70012.sh'
ARCHIVE_BASE_6_MD5='a9cfabc33e91f0d7b7649e302966b591'
ARCHIVE_BASE_6_SIZE='2310224'
ARCHIVE_BASE_6_VERSION='2.0.2-gog70012'

ARCHIVE_BASE_5_NAME='unavowed_2_0_1_66601.sh'
ARCHIVE_BASE_5_MD5='bacc5d7cf9da4970da580c7ee43e1a2e'
ARCHIVE_BASE_5_SIZE='2400000'
ARCHIVE_BASE_5_VERSION='2.0.1-gog66601'

ARCHIVE_BASE_4_NAME='unavowed_2_0_66472.sh'
ARCHIVE_BASE_4_MD5='771d43bf86a85da9a70d02da80a6eb01'
ARCHIVE_BASE_4_SIZE='2400000'
ARCHIVE_BASE_4_VERSION='2.0-gog66472'

ARCHIVE_BASE_3_NAME='unavowed_1_5_64507.sh'
ARCHIVE_BASE_3_MD5='0029204a1edc703c2fc90bcc2741c442'
ARCHIVE_BASE_3_SIZE='2500000'
ARCHIVE_BASE_3_VERSION='1.5-gog64507'

ARCHIVE_BASE_2_NAME='unavowed_1_4_57370.sh'
ARCHIVE_BASE_2_MD5='c785b049e60d60e45296acda30bd9aa2'
ARCHIVE_BASE_2_SIZE='2500000'
ARCHIVE_BASE_2_VERSION='1.4-gog57370'

ARCHIVE_BASE_1_NAME='unavowed_1_32_55233.sh'
ARCHIVE_BASE_1_MD5='6d92c60a1a3406f619b4fb4c204adecc'
ARCHIVE_BASE_1_SIZE='2500000'
ARCHIVE_BASE_1_VERSION='1.3.2-gog55233'

ARCHIVE_BASE_0_NAME='unavowed_1_31_54237.sh'
ARCHIVE_BASE_0_MD5='f0060c808571a4324714c69f653a4caa'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.3.1-gog54237'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='unavowed_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='359eae49cc167a34a1cc5525cf6ab340'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/unavowed/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
24x24
32x32
48x48
256x256'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
ENGV.tmp
Music
Sounds
*.ags
*.cfg
*.exe
*.ogv
*.tra
*.vox'
CONTENT_DOC_MAIN_FILES='
licenses'

APP_MAIN_SCUMMID='ags:unavowed'
APP_MAIN_ICON='../support/icon.png'
APP_MAIN_ICON_3='Unavowed.exe'
APP_MAIN_ICON_2='Unavowed.exe'
APP_MAIN_ICON_1='Unavowed.exe'
APP_MAIN_ICON_0='Unavowed.exe'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
