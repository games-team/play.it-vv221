#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Renowned Explorers: International Society
###

script_version=20250104.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='renowned-explorers'
GAME_NAME='Renowned Explorers: International Society'

ARCHIVE_BASE_LINUX_9_NAME='renowned_explorers_international_society_522_26056.sh'
ARCHIVE_BASE_LINUX_9_MD5='fe38ae1c4dc2607923cc2a60019bff38'
ARCHIVE_BASE_LINUX_9_SIZE='1107554'
ARCHIVE_BASE_LINUX_9_VERSION='522-gog26056'
ARCHIVE_BASE_LINUX_9_URL='https://www.gog.com/game/renowned_explorers'

ARCHIVE_BASE_LINUX_8_NAME='renowned_explorers_international_society_520_25983.sh'
ARCHIVE_BASE_LINUX_8_MD5='2af1dedb29ac1b929971cc0912722760'
ARCHIVE_BASE_LINUX_8_SIZE='1200000'
ARCHIVE_BASE_LINUX_8_VERSION='520-gog25983'

ARCHIVE_BASE_LINUX_7_NAME='renowned_explorers_international_society_516_25864.sh'
ARCHIVE_BASE_LINUX_7_MD5='d868d4b76613b93a94650b750a52752f'
ARCHIVE_BASE_LINUX_7_SIZE='1200000'
ARCHIVE_BASE_LINUX_7_VERSION='516-gog25864'

ARCHIVE_BASE_LINUX_6_NAME='renowned_explorers_international_society_512_25169.sh'
ARCHIVE_BASE_LINUX_6_MD5='3f2eb242da5200a78c53162d152a3cac'
ARCHIVE_BASE_LINUX_6_SIZE='1100000'
ARCHIVE_BASE_LINUX_6_VERSION='512-gog25169'

ARCHIVE_BASE_LINUX_5_NAME='renowned_explorers_international_society_508_23701.sh'
ARCHIVE_BASE_LINUX_5_MD5='247551613c7aba4b4b31f7a98fa31949'
ARCHIVE_BASE_LINUX_5_SIZE='1100000'
ARCHIVE_BASE_LINUX_5_VERSION='508-gog23701'

ARCHIVE_BASE_LINUX_4_NAME='renowned_explorers_international_society_503_23529.sh'
ARCHIVE_BASE_LINUX_4_MD5='6b7555749bc89cc3dda223e2d43bd838'
ARCHIVE_BASE_LINUX_4_SIZE='1100000'
ARCHIVE_BASE_LINUX_4_VERSION='503-gog23529'

ARCHIVE_BASE_LINUX_3_NAME='renowned_explorers_international_society_en_489_21590.sh'
ARCHIVE_BASE_LINUX_3_MD5='9fb2cbe095d437d788eb8ec6402db20b'
ARCHIVE_BASE_LINUX_3_SIZE='1100000'
ARCHIVE_BASE_LINUX_3_VERSION='489-gog21590'

ARCHIVE_BASE_LINUX_2_NAME='renowned_explorers_international_society_en_489_20916.sh'
ARCHIVE_BASE_LINUX_2_MD5='42d0ecb54d8302545e78f41ed43acef6'
ARCHIVE_BASE_LINUX_2_SIZE='1100000'
ARCHIVE_BASE_LINUX_2_VERSION='489-gog20916'

ARCHIVE_BASE_LINUX_1_NAME='renowned_explorers_international_society_en_466_15616.sh'
ARCHIVE_BASE_LINUX_1_MD5='fbad4b4d361a0e7d29b9781e3c5a5e85'
ARCHIVE_BASE_LINUX_1_SIZE='1100000'
ARCHIVE_BASE_LINUX_1_VERSION='466-gog15616'

ARCHIVE_BASE_LINUX_0_NAME='renowned_explorers_international_society_en_459_14894.sh'
ARCHIVE_BASE_LINUX_0_MD5='ff6b368b3919002d2db750213d33fcef'
ARCHIVE_BASE_LINUX_0_SIZE='1100000'
ARCHIVE_BASE_LINUX_0_VERSION='459-gog14894'

ARCHIVE_BASE_WINDOWS_0_NAME='setup_renowned_explorers_international_society_525_(64bit)_(46550).exe'
ARCHIVE_BASE_WINDOWS_0_MD5='dbb958ec50e3e39d282a92dac1b0953a'
ARCHIVE_BASE_WINDOWS_0_TYPE='innosetup'
ARCHIVE_BASE_WINDOWS_0_SIZE='1114877'
ARCHIVE_BASE_WINDOWS_0_VERSION='525-gog46550'
ARCHIVE_BASE_WINDOWS_0_URL='https://www.gog.com/game/renowned_explorers'

CONTENT_PATH_DEFAULT_LINUX='data/noarch/game'
CONTENT_PATH_DEFAULT_WINDOWS='.'
CONTENT_GAME_BIN64_FILES_LINUX='
x86_64/abbeycore'
CONTENT_GAME_BIN64_FILES_WINDOWS='
win64/amd_ags_x64.dll
win64/d3d11.dll
win64/d3dcompiler_47.dll
win64/dxgi.dll
win64/abbeycore_win32.exe'
## TODO: Check if the Galaxy libraries are required
CONTENT_GAME0_BIN64_FILES_WINDOWS='
win64/galaxy64.dll
win64/galaxypeer64.dll
win64/goggalaxyhooks.dll'
CONTENT_GAME_BIN32_FILES_LINUX='
x86/abbeycore'
CONTENT_GAME_BIN32_FILES_WINDOWS='
win32/amd_ags_x86.dll
win32/d3d11.dll
win32/d3dcompiler_47.dll
win32/dxgi.dll
win32/abbeycore_win32.exe'
## TODO: Check if the Galaxy libraries are required
CONTENT_GAME0_BIN32_FILES_WINDOWS='
win32/galaxy.dll
win32/galaxypeer.dll
win32/goggalaxyhooks.dll'
CONTENT_GAME_DATA_FILES='
data
soundbanks
settings.ini
*.bni'

## The game renders only a black screen and the mouse cursor
## when using the default wined3d renderer (WINE 10.0).
WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Renowned Explorers International Society'

## The game seems to save files randomly in either "savedata" or "userdata".
## We save both, even if it will probably end up using only one.
USER_PERSISTENT_DIRECTORIES_LINUX='
savedata
userdata'

APP_MAIN_EXE_BIN64_LINUX='x86_64/abbeycore'
APP_MAIN_EXE_BIN64_WINDOWS='win64/abbeycore_win32.exe'
APP_MAIN_EXE_BIN32_LINUX='x86/abbeycore'
APP_MAIN_EXE_BIN32_WINDOWS='win32/abbeycore_win32.exe'
APP_MAIN_ICON_LINUX='../support/icon.png'
APP_MAIN_ICON_WINDOWS='app/goggame-1435329098.ico'
## Work around inconsistent behaviour with some locales
APP_MAIN_PRERUN='
# Work around inconsistent behaviour with some locales
export LANG=C
'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN_ID="${GAME_ID}-bin"
PKG_BIN_ID_LINUX="${PKG_BIN_ID}-linux"
PKG_BIN_ID_WINDOWS="${PKG_BIN_ID}-windows"
PKG_BIN64_ID_LINUX="$PKG_BIN_ID_LINUX"
PKG_BIN64_ID_WINDOWS="$PKG_BIN_ID_WINDOWS"
PKG_BIN32_ID_LINUX="$PKG_BIN_ID_LINUX"
PKG_BIN32_ID_WINDOWS="$PKG_BIN_ID_WINDOWS"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN64_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN32_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX='
libc++abi.so.1
libc++.so.1
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES_LINUX="$PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX"
PKG_BIN32_DEPENDENCIES_LIBRARIES_LINUX="$PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX"
## Ensure easy upgrades from packages generated with pre-20250104.1 game scripts
PKG_BIN_PROVIDES="${PKG_BIN_PROVIDES:-}
renowned-explorers"
PKG_BIN64_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN32_PROVIDES="$PKG_BIN_PROVIDES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_LINUX="${PKG_DATA_ID}-linux"
PKG_DATA_ID_WINDOWS="${PKG_DATA_ID}-windows"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

APP_MAIN_EXE_BIN64=$(context_value 'APP_MAIN_EXE_BIN64')
APP_MAIN_EXE_BIN32=$(context_value 'APP_MAIN_EXE_BIN32')
launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
