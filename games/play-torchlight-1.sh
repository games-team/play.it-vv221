#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Torchlight
###

script_version=20241229.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='torchlight-1'
GAME_NAME='Torchlight'

ARCHIVE_BASE_1_NAME='setup_torchlight_1.15(a)_(23675).exe'
ARCHIVE_BASE_1_MD5='a29e51f55aae740f4046d227d33fa64b'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='453318'
ARCHIVE_BASE_1_VERSION='1.15-gog23675'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/torchlight'

ARCHIVE_BASE_0_NAME='setup_torchlight_2.0.0.12.exe'
ARCHIVE_BASE_0_MD5='4b721e1b3da90f170d66f42e60a3fece'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='460000'
ARCHIVE_BASE_0_VERSION='1.15-gog2.0.0.12'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
plugins.cfg
resources.cfg
ceguibase.dll
ceguiexpatparser.dll
ceguifalagardwrbase.dll
cg.dll
d3dx9_39.dll
fmodex.dll
msvcp90.dll
msvcr90.dll
ogreguirenderer.dll
ogremain.dll
ois.dll
particleuniverse.dll
plugin_cgprogrammanager.dll
plugin_octreescenemanager.dll
plugin_particlefx.dll
referenceapplayer.dll
rendersystem_direct3d9.dll
rendersystem_gl.dll
torchlight.exe'
CONTENT_GAME_DATA_FILES='
icons
music
programs
logo.bmp
runicgames.ico
torchlight.ico
buildver.txt
pak.zip'
CONTENT_DOC_DATA_FILES='
*.pdf'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/runic games/torchlight'

APP_MAIN_EXE='torchlight.exe'
APP_MAIN_ICON='torchlight.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
