#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2019 BetaRays
set -o errexit

###
# The Witcher
###

script_version=20241121.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-witcher-1'
GAME_NAME='The Witcher'

ARCHIVE_BASE_1_NAME='setup_the_witcher_enhanced_edition_directors_cut_1.5_(cs)_gog_0.2_(77554).exe'
ARCHIVE_BASE_1_MD5='3d8467c76abe8cd16711ada268e1600f'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_the_witcher_enhanced_edition_directors_cut_1.5_(cs)_gog_0.2_(77554)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='149adafe38e79c246103e664613558c7'
ARCHIVE_BASE_1_PART2_NAME='setup_the_witcher_enhanced_edition_directors_cut_1.5_(cs)_gog_0.2_(77554)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='baa94a6d83dc3a4703d8c414b4442ce5'
ARCHIVE_BASE_1_PART3_NAME='setup_the_witcher_enhanced_edition_directors_cut_1.5_(cs)_gog_0.2_(77554)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='5b74090cd3314dd0db1849c0dce5aa84'
ARCHIVE_BASE_1_SIZE='14781823'
ARCHIVE_BASE_1_VERSION='1.5.726-gog77554'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_witcher'

ARCHIVE_BASE_0_NAME='setup_the_witcher_enhanced_edition_1.5_(a)_(10712).exe'
ARCHIVE_BASE_0_MD5='2440cfb5fb4890ff4b9bc4b88b434d38'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_the_witcher_enhanced_edition_1.5_(a)_(10712)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='e530a1a2e86094740b45a14f63260804'
ARCHIVE_BASE_0_PART2_NAME='setup_the_witcher_enhanced_edition_1.5_(a)_(10712)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='fb3a478bcb6e4702e1e8d392cb55391d'
ARCHIVE_BASE_0_PART3_NAME='setup_the_witcher_enhanced_edition_1.5_(a)_(10712)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='2df8369af401815a736f5d88f85fbf8d'
ARCHIVE_BASE_0_SIZE='15000000'
ARCHIVE_BASE_0_VERSION='1.5.726-gog10712'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_witcher'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
installed
launcher
register
system
thewitchergdf.dll'
## TODO: Check if these binaries are required
CONTENT_GAME0_BIN_FILES='
launcher.exe
register.exe'
CONTENT_GAME_VOICES_FILES='
data/voices_*.bif'
CONTENT_GAME_DATA_FILES='
data'
CONTENT_DOC_DATA_FILES='
manual.pdf
readme.rtf
release.txt'
## TODO: Move the add-ons to a dedicated package.
CONTENT_ADDONS_DATA_RELATIVE_PATH='__support/add/the witcher'
CONTENT_ADDONS_DATA_FILES='
*.adv'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/The Witcher'
WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\CD Projekt RED\Witcher\Bindings
HKEY_CURRENT_USER\Software\CD Projekt RED\Witcher\Settings'
## Work around texture display problems.
## cf. https://bugs.winehq.org/show_bug.cgi?id=46553
WINE_WINETRICKS_VERBS='d3dx9_35'
## Work around invisible models and flickering.
## cf. https://bugs.winehq.org/show_bug.cgi?id=34052
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} cfc=enabled"

APP_MAIN_EXE='system/witcher.exe'
## Include "Enhanced Edition" add-ons
APP_MAIN_PRERUN='# Include "Enhanced Edition" add-ons
addons_destination="${WINEPREFIX}/drive_c/users/Public/Documents/the witcher"
if [ ! -e "$addons_destination" ]; then
	install -D --mode=644 \
		--target-directory="$addons_destination" \
		"${PATH_GAME_DATA}/addons"/*.adv
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_VOICES
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_VOICES_ID="${GAME_ID}-voices"
PKG_VOICES_DESCRIPTION='voices'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_VOICES'

# Set required registry keys

# shellcheck disable=SC1003
install_folder='C:\\'"${GAME_ID}"'\\'

registry_dump_init_file='registry-dumps/init.reg'
registry_dump_init_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\CD Projekt Red\The Witcher]
"InstallFolder"="'"${install_folder}"'"
"IsDjinniInstalled"=dword:00000001
"Language"="3"
"RegionVersion"="WE"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_init_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set required registry keys
	mkdir --parents "$(dirname "$registry_dump_init_file")"
	printf '%s' "$registry_dump_init_content" |
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_init_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion 'ADDONS_DATA' 'PKG_DATA' "$(path_game_data)/addons"
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
