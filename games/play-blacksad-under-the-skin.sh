#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Blacksad: Under the Skin
###

script_version=20241230.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='blacksad-under-the-skin'
GAME_NAME='Blacksad: Under the Skin'

ARCHIVE_BASE_2_NAME='setup_blacksad_under_the_skin_20270705_blacksad_patch_(74700).exe'
ARCHIVE_BASE_2_MD5='70a5fc8fb6321cf832154a6db74366f5'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_blacksad_under_the_skin_20270705_blacksad_patch_(74700)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='0d258ccdb79b34e9124108c73a18b49f'
ARCHIVE_BASE_2_PART2_NAME='setup_blacksad_under_the_skin_20270705_blacksad_patch_(74700)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='3ef508b9e836742dc5ead2519799b5b5'
ARCHIVE_BASE_2_PART3_NAME='setup_blacksad_under_the_skin_20270705_blacksad_patch_(74700)-3.bin'
ARCHIVE_BASE_2_PART3_MD5='c0a9371f54b7dd6150ecdf4d7f8c9a09'
ARCHIVE_BASE_2_PART4_NAME='setup_blacksad_under_the_skin_20270705_blacksad_patch_(74700)-4.bin'
ARCHIVE_BASE_2_PART4_MD5='5d9e0d6b7b1af4c423f44e599ce70fa1'
ARCHIVE_BASE_2_SIZE='15210283'
## "20270705" is most probably a typo for "20240705".
ARCHIVE_BASE_2_VERSION='2024.07.05-gog74700'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/blacksad_under_the_skin'

ARCHIVE_BASE_1_NAME='setup_blacksad_under_the_skin_20240621_(74269).exe'
ARCHIVE_BASE_1_MD5='9e1bafb70073291b5bbf554ba9f48cb7'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_blacksad_under_the_skin_20240621_(74269)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='19b038f25821bb10fe8439af45db294b'
ARCHIVE_BASE_1_PART2_NAME='setup_blacksad_under_the_skin_20240621_(74269)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='9085f255243fd1eddc058ded6a69da97'
ARCHIVE_BASE_1_PART3_NAME='setup_blacksad_under_the_skin_20240621_(74269)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='14e9c32b5fbb26a4c004cca8175dfd42'
ARCHIVE_BASE_1_PART4_NAME='setup_blacksad_under_the_skin_20240621_(74269)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='b1f14b3ae72154f77f0905f1c51e5bcb'
ARCHIVE_BASE_1_SIZE='15208271'
ARCHIVE_BASE_1_VERSION='2024.06.21-gog74269'

ARCHIVE_BASE_0_NAME='setup_blacksad_under_the_skin_1.0.5_12210.2935.2020040301_(37312).exe'
ARCHIVE_BASE_0_MD5='9c9211aac30170bf4ac85950094f99a2'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_blacksad_under_the_skin_1.0.5_12210.2935.2020040301_(37312)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='0d21ed49ef3377b4816ae364cbb5eee1'
ARCHIVE_BASE_0_PART2_NAME='setup_blacksad_under_the_skin_1.0.5_12210.2935.2020040301_(37312)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='58e82951055a5c863cd804f2421e729f'
ARCHIVE_BASE_0_PART3_NAME='setup_blacksad_under_the_skin_1.0.5_12210.2935.2020040301_(37312)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='acf9dc21ffa4d483b124561e5b19633d'
ARCHIVE_BASE_0_PART4_NAME='setup_blacksad_under_the_skin_1.0.5_12210.2935.2020040301_(37312)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='d1c80d50a67c2a4212e9898416193562'
ARCHIVE_BASE_0_SIZE='16000000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog37312'

UNITY3D_NAME='blacksad'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_ASSETS_FILES="
${UNITY3D_NAME}_data/streamingassets"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/PenduloStudios/Blacksad'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_ASSETS
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_ASSETS'

PKG_DATA_ASSETS_ID="${PKG_DATA_ID}-assets"
PKG_DATA_ASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - assets"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
