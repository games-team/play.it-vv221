#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Little Inferno expansions:
# - Ho Ho Holiday
###

script_version=20241227.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='little-inferno'
GAME_NAME='Little Inferno'

EXPANSION_ID_HOHOHOLIDAY='ho-ho-holiday'
EXPANSION_NAME_HOHOHOLIDAY='Ho Ho Holiday'

# Archives

## Ho Ho Holiday

ARCHIVE_BASE_HOHOHOLIDAY_0_NAME='LittleInfernoHoHoHoliday-Linux-2022-12-07.sh'
ARCHIVE_BASE_HOHOHOLIDAY_0_MD5='7124105953b1a899f239a0fdbd492458'
ARCHIVE_BASE_HOHOHOLIDAY_0_SIZE='381'
ARCHIVE_BASE_HOHOHOLIDAY_0_VERSION='1.0-humble.2022.12.07'
ARCHIVE_BASE_HOHOHOLIDAY_0_URL='http://tomorrowcorporation.com/littleinferno#hohoholiday'


CONTENT_PATH_DEFAULT='data/noarch'
CONTENT_GAME_MAIN_FILES='
.tcgame-4097.info'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
