#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rayman
###

script_version=20250103.2

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='rayman-1'
GAME_NAME='Rayman'

ARCHIVE_BASE_1_NAME='setup_rayman_forever_1.21_(28045).exe'
ARCHIVE_BASE_1_MD5='304cca5f14923730c76bb61f669be575'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='279703'
ARCHIVE_BASE_1_VERSION='1.21-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/rayman_forever'

ARCHIVE_BASE_0_NAME='setup_rayman_forever_2.0.0.15.exe'
ARCHIVE_BASE_0_MD5='96e71ea03261646f7f5ce4cb27d6a222'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='290000'
ARCHIVE_BASE_0_VERSION='1.21-gog2.0.0.15'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
game.gog
game.ins
music
rayfan
raykit
rayman'
CONTENT_DOC_DATA_FILES='
manual.pdf'

GAME_IMAGE='game.ins'

USER_PERSISTENT_FILES='
rayman/vignet.dat
*.cfg
*.SAV'

APP_MAIN_EXE='rayman/rayman.exe'
APP_MAIN_OPTIONS='save=C:\rayman'
APP_MAIN_ICON='goggame-1207658919.ico'
APP_MAIN_ICON_0='gfw_high.ico'
## Use fixed cpu cycles, to improve performances
APP_MAIN_DOSBOX_PRERUN='
config -set cpu cycles=fixed 80000
'

APP_FAN_ID="${GAME_ID}-rayfan"
APP_FAN_NAME="$GAME_NAME - Rayman by his Fans"
APP_FAN_EXE='rayfan/rayfan.exe'
APP_FAN_OPTIONS='ver=usa'
APP_FAN_ICON='rayfan/rayfan.ico'
## Use fixed cpu cycles, to improve performances
APP_FAN_DOSBOX_PRERUN='
config -set cpu cycles=fixed 20000
'

APP_KIT_ID="${GAME_ID}-raykit"
APP_KIT_NAME="$GAME_NAME - Rayman Designer"
APP_KIT_EXE='raykit/raykit.exe'
APP_KIT_OPTIONS='ver=usa'
APP_KIT_ICON='raykit/raykit.ico'
## Use fixed cpu cycles, to improve performances
APP_KIT_DOSBOX_PRERUN='
config -set cpu cycles=fixed 20000
'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Force the name of the disk image table of contents
	if [ -e 'game.inst' ]; then
		mv 'game.inst' "$GAME_IMAGE"
	fi
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	local application
	application="$1"

	local application_exe application_options
	application_exe=$(application_exe "$application")
	application_options=$(application_options "$application")
	cat <<- EOF
	cd $(dirname "$application_exe")
	$(basename "$application_exe") $application_options \$@
	EOF
}

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
