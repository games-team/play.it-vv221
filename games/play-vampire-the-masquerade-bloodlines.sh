#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 Mopi
set -o errexit

###
# Vampire: The Masquerade - Bloodlines
###

script_version=20250223.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='vampire-the-masquerade-bloodlines'
GAME_NAME='Vampire: The Masquerade - Bloodlines'

## The DotEmu store closed down in 2017, since then they only advertise Steam.
ARCHIVE_BASE_DOTEMU_0_NAME='vampire_the_masquerade_bloodlines_v1.2.exe'
ARCHIVE_BASE_DOTEMU_0_MD5='8981da5fa644475583b2888a67fdd741'
ARCHIVE_BASE_DOTEMU_0_EXTRACTOR='unar'
ARCHIVE_BASE_DOTEMU_0_SIZE='5013863'
ARCHIVE_BASE_DOTEMU_0_VERSION='1.2-dotemu1'

ARCHIVE_BASE_GOG_EN_6_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(68368).exe'
ARCHIVE_BASE_GOG_EN_6_MD5='553f6bdbd8b23fa143da22d9ea47a20b'
ARCHIVE_BASE_GOG_EN_6_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_6_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(68368)-1.bin'
ARCHIVE_BASE_GOG_EN_6_PART1_MD5='f0ca81565507da32075e6334a96a173f'
ARCHIVE_BASE_GOG_EN_6_SIZE='4152647'
ARCHIVE_BASE_GOG_EN_6_VERSION='1.2-gog68368'
ARCHIVE_BASE_GOG_EN_6_URL='https://www.gog.com/game/vampire_the_masquerade_bloodlines'

ARCHIVE_BASE_GOG_FR_6_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(french)_(68368).exe'
ARCHIVE_BASE_GOG_FR_6_MD5='5bb92c9507ef442ec95dade41f940c48'
ARCHIVE_BASE_GOG_FR_6_TYPE='innosetup'
ARCHIVE_BASE_GOG_FR_6_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(french)_(68368)-1.bin'
ARCHIVE_BASE_GOG_FR_6_PART1_MD5='c003bb4e2973fb7487bef0b50ed46c6a'
ARCHIVE_BASE_GOG_FR_6_SIZE='4184265'
ARCHIVE_BASE_GOG_FR_6_VERSION='1.2-gog68368'
ARCHIVE_BASE_GOG_FR_6_URL='https://www.gog.com/game/vampire_the_masquerade_bloodlines'

ARCHIVE_BASE_GOG_EN_5_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(68368).exe'
ARCHIVE_BASE_GOG_EN_5_MD5='b2c1eff2640a4b7c52922a3c1ce3f36b'
ARCHIVE_BASE_GOG_EN_5_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_5_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(68368)-1.bin'
ARCHIVE_BASE_GOG_EN_5_PART1_MD5='96b585f441224ab5f8658815b74a0763'
ARCHIVE_BASE_GOG_EN_5_SIZE='4209268'
ARCHIVE_BASE_GOG_EN_5_VERSION='1.2-gog68368'

ARCHIVE_BASE_GOG_FR_5_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(french)_(68368).exe'
ARCHIVE_BASE_GOG_FR_5_MD5='ea6cec86b0eb1986164eefb6ddf458ef'
ARCHIVE_BASE_GOG_FR_5_TYPE='innosetup'
ARCHIVE_BASE_GOG_FR_5_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.511.410.2)_(french)_(68368)-1.bin'
ARCHIVE_BASE_GOG_FR_5_PART1_MD5='01194704a7c9b1e7c67e4743bcdb81f7'
ARCHIVE_BASE_GOG_FR_5_SIZE='4242068'
ARCHIVE_BASE_GOG_FR_5_VERSION='1.2-gog68368'

ARCHIVE_BASE_GOG_EN_4_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_hotfix_(55110).exe'
ARCHIVE_BASE_GOG_EN_4_MD5='9a1c0824945008a3d520103002479fbc'
ARCHIVE_BASE_GOG_EN_4_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_4_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_hotfix_(55110)-1.bin'
ARCHIVE_BASE_GOG_EN_4_PART1_MD5='50cbbf0ad7063e089ba4663cb599b7ac'
ARCHIVE_BASE_GOG_EN_4_SIZE='4300000'
ARCHIVE_BASE_GOG_EN_4_VERSION='1.2-gog55110'

ARCHIVE_BASE_GOG_FR_4_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_hotfix_(french)_(55110).exe'
ARCHIVE_BASE_GOG_FR_4_MD5='2cf2aa09a1bfbd3151ded2457037f2ab'
ARCHIVE_BASE_GOG_FR_4_TYPE='innosetup'
ARCHIVE_BASE_GOG_FR_4_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_hotfix_(french)_(55110)-1.bin'
ARCHIVE_BASE_GOG_FR_4_PART1_MD5='3925827a5aa7dfcd53609130ef128a18'
ARCHIVE_BASE_GOG_FR_4_SIZE='4300000'
ARCHIVE_BASE_GOG_FR_4_VERSION='1.2-gog55110'

ARCHIVE_BASE_GOG_EN_3_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_(54206).exe'
ARCHIVE_BASE_GOG_EN_3_MD5='97cc63738b2eab02140c38883d82fa8b'
ARCHIVE_BASE_GOG_EN_3_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_3_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_(54206)-1.bin'
ARCHIVE_BASE_GOG_EN_3_PART1_MD5='5024eb1ddef8ec13c2ed433d399193cf'
ARCHIVE_BASE_GOG_EN_3_SIZE='4300000'
ARCHIVE_BASE_GOG_EN_3_VERSION='1.2-gog54206'

ARCHIVE_BASE_GOG_FR_3_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_(french)_(54206).exe'
ARCHIVE_BASE_GOG_FR_3_MD5='80bb9fab0f97230989b5f15aa5118f51'
ARCHIVE_BASE_GOG_FR_3_TYPE='innosetup'
ARCHIVE_BASE_GOG_FR_3_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_11.111.010.2)_(french)_(54206)-1.bin'
ARCHIVE_BASE_GOG_FR_3_PART1_MD5='3624eb7399c9d260889ab245f9bd43b1'
ARCHIVE_BASE_GOG_FR_3_SIZE='4300000'
ARCHIVE_BASE_GOG_FR_3_VERSION='1.2-gog54206'

ARCHIVE_BASE_GOG_EN_2_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_10.2)_(28160).exe'
ARCHIVE_BASE_GOG_EN_2_MD5='8c1907871d2ded8afda77d5b570d5383'
ARCHIVE_BASE_GOG_EN_2_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_2_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_10.2)_(28160)-1.bin'
ARCHIVE_BASE_GOG_EN_2_PART1_MD5='a28edc25dc3c0f818673196852490628'
ARCHIVE_BASE_GOG_EN_2_SIZE='4100000'
ARCHIVE_BASE_GOG_EN_2_VERSION='1.2-gog28160'

ARCHIVE_BASE_GOG_FR_2_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_10.2)_(french)_(28160).exe'
ARCHIVE_BASE_GOG_FR_2_MD5='8877c5ab14363b249e72034fe5333921'
ARCHIVE_BASE_GOG_FR_2_TYPE='innosetup'
ARCHIVE_BASE_GOG_FR_2_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_10.2)_(french)_(28160)-1.bin'
ARCHIVE_BASE_GOG_FR_2_PART1_MD5='0dddbbcd2dee5474066b4863c56aa5f0'
ARCHIVE_BASE_GOG_FR_2_SIZE='4200000'
ARCHIVE_BASE_GOG_FR_2_VERSION='1.2-gog28160'

ARCHIVE_BASE_GOG_EN_1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_10.0)_(22135).exe'
ARCHIVE_BASE_GOG_EN_1_MD5='095771daf8fd1b26d34a099f182c8d4a'
ARCHIVE_BASE_GOG_EN_1_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_1_PART1_NAME='setup_vampire_the_masquerade_-_bloodlines_1.2_(up_10.0)_(22135)-1.bin'
ARCHIVE_BASE_GOG_EN_1_PART1_MD5='ef8a3fe212da189d811fcf6bc70a1e40'
ARCHIVE_BASE_GOG_EN_1_SIZE='4100000'
ARCHIVE_BASE_GOG_EN_1_VERSION='1.2-gog22135'

ARCHIVE_BASE_GOG_EN_0_NAME='setup_vtmb_1.2_(up_9.7_basic)_(11362).exe'
ARCHIVE_BASE_GOG_EN_0_MD5='62b8db3b054595fb46bd8eaa5f8ae7bc'
ARCHIVE_BASE_GOG_EN_0_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_0_PART1_NAME='setup_vtmb_1.2_(up_9.7_basic)_(11362)-1.bin'
ARCHIVE_BASE_GOG_EN_0_PART1_MD5='4177042d5a6e03026d52428e900e6137'
ARCHIVE_BASE_GOG_EN_0_SIZE='4100000'
ARCHIVE_BASE_GOG_EN_0_VERSION='1.2-gog11362'

CONTENT_PATH_DEFAULT_DOTEMU='.'
CONTENT_PATH_DEFAULT_GOG='.'
CONTENT_PATH_DEFAULT_GOG_EN_0='app'
CONTENT_GAME_BIN_FILES='
bin
vampire/dlls
vampire/cl_dlls
loader.exe
vampire.exe
vampire.exe.12
dbghelp.dll
loader.dll'
## These paths are created at the "Extract game data" step.
CONTENT_GAME_L10N_EN_PATH_DOTEMU='l10n-en'
CONTENT_GAME_L10N_FR_PATH_DOTEMU='l10n-fr'
CONTENT_GAME_L10N_FILES='
vampire/pack101.vpk
vampire/pack103.vpk'
CONTENT_GAME_L10N_EN_FILES="$CONTENT_GAME_L10N_FILES"
CONTENT_GAME_L10N_FR_FILES="$CONTENT_GAME_L10N_FILES"
CONTENT_GAME_DATA_FILES='
vampire/maps
vampire/media
vampire/pack000.vpk
vampire/pack001.vpk
vampire/pack002.vpk
vampire/pack003.vpk
vampire/pack004.vpk
vampire/pack005.vpk
vampire/pack006.vpk
vampire/pack007.vpk
vampire/pack008.vpk
vampire/pack009.vpk
vampire/pack010.vpk
vampire/pack100.vpk
vampire/pack102.vpk
vampire/python
vampire/resource
vampire/sound
vtmbup-loader.txt
vtmbup-readme.txt
*.dat
*.mpg
*.tth'
## These paths are created at the "Extract game data" step.
CONTENT_DOC_L10N_EN_PATH_DOTEMU='l10n-en'
CONTENT_DOC_L10N_FR_PATH_DOTEMU='l10n-fr'
CONTENT_DOC_L10N_FILES='
docs
*.pdf'
CONTENT_DOC_L10N_EN_FILES="$CONTENT_DOC_L10N_FILES"
CONTENT_DOC_L10N_FR_FILES="$CONTENT_DOC_L10N_FILES"

USER_PERSISTENT_DIRECTORIES='
vampire/cfg
vampire/maps/graphs
vampire/python
vampire/save'
USER_PERSISTENT_FILES='
vampire/vidcfg.bin'

WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE_DOTEMU='vampire.exe'
## Using vampire.exe directly with the GOG build leads to lingering processes
## cf. https://bugs.winehq.org/show_bug.cgi?id=56713
## Vampire: The Masquerade - Bloodlines Vampire.exe process keeps lingering
APP_MAIN_EXE_GOG='loader.exe'
APP_MAIN_ICON='vampire.exe'
## Work around the deletion of zlib1.dll
## TODO: Check if it is still required with current WINE
APP_MAIN_PRERUN='
# Work around the deletion of zlib1.dll
system_library="${WINEPREFIX}/drive_c/windows/system32/zlib1.dll"
if [ ! -e "$system_library" ]; then
	cp "bin/zlib1.dll" "$system_library"
fi
'
## Switch French keyboard layout to us-azerty to provide direct access to digits.
## TODO: Update this snippet to make it work on Wayland too.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Switch French keyboard layout to us-azerty to provide direct access to digits.
# This is required to not get stuck when asked to type digits in-game.
## This works on X.org only, not on Wayland.
if [ -z "${WAYLAND_DISPLAY:-}" ] && [ -n "${DISPLAY:-}" ] && command -v setxkbmap >/dev/null 2>&1; then
	KEYBOARD_RESTORE_VARIANT=0
	KEYBOARD_LAYOUT=$(LANG=C setxkbmap -query | awk "/layout:/ {print \$2}")
	if [ "$KEYBOARD_LAYOUT" = "fr" ]; then
		KEYBOARD_VARIANT=$(LANG=C setxkbmap -query | awk "/variant:/ {print \$2}")
		if [ "$KEYBOARD_VARIANT" != "us-azerty" ]; then
			KEYBOARD_RESTORE_VARIANT=1
			setxkbmap -variant us-azerty
		fi
	fi
fi
'
APP_MAIN_POSTRUN='
# Restore the keyboard variant, if it has previously been switched to us-azerty.
## This works on X.org only, not on Wayland.
if [ -z "${WAYLAND_DISPLAY:-}" ] && [ -n "${DISPLAY:-}" ] && command -v setxkbmap >/dev/null 2>&1; then
	if [ $KEYBOARD_RESTORE_VARIANT -eq 1 ]; then
		setxkbmap -variant "$KEYBOARD_VARIANT"
	fi
fi
'

PACKAGES_LIST_DOTEMU='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA'
PACKAGES_LIST_GOG_EN='
PKG_BIN
PKG_L10N_EN
PKG_DATA'
PACKAGES_LIST_GOG_FR='
PKG_BIN
PKG_L10N_FR
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_BASE_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_ENSCRIPTION='English localization'
PKG_L10N_FR_FRSCRIPTION='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# DotEmu archive - unzip is required to extract the content of inner archives

case "$(current_archive)" in
	('ARCHIVE_BASE_DOTEMU_'*)
		REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
		unzip"
		requirements_check
	;;
esac

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	case "$(current_archive)" in
		('ARCHIVE_BASE_DOTEMU_'*)
			localized_paths='
			Docs
			Vampire/pack101.vpk
			Vampire/pack103.vpk'
			localized_paths_en="
			$localized_paths
			manual_en.pdf"
			localized_paths_fr="
			$localized_paths
			Manual_fr.pdf"

			ARCHIVE_COMMON1_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/common1.zip"
			archive_extraction 'ARCHIVE_COMMON1'
			rm "$(archive_path 'ARCHIVE_COMMON1')"

			ARCHIVE_COMMON2_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/common2.zip"
			archive_extraction 'ARCHIVE_COMMON2'
			rm "$(archive_path 'ARCHIVE_COMMON2')"

			ARCHIVE_EN_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/en.zip"
			archive_extraction 'ARCHIVE_EN'
			rm "$(archive_path 'ARCHIVE_EN')"
			## Localized paths are moved out of the way to prevent them from being overwritten.
			mkdir 'l10n-en'
			## Silence a ShellCheck false positive, word splitting is expected here
			## SC2086 (info): Double quote to prevent globbing and word splitting.
			# shellcheck disable=SC2086
			cp --link --parents --recursive $localized_paths_en 'l10n-en'
			## Silence a ShellCheck false positive, word splitting is expected here
			## SC2086 (info): Double quote to prevent globbing and word splitting.
			# shellcheck disable=SC2086
			rm --recursive $localized_paths
	
			ARCHIVE_FR_PATH="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/fr.zip"
			archive_extraction 'ARCHIVE_FR'
			rm "$(archive_path 'ARCHIVE_FR')"
			## Localized paths are moved out of the way to prevent them from being overwritten.
			mkdir 'l10n-fr'
			## Silence a ShellCheck false positive, word splitting is expected here
			## SC2086 (info): Double quote to prevent globbing and word splitting.
			# shellcheck disable=SC2086
			cp --link --parents --recursive $localized_paths_fr 'l10n-fr'
			## Silence a ShellCheck false positive, word splitting is expected here
			## SC2086 (info): Double quote to prevent globbing and word splitting.
			# shellcheck disable=SC2086
			rm --recursive $localized_paths

			tolower .
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(current_archive)" in
	('ARCHIVE_BASE_DOTEMU_'*)
		case "$(messages_language)" in
			('fr')
				lang_string='version %s :'
				lang_en='anglaise'
				lang_fr='française'
			;;
			('en'|*)
				lang_string='%s version:'
				lang_en='English'
				lang_fr='French'
			;;
		esac
		printf '\n'
		printf "$lang_string" "$lang_en"
		print_instructions 'PKG_BIN' 'PKG_L10N_EN' 'PKG_DATA'
		printf "$lang_string" "$lang_fr"
		print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA'
	;;
	(*)
		print_instructions
	;;
esac

# Clean up

working_directory_cleanup

exit 0
