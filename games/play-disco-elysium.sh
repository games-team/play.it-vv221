#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Disco Elysium
###

script_version=20241209.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='disco-elysium'
GAME_NAME='Disco Elysium'

ARCHIVE_BASE_7_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f0_(77603).exe'
ARCHIVE_BASE_7_MD5='8dad100dd4462ac2cbb01f9f11dd0be1'
ARCHIVE_BASE_7_TYPE='innosetup'
ARCHIVE_BASE_7_PART1_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f0_(77603)-1.bin'
ARCHIVE_BASE_7_PART1_MD5='9743142f176fe8f61617d87d8518aef3'
ARCHIVE_BASE_7_PART2_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f0_(77603)-2.bin'
ARCHIVE_BASE_7_PART2_MD5='cf4a3e7c58b00a9b90dbd722075350f6'
ARCHIVE_BASE_7_PART3_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f0_(77603)-3.bin'
ARCHIVE_BASE_7_PART3_MD5='9aa0fdaec98a1b97a9e2059e2db7cfca'
ARCHIVE_BASE_7_SIZE='10092905'
ARCHIVE_BASE_7_VERSION='1.0.a0a062f0-gog77603'
ARCHIVE_BASE_7_URL='https://www.gog.com/game/disco_elysium'

ARCHIVE_BASE_6_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f7_(75177).exe'
ARCHIVE_BASE_6_MD5='6cced47952e91f7acbaaf718f457ca29'
ARCHIVE_BASE_6_TYPE='innosetup'
ARCHIVE_BASE_6_PART1_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f7_(75177)-1.bin'
ARCHIVE_BASE_6_PART1_MD5='fa7646cb5754722dcc40dd4631f22587'
ARCHIVE_BASE_6_PART2_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f7_(75177)-2.bin'
ARCHIVE_BASE_6_PART2_MD5='2255e435c92bd2507822469d851fcf79'
ARCHIVE_BASE_6_PART3_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f7_(75177)-3.bin'
ARCHIVE_BASE_6_PART3_MD5='33bf8ad6a9421f58c5418103f3a95bd6'
ARCHIVE_BASE_6_SIZE='10074505'
ARCHIVE_BASE_6_VERSION='1.0.a0a062f7-gog75177'

ARCHIVE_BASE_5_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f3_(74350).exe'
ARCHIVE_BASE_5_MD5='52a20a526a898a173d98b800f331616c'
ARCHIVE_BASE_5_TYPE='innosetup'
ARCHIVE_BASE_5_PART1_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f3_(74350)-1.bin'
ARCHIVE_BASE_5_PART1_MD5='29cac3aca83c0c3ef1b0c5923b7a8f22'
ARCHIVE_BASE_5_PART2_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f3_(74350)-2.bin'
ARCHIVE_BASE_5_PART2_MD5='82fe3cd0526d89342f41df1f58dfec01'
ARCHIVE_BASE_5_PART3_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f3_(74350)-3.bin'
ARCHIVE_BASE_5_PART3_MD5='4cbf7779c69ca856d689cad0bc568ef3'
ARCHIVE_BASE_5_SIZE='10074442'
ARCHIVE_BASE_5_VERSION='1.0.a0a062f3-gog74350'

ARCHIVE_BASE_4_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f1_(73330).exe'
ARCHIVE_BASE_4_MD5='fa1ab8b1023dbdd96464e48426f724de'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_PART1_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f1_(73330)-1.bin'
ARCHIVE_BASE_4_PART1_MD5='fa2edf58880e63def21f2565ac14c73f'
ARCHIVE_BASE_4_PART2_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f1_(73330)-2.bin'
ARCHIVE_BASE_4_PART2_MD5='b4d7b67b7633f29688380c6fd83a426b'
ARCHIVE_BASE_4_PART3_NAME='setup_disco_elysium_-_the_final_cut_wina0a062f1_(73330)-3.bin'
ARCHIVE_BASE_4_PART3_MD5='2dff22e62041509591e11d91d5567ee0'
ARCHIVE_BASE_4_SIZE='10076178'
ARCHIVE_BASE_4_VERSION='1.0.a0a062f1-gog73330'

ARCHIVE_BASE_3_NAME='setup_disco_elysium_-_the_final_cut_wina0a062ed_(72872).exe'
ARCHIVE_BASE_3_MD5='8f2bc041892c5edea9fbcf5340ad05c1'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_PART1_NAME='setup_disco_elysium_-_the_final_cut_wina0a062ed_(72872)-1.bin'
ARCHIVE_BASE_3_PART1_MD5='f150ff79eabe46ac6440ac85f1ed63a9'
ARCHIVE_BASE_3_PART2_NAME='setup_disco_elysium_-_the_final_cut_wina0a062ed_(72872)-2.bin'
ARCHIVE_BASE_3_PART2_MD5='798ce91c5204e3d0bf810033734700bd'
ARCHIVE_BASE_3_PART3_NAME='setup_disco_elysium_-_the_final_cut_wina0a062ed_(72872)-3.bin'
ARCHIVE_BASE_3_PART3_MD5='3fccee3836168ab2d76b5759258d8363'
ARCHIVE_BASE_3_SIZE='10078050'
ARCHIVE_BASE_3_VERSION='1.0.a0a062ed-gog72872'

ARCHIVE_BASE_2_NAME='setup_disco_elysium_a0a062e7_(64bit)_(63158).exe'
ARCHIVE_BASE_2_MD5='1d9cab4e4952a588a5a8af30ac7dd7aa'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_disco_elysium_a0a062e7_(64bit)_(63158)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='fbe7a581843d47681ba0f2aa1696a58c'
ARCHIVE_BASE_2_PART2_NAME='setup_disco_elysium_a0a062e7_(64bit)_(63158)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='8925592fc18c4565e262fd4e3008e9fc'
ARCHIVE_BASE_2_PART3_NAME='setup_disco_elysium_a0a062e7_(64bit)_(63158)-3.bin'
ARCHIVE_BASE_2_PART3_MD5='ddbda58eb8c570662689721246b4f081'
ARCHIVE_BASE_2_SIZE='10000000'
ARCHIVE_BASE_2_VERSION='1.0.a0a062e7-gog63158'

ARCHIVE_BASE_1_NAME='setup_disco_elysium_3c54ad81_(64bit)_(60250).exe'
ARCHIVE_BASE_1_MD5='c915229dfa223b16f4399475589ff346'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_disco_elysium_3c54ad81_(64bit)_(60250)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='a6d13f109f26f17a2f1a9d06f519ba1f'
ARCHIVE_BASE_1_PART2_NAME='setup_disco_elysium_3c54ad81_(64bit)_(60250)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='816a2fed25591f9f26b094f9ead7a1af'
ARCHIVE_BASE_1_PART3_NAME='setup_disco_elysium_3c54ad81_(64bit)_(60250)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='a12113e41085154d94209cd076ce5057'
ARCHIVE_BASE_1_SIZE='10000000'
ARCHIVE_BASE_1_VERSION='1.0.3c54ad81-gog60250'

ARCHIVE_BASE_0_NAME='setup_disco_elysium_ee8d3b39_(64bit)_(58288).exe'
ARCHIVE_BASE_0_MD5='6b196bb79102de695bbc45904342b8ad'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_disco_elysium_ee8d3b39_(64bit)_(58288)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='120e95e8fef4537283705b5510af6d86'
ARCHIVE_BASE_0_PART2_NAME='setup_disco_elysium_ee8d3b39_(64bit)_(58288)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='82c73e068dc3b18893e11a45bf74242e'
ARCHIVE_BASE_0_PART3_NAME='setup_disco_elysium_ee8d3b39_(64bit)_(58288)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='20d337ae2de345628b5e4a6b9a2f55c7'
ARCHIVE_BASE_0_SIZE='10000000'
ARCHIVE_BASE_0_VERSION='1.0.ee8d3b39-gog58288'

UNITY3D_NAME='disco'
UNITY3D_NAME_2='disco elysium'
UNITY3D_NAME_1='disco elysium'
UNITY3D_NAME_0='disco elysium'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_STREAMINGASSETS_FILES="
${UNITY3D_NAME}_data/streamingassets"
CONTENT_GAME_DATA_STREAMINGASSETS_FILES_2="
${UNITY3D_NAME_2}_data/streamingassets"
CONTENT_GAME_DATA_STREAMINGASSETS_FILES_1="
${UNITY3D_NAME_1}_data/streamingassets"
CONTENT_GAME_DATA_STREAMINGASSETS_FILES_0="
${UNITY3D_NAME_0}_data/streamingassets"

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/ZAUM Studio/Disco Elysium'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_STREAMINGASSETS
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_STREAMINGASSETS'

PKG_DATA_STREAMINGASSETS_ID="${PKG_DATA_ID}-streamingassets"
PKG_DATA_STREAMINGASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - streamingassets"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
