#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shardlight
###

script_version=20250103.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='shardlight'
GAME_NAME='Shardlight'

ARCHIVE_BASE_4_NAME='shardlight_3_2_73879.sh'
ARCHIVE_BASE_4_MD5='0207dcb6ee38949386c7ebe49ef18160'
ARCHIVE_BASE_4_SIZE='952797'
ARCHIVE_BASE_4_VERSION='3.2-gog73879'
ARCHIVE_BASE_4_URL='https://www.gog.com/game/shardlight'

ARCHIVE_BASE_3_NAME='shardlight_3_1_70695.sh'
ARCHIVE_BASE_3_MD5='824cd0bf02463cfce8acd9c491399197'
ARCHIVE_BASE_3_SIZE='952796'
ARCHIVE_BASE_3_VERSION='3.1-gog70695'

ARCHIVE_BASE_2_NAME='shardlight_3_0_66305.sh'
ARCHIVE_BASE_2_MD5='2c3d7b7e0a541ee3a635189449608713'
ARCHIVE_BASE_2_SIZE='960000'
ARCHIVE_BASE_2_VERSION='3.0-gog66305'

ARCHIVE_BASE_1_NAME='shardlight_2_4_56258.sh'
ARCHIVE_BASE_1_MD5='854c59dcb914d934468a9475d17181e4'
ARCHIVE_BASE_1_SIZE='1200000'
ARCHIVE_BASE_1_VERSION='2.4-gog56258'

ARCHIVE_BASE_0_NAME='shardlight_2_2_53577.sh'
ARCHIVE_BASE_0_MD5='b48f7b1aac9b32ee7f5b236b6cc82e81'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='2.2-gog53577'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='shardlight_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='59bce49abce9678d963cecd72be8b512'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/shardlight/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
32x32'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
ENGV.tmp
Music
Sounds
*.ags
*.cfg
*.exe
*.ogv
*.tra
*.vox'
CONTENT_DOC_MAIN_FILES='
licenses'

APP_MAIN_SCUMMID='ags:shardlight'
APP_MAIN_ICON='../support/icon.png'
APP_MAIN_ICON_1='Shardlight.exe'
APP_MAIN_ICON_0='Shardlight.exe'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
