#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Distance
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='distance'
GAME_NAME='Distance'

ARCHIVE_BASE_5_NAME='distance_6895_linux.tar.gz'
ARCHIVE_BASE_5_MD5='8c71eb4ebd4dbe0b89ee90ba538eadd9'
ARCHIVE_BASE_5_SIZE='2600000'
ARCHIVE_BASE_5_VERSION='1.4.0-humble190905'
ARCHIVE_BASE_5_URL='https://www.humblebundle.com/store/distance'

ARCHIVE_BASE_4_NAME='distance_6842_linux.tar.gz'
ARCHIVE_BASE_4_MD5='58e05033c1a0ae206718be5d5ee800db'
ARCHIVE_BASE_4_SIZE='2400000'
ARCHIVE_BASE_4_VERSION='1.3.1-humble190618'

ARCHIVE_BASE_3_NAME='distance_6839_linux.tar.gz'
ARCHIVE_BASE_3_MD5='308b8276a490f641476098900180d1d3'
ARCHIVE_BASE_3_SIZE='2400000'
ARCHIVE_BASE_3_VERSION='1.3.0-humble190609'

ARCHIVE_BASE_2_NAME='distance_6802_linux.tar.gz'
ARCHIVE_BASE_2_MD5='e98812e71ce42e667c33cdf3f38793b7'
ARCHIVE_BASE_2_SIZE='2400000'
ARCHIVE_BASE_2_VERSION='1.2.4-humble190418'

ARCHIVE_BASE_1_NAME='distance_6714_linux.tar.gz'
ARCHIVE_BASE_1_MD5='6b82a258c4fe4c5fe5dcf3ec70f7c326'
ARCHIVE_BASE_1_SIZE='2300000'
ARCHIVE_BASE_1_VERSION='1.1.1-humble190120'

ARCHIVE_BASE_0_NAME='distance_6670_linux.tar.gz'
ARCHIVE_BASE_0_MD5='7542f19db3aa2f00368b4efb91907a4f'
ARCHIVE_BASE_0_SIZE='1800000'
ARCHIVE_BASE_0_VERSION='1.0.2-humble181103'

UNITY3D_NAME='Distance'
UNITY3D_PLUGINS='
libAkFlanger.so
libAkGuitarDistortion.so
libAkHarmonizer.so
libAkPitchShifter.so
libAkSoundEngine.so
libAkStereoDelay.so
libAkTremolo.so
libSynthOne.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='bin'
CONTENT_DOC_DATA_PATH='.'
CONTENT_DOC_DATA_FILES='
EULA.txt'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
