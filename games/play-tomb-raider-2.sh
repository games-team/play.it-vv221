#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tomb Raider 2
###

script_version=20241125.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='tomb-raider-2'
GAME_NAME='Tomb Raider Ⅱ'

ARCHIVE_BASE_EN_0_NAME='setup_tomb_raider_2_20180108_(17720).exe'
ARCHIVE_BASE_EN_0_MD5='858aa88bf7654a8fb7b7f8070cf1021b'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_VERSION='1.1-gog17720'
ARCHIVE_BASE_EN_0_SIZE='340000'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/tomb_raider_123'

ARCHIVE_BASE_FR_0_NAME='setup_tomb_raider_2_french_20180108_(17720).exe'
ARCHIVE_BASE_FR_0_MD5='12ebe01ec7b4366862af31b860097f14'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_VERSION='1.1-gog17720'
ARCHIVE_BASE_FR_0_SIZE='410000'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/tomb_raider_123'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
dec130.dll
edec.dll
libogg-0.dll
libvorbis-0.dll
libvorbisfile-3.dll
msvcp90.dll
msvcr90.dll
sound.dll
winplay.dll
winsdec.dll
winstr.dll
data/dsetup16.dll
data/dsetup32.dll
data/dsetup.dll
data/_setup.dll
tomb2.exe'
CONTENT_GAME_L10N_FILES='
audio
data/*.dat
data/*.pcx'
CONTENT_GAME_DATA_FILES='
fmv
music
tombpc.dat
data/*.sfx
data/*.tr2'
CONTENT_DOC_L10N_FILES='
manual.pdf
readme.txt'
CONTENT_DOC0_L10N_FILES_EN='
tomb raider ii_pc eula english.docx'
CONTENT_DOC0_L10N_FILES_FR='
tomb raider ii_pc eula french.doc'

USER_PERSISTENT_FILES='
savegame.*'

APP_MAIN_EXE='tomb2.exe'

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_NAME="$GAME_NAME - setup"
APP_SETUP_CAT='Settings'
APP_SETUP_EXE='tomb2.exe'
APP_SETUP_OPTIONS='-setup'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
application/x-id3'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
