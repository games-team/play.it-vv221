#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Worms 2
###

script_version=20241125.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='worms-2'
GAME_NAME='Worms 2'

ARCHIVE_BASE_0_NAME='setup_worms2_2.0.0.23.exe'
ARCHIVE_BASE_0_MD5='bbe752d1b716432bb67e574ad9f3e9f3'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='190000'
ARCHIVE_BASE_0_VERSION='1.05-gog2.0.0.23'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/worms_2'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
binaries
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
data
levels
music
options
saves
teams
weapons
stats.txt
template.bmp
*.wmv'
CONTENT_DOC_DATA_FILES='
manual.pdf
patch.txt'

USER_PERSISTENT_DIRECTORIES='
levels
options
saves
teams
weapons'
USER_PERSISTENT_FILES='
stats.txt
data/*.dat'

## Running the game in a virtual desktop prevents it from being stuck into a corner of the screen.
WINE_VIRTUAL_DESKTOP='640x480'

APP_MAIN_EXE='frontend.exe'
## Play the introduction videos on launch
APP_MAIN_PRERUN='
# Play the introduction videos on launch
RANDOM_MOVIE=$(find . -mindepth 1 -maxdepth 1 -name "*.wmv" ! -name "intro.wmv" | shuf --head-count=1)
mpv --fs --no-osc intro.wmv
mpv --fs --no-osc "$RANDOM_MOVIE"
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_COMMANDS='
mpv'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
