#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# BallisticNG
###

script_version=20241230.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='ballisticng'
GAME_NAME='BallisticNG'

ARCHIVE_BASE_0_NAME='ballisticng_1_3_3_1_69502.sh'
ARCHIVE_BASE_0_MD5='4fd3dd5695e9f94523bb699df024dbee'
ARCHIVE_BASE_0_SIZE='2749158'
ARCHIVE_BASE_0_VERSION='1.3.3.1-gog69502'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ballisticng'

UNITY3D_NAME='BallisticNG'
UNITY3D_PLUGINS='
cimgui.so
libaudioplugin_phonon.so
libphonon.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_BIN_FILES='
DedicatedServerExample.cfg
SvrScripts'
CONTENT_GAME_EXPANSION_TRACKS_FILES='
DLC/free extra tracks.cpd
DLC/free extra tracks.cps'
CONTENT_GAME_EXPANSION_VOICES_FILES='
User/Mods/Sounds/Legacy System Voices'
CONTENT_GAME_MODDING_FILES='
Modding'
CONTENT_DOC_DATA_FILES='
Legacy Track Creator Commands.txt'

USER_PERSISTENT_DIRECTORIES='
User'
USER_PERSISTENT_FILES='
DedicatedServerExample.cfg'

APPLICATIONS_LIST='
APP_MAIN
APP_SERVER'

APP_SERVER_ID="${GAME_ID}-server"
APP_SERVER_NAME="$GAME_NAME - Server"
APP_SERVER_OPTIONS='-batchmode -nographics -logfile -svrcfg DedicatedServerExample.cfg'

PACKAGES_LIST='
PKG_BIN
PKG_EXPANSION_TRACKS
PKG_EXPANSION_VOICES
PKG_MODDING
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_EXPANSION_TRACKS_ID="${GAME_ID}-expansion-free-extra-tracks"
PKG_EXPANSION_TRACKS_DESCRIPTION='expansion - free extra tracks'
PKG_EXPANSION_TRACKS_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_EXPANSION_VOICES_ID="${GAME_ID}-expansion-legacy-system-voices"
PKG_EXPANSION_VOICES_DESCRIPTION='expansion - legacy system voices'
PKG_EXPANSION_VOICES_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_MODDING_ID="${GAME_ID}-modding"
PKG_MODDING_DESCRIPTION='modding'
PKG_MODDING_DEPENDENCIES_SIBLINGS='
PKG_BIN'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
