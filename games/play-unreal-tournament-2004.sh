#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Unreal Tournament 2004
###

script_version=20250104.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='unreal-tournament-2004'
GAME_NAME='Unreal Tournament 2004'

# Archives

## Unreal Tournament 2004 (game installer)
## This game is no longer available for sale from gog.com since 2022-12-23.

ARCHIVE_BASE_0_NAME='setup_unreal_tournament_2004_1.0_(18947).exe'
ARCHIVE_BASE_0_MD5='243376d34413b830324c5879ac2f9cfd'
## Do not convert file paths to lowercase
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_unreal_tournament_2004_1.0_(18947)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='a211e2a6feed7334bb3b7deef6b858b5'
ARCHIVE_BASE_0_SIZE='6000000'
ARCHIVE_BASE_0_VERSION='3369-gog18947'

## Native Linux engine

ARCHIVE_REQUIRED_ENGINE_0_NAME='ut2004-lnxpatch3369-2.tar.bz2'
ARCHIVE_REQUIRED_ENGINE_0_MD5='0fa447e05fe5a38e0e32adf171be405e'
ARCHIVE_REQUIRED_ENGINE_0_URL='https://liandri.beyondunreal.com/Unreal_Tournament_2004#Essential_Files'

# Archives content

## Unreal Tournament 2004 (game installer)

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
System
ut2004.txt'
CONTENT_GAME0_BIN_PATH='__support/app'
CONTENT_GAME0_BIN_FILES='
*.ini'
CONTENT_GAME_DATA_FILES='
Animations
ForceFeedback
KarmaData
Maps
Music
Sounds
Speech
StaticMeshes
Textures
Web'
CONTENT_DOC0_DATA_RELATIVE_PATH='Help'
CONTENT_DOC0_DATA_FILES='
*.txt'
CONTENT_DOC1_DATA_RELATIVE_PATH='Manual'
CONTENT_DOC1_DATA_FILES='
*.pdf'

## Native Linux engine

CONTENT_GAME1_BIN_PATH='UT2004-Patch'
CONTENT_GAME1_BIN_FILES='
System'
CONTENT_GAME0_DATA_PATH='UT2004-Patch'
CONTENT_GAME0_DATA_FILES='
Animations
Speech
Textures
Web'
CONTENT_DOC2_DATA_PATH='UT2004-Patch/Help'
CONTENT_DOC2_DATA_FILES='
*.txt'


APP_MAIN_EXE='System/ut2004-bin-linux-amd64'
APP_MAIN_ICON='System/Unreal.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.5'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Chek for the presence of the required game engine

archive_initialize_required \
	'ARCHIVE_ENGINE' \
	'ARCHIVE_REQUIRED_ENGINE_0'

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Include required CD key
	grep --before=1 --fixed-strings 'CDKey' 'goggame-1207658691.script' |
		sed --silent 's/\s*"valueData": "\(.*\)",/\1/p' > 'System/CDKey'
)
archive_extraction 'ARCHIVE_ENGINE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME_ENGINE_BIN')"

	## Delete unwanted 32-bit binaries
	rm --force \
		'System/ucc-bin' \
		'System/ut2004-bin'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Create required links for some libraries
path_libraries_source=$(path_libraries_system)
path_libraries_destination="$(package_path 'PKG_BIN')$(path_game_data)/System"
ln --symbolic "${path_libraries_source}/libSDL-1.2.so.0" "${path_libraries_destination}/libSDL-1.2.so.0"
ln --symbolic "${path_libraries_source}/libopenal.so.1" "${path_libraries_destination}/openal.so"

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd System
	./ut2004-bin-linux-amd64 "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
