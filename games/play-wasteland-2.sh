#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Anna Lea
set -o errexit

###
# Wasteland 2
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='wasteland-2'
GAME_NAME='Wasteland 2'

GAME_ID_CLASSIC="$GAME_ID"
GAME_NAME_CLASSIC="$GAME_NAME"

GAME_ID_DIRECTORSCUT="${GAME_ID}-directors-cut"
GAME_NAME_DIRECTORSCUT="$GAME_NAME - Directorʼs Cut"

# Archives

## Wasteland 2 (classic)

ARCHIVE_BASE_CLASSIC_0_NAME='gog_wasteland_2_2.9.0.14.sh'
ARCHIVE_BASE_CLASSIC_0_MD5='8421db3519ed0074ff2647f5ea53f6f6'
ARCHIVE_BASE_CLASSIC_0_SIZE='20000000'
ARCHIVE_BASE_CLASSIC_0_VERSION='1.0-gog2.9.0.14'
ARCHIVE_BASE_CLASSIC_0_URL='https://www.gog.com/game/wasteland_2_directors_cut_digital_classic_edition'

## Wasteland 2 Director's Cut

ARCHIVE_BASE_DIRECTORSCUT_0_NAME='gog_wasteland_2_director_s_cut_2.3.0.5.sh'
ARCHIVE_BASE_DIRECTORSCUT_0_MD5='dc697b13e1f08de606add7684b5b3f78'
ARCHIVE_BASE_DIRECTORSCUT_0_SIZE='16000000'
ARCHIVE_BASE_DIRECTORSCUT_0_VERSION='1.1.92788-gog2.3.0.5'
ARCHIVE_BASE_DIRECTORSCUT_0_URL='https://www.gog.com/game/wasteland_2_directors_cut_digital_classic_edition'


UNITY3D_NAME='WL2'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_DATA_SCENES_AZ_FILES="
${UNITY3D_NAME}_Data/Streaming/Scenes/ArizonaWorldMap.unity3d
${UNITY3D_NAME}_Data/Streaming/Scenes/AZ_*.unity3d
${UNITY3D_NAME}_Data/Streaming/Scenes/AZ??_*.unity3d
${UNITY3D_NAME}_Data/Streaming/Scenes/az??_*.unity3d"
CONTENT_GAME_DATA_SCENES_CA_FILES="
${UNITY3D_NAME}_Data/Streaming/Scenes/CA_*.unity3d
${UNITY3D_NAME}_Data/Streaming/Scenes/CA??_*.unity3d"
CONTENT_GAME_DATA_RESOURCES_FILES_DIRECTORSCUT="
${UNITY3D_NAME}_Data/*.resource"
CONTENT_DOC_DATA_PATH='data/noarch/docs'
CONTENT_DOC_DATA_FILES='
Map.pdf
Wasteland 2 Ranger Field Manual.pdf
Wasteland 2 Reference Guide.pdf'

## Work around the engine overuse of file descriptors.
APP_MAIN_PRERUN='
# Work around the engine overuse of file descriptors
## 4096 is an arbitrary value, 4 times the default (1024), that seems to work for all setups.
if ! ulimit -n 4096; then
	{
		printf "\\n\\033[1;33mWarning:\\033[0m\\n"
		printf "Your current shell interpreter has no support for ulimit -n.\\n"
		printf "This might lead to unending loading screens if the game engine hits the file descriptors use limit.\\n"
		printf "\\n"
	} > /dev/stderr
fi
'

# Packages

PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_DESCRIPTION='data'

## Wasteland 2 (classic)

PACKAGES_LIST_CLASSIC='
PKG_BIN
PKG_DATA_SCENES_AZ
PKG_DATA_SCENES_CA
PKG_DATA'

PKG_BIN_ARCH_CLASSIC='32'
PKG_BIN_DEPENDENCIES_LIBRARIES_CLASSIC='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2'

PKG_DATA_ID_CLASSIC="${GAME_ID_CLASSIC}-data"
PKG_DATA_DEPENDENCIES_SIBLINGS_CLASSIC='
PKG_DATA_SCENES_AZ
PKG_DATA_SCENES_CA'

PKG_DATA_SCENES_AZ_ID="${PKG_DATA_ID_CLASSIC}-scenes-az"
PKG_DATA_SCENES_AZ_DESCRIPTION="$PKG_DATA_DESCRIPTION - Arizona scenes"

PKG_DATA_SCENES_CA_ID="${PKG_DATA_ID_CLASSIC}-scenes-ca"
PKG_DATA_SCENES_CA_DESCRIPTION="$PKG_DATA_DESCRIPTION - California scenes"

## Wasteland 2 Director's Cut

PACKAGES_LIST_DIRECTORSCUT='
PKG_BIN
PKG_DATA_RESOURCES
PKG_DATA'

PKG_BIN_ARCH_DIRECTORSCUT='64'
PKG_BIN_DEPENDENCIES_LIBRARIES_DIRECTORSCUT='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

PKG_DATA_ID_DIRECTORSCUT="${GAME_ID_DIRECTORSCUT}-data"
PKG_DATA_DEPENDENCIES_SIBLINGS_DIRECTORSCUT='
PKG_DATA_RESOURCES'

PKG_DATA_RESOURCES_ID="${PKG_DATA_ID_DIRECTORSCUT}-resources"
PKG_DATA_RESOURCES_DESCRIPTION='resources'
## Ensure smooth upgrades from packages generated with pre-20231021.1 scripts.
PKG_DATA_RESOURCES_PROVIDES="${PKG_DATA_RESOURCES_PROVIDES:-}
wasteland-2-directors-cut-resources"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
