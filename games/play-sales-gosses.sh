#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sales Gosses
###

script_version=20250111.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='sales-gosses'
GAME_NAME='Sales Gosses !'

ARCHIVE_BASE_1_NAME='little-brats-3.1-gnunux.tar.gz'
ARCHIVE_BASE_1_MD5='0d89b9d55c2873ce0407070d181e03fa'
ARCHIVE_BASE_1_SIZE='225679'
ARCHIVE_BASE_1_VERSION='3.1-itch.2024.12.28'
ARCHIVE_BASE_1_URL='https://ptilouk.itch.io/little-brats'

ARCHIVE_BASE_0_NAME='little-brats-2.2-gnunux.tar.gz'
ARCHIVE_BASE_0_MD5='d8518a619ccb1391311204557ac9441c'
ARCHIVE_BASE_0_SIZE='211184'
ARCHIVE_BASE_0_VERSION='2.2-itch.2024.11.12'

CONTENT_PATH_DEFAULT_1='little-brats-3.1-gnunux'
CONTENT_PATH_DEFAULT_0='little-brats-2.2-gnunux'
CONTENT_GAME_BIN_RELATIVE_PATH='lib/little-brats'
CONTENT_GAME_BIN_FILES='
little-brats.x86_64'
CONTENT_GAME_DATA_RELATIVE_PATH='share/little-brats'
CONTENT_GAME_DATA_FILES='
little-brats.pck
little-brats-*-resource-pack.pck'

APP_MAIN_EXE='little-brats.x86_64'
APP_MAIN_OPTIONS='--main-pack "${PWD}/little-brats.pck" --pack-path "${PWD}"'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libm.so.6
libpthread.so.0
librt.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

## Include the game icon
## TODO: The library should be updated to automatically install .svg files in the "scalable" path
install -D --mode=644 --no-target-directory \
	"${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/share/icons/little-brats.svg" \
	"$(package_path 'PKG_DATA')$(path_icons)/scalable/apps/$(game_id).svg"

content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
